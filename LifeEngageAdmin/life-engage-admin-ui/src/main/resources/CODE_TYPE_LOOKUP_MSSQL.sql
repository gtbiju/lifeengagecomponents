CREATE TABLE [~~~].[CODE_TYPE_LOOKUP] (
  [id] bigint NOT NULL IDENTITY,
  [basicDataCompleteCode] int DEFAULT NULL,
  [contextId] int DEFAULT NULL,
  [creationDateTime] date DEFAULT NULL,
  [transactionId] varchar(255) DEFAULT NULL,
  [typeName] varchar(255) DEFAULT NULL,
  [Name] varchar(255) DEFAULT NULL,
  [carrier_id] bigint DEFAULT NULL FOREIGN KEY REFERENCES [~~~].[CORE_CARRIER] ([id]),
  PRIMARY KEY ([id])
);