CREATE TABLE `AGENT` (
  `agentCode` varchar(255) NOT NULL,
  `briefWriteUp` longtext DEFAULT NULL,
  `employeeId` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `name_of_excelsheet` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `uploaded_timestamp` datetime DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `emp_type` varchar(50) DEFAULT NULL,
  `supervisor_code` varchar(255) DEFAULT NULL,
  `office` varchar(20) DEFAULT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `agent_group` varchar(20) DEFAULT NULL,
  `yoe` decimal(3,0) DEFAULT NULL,
  `business_sourced` decimal(20,0) DEFAULT NULL,
  `num_of_service` decimal(20,0) DEFAULT NULL,
  `license_num` varchar(30) DEFAULT NULL,
  `license_issue_date` date DEFAULT NULL,
  `license_exp_date` date DEFAULT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  `mob_num` decimal(20,0) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) DEFAULT NULL,
  `invalidated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supervisor_code` (`supervisor_code`),
  KEY `agentCode` (`agentCode`),
  KEY `FK_AGENT_CODES` (`userid`),
  CONSTRAINT `FK_AGENT_CODES` FOREIGN KEY (`userid`) REFERENCES `LE_LOGIN_USER` (`USER_ID`),
  CONSTRAINT `FK_AGENT_SUPER_CODES` FOREIGN KEY (`supervisor_code`) REFERENCES `AGENT` (`agentCode`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;




CREATE TABLE `ACHIEVEMENT` (
  `agent_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `achievementDetails_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agent_id` (`agent_id`),
  KEY `idx_achievement_dtl` (`achievementDetails_id`),
  CONSTRAINT `fk_achievement_dtl` FOREIGN KEY (`achievementDetails_id`) REFERENCES `ACHIEVEMENT_DETAILS` (`id`),
  CONSTRAINT `FK_ACHIEVEMENT_AGENT` FOREIGN KEY (`agent_id`) REFERENCES `AGENT` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=utf8;



CREATE TABLE `ACHIEVEMENT_DETAILS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` longtext DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;


CREATE TABLE `AGENT_APP_LOGIN` (
  `userid` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(256) DEFAULT NULL,
  `active` varchar(20) DEFAULT NULL,
  `agentcode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `APPLICATION_CONTEXTS` (
  `Id` int(11) NOT NULL,
  `Context` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `CONTENT_LANGUAGES` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;


CREATE TABLE `CONTENT_TYPES` (
  `Id` int(11) NOT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `extensions` varchar(256) DEFAULT NULL,
  `optionality` tinyint(4) DEFAULT NULL,
  `previewable` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `CONTENTFILES` (
  `fileName` varchar(255) NOT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `publicURL` varchar(255) DEFAULT NULL,
  `tabletFullPath` varchar(255) DEFAULT NULL,
  `Type_Id` int(11) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `is_mandatory` tinyint(1) DEFAULT '1',
  `description` varchar(255) DEFAULT NULL,
  `size` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`fileName`),
  KEY `FKBC11519E360674B9` (`Type_Id`),
  KEY `FK_CONFILE_CONLANG` (`lang_id`),
  CONSTRAINT `FKBC11519E360674B9` FOREIGN KEY (`Type_Id`) REFERENCES `CONTENT_TYPES` (`Id`),
  CONSTRAINT `FK_CONFILE_CONLANG` FOREIGN KEY (`lang_id`) REFERENCES `CONTENT_LANGUAGES` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `CONTENTFILES_APPCONTEXT_X` (
  `contentFile` varchar(255) NOT NULL,
  `appContextId` int(11) NOT NULL,
  KEY `FK9E3B01C826F5859` (`contentFile`),
  KEY `FK9E3B01C8227E8B28` (`appContextId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;