'use strict';

/* Directives */
angular.module('myApp.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
} ]);

angular.module('myApp.directives', []).directive('menunavlinks', function () {
    return {
        restrict: "A",
        scope: {},
        templateUrl:'partials/menunavigation.html',
        replace:true
    }
});
