package com.cognizant.le.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/LogoutAction")
public class LogoutAction extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    if(request.getSession(false) != null)
	    {    
	        request.getSession().invalidate();
	    }
	    response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
	    // Set standard HTTP/1.0 no-cache header.
	    response.setHeader("Pragma", "no-cache");
	    request.getRequestDispatcher("/index.jsp").forward(request, response);

	}


}
