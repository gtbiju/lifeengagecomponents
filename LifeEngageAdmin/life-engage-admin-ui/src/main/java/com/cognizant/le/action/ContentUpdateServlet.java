package com.cognizant.le.action;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.cognizant.le.model.dao.ContentAdminDAO;
import com.cognizant.le.model.dao.ContentAdminDAOImpl;
import com.cognizant.le.model.dto.ContentFileDTO;
import com.cognizant.le.model.dto.FileBean;

/**
 * Servlet to handle File upload request from Client
 * 
 * @author 304003
 */

@WebServlet("/ContentUpdateServlet")
public class ContentUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 4470044101132876855L;
	private static Logger LOGGER = Logger.getLogger(ContentUpdateServlet.class);
	private String contentFolder = "";

	@Override
	public void init(ServletConfig config) throws ServletException {
		LOGGER.debug("Method Entry : ContentUpdateServlet.init");
		super.init(config);
		String propertiesFile = getServletContext().getInitParameter(
				"CONFIG_PROPERTIES");
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread()
					.getContextClassLoader();
			InputStream stream = classLoader.getResourceAsStream("/"
					+ propertiesFile);
			if (null == stream) {
				stream = classLoader.getResourceAsStream(propertiesFile);
			}
			properties.load(stream);
			contentFolder = properties.getProperty("SHARED_CONTENT_FOLDER");
			LOGGER.debug("Upload directory for contents is " + contentFolder);
			LOGGER.debug("Method Exit : ContentUpdateServlet.init");
		} catch (IOException e) {
			LOGGER.error(propertiesFile + " is missing", e);
			throw new ServletException(e.getMessage(), e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		LOGGER.debug("Method Entry : ContentUpdateServlet.doPost");
		HttpSession session = request.getSession(true);
		String fileName = null;
		String comments = null;
		String fileDesc = null;
		Long fileSize = null;
		FileBean fileBean= new FileBean();
		// process only if its multipart content
		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				List<FileItem> multiparts = new ServletFileUpload(
						new DiskFileItemFactory()).parseRequest(request);

				for (FileItem item : multiparts) {
					if (item.isFormField()) {
						if ("update_comment_textarea".equalsIgnoreCase(item.getFieldName())) {
							comments = item.getString();
						} else if ("up_desc_text".equalsIgnoreCase(item.getFieldName())) {
							fileDesc = item.getString();
						}
					}
					if (!item.isFormField()) {
						fileName = new File(item.getName()).getName();
						if (fileName.lastIndexOf("/") != -1) {
							fileName = fileName.substring(
									fileName.lastIndexOf("/") + 1,
									fileName.length());
						} else if (fileName.lastIndexOf("\\") != -1) {
							fileName = fileName.substring(
									fileName.lastIndexOf("\\") + 1,
									fileName.length());
						}
						fileSize = item.getSize();
						fileBean.setInputStream(item.getInputStream());
						fileBean.setFileName(fileName);
						fileBean.setContentFolder(contentFolder);
				/*		item.write(new File(contentFolder + File.separator
								+ fileName));*/
					}
				}
				// File update to db
				ContentFileDTO contentFileDTO= new ContentFileDTO();
				contentFileDTO.setFileName(fileName);
				contentFileDTO.setComments(comments);
				contentFileDTO.setFileDesc(fileDesc);
				contentFileDTO.setFileSize(fileSize);
				ContentAdminDAO contentAdminServiceDAO = new ContentAdminDAOImpl();
				contentAdminServiceDAO.updateContent(contentFileDTO, fileBean);
				session.setAttribute("msg", "File Updated Successfully");
				request.setAttribute("filename", fileName);
				request.setAttribute("comments", comments!=null?comments:"");

			} catch (Exception ex) {
				LOGGER.error("Content Update Failed!!!", ex);
				request.setAttribute("filename", fileName);
				request.setAttribute("comments", comments!=null?comments:"");
				session.setAttribute("msg",
						"File Update Failed due to " + ex.getMessage());
			}
		} else {
			session.setAttribute("msg",
					"Sorry this Servlet only handles file upload request");
		}
		request.getRequestDispatcher("/jsp/ContentList.jsp").forward(request, response);
	}
}
