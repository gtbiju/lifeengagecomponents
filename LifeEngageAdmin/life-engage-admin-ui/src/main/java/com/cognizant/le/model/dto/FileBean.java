package com.cognizant.le.model.dto;

import java.io.InputStream;

/**
 * The Class FileBean.
 */
public class FileBean {
	
	/** The input stream. */
	private InputStream inputStream;
	
	/** The file name. */
	private String fileName;
	
	/** The content folder. */
	private String contentFolder;
	
	/** The version number. */
	private int prevVersionNumber;

	/**
	 * Gets the input stream.
	 *
	 * @return the input stream
	 */
	public InputStream getInputStream() {
		return inputStream;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Gets the content folder.
	 *
	 * @return the content folder
	 */
	public String getContentFolder() {
		return contentFolder;
	}

	/**
	 * Sets the input stream.
	 *
	 * @param inputStream the new input stream
	 */
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Sets the content folder.
	 *
	 * @param contentFolder the new content folder
	 */
	public void setContentFolder(String contentFolder) {
		this.contentFolder = contentFolder;
	}

	/**
	 * Gets the version number.
	 *
	 * @return the version number
	 */
	public int getPrevVersionNumber() {
		return prevVersionNumber;
	}

	/**
	 * Sets the version number.
	 *
	 * @param versionNumber the new version number
	 */
	public void setPrevVersionNumber(int versionNumber) {
		this.prevVersionNumber = versionNumber;
	}
	
	

}
