/**
 * 
 */
package com.cognizant.le.model.dao;

import java.util.List;

import org.json.simple.JSONObject;

import com.cognizant.le.model.dto.ContentFileDTO;
import com.cognizant.le.model.dto.ContentLanguageDTO;
import com.cognizant.le.model.dto.ContentTypeDTO;
import com.cognizant.le.model.dto.FileBean;
import com.cognizant.le.model.dto.FileInfo;

/**
 * @author 291422
 *
 */
public interface ContentAdminDAO {
	public Boolean insertContent(ContentFileDTO contentFileDTO) throws Exception;
	
	public List <ContentFileDTO> retriveContent(String option, FileInfo fileInfo) throws Exception;
	
	public List <ContentFileDTO> retriveContent(FileInfo fileInfo) throws Exception;
	
	public Boolean updateContent(ContentFileDTO contentFileDTO, FileBean fileBean) throws Exception;
	
	public Boolean isContentExists(ContentFileDTO contentFileDTO) throws Exception;
	
	public Boolean rollBackContent(ContentFileDTO contentFileDTO) throws Exception ;
	
	public Boolean tagUntagContext(ContentFileDTO contentFileDTO) throws Exception;
	
	public JSONObject getApplicationContexts();
	
	public JSONObject getContentPreference();
	
	public JSONObject getContentTypes();
}
