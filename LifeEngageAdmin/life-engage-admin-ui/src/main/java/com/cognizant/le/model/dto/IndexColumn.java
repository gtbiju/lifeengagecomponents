/*
 * 
 */
package com.cognizant.le.model.dto;

// TODO: Auto-generated Javadoc
/**
 * The Class IndexColumn.
 */
public class IndexColumn {
	
	/** The column name. */
	private String columnName;
	
	/** The is ascending. */
	private Boolean isAscending;

	/**
	 * Gets the column name.
	 *
	 * @return the column name
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * Sets the column name.
	 *
	 * @param columnName the new column name
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * Gets the checks if is ascending.
	 *
	 * @return the checks if is ascending
	 */
	public Boolean getIsAscending() {
		return isAscending;
	}

	/**
	 * Sets the checks if is ascending.
	 *
	 * @param isAscending the new checks if is ascending
	 */
	public void setIsAscending(Boolean isAscending) {
		this.isAscending = isAscending;
	}

}
