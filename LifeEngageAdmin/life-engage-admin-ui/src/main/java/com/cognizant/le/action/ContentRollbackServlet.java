package com.cognizant.le.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cognizant.le.model.dao.ContentAdminDAO;
import com.cognizant.le.model.dao.ContentAdminDAOImpl;
import com.cognizant.le.model.dto.ContentFileDTO;

@WebServlet("/ContentRollbackServlet")
public class ContentRollbackServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8244769034068106863L;
	private static Logger LOGGER = Logger.getLogger(ContentRollbackServlet.class);
	
	
	private String contentFolder = "";

	@Override
	public void init(ServletConfig config) throws ServletException {
		LOGGER.debug("Method Entry : ContentUpdateServlet.init");
		super.init(config);
		String propertiesFile = getServletContext().getInitParameter(
				"CONFIG_PROPERTIES");
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread()
					.getContextClassLoader();
			InputStream stream = classLoader.getResourceAsStream("/"
					+ propertiesFile);
			if (null == stream) {
				stream = classLoader.getResourceAsStream(propertiesFile);
			}
			properties.load(stream);
			contentFolder = properties.getProperty("SHARED_CONTENT_FOLDER");
			LOGGER.debug("Upload directory for contents is " + contentFolder);
			LOGGER.debug("Method Exit : ContentUpdateServlet.init");
		} catch (IOException e) {
			LOGGER.error(propertiesFile + " is missing", e);
			throw new ServletException(e.getMessage(), e);
		}
	}
	

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
			HttpSession session = request.getSession(true);
			String fileName = request.getParameter("rollback_name");
			String comments = request.getParameter("pop_up_comment_rollback");
			int prevVer=0;
		try {
			
			LOGGER.debug("Method Entry : ContentRollbackServlet.doPost");
			System.out.println("############"
					+ request.getParameter("pop_up_comment_rollback"));
			System.out.println(request.getParameter("rollback_name"));
			Integer version= Integer.parseInt(request.getParameter("rollback_version"));
			//Integer rollbackVersion= Integer.parseInt(request.getParameter("rollback_dropdown"));
			prevVer= version-1;
			File sourceDir;
			sourceDir = new File(contentFolder+File.separator+prevVer);
			if(!sourceDir.exists()){
				throw new Exception("Backup file does not exist.");
			}

			File sourceFile= new File(sourceDir,fileName);
			File targetFile= new File(contentFolder,fileName);
	    	InputStream inStream = null;
	    	OutputStream outStream = null;
		    inStream = new FileInputStream(sourceFile);
    	    outStream = new FileOutputStream(targetFile);
    	    byte[] buffer = new byte[1024];
    	    int length;
    	    //copy the file content in bytes 
    	    while ((length = inStream.read(buffer)) > 0){
 
    	    	outStream.write(buffer, 0, length);
    	    }
    	    outStream.flush();
    	    inStream.close();
    	    outStream.close();
    	    
    	    
			//delete backups for future copies of rollbacked version
			for(int i=version; i>=version-1; i--){
				File inBtnFile;
				inBtnFile = new File(contentFolder
						+ File.separator + i +File.separator+fileName);
				if(inBtnFile.exists()){
					inBtnFile.delete();
				}
			}
			ContentFileDTO contentFileDTO = new ContentFileDTO();
			contentFileDTO.setFileName(fileName);
			contentFileDTO.setComments(comments);
			contentFileDTO.setVersion(prevVer);
			request.setAttribute("contentFileName", contentFileDTO.getFileName());
			request.setAttribute("contentComments", contentFileDTO.getComments()!=null? contentFileDTO.getComments():"");
			request.setAttribute("contentVersion", contentFileDTO.getVersion());
			ContentAdminDAO contentAdminServiceDAO = new ContentAdminDAOImpl();
			contentAdminServiceDAO.rollBackContent(contentFileDTO);
			request.setAttribute("rollbackMsg", "File Rollback Successful");
		} catch (Exception e) {
			LOGGER.error("Content Rollback Failed!!!", e);
			request.setAttribute("contentFileName", fileName);
			request.setAttribute("contentComments", comments!=null? comments:"");
			request.setAttribute("contentVersion",prevVer);
			request.setAttribute("rollbackMsg",
					"File Rollback Failed due to " + e.getMessage());
		}
		finally{
			request.getRequestDispatcher("/jsp/ContentList.jsp").forward(request, response);
		}

	}

}
