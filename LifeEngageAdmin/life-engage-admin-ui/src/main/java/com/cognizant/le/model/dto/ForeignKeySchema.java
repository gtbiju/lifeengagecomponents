/**
 *
 * � Copyright 2012, Cognizant
 *
 * @author        : 287304
 * @version       : 0.1, May 16, 2014
 */
package com.cognizant.le.model.dto;

// TODO: Auto-generated Javadoc
/**
 * The Class ForeignKeySchema.
 */
public class ForeignKeySchema {

    /** The table name. */
    private String tableName;

	/** The column name. */
	private String columnName;

	/** The foreign table name. */
	private String foreignTableName;

	/** The foreign column name. */
	private String foreignColumnName;

    /** The cascade on delete. */
    private Boolean cascadeOnDelete;

    /** The is nullable. */
    private Boolean isNullable;

	/**
	 * Gets the table name.
	 *
	 * @return the table name
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * Sets the table name.
	 *
	 * @param tableName the new table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Gets the column name.
	 *
	 * @return the column name
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * Sets the column name.
	 *
	 * @param columnName the new column name
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * Gets the foreign table name.
	 *
	 * @return the foreign table name
	 */
	public String getForeignTableName() {
		return foreignTableName;
	}

	/**
	 * Sets the foreign table name.
	 *
	 * @param foreignTableName the new foreign table name
	 */
	public void setForeignTableName(String foreignTableName) {
		this.foreignTableName = foreignTableName;
	}

	/**
	 * Gets the foreign column name.
	 *
	 * @return the foreign column name
	 */
	public String getForeignColumnName() {
		return foreignColumnName;
	}

	/**
	 * Sets the foreign column name.
	 *
	 * @param foreignColumnName the new foreign column name
	 */
	public void setForeignColumnName(String foreignColumnName) {
		this.foreignColumnName = foreignColumnName;
	}

	/**
	 * Gets the cascade on delete.
	 *
	 * @return the cascade on delete
	 */
	public Boolean getCascadeOnDelete() {
		return cascadeOnDelete;
	}

	/**
	 * Sets the cascade on delete.
	 *
	 * @param cascadeOnDelete the new cascade on delete
	 */
	public void setCascadeOnDelete(Boolean cascadeOnDelete) {
		this.cascadeOnDelete = cascadeOnDelete;
	}

	/**
	 * Gets the checks if is nullable.
	 *
	 * @return the checks if is nullable
	 */
	public Boolean getIsNullable() {
		return isNullable;
	}

	/**
	 * Sets the checks if is nullable.
	 *
	 * @param isNullable the new checks if is nullable
	 */
	public void setIsNullable(Boolean isNullable) {
		this.isNullable = isNullable;
	}
}
