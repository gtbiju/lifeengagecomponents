package com.cognizant.le.model.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.cognizant.le.model.dto.TableSchema;
import com.cognizant.le.util.Constants;
import com.cognizant.le.util.MSSQLConverter;
import com.cognizant.le.util.SQLiteRunner;

@Repository
public class SQLiteExportDAOImpl implements SQLiteExportDAO {

	/** The jdbc template. */
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private String schemaName;
	
	private String dbType;
	
	private final SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy/MM/dd HH:mm:ss");

	public void generateSQLiteScriptsForMysql(List<String> tablesWithEntries,
			Writer fileWriter) throws Exception {

		for (String table : tablesWithEntries) {
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			String sql = "SHOW CREATE TABLE " + table;
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getString(2));
				StringBuilder createScript = new StringBuilder(
						getSqliteScriptforCreate(table, rs.getString(2)));
				StringBuilder insertScript = new StringBuilder();

				insertScript = generateInsertStatements(connection, table);
				fileWriter.write("\n");
				fileWriter.write(createScript.toString());
				fileWriter.write("\n");
				fileWriter.write(insertScript.toString());
			}
			ps.close();
			connection.close();
		}

	}

	public List<String> checkForEntries(String tableName,
			List<String> tablesWithEntries) {
		return null;

	}

	public String getSqliteScriptforCreate(String tableName, String mysqlScript) {
		String[] lines = mysqlScript.split("\n");
		StringBuilder createScript = new StringBuilder();
		List<String> allMatches = new ArrayList<String>();
		List<String> sqliteScript = new ArrayList<String>();
		List<String> indexes = new ArrayList<String>();
		int counter = 0;
		for (String line : lines) {
			counter++;
			line = line.replaceAll("COMMENT .*", ",");
			line = line.replaceAll("UNSIGNED ", " ");
			line = line.replaceAll("on update [^,]*", "");
			line = line.replaceAll("(small|tiny|medium|big|)int\\([0-9]*\\)",
					" INTEGER");
			line = line.replaceAll("(VARCHAR|varchar)\\([0-9]*\\)", "TEXT");
			line = line.replaceAll("(DECIMAL|decimal)\\([0-9,]*\\)", "TEXT");
			line = line.replaceAll("(longtext|LONGTEXT)", "TEXT");
			line = line.toUpperCase();
			if(line.contains("COLLATE UTF8_UNICODE_CI")){
				line = line.replaceAll("COLLATE UTF8_UNICODE_CI", "");
			}
			if (line.contains("ENGINE")) {
				line = ");";
				sqliteScript.add(line);
			} else if (line.contains("CREATE TABLE")) {
				line = line.replace("`", "");
				sqliteScript.add(line);
			} else if (line.contains("AUTO_INCREMENT")) {
				line = line.replaceAll(" AUTO_INCREMENT",
						" PRIMARY KEY AUTOINCREMENT");
				sqliteScript.add(line);
			} else if (line.contains("CURRENT_TIMESTAMP")) {
				line = line.replaceAll("CURRENT_TIMESTAMP",
						"0000-00-00 00:00:00");
				sqliteScript.add(line);
			} else if (line.contains("PRIMARY KEY")) {
				if (counter == lines.length - 1) {
					String lastString = sqliteScript
							.get(sqliteScript.size() - 1);
					sqliteScript.remove(sqliteScript.size() - 1);
					sqliteScript.add(lastString.substring(0,
							lastString.length() - 1));
				}

			} else if (line.contains(" DATETIME")) {
				line = line.replaceAll(" DATETIME", " TEXT");
				sqliteScript.add(line);
			} else if (line.contains(" DATE")) {
				line = line.replaceAll(" DATE", " TEXT");
				sqliteScript.add(line);
			} else if (line.contains(" TIMESTAMP")) {
				line = line.replaceAll(" TIMESTAMP", " TEXT");
				sqliteScript.add(line);
			} else if (line.contains("ENUM")) {
				Matcher m = Pattern.compile(
						"(`)[a-zA-Z_0-9 ]*(` ENUM\\()[A-Za-z _0-9,']*\\)")
						.matcher(line);
				while (m.find()) {
					allMatches.add(m.group());
				}
				for (String match : allMatches) {
					match = match.replaceAll("'", "");
					Double enumSize = (double) 0;
					for (String enum1 : match.split(",")) {
						if (enum1.length() > enumSize) {
							enumSize = new Double(enum1.length());
						}
					}
					enumSize = Math.ceil((enumSize / 10) * 10);
					if (enumSize > 255) {
						enumSize = (double) 255;
					}
					sqliteScript.add(line.replaceAll(
							"(ENUM\\()[A-Za-z _0-9,']*\\)", "VARCHAR("
									+ enumSize.intValue() + "),"));
				}
			} else if (line.contains("KEY ") && (!line.contains("FOREIGN KEY"))
					&& (!line.contains("CODE_MAPPING_KEY"))) {
				line = line.replace(" FULLTEXT", "");
				line = line.trim();
				String columnName = "";
				String[] dummyArray = line.split("`");
				StringBuilder columnsList = new StringBuilder();
				int k = 2;
				if (dummyArray.length > k) {
					while (k < dummyArray.length) {
						columnsList.append(dummyArray[k]);
						k++;
					}
					System.out.println(columnsList);
					if (columnsList.charAt(columnsList.length() - 1) == ',') {
						columnsList = columnsList.deleteCharAt(columnsList
								.length() - 1);
					}
				}
				if (dummyArray.length > 0) {
					columnName = dummyArray[dummyArray.length - 2];
				}
				if (line.charAt((line.length() - 1)) == ',') {
					line = line.substring(0, line.length() - 1);
				}
				line = line.trim();
				line = "CREATE "
						+ line.replaceFirst(
								"(KEY) (`)[A-Za-z0-9_]*(`) (\\(`)[a-zA-Z0-9 _,`]*(`\\))",
								"INDEX idx_" + tableName + columnName + " ON "
										+ tableName + columnsList + ";");
				indexes.add(line);
				System.out.println(line);
			} else {
				sqliteScript.add(line);
			}

		}
		int scriptCtr = 0;
		for (String script : sqliteScript) {
			if (scriptCtr == sqliteScript.size() - 2
					&& script.charAt(script.length() - 1) == ',') {
				script = script.substring(0, script.length() - 1);
			}
			createScript.append(script);
			createScript.append("\n");
			System.out.println("CREATE SCRIPT " + script);
			scriptCtr++;
		}
		for (String script : indexes) {
			createScript.append(script);
			createScript.append("\n");
			System.out.println(script);
		}
		return createScript.toString();
	}

	private StringBuilder generateInsertStatements(Connection conn,
			String tableName) throws Exception {
		ResultSet rs;
		StringBuilder insertScripts = new StringBuilder();
		PreparedStatement ps = null;
		StringBuilder sql  = new StringBuilder("SELECT * FROM " + tableName + ";");

		if (sql != null) {
			ps = conn.prepareStatement(sql.toString());
			rs = ps.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();
			int[] columnTypes = new int[numColumns];
			StringBuilder columnNames = new StringBuilder();
			for (int i = 0; i < numColumns; i++) {
				columnTypes[i] = rsmd.getColumnType(i + 1);
				if (i != 0) {
					columnNames.append(",");
				}
				columnNames.append("'").append(rsmd.getColumnName(i + 1))
						.append("'");
			}

			java.util.Date d = null;
			String sqlfile = new String(tableName + "_insert");
			File inputFile = File.createTempFile(sqlfile, ".sql");
			PrintWriter p = new PrintWriter(new FileWriter(inputFile));
			insertScripts = new StringBuilder();
			while (rs.next()) {
				StringBuilder columnValues = new StringBuilder("");
				for (int i = 0; i < numColumns; i++) {
					if (i != 0) {
						columnValues.append(",");
					}

					switch (columnTypes[i]) {
					case Types.BIGINT:
					case Types.BIT:
					case Types.BOOLEAN:
					case Types.DECIMAL:
					case Types.DOUBLE:
					case Types.FLOAT:
					case Types.INTEGER:
					case Types.SMALLINT:
					case Types.TINYINT:
						String v = rs.getString(i + 1);
						columnValues.append(v);
						break;

					case Types.DATE:
						d = rs.getDate(i + 1);
					case Types.TIME:
						if (d == null)
							d = rs.getTime(i + 1);
					case Types.TIMESTAMP:
						if (d == null)
							d = rs.getTimestamp(i + 1);

						if (d == null) {
							columnValues.append("null");
						} else {
							columnValues.append("'")
									.append(dateFormat.format(d)).append("'");
						}
						break;

					default:
						v = rs.getString(i + 1);
						if (v != null) {
							columnValues.append("'")
									.append(v.replaceAll("'", "''"))
									.append("'");
						} else {
							columnValues.append("null");
						}
						break;
					}
				}
				insertScripts.append(String.format(
						"INSERT INTO %s (%s) values (%s);",
						tableName.toUpperCase(), columnNames,
						columnValues.toString()));
				insertScripts.append("\n");

			}
			p.close();
		}
		return insertScripts;
	}

	public List<String> getTablesList() throws Exception {
		Connection connection = jdbcTemplate.getDataSource().getConnection();
		String dbName = connection.getMetaData().getURL();
		dbName = dbName.substring(dbName.lastIndexOf("/") + 1);
		if(dbName.contains("?")){
			dbName = dbName.substring(0, dbName.lastIndexOf("?"));
		}
		
		System.out.println("$$$$" + dbName);
		List<String> tableList = new ArrayList<String>();
		String sql = "SHOW TABLES FROM " + dbName + ";";
		PreparedStatement ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			tableList.add(rs.getString(1));
		}
		ps.close();
		connection.close();
		return tableList;
	}

	public  final File executeSQLiteScripts(File sql)
			throws SQLException, FileNotFoundException, IOException {
		Connection c = null;
		String fileName = sql.getName();
		int lastIndex = fileName.lastIndexOf(".");
		String dbName = fileName.substring(0, lastIndex);
		dbName = dbName.replaceAll("[0-9]{9,}", "");
		String outFilename = new StringBuilder(
				System.getProperty("java.io.tmpdir")).append(File.separator)
				.append(dbName).append(".db").toString();
		File dbFile = null;
		File file = new File(outFilename);
		Boolean flag1 = file.delete();
		System.out.println(flag1);
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + outFilename);
			Statement stmt = c.createStatement();
			String query1 = "SELECT name FROM sqlite_master WHERE type = 'table'";
			ResultSet rs = stmt.executeQuery(query1);
			List<String> tables = new ArrayList<String>();
			while (rs.next()) {
				tables.add(rs.getString(1));
			}
			stmt.close();

			System.out.println(tables.size());
			for (String table : tables) {
				System.out.println(table);
				if (!table.equalsIgnoreCase("sqlite_sequence")) {
					Statement stmt2 = c.createStatement();
					String query2 = "drop table if exists " + table + ";";
					int res = stmt2.executeUpdate(query2);
					System.out.println("Result :" + res);
					stmt2.close();
				}
			}
			PrintWriter in = new PrintWriter(new StringWriter(), true);
			PrintWriter out = new PrintWriter(new StringWriter(), true);
			SQLiteRunner runner = new SQLiteRunner(c, in, out, false, true);
			runner.runScript(new BufferedReader(new InputStreamReader(
                    new FileInputStream(sql), "UTF8")));
			dbFile = new File(outFilename);
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return dbFile;
	}
	
	public List<String> getTablesListForSqlServer() throws Exception {
		Connection connection = jdbcTemplate.getDataSource().getConnection();
		List<String> tableList = new ArrayList<String>();
		String sql = "select * from INFORMATION_SCHEMA.TABLES  where TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA = '"+schemaName+"';";
		PreparedStatement ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			tableList.add(rs.getString("TABLE_NAME"));
		}
		ps.close();
		connection.close();
		return tableList;
	}

	@Autowired
	public SQLiteExportDAOImpl(Environment environment) {
		super();
		this.dbType = environment.getProperty(Constants.DBTYPE);
		this.schemaName = environment.getProperty(Constants.SCHEMA);
		if(schemaName == null){
			schemaName ="dbo";
		}
		
	}
	
	public void generateSQLiteScriptsMSSQL(List<String> tbls, Writer writer){
		try{
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			for(String tableName : tbls){
				TableSchema ts = MSSQLConverter
						.createTableSchema(connection, tableName, schemaName);
				MSSQLConverter.createForeignKeySchema(ts, connection);
				String createQueries = MSSQLConverter.buildCreateTableQuery(ts);
				String insertQueries = MSSQLConverter.copySqlServerRowsToSQLiteDB(ts,
						connection);
				writer.write("\n");
				writer.write(createQueries.toString());
				writer.write("\n");
				writer.write(insertQueries.toString());
			}
			if(connection!=null){
				try{
				connection.close();
				}catch(SQLException sq){
					sq.printStackTrace();
				}
			}
	}catch (Exception e) {
		e.printStackTrace();
	}
	}
}
