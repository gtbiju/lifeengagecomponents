/**
 *
 * � Copyright 2012, Cognizant
 *
 * @author        : 287304
 * @version       : 0.1, May 16, 2014
 */
package com.cognizant.le.enums;

public enum DType {
	Byte,

	Int32,
	
	Int16,
	
	Int64,
	
	Single,
	
	Double,
	
	String,
	
	Time,
	
	DateTime,
	
	Date,
	
	DateTime2,
	
	Binary,
	
	Boolean,
	
	GUID,
	
	Object,
	
}
