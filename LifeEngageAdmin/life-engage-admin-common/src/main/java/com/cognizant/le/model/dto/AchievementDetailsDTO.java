package com.cognizant.le.model.dto;

// TODO: Auto-generated Javadoc
/**
 * The Class AchievementDetailsDTO.
 */
public class AchievementDetailsDTO {
	
	/** The title. */
	private String title;
	
	/** The image. */
	private String image;
	
	/** The description. */
	private String description;

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the image.
	 *
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Sets the image.
	 *
	 * @param image the new image
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(null!=obj && obj instanceof AchievementDetailsDTO){
			return ((AchievementDetailsDTO)obj).getTitle().equals(this.getTitle());
		}else{
			return false;
		}
		
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 17*13;
	}
	
	

}
