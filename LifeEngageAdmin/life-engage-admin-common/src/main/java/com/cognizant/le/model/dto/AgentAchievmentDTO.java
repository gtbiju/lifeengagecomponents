package com.cognizant.le.model.dto;

// TODO: Auto-generated Javadoc
/**
 * The Class AgentAchievmentDTO.
 */
public class AgentAchievmentDTO {
	
	
	/** The agent code. */
	private String agentCode;
	
	/** The title ref. */
	private String titleRef;
	
	/** The orphan child. */
	private boolean orphanChild= false;
	
	/** The status. */
	private String status;
	
	/** The message. */
	private String message;
	
	int rowNumber;
	
	
	/**
	 * Checks if is orphan child.
	 *
	 * @return true, if is orphan child
	 */
	public boolean isOrphanChild() {
		return orphanChild;
	}

	/**
	 * Sets the orphan child.
	 *
	 * @param orphanChild the new orphan child
	 */
	public void setOrphanChild(boolean orphanChild) {
		this.orphanChild = orphanChild;
	}


	
	
	/**
	 * Gets the agent code.
	 *
	 * @return the agent code
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * Sets the agent code.
	 *
	 * @param agentCode the new agent code
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}


	/**
	 * Gets the title ref.
	 *
	 * @return the title ref
	 */
	public String getTitleRef() {
		return titleRef;
	}

	/**
	 * Sets the title ref.
	 *
	 * @param achievementId the new title ref
	 */
	public void setTitleRef(String achievementId) {
		this.titleRef = achievementId;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public AgentAchievmentDTO() {
		super();
		this.status ="";
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}
	
	


}
