package com.cognizant.le.model.dto;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class ResponseBean.
 */
public class ResponseBean {
	
	/** The table name. */
	private String tableName;
	
	/** The data count. */
	private int dataCount;
	
	/** The master db entries. */
	private List<? extends MasterDBEntry> masterDbEntries;
	
	/** The message. */
	private String message;
	
	/** The response status. */
	private String responseStatus = "";

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the table name.
	 *
	 * @return the table name
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * Sets the table name.
	 *
	 * @param tableName the new table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Gets the data count.
	 *
	 * @return the data count
	 */
	public int getDataCount() {
		return dataCount;
	}

	/**
	 * Sets the data count.
	 *
	 * @param dataCount the new data count
	 */
	public void setDataCount(int dataCount) {
		this.dataCount = dataCount;
	}

	/**
	 * Gets the master db entries.
	 *
	 * @return the master db entries
	 */
	public List<? extends MasterDBEntry> getMasterDbEntries() {
		return masterDbEntries;
	}

	/**
	 * Sets the master db entries.
	 *
	 * @param masterDbEntries the new master db entries
	 */
	public void setMasterDbEntries(List<? extends MasterDBEntry> masterDbEntries) {
		this.masterDbEntries = masterDbEntries;
	}

	/**
	 * Gets the response status.
	 *
	 * @return the response status
	 */
	public String getResponseStatus() {
		return responseStatus;
	}

	/**
	 * Sets the response status.
	 *
	 * @param responseStatus the new response status
	 */
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

}
