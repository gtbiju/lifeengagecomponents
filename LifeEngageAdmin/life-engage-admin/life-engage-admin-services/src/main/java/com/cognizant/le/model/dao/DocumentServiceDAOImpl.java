package com.cognizant.le.model.dao;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.cognizant.le.model.dto.ContentFile;
import com.cognizant.le.model.dto.FileInfo;
import com.cognizant.le.model.dto.RequestInfo;
import com.cognizant.le.util.HibernateUtil;
import com.cognizant.le.util.LoggerUtil;

public class DocumentServiceDAOImpl implements DocumentServiceDAO {
	static Logger logger = Logger.getLogger(DocumentServiceDAOImpl.class);

	@Override
	public List<ContentFile> getUpdatedDocs(List<ContentFile> inputContentFiles)
			throws Exception {
		logger.info(LoggerUtil
				.getLogMessage("Method Entry 3: DocumentServiceDAOImpl.getUpdatedDocs() in LifeEngageAdminWS"));

		Session session = null;
		List<ContentFile> tempListFiles = null;
		List<ContentFile> inputFile = new ArrayList<ContentFile>();
		List<String> fileNames = new ArrayList<String>();
		for (ContentFile conFile : inputContentFiles) {
			fileNames.add(conFile.getFileName());
		}
		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query query = session.getNamedQuery("fetchContentFileByPriority");
			query.setParameterList("fileNames", fileNames);
			logger.debug("Select * from ContentFile where fileName IN"
					+ fileNames);
			// Query query = session
			// .createSQLQuery("select fileName,tabletFullPath,publicURL,lastUpdated,version from [MLIDB].[MLI_schema].[contentFiles] where fileName='"+fileName);
			tempListFiles = query.list();

			if (!tempListFiles.isEmpty()) {
				for (ContentFile conFile : tempListFiles) {
					for (ContentFile contentFile : inputContentFiles) {
						if (contentFile.getVersion() != conFile.getVersion()
								&& conFile.getFileName().equals(
										contentFile.getFileName())) {
							logger.debug("*********File Name ******"
									+ contentFile.getFileName());
							inputFile.add(conFile);
						}
					}
				}
			}
			session.getTransaction().commit();

		} catch (Exception e) {
			logger.error(
					LoggerUtil
							.getLogMessage("Exception in getUpdatedDocs() in DocumentServiceDAOImpl"),
					e);
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}

		// End of OuterFor loop

		logger.info(LoggerUtil
				.getLogMessage("Method Exit 3: DocumentServiceDAOImpl.getUpdatedDocs() in LifeEngageAdminWS"));
		return inputFile;
	}

	/*
	 * @Override public List<ContentFile> getUpdatedDocs(List<ContentFile>
	 * inputContentFileList) throws Exception { List<ContentFile>
	 * ouptputContentFiles = new ArrayList<ContentFile>();
	 * logger.debug(LoggerUtil.getLogMessage(
	 * "Method Entry: DocumentServiceDAOImpl.getUpdatedDocs() in LifeEngageAdminWS"
	 * )); Session session = null; List<ContentFile> contentFileList = null; try
	 * {
	 * 
	 * session = HibernateUtil.getSession(); session.beginTransaction(); Query
	 * query = session.createQuery("from ContentFile"); contentFileList =
	 * query.list();
	 * 
	 * if (contentFileList != null && contentFileList.size() > 0) {
	 * ouptputContentFiles =
	 * getNewAndUpdatedDocuments(contentFileList,inputContentFileList);
	 * 
	 * }
	 * 
	 * } catch (Exception e) { logger.error( LoggerUtil
	 * .getLogMessage("Exception in getUpdatedDocs() in DocumentServiceDAOImpl"
	 * ), e); throw e; }
	 * 
	 * logger.debug(LoggerUtil .getLogMessage(
	 * "Method Exit: DocumentServiceDAOImpl.getUpdatedDocs() in LifeEngageAdminWS"
	 * )); return ouptputContentFiles;
	 * 
	 * }
	 */

	public List<ContentFile> getUpdatedDocumentsForContext(
			List<ContentFile> inputContentFileList, RequestInfo requestInfo,
			FileInfo fileInfo) throws Exception {
		logger.info(LoggerUtil
				.getLogMessage("Method Entry 3: DocumentServiceDAOImpl.getUpdatedDocumentsForContext in LifeEngageAdminWS"));
		Session session = null;
		List<ContentFile> contentFileList = null;
		List<ContentFile> ouptputContentFiles = new ArrayList<ContentFile>();
		try {

			session = HibernateUtil.getSession();
			session.beginTransaction();
			String contentFilter = requestInfo.getContentFilter();
			String contentLanguage = requestInfo.getLanguage();
			Integer applicationContextId = getApplicationContextId(requestInfo
					.getAppContext());
			if (applicationContextId != null) {
				Query query = null;
				List<Integer> applicationContextIds = new ArrayList<Integer>();
				applicationContextIds.add(applicationContextId);
				/*
				 * if (contentFilter.equalsIgnoreCase("all")) { query = session
				 * .getNamedQuery("fetchContentFileByAppContext");
				 * query.setParameterList("applicationcontextIdList",
				 * applicationContextIds); } else { query = session
				 * .getNamedQuery("fetchContentFileByAppContextAndType");
				 * query.setParameterList("applicationcontextIdList",
				 * applicationContextIds); query.setParameter("type",
				 * contentFilter);
				 * 
				 * }
				 */
				int contentFilterOpt = 0;
				int contentLangOpt = 0;
				int contentPrefOpt = 0;
				Boolean contentPref = null;
				if (contentFilter.equalsIgnoreCase("all")) {
					contentFilterOpt = 1;
				}
				if (contentLanguage.equalsIgnoreCase("all")) {
					contentLangOpt = 1;
				}
				if (requestInfo.getContentPreference().equalsIgnoreCase(
						"mandatory")) {
					contentPref = true;
				} else if (requestInfo.getContentPreference().equalsIgnoreCase(
						"optional")) {
					contentPref = false;
				} else if (requestInfo.getContentPreference().equalsIgnoreCase(
						"all")) {
					contentPrefOpt = 1;
				}

				query = session
						.getNamedQuery("fetchContentFileByAppContextGlobal");
				query.setParameterList("applicationcontextIdList",
						applicationContextIds);
				query.setParameter("type", contentFilter);
				query.setParameter("contentTypeOptional", contentFilterOpt);
				query.setParameter("name", contentLanguage);
				query.setParameter("contentLangOptional", contentLangOpt);
				query.setParameter("isMandatory", contentPref);
				query.setParameter("isMandatoryOptional", contentPrefOpt);
				query.setReadOnly(true);
				contentFileList = query.list();
				session.getTransaction().commit();
				if (contentFileList != null && contentFileList.size() > 0) {
					ouptputContentFiles = getNewAndUpdatedDocuments(
							contentFileList, inputContentFileList,
							requestInfo.getSyncMode(), fileInfo);

				}
			}
			

		} catch (Exception e) {
			logger.error(
					LoggerUtil
							.getLogMessage("Exception in getUpdatedDocumentsForContext in DocumentServiceDAOImpl"),
					e);
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		logger.info(LoggerUtil
				.getLogMessage("Method Exit 3: DocumentServiceDAOImpl.getUpdatedDocumentsForContext in LifeEngageAdminWS"));
		return ouptputContentFiles;

	}

	private Integer getApplicationContextId(String appContext) throws Exception {
		logger.debug(LoggerUtil
				.getLogMessage("Method Entry: DocumentServiceDAOImpl.getApplicationContextId in LifeEngageAdminWS"));
		Integer applicationContextId = null;
		Session session = null;
		try {

			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query query = session.getNamedQuery("fetchId");
			query.setParameter("appContext", appContext);
			applicationContextId = (Integer) query.uniqueResult();
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error(
					LoggerUtil
							.getLogMessage("Exception in getApplicationContextId in DocumentServiceDAOImpl"),
					e);
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		logger.debug(LoggerUtil
				.getLogMessage("Method Exit: DocumentServiceDAOImpl.getApplicationContextId in LifeEngageAdminWS"));
		return applicationContextId;

	}

	private List<ContentFile> getNewAndUpdatedDocuments(
			List<ContentFile> contentFileList,
			List<ContentFile> inputContentFileList, String syncMode,
			FileInfo fileInfo) {
		logger.debug(LoggerUtil
				.getLogMessage("Method Entry: DocumentServiceDAOImpl.getNewAndUpdatedDocuments in LifeEngageAdminWS"));
		List<ContentFile> ouptputContentFiles = new ArrayList<ContentFile>();


		constructDownloadUrl(contentFileList, fileInfo);

		for (ContentFile contentFile : contentFileList) {
			boolean isFileExists = false;
			for (ContentFile inputContentFile : inputContentFileList) {
				if (contentFile.getFileName().equals(
						inputContentFile.getFileName())) {
					isFileExists = true;
					if (syncMode.equalsIgnoreCase("all")
							|| syncMode.equalsIgnoreCase("update")) {
						if (contentFile.getVersion() != inputContentFile
								.getVersion()) {
							ouptputContentFiles.add(contentFile);
						}
					}
					break;
				}
			}
			if (syncMode.equalsIgnoreCase("all")
					|| syncMode.equalsIgnoreCase("new")) {
				if (!isFileExists) {
					ouptputContentFiles.add(contentFile);
				}
			}

		}
		logger.debug(LoggerUtil
				.getLogMessage("Method Exit: DocumentServiceDAOImpl.getNewAndUpdatedDocuments in LifeEngageAdminWS"));
		return ouptputContentFiles;
	}

	private void constructDownloadUrl(List<ContentFile> contentFileList, FileInfo fileInfo) {
		
		StringBuffer downloadURL = new StringBuffer(fileInfo.getServerUrl());
		if(!(fileInfo.getServerUrl().endsWith("/"))) {
			downloadURL.append("/");
		} 
		StringBuilder contextPath= new StringBuilder(fileInfo.getContextPath());
		if(contextPath.charAt(0)=='/'){
			contextPath.deleteCharAt(0);
		}
		downloadURL.append(contextPath);
		//StringBuilder downloadURL = new StringBuilder("/downloadContent?fileName=");
		
		for(int i=0; i<contentFileList.size(); i++){
			ContentFile contentFile = contentFileList.get(i);
			String oldUrl = contentFile.getPublicURL();
			contentFile.setPublicURL(downloadURL+oldUrl);
		}

	}

}
