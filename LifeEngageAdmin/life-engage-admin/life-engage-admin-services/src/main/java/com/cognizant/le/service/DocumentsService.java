package com.cognizant.le.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.cognizant.le.manager.DocumentServiceManageImpl;
import com.cognizant.le.manager.DocumentServiceManager;
import com.cognizant.le.model.dto.AppContext;
import com.cognizant.le.model.dto.ContentFile;
import com.cognizant.le.model.dto.DocumentContextRequest;
import com.cognizant.le.model.dto.Documents;
import com.cognizant.le.model.dto.FileInfo;
import com.cognizant.le.model.dto.RequestInfo;
import com.cognizant.le.util.LoggerUtil;
import com.sun.jersey.api.core.ResourceConfig;

@Path("/documentService")
public class DocumentsService {
	static Logger logger = Logger.getLogger(DocumentsService.class);
	
	@Context
	ResourceConfig context;

	// Get Updated Documents Service
	@POST
	@Path("/getupdatedDocs")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Documents> getUpdatedDocs(List<Documents> contentFiles,
			@HeaderParam("nwsAuthToken") String nwsAuthToken) {
		logger.info(LoggerUtil.getLogMessage("Method Entry 1: DocumentsService.getUpdatedDocs() in LifeEngageAdminWS"));
		List<Documents> outputContentFiles = null;
		List<ContentFile> updatedContentFiles = null;
		String output = "";
		// Added this to check the Security token through header
		if ("e03364fd-a8c6-7878-3d44-cff2a9d8e333".equals(nwsAuthToken)) {
			logger.info(LoggerUtil.getLogMessage("Authentication Success"));
			DocumentServiceManager documentServiceManager = new DocumentServiceManageImpl();
			try {
				updatedContentFiles = documentServiceManager
						.convertJSONTOJava(contentFiles);
				updatedContentFiles = documentServiceManager
						.getUpdatedDocuments(updatedContentFiles);
				outputContentFiles = documentServiceManager
						.convertJavaTOJSON(updatedContentFiles);
				output = "Updated Files :" + outputContentFiles;

			} catch (Exception e) {
				logger.error(LoggerUtil.getLogMessage("Exception in DocumentsService.getUpdatedDocs() in DocumentsService"),e);
			}
		} else {
			logger.info(LoggerUtil.getLogMessage("FAILURE:: INSIDE DocumentsService.getUpdatedDocs():: REQUEST IS NOT FROM AN AUTHORISED SOURCE"));
			outputContentFiles = new ArrayList<Documents>();
		}
		logger.info(LoggerUtil.getLogMessage("Method Exit 1: DocumentsService.getUpdatedDocs() in LifeEngageAdminWS"));
		return outputContentFiles;

	}

	@POST
	@Path("/getUpdatedDocumentsForContext")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<Documents> getUpdatedDocumentsForContext(DocumentContextRequest contextRequest,
			@HeaderParam("nwsAuthToken") String nwsAuthToken, @Context ServletContext servletContext) throws IOException {
		logger.info(LoggerUtil.getLogMessage(new Date()+" : Method Entry 1: DocumentsService.getUpdatedDocumentsForContext in LifeEngageAdminWS"));
		List<Documents> outputContentFiles = null;
		List<ContentFile> updatedContentFiles = null;
		AppContext requestInfo = null;
		RequestInfo reqInfo= null;
		String propertyFileName = servletContext.getInitParameter("CONFIG_PROPERTIES");
		
		
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread()
					.getContextClassLoader();
			InputStream stream = classLoader.getResourceAsStream("/"
					+ propertyFileName);
			if (null == stream) {
				stream = classLoader.getResourceAsStream(propertyFileName);
			}
			properties.load(stream);
		} catch (Exception e) {
			logger.debug(LoggerUtil.getLogMessage(e.getMessage()));
		}

		String serverURL = properties.getProperty("SERVER_URL");
		if ((null == serverURL) || "".equals(serverURL.trim())) {
			logger.error("Property SERVER_URL is not defined");
		}
		
		FileInfo fileInfo = new FileInfo();
		fileInfo.setServerUrl(serverURL);
		fileInfo.setContextPath(servletContext.getContextPath());

		// Added this to check the Security token through header
		if ("e03364fd-a8c6-7878-3d44-cff2a9d8e333".equals(nwsAuthToken)) {
			logger.info(LoggerUtil.getLogMessage("Authentication Success"));
			DocumentServiceManager documentServiceManager = new DocumentServiceManageImpl();
			try {
				if(contextRequest != null){
				
				updatedContentFiles = documentServiceManager
						.convertJSONTOJava(contextRequest.getContentFiles());
				reqInfo = contextRequest.getRequestInfo();
				
				//requestInfo = documentServiceManager.getRequestInfo(contextRequest.getRequestInfo());
				reqInfo= documentServiceManager.validateModes(reqInfo);
				updatedContentFiles = documentServiceManager
						.getUpdatedDocumentsForContext(updatedContentFiles,reqInfo,fileInfo);
				outputContentFiles = documentServiceManager
						.convertJavaTOJSON(updatedContentFiles);
				} else {
					logger.debug(LoggerUtil.getLogMessage("Input DocumentContextRequest is empty in DocumentsService.getUpdatedDocumentsForContext"));
				}

			} catch (Exception e) {
				logger.error(LoggerUtil.getLogMessage("Exception in DocumentsService.getUpdatedDocumentsForContext in DocumentsService"),e);
			}
		} else {
			logger.info(LoggerUtil.getLogMessage("FAILURE:: INSIDE DocumentsService.getUpdatedDocumentsForContext : REQUEST IS NOT FROM AN AUTHORISED SOURCE"));
			outputContentFiles = new ArrayList<Documents>();
		}
		logger.info(LoggerUtil.getLogMessage(new Date()+" :Method Exit 1: DocumentsService.getUpdatedDocumentsForContext in LifeEngageAdminWS"));
		return outputContentFiles;

	}

	
	
	
	/*
	 * @POST
	 * 
	 * @Path("/getupdatedDocs2")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON)
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON) public List<ContentFile>
	 * getUpdatedDocs2(JSONObject contentFiles) {
	 * 
	 * //MyObject ob = new ObjectMapper().readValue(jsonString, MyObject.class);
	 * 
	 * List<ContentFile> inputContent = null; String output = "";
	 * DocumentServiceManager documentServiceManager = new
	 * DocumentServiceManageImpl(); try{ //inputContent =
	 * documentServiceManager.getUpdatedDocuments(contentFiles) ; output =
	 * "Updated Files :" + contentFiles.toString();
	 * 
	 * }catch (Exception e) {
	 * 
	 * // TODO: handle exception e.printStackTrace(); } return inputContent;
	 * 
	 * }
	 */

}
