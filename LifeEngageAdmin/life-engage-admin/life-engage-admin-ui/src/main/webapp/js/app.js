'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives', 'myApp.controllers','plunker','date_picker']).
  config(['$routeProvider', function($routeProvider) {
      $routeProvider.when('/home', { templateUrl: 'partials/partial1.html', controller: 'TabsDemoCtrl' });
    $routeProvider.when('/view2', {templateUrl: 'partials/partial2.html', controller: 'MyCtrl2'});
    $routeProvider.otherwise({redirectTo: '/home'});
} ]);

angular.module('plunker', ['ui.bootstrap']);
var TabsDemoCtrl = function ($scope) { 
    
};

angular.module('date_picker', ['$strap.directives']);
