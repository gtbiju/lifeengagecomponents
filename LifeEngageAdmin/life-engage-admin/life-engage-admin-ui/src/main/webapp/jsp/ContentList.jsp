<%@page import="com.cognizant.le.model.dao.CommonAdminDAO"%>
<%@page import="com.cognizant.le.model.dao.CommonAdminDAOImpl"%>
<%@page import="com.cognizant.le.model.dto.ContentTypeDTO"%>
<%@page import="com.cognizant.le.model.dto.ContentLanguageDTO"%>
<%@page import="com.cognizant.le.model.dao.AgentServiceDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="com.cognizant.le.model.dao.AgentServiceDaoImpl"%>
<%@ page import="java.util.List"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title></title>
<link rel="stylesheet" href="css/css_reset.css" />
<link rel="stylesheet" href="css/styles_workbench.css" />
<title>DAP</title>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/moment.min.js"></script>

<script>
defaultLanguage="";
$(document).ready(function() { 
	createAppContextChecks();
	$('.custom-upload input[type=file]').bind('change focus click',
			SITE.fileInputs);
	var msg = "<%=session.getAttribute("msg")%>";
	var rollbackMsg = "<%=request.getAttribute("rollbackMsg")%>";
	var updatedFileName = "<%=session.getAttribute("updatedFileName")%>";
	defaultLanguage = "<%=session.getAttribute("DEFAULT_LANGUAGE")%>";
	if(defaultLanguage == null || defaultLanguage.length==0){
		defaultLanguage="English";
	}
	if (msg.length > 4) 
				{
				<%session.removeAttribute("msg");%>
				if(msg.indexOf("Success")!=-1){
				$('.upload_message').css('color','green');
				}
				var contentFileName = "<%=request.getAttribute("filename")%>";
			    $('#page_loader').empty();
				var dynamic_height = $('body').height();
				$('#page_loader').css('height',dynamic_height).fadeIn(300);
				updateContent();
				document.getElementById('update_message_span').innerHTML = msg;
				document.getElementById('updateFileSp').innerHTML = contentFileName;
				selectedFile=contentFileName;
				}
	var uploadMsg = "<%=session.getAttribute("uploadMsg")%>";
	if (uploadMsg.length > 4) 
		{
		<%session.removeAttribute("uploadMsg");%>
		if(uploadMsg.indexOf("Success")!=-1){
			$('.upload_message').css('color','green');
		}
	    $('#page_loader').empty();
		var dynamic_height = $('body').height();
		$('#page_loader').css('height',dynamic_height).fadeIn(300);
		addMore();
	 	document.getElementById('upload_message_span').innerHTML = uploadMsg;
		}
	
	if (rollbackMsg.length > 4) 
		{
		var prevVerExists=true;
		var contentFileName = "<%=request.getAttribute("contentFileName")%>";
		var contentVersion = "<%=request.getAttribute("contentVersion")%>";
		var previousVrsn= contentVersion-1;
		
		<%session.removeAttribute("rollbackMsg");%>
   	 	$('#page_loader').empty();
		var dynamic_height = $('body').height();
		$('#page_loader').css('height',dynamic_height).fadeIn(300);
		rollbackVersion();
		$('#rollback_pop_up_btn').attr('disabled',false);
		document.getElementById('rollbackmsg').innerHTML=rollbackMsg;
		document.getElementById('rollback_item_value').innerHTML=contentFileName;
		document.getElementById('rollback_item_name').innerHTML=contentFileName;
		if(rollbackMsg.indexOf("Success")!=-1){
			$('.rollback_returnMsg').css('color','green');
			if(previousVrsn==0){
				previousVrsn=1;
			}
		}else if(rollbackMsg.indexOf("Failed")!=-1){
			previousVrsn = contentVersion;
			contentVersion ++;
		}
		document.getElementById('rollback_span').innerHTML = "To Version "+ previousVrsn;
  	 	document.getElementById('rollback_label').innerHTML= "From Version "+contentVersion;
		document.getElementById('rollback_name').value=contentFileName;
		document.getElementById('rollback_version').value=contentVersion;
		
		}
	var tagUntagMsg = "<%=request.getAttribute("tagUntagMsg")%>";
	if (tagUntagMsg != null && tagUntagMsg.length > 4) 
	{
		var contentFileName = "<%=request.getAttribute("contentFileName")%>";
		var selectedAppContext = "<%=request.getAttribute("selectedAppContext")%>";
		selectedLanguageId = "<%=request.getAttribute("language")%>";
		selectedPreference= <%=request.getAttribute("isMandatory")%>
		selectedTypeId= <%=request.getAttribute("selectedContentType")%>
		document.getElementById('doc_opt_udt_val').val=selectedPreference;
		document.getElementById('edt_slctd_lng').val=selectedLanguageId;
		
		
		<%request.removeAttribute("tagUntagMsg");%>
		if(tagUntagMsg.indexOf("successfully")!=-1){
			$('.upload_message').css('color','green');
		}
		$('#page_loader').empty();
		var dynamic_height = $('body').height();
		$('#page_loader').css('height',dynamic_height).fadeIn(300);
		tagUntagContext();
		document.getElementById('tagUntagMsg').innerHTML=tagUntagMsg;
		document.getElementById('tagUntagContentFile').innerHTML=contentFileName;
		document.getElementById('tagUntagContentFileName').value=contentFileName;
		document.getElementById('selectedAppContext').value=selectedAppContext;
		selectedFile = contentFileName;
		selectedContext = selectedAppContext;
		tagUntagContextFunction();
	}
	
/* 	$('input:file').change(
            function(){
            	document.getElementById("upload_message_span").innerHTML="";
            	document.getElementById("update_message_span").innerHTML="";
                if ($(this).val()) {
                    $('#upload_pop_up_btn').attr('disabled',false);
                    $('#update_pop_up_btn').attr('disabled',false);
                } 
            }
            ); */
			
	$('#rollback_pop_up_btn').click(function (e) {
		var prevVern=document.getElementById('rollback_version').value;
		var rollbackComment = document.getElementById('rollback_comment_textarea').value;
		if(prevVern==1){
			document.getElementById('rollbackmsg').innerHTML="";
			 $('#prvVrnWithRollBackSucssMsg').css('color','#358bae');
			document.getElementById('prvVrnWithRollBackSucssMsg').innerHTML= "This is the initial version, no further rollback allowed.";
			e.preventDefault();
		}else if(!hasValue(rollbackComment)){
			document.getElementById('rollbackmsg').innerHTML="";
			$('#prvVrnWithRollBackSucssMsg').css('color','red');
			document.getElementById('prvVrnWithRollBackSucssMsg').innerHTML= "Please enter Comments";
			e.preventDefault();
		}
	});
	$('#update_pop_up_btn').click(function (e) {
		var name = selectedFile;
		var fileName="";
		var file = $("#fileName").val();
		fileName = file.replace(/^.*[\\\/]/, '');
		var updateComment = document.getElementById('update_comment_textarea').value;
		if (fileName.length==0) {
			$('.upload_message').css('color','red');
			 document.getElementById('update_message_span').innerHTML = "File not selected!";
			 e.preventDefault();	
		}
		
		else if (fileName != name) {
			$('.upload_message').css('color','red');
			 document.getElementById('update_message_span').innerHTML = "File selected doesn't match with the existing entry.";
			 e.preventDefault();
		}else if(!hasValue(updateComment)){
			$('.upload_message').css('color','red');
			document.getElementById('update_message_span').innerHTML= "Please enter Comments";
			e.preventDefault();
		}
	});		
	
    $("#FilterInAdd").change(function(){
    	clearUpdateMsg();
    	var contentPref= <%=session.getAttribute("contentPreference").toString()%>;
    	var selectedDropDown = $("select#FilterInAdd option:selected").val();
		/* $('select#FilterInLanguage option:nth(0)').attr("selected", "selected"); */
		$("select#FilterInLanguage option:contains('"+defaultLanguage+"')").attr("selected", "selected");
    	for(var id in contentPref.contentPreference){
    		if(id==selectedDropDown){
    			if(contentPref.contentPreference[id].optionality==true){
    				$( "#is_mandatory" ).show();
					$('#FilterInLanguage').prop('disabled', false);
					
					$('#doc_req').prop('disabled', false);
					$('#doc_req').prop('checked', false);
					
    			}else{
					$('#FilterInLanguage').prop('disabled', true);
    				$( "#is_mandatory" ).hide();
    				
    				$('#doc_req').prop('disabled', true);
					$('#doc_req').prop('checked', true);
    			}
    		}else{
    			
    		}
    	}
   });
});


var SITE = SITE || {};

SITE.fileInputs = function() {
	var $this = $(this), $val = $this.val(), valArray = $val.split('\\'), newVal = valArray[valArray.length - 1], $button = $this
			.siblings('.button'), $fakeFile = $this
			.siblings('.file-holder');
	if (newVal !== '') {
		$button.text('Browse');
		if ($fakeFile.length === 0) {
			$button
					.after('<span class="file-holder">' + newVal
							+ '</span>');
		} else {
			$fakeFile.text(newVal);
		}
	}
};

function update(selectedFile,selectedType)
{
	var selectionInfo=selectedFile;
	window.location.href="<%=request.getContextPath()%>/updateFile?type="+selectionInfo;

	}
	
	
function uploadCheckAdd() {

	var filterOption = document.getElementById("FilterInAdd");

	var selection = filterOption.options[filterOption.selectedIndex].text;

	var fileName = $("#uploadfileName").val();
	
	var extension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
	var check = false;
	if (selection != "-Select-") {
		var validation= <%=session.getAttribute("extensions").toString()%>
		var i = 0
		for ( var i = 0; i < validation.contentTypes.length; i++) {
			var fileType = validation.contentTypes[i];
			var Val;
			for ( var type in fileType) {
				if (selection == type) {
					Val = fileType[type];
					for ( var ext in Val) {
						if (extension == Val[ext]) {
							check = true;
							$("#FilterInLanguage").prop("disabled", false);
							document.uploadForm.submit();
							return false;
						}
					}
					}
				}
			}
		
		if (fileName.length==0) {
			$('.upload_message').css('color','red');
			 document.getElementById('upload_message_span').innerHTML = "File not selected!";
			 e.preventDefault();	
			 document.getElementById('uploadForm').onsubmit = function() {
				    return false;
				}
		}
		
		if(check==false){
			$('.upload_message').css('color','red');
			document.getElementById('upload_message_span').innerHTML = "Invalid File or Type";
			document.getElementById('uploadForm').onsubmit = function() {
			    return false;
			}
			
		}
		} else {
			$('.upload_message').css('color','red');
			document.getElementById('upload_message_span').innerHTML = "Please Select Content Type";
			document.getElementById('uploadForm').onsubmit = function() {
			    return false;
			}
	}
	

}


</script>
<script>
function filterCheck()
{
	 $("#rollback_btn").attr("disabled", true);
	 $("#update_btn").attr("disabled", true);
	 $("#tag_untag_btn").attr("disabled", true);
     $("#rollback_btn").removeClass('enabledbtn').addClass('disabled_btn');
     $("#tag_untag_btn").removeClass('enabledbtn').addClass('disabled_btn');
     $("#update_btn").removeClass('enabledbtn').addClass('disabled_btn');
   
var filterOption = document.getElementById("Filter");

var option = filterOption.options[filterOption.selectedIndex].value;

$.ajax({
	
		type: "POST",
	    url: "<%=request.getContextPath()%>/loadFile",
	    data:"option="+option,
	 	async : false,
		cache : false,												
		success : function(data) {
			
			var len = data.length ;
			loadResult = data;
			var loadBORow='';
			var serial=1;
			for(var i = 0; i < len; i++){
				var contentFile = "";
 				var contentPref= <%=session.getAttribute("contentPreference").toString()%>;
 				for(var id in contentPref.contentPreference){
					if(id==loadResult[i].contentType.id){
						if(contentPref.contentPreference[id].previewable==true){
							contentFile = '<td><a href="'+ loadResult[i].downloadUrl+'">'+loadResult[i].fileName+'</a></td>';
						}else{
							contentFile = '<td>'+ loadResult[i].fileName+'</td>';
						}
					}	
 				}
 				if(i%2==0){
 					sClass1="style=background-color:#f0f0f0";
 				}else{
 					sClass1="style=background-color:#ffffff;";
 				}
 				loadBORow+='<tr '+sClass1+'>'+
 				'<td class="align_center">'+ serial +'</td>'+
 				'<td class="align_center">'+loadResult[i].contentType.contentType +'</td>'+
 				contentFile +
 				'<td>'+ loadResult[i].appContext +'</td>'+
				'<td>'+ loadResult[i].contentLanguage.language +'</td>'+
 				'<td class="align_center">'+loadResult[i].version+'</td>'+
 				'<td class="align_center"><input type="radio" name="selectionRadio" value="'+i+'" onclick="radiocheck('+i+')"></td></tr>'; 
				
 				serial+=1;
				    				
			}
			$('#tblBody').html(loadBORow);
			
		}, 
		error : function(data){
			alert("error");
		}
	});

	
	}
function tagUntagContextFunction()
{	 
	$("select#FilterInLanguage option:contains('"+defaultLanguage+"')").attr("selected", "selected");
	document.getElementById('tagUntagContentFile').innerHTML = selectedFile;
	document.getElementById('tagUntagContentFileName').value=selectedFile;
	document.getElementById('tagUntagSelectedType').value=selectedTypeId;
	document.getElementById('edt_slctd_lng').value=selectedLanguageId;
	document.getElementById('tagUntagLanguage').value=selectedLanguageId;
	
/* 	if(isNaN(selectedLanguage)){
	$("select#tagUntagLanguage option:contains('"+selectedLanguage+"')").attr('selected', true);
	}else{
	document.getElementById('tagUntagLanguage').value=selectedLanguage;
	}
	 */
	
	var contentPref= <%=session.getAttribute("contentPreference").toString()%>;
	
	$('#doc_req_udt').prop('checked', selectedPreference);
	
	
	for(var id in contentPref.contentPreference){
		if(id==selectedTypeId){
			if(contentPref.contentPreference[id].optionality==true){
				$('#tagUntagLanguage').prop('disabled', false);
				$('#doc_req_udt').prop('disabled', false);
				document.getElementById('is_disabled').value="false";
			}else{
				$('#tagUntagLanguage').prop('disabled', true);
				$('#doc_req_udt').prop('disabled', true);
				document.getElementById('is_disabled').value="true";
			}
		}else{
			
		}
	}
	//$("select#tagUntagLanguage option:contains('"+selectedLanguage+"')").attr('selected', true);
	var selectedAppContext = "";
	var selectedContextList =  selectedContext.split(",");
	var appContextsList= <%=session.getAttribute("appContexts") %>; 
	var r = new Array(); 
	var j = -1;
	var col = 1;
	if( appContextsList != null )	{
	$(appContextsList.appContexts).each(function(index, element){
		var checkBox = false;
		for(var selContext in selectedContextList){
			if( element.context == selectedContextList[selContext] || element.id == selectedContextList[selContext]){
				checkBox = true;
				selectedAppContext += element.id+",";
				break;
			}
		}
		
		if(col%2 != 0){
		 	r[++j] = '<tr class="align_center">';
		}
		 r[++j] = '<td class="align_center">';
		 if(checkBox){
			 r[++j] = '<input type="checkbox" name="appContext" checked value="'+element.id+'" onclick="updateSelectdAppContext('+element.id+')"></td>';
		 }else{
			 r[++j] = '<input type="checkbox" name="appContext"  value="'+element.id+'" onclick="updateSelectdAppContext('+element.id+')"></td>';
		 }
		 
		 r[++j] ='<td class="tag_Untag_col_width">';
		 r[++j] = element.context;
		 r[++j] = '</td>';
		 if(col%2 == 0 || (col%2 != 0 && index == appContextsList.appContexts.length -1 )){
		 	r[++j] ='</tr>';
		 }
		 col++;	
			
		$('#contextDataTable').html(r.join('')); 
	});
	}
	if( selectedAppContext.length > 0){
			selectedAppContext = selectedAppContext.substring(0, selectedAppContext.length-1);
	}
	document.getElementById('selectedAppContext').value=selectedAppContext;
}
</script>
<script type="text/javascript">
selectedFile ="";
selectedLanguage="";
selectedType ="";
selectedTypeId="";
loadResult="";
selectedPreference="";
selectedContext = "";
selectedUrl="";
selectedfileDesc="";
      $(document).ready(function() {  
         $('#spanId').append(moment().format('h:mm A Do MMMM YYYY'));       
     	$.ajax({
     		type: "POST",
     	    url: "<%=request.getContextPath()%>/loadFile",
     	 	async : false,
     		cache : false,												
     		success : function(data) {
     			
     			var len = data.length ;
     			loadResult = data;
     			var loadBORow='';
     			var serial=1;
     			for(var i = 0; i < len; i++){
    				var contentFile = "";
     				var contentPref= <%=session.getAttribute("contentPreference").toString()%>;
     				for(var id in contentPref.contentPreference){
     				if(id==loadResult[i].contentType.id){
     					if(contentPref.contentPreference[id].previewable==true){
     						contentFile = '<td><a href='+ loadResult[i].downloadUrl+'>'+loadResult[i].fileName+'</a></td>';
     					}else{
     						contentFile = '<td>'+ loadResult[i].fileName+'</td>';
     					}
     				}	
     				}
     				
     				if(i%2==0){
     					sClass1="style=background-color:#f0f0f0";
     				}else{
     					sClass1="style=background-color:#ffffff;";
     				}
     				
     				loadBORow+='<tr '+sClass1+'>'+
     				'<td class="align_center">'+serial +'</td>'+
     				'<td class="align_center">'+loadResult[i].contentType.contentType +'</td>'+
     				contentFile+
     				'<td>'+ loadResult[i].appContext +'</td>'+
					'<td>'+ loadResult[i].contentLanguage.language +'</td>'+
     				'<td class="align_center">'+loadResult[i].version+'</td>'+
     				'<td class="last-column"><input type="radio" name="selectionRadio" value="'+i+'" onclick="radiocheck('+i+')"></td></tr>'; 
     				
     				serial+=1;
     				    				
     			}
     			$('#tblBody').append(loadBORow);
     			
     		}, 
     		error : function(data){
     			alert("error");
     		}
     	});
         
    });
      function radiocheck(row_id)
      {
    	selectedFile=loadResult[row_id].fileName;
        selectedType=loadResult[row_id].contentType.contentType;
        selectedTypeId=loadResult[row_id].contentType.id;
		selectedLanguage=loadResult[row_id].contentLanguage.language;
		selectedLanguageId=loadResult[row_id].contentLanguage.id;
		selectedVersion=loadResult[row_id].version;
        selectedContext = loadResult[row_id].appContext;
        selectedPreference=loadResult[row_id].isMandatory;
        selectedfileDesc=loadResult[row_id].fileDesc;
        
        if(parseInt(selectedVersion)!=1){
        	$("#rollback_btn").attr("disabled", false);
		 	$("#rollback_btn").removeClass('disabled_btn').addClass('enabledbtn');
		}else{
			$("#rollback_btn").attr("disabled", true);
			$("#rollback_btn").removeClass('enabledbtn').addClass('disabled_btn');
		}
        $("#update_btn").attr("disabled", false);
      	$("#tag_untag_btn").attr("disabled", false);
        $("#tag_untag_btn").removeClass('disabled_btn').addClass('enabledbtn');
        $("#update_btn").removeClass('disabled_btn').addClass('enabledbtn');
        
      }
      function updateFunction()
      {
    	 /*  update(selectedFile,selectedType); */
    	 
    	 document.getElementById('updateFileSp').innerHTML = selectedFile;
    	 if(typeof(selectedfileDesc)!='undefined') {
    	 	document.getElementById('up_desc_text').innerHTML = selectedfileDesc;
    	 }else{
    		document.getElementById('up_desc_text').innerHTML = "";
    	 }
      }
      
      function rollbackFunction()
      {
		var prevVerExists= true;
    	 /*  update(selectedFile,selectedType); */
    	 document.getElementById('rollback_item_name').innerHTML = selectedFile;
    	 document.getElementById('rollback_item_value').innerHTML = selectedFile;
    	 document.getElementById('rollback_name').value = selectedFile;
    	 document.getElementById('rollback_version').value = selectedVersion;
    	 var prevVer=selectedVersion-1;
		 if(prevVer==0){
			prevVerExists=false;
		 }
		 if(prevVerExists){
			 $('#rollbackVersionDiv').css("display","block");
    		 document.getElementById('rollback_span').innerHTML = "To Version "+ prevVer;
    	 	 document.getElementById('rollback_label').innerHTML= "From Version "+selectedVersion;
		 }else{
			 $('#rollbackVersionDiv').css("display","none");
		 }
		 document.getElementById('rollbackmsg').innerHTML="";
		 document.getElementById('prvVrnWithRollBackSucssMsg').innerHTML="";
		 $('.modal_pop_up.rollback_content_pop_up').css("display","block");
    	 
      }
  	
  	
  	function rollbackCheck(){
  			var fileName= document.getElementById("rollback_name");
  			if(typeof fileName != 'undefined'){
  				document.rollbackForm.submit();
  			}
  	}
  	
  	function createAppContextChecks(){
  		var contexts= <%=session.getAttribute("appContexts") %>
  		mytable = $('<table></table>').attr({ id: "app_context_table" });
  		var row;
		$(contexts.appContexts).each(function(index, element){
		if(index==0 || index%2==0){
			row = $('<tr></tr>').attr({ 'class': ["class1", "class2", "class3"].join(' ') }).appendTo(mytable);
		}
		$('<td class="tag_Untag_col_width"></td>').html('<input type="checkbox" id="context_id" class="context_class" onclick="clearUpdateMsg()" name="context_name" value= '+element.id+'>'+element.context+ '').appendTo(row); 	
		})
  		mytable.appendTo("#app_context_id");	       
  	}
  	

	
  	

</script>

<style>
 .appTableStyles td{
  width:250px;
 }
 
 textarea {
   resize: none;
}
 
</style>
<script type="text/javascript">
	/*Logout Function  */

	
	function logoutClick(){
		document.location.href="<%=request.getContextPath()%>/Logout";
	}
	
	function agentPage(){
		document.location.href="<%=request.getContextPath()%>/AgentUpload";
	}
	function mdbLoader(){
		document.location.href="<%=request.getContextPath()%>/jsp/masterDataTemplate.jsp";
	}
</script>

</head>
<body>
	<div class="blue_bg_header">
		<div class="top_header_components clearfix">
			<span class="environment_txt">Environment</span> <select
				id="filter_environment">
				<option value="Model Office">Model Office</option>
				<option value="Production">Production</option>
				<option value="UAT">UAT</option>
				<option value="Dev">Dev</option>
			</select>
			<button class="logout_action_btn" id="logout_btn" onClick="logoutClick()">Logout</button>
		</div>
	</div>
	<div class="logo_header">
		<img src="img/logo-main.png" alt="logo-main" class="mainLogo" > <span
			class="header_title">LifeEngage Configurator</span>
	</div>
	<!-- <div class="main_nav_bar nav_bg_green">
			<ul class ="main_nav_bar_list clearfix">			
                <li class=""><a class="clearfix"  href="#/home"><span class="menu_link_text">Products</span></a></li>                 
				<li ><a class="clearfix" href="#/myaccount"><span class="menu_link_text">Users</span></a></li>				
				<li ><a class="clearfix" href="#/myaccount"><span class="menu_link_text">Content</span></a></li>
				<li ><a class="clearfix" href="#/myaccount"><span class="menu_link_text">App Behaviour</span></a></li>
				<li ><a class="clearfix" href="#/myaccount"><span class="menu_link_text">Rules</span></a></li>
				<li class="active last_li"><a class="clearfix" href="#/myaccount"><span class="menu_link_text">Calcs</span></a></li>
            </ul>
        </div>
		 -->
	<!-- ADDED CODE FOR SLIDING MENU -->
	<div class="containerMain">
		<div class="containerMenu ">
			<div class="TopSlideMenu main_nav_bar nav_bg_green">
				<ul class="main_nav_bar_list clearfix">
					<!-- <li class="activeList"><a href="javascript:void(0)" rel="#showTop">About us</a></li>
					<li class="activeList"><a href="javascript:void(0)" rel="#showTop1">Services</a></li>
					<li class="activeList"><a href="javascript:void(0)" rel="#showTop2">Products</a></li> -->

					<li class="activeList"><a class="clearfix"
						onclick="agentPage()"><span
							class="menu_link_text">Users</span></a></li>
					<li class="active activeList last_li"><a class="clearfix"
						onClick="" rel="#showContent"><span
							class="menu_link_text">Content</span></a></li>
					<li class="activeList last_li"><a class="clearfix"
						onClick="mdbLoader();" rel="#showContent"><span
							class="menu_link_text">Master Data</span></a></li>		
				</ul>
			</div>
			<div class="submenu_container slide-menu-top" id="showProducts">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li class="activated"><a href="#" title="menu">Application
									innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>
							<li><a href="#" title="menu">Business strategy</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>

			<div class="submenu_container slide-menu-top" id="showUsers">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>
							<li><a href="#" title="menu">Business strategy</a></li>

						</ul>
					</div>
				</div>
				<div class="submenu_column2">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>
							<li><a href="#" title="menu">Business strategy</a></li>

						</ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="submenu_container slide-menu-top" id="showContent">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li class="activated"><a href="#" title="menu">
									Add/Update Content </a></li>
						</ul>
					</div>
				</div>

				<div class="clear"></div>
			</div>
			<div class="submenu_container slide-menu-top" id="showApp">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>
			<div class="submenu_container slide-menu-top" id="showRules">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>
			<div class="submenu_container slide-menu-top" id="showCalcs">
				<div class="submenu_column1">
					<div class="submenu_link">
						<ul>
							<li><a href="#" title="menu">Application innovation</a></li>
							<li><a href="#" title="menu">Business analytics</a></li>

						</ul>
					</div>
				</div>

				<div class="clear"></div>

			</div>
		</div>
	<!-- ADDED CODE FOR SLIDING MENU -->
	<div class="bg_white">
		<ul class="breadcrumb breadcrumb_gradientbg">
			<li><a href="#" class="brd_link_sel">Content</a></li>
		</ul>
	</div>

	<!-- CONTENT CONTAINER -->
	<div class="content_container">
		<div class="action_btn_container clearfix">
		</div>
<div class="container_tlr_fluid">
	<div class="blankContainer">
		<div class="blankContainerInner">
			<%
				CommonAdminDAO contentAdminServiceDAO = new CommonAdminDAOImpl();
				List <ContentTypeDTO> contentTypeDTOs = contentAdminServiceDAO.retriveContentType();
				List <ContentLanguageDTO> contentLanguageDTOs = contentAdminServiceDAO.retriveLanguage();
			%>
			
			<div class="action_btn_container clearfix">
			<div class="left_filter_content">
				<label for="filter_content_sel" class="filter_content_label">Filter
					by Content Type</label> 
					<select name="Filter" id="Filter"
								onChange="filterCheck()">
								<option value="">-All-</option>
								<%
									for (ContentTypeDTO item1 : contentTypeDTOs) {
								         if((item1.getContentType()!=null)) {
								%>
								<option value="<%=item1.getId()%>"><%=item1.getContentType()%></option>
								<%}
                                                    	   }%>
					</select>
					
					
					
					
					
			</div>
			<div class="right_action_btns">
				<button class="action_btn" id="add_more_btn">Add More</button>
				<button disabled id="tag_untag_btn"
					class="action_btn disabled_btn" onclick="tagUntagContextFunction();clearMsg();">Edit</button>
				<button disabled id="rollback_btn"
					class="action_btn disabled_btn" onclick="rollbackFunction()">Rollback Version</button>
				<button disabled id="update_btn"
					class="action_btn disabled_btn" onclick="updateFunction()">Update Version</button>
				
			</div>
		</div>
			
			
			
			<div class="table_container">
			<table name=ruleSetTable width="100%" border="0" cellpadding="0" id="tbl_content_version_list" cellspacing="0" class="cust_tbl">
				<thead>
					<tr class="tbl_head_bg">
						<th>Serial No</th>
						<th>Content Type</th>
						<th>Content Name</th>
						<th>Application Contexts</th>
						<th>Language</th>
						<th>Version</th>
						<th class="last-column">Select</th>
					</tr>
				</thead>
				<tbody id="tblBody" cellpadding='5' border='0'
				width='100%' class="cust_tbl"></tbody>
			</table>
			</div>
		</div>

		

	</div>
</div>
		
		

	</div>
	<!-- CONTENT CONTAINER -->



	<div class="footer_container">
		<span class="copyright_label">&copy;Copyright 2013. LifeEngage
		</span>
	</div>

	<div id="page_loader"></div>

	<!-- THE MODAL ADD MORE POPUP -->
	<div class="modal_pop_up add_more_pop_up">
		<p class="modal_pop_up_header ">
			Add Content <span class="close_pop_up"></span>
		</p>
		  	<form action="contentUpload" name="uploadForm" method="post" enctype="multipart/form-data" id="uploadForm">
		<div class="pop_up_content_container clearfix">
			<div class="pop_up_row clearfix">
				<label class="pop_up_label pdg_top" for="filter_pop_up_content_sel">Content
					Type</label>
				<div class="pop_up_item">&nbsp;
					<select name="Filter" id="FilterInAdd">
						<option value="">-Select-</option>
						<%
						    for (ContentTypeDTO item1 : contentTypeDTOs) {
						        if (item1.getContentType() != null) {
						%>
						<option value="<%=item1.getId()%>"><%=item1.getContentType()%></option>
						<%
						    }
						    }
						%>
					</select>
				</div>
			</div>

			<div class="pop_up_row row_even clearfix">
				<label class="pop_up_label pdg_top">Browse a File to Upload</label>
				<div class="pop_up_item">
					<!-- CUSTOM UPLOAD BUTTON -->
					<div class="custom-upload">
						<input type="file" id="uploadfileName" name="file">
						<div class="upload_cont">
							<input disabled="disabled">
							<button class="browse_pop_up_btn" id="browse_btn">
								Browse</button>
						</div>
						
					</div>
					<!-- CUSTOM UPLOAD BUTTON -->
				</div>
			</div>
			
			<div class="pop_up_row clearfix">
				<label class="pop_up_label pdg_top" for="filter_pop_up_content_sel">Language</label>
				<div class="pop_up_item">&nbsp;
					<select name="Language" id="FilterInLanguage">
						
						<%
						    for (ContentLanguageDTO item1 : contentLanguageDTOs) {
						        if (item1.getLanguage() != null) {
						%>
						<option value="<%=item1.getId()%>"><%=item1.getLanguage()%></option>
						<%
						    }
						    }
						%>
					</select>
				</div>
			</div>
			
			<div class="pop_up_row row_even clearfix">
				<label class="pop_up_label pdg_top">Tag Application Contexts </label>
				<div class="pop_up_item">
					<!-- <table id="app_context_id"></table> -->
					<span class="pop_up_item_value" id="app_context_id" name="app_context_id"></span>
				</div>
			</div>
			
			<div class="pop_up_row clearfix" id="is_mandatory">
				<label class="pop_up_label pdg_top">Mandatory Content</label>
				<div class="pop_up_item">
					<!-- <table id="app_context_id"></table> -->
					<span class="pop_up_item_value"  name="is_mandatory">
						<table>
							<tr>
								<td><input type="checkbox" id="doc_req" name="doc_req" value="true" />  </td>
							</tr>
						</table>
					</span>
				</div>
			</div>
			<div class="pop_up_row row_even clearfix" id="description">
				<label class="pop_up_label pdg_top">Description<br/>(Max 250 characters)</label>
				<div class="pop_up_item">
					<!-- <table id="app_context_id"></table> -->
					<span class="pop_up_item_value"  name="description">
						<table>
							<tr>
								<td><textarea  id="desc_text" name="desc_text" rows="4" cols="25" maxlength="250"  /></textarea></td>
							</tr>
						</table>
					</span>
				</div>
			</div>
			<span class="clearfix upload_message" id="upload_message_span" style="width: 40%;"></span>
		</div>

		<div class="pop_up_main_action_btn add_more_pop_up_upload">
			<button class="pop_up_upload_btn" id="upload_pop_up_btn" onclick="uploadCheckAdd()">Upload</button>
		</div>
	</form>
	</div>
	<!-- THE MODAL ADD MORE POPUP -->

	<!-- THE MODAL UPDATE CONTENT POPUP -->
	<div class="modal_pop_up update_content_pop_up" id="UpdatePopUp">
		<p class="modal_pop_up_header">
			Update Version <span class="close_pop_up"></span>
		</p>
		<form action="update" name="updateForm" method="post"
		enctype="multipart/form-data">
		<div class="pop_up_content_container pdg_update_cont  clearfix">
			<div class="pop_up_row clearfix">
				<label class="pop_up_label ">Content Name </label>
				<div class="pop_up_item">
					<span class="pop_up_item_value" id="updateFileSp"></span>
				</div>
			</div>
			<div class="pop_up_row row_even clearfix">
				<label class="pop_up_label pdg_top">Browse a File to Upload</label>
				<div class="pop_up_item">
					<!-- CUSTOM UPLOAD BUTTON -->
					<div class="custom-upload">
						<input type="file" id="fileName" name="file">
						<div class="upload_cont">
							<input disabled="disabled">
							<button class="browse_pop_up_btn" id="browse_btn" >
								Browse</button>

						</div>
					</div>
					<!-- CUSTOM UPLOAD BUTTON -->
				</div>
			</div>
			<div class="pop_up_row clearfix">
				<label class="pop_up_label pdg_top">Comment<span class="mand_symbol">*</span></label>
				<div class="pop_up_item">
					<textarea id="update_comment_textarea" name="update_comment_textarea"
						cols="35" rows="3" maxlength="255" onchange="truncCommentLength(this)" onkeyup="truncCommentLength(this)"></textarea>
				</div>

			</div>
			<div class="pop_up_row row_even clearfix" id="up_description">
				<label class="pop_up_label pdg_top">Description<br/>(Max 250 characters)</label>
				<div class="pop_up_item">
					<!-- <table id="app_context_id"></table> -->
					<span class="pop_up_item_value"  name="description">
						<table>
							<tr>
								<td><textarea  id="up_desc_text" name="up_desc_text" rows="4" cols="35" maxlength="250"  /></textarea></td>
							</tr>
						</table>
					</span>
				</div>
			</div>
			<div>

			</div>
			<span class="clearfix upload_message" id="update_message_span"></span>
		</div>
		<div class="pop_up_main_action_btn">
			<button class="pop_up_upload_btn" id="update_pop_up_btn" >Upload</button>
		</div>
		</form>
	</div>
	<!-- THE MODAL UPDATE CONTENT POPUP -->

	<!-- THE MODAL ROLLBACK CONTENT POPUP -->
	<form action="rollback" name="rollbackForm" method="post" id="rollbackForm">
	<div class="modal_pop_up rollback_content_pop_up" id="rollback_content_pop_up">
		<p class="modal_pop_up_header">
			Rollback Version <span class="close_pop_up"></span>
		</p>
		<span class="clearfix rollback_message">Do you want to rollback
			'<span class="content_name_value_title" id="rollback_item_name"></span>'?
		</span>
		<div class="pop_up_content_container pdg_rollback_cont clearfix">
			<div class="pop_up_row row_even clearfix">
				<label class="pop_up_label">Content Name</label>
				<div class="pop_up_item">
					<span class="pop_up_item_value" id="rollback_item_value"></span>
					<input type="hidden" name="rollback_name" id="rollback_name" />
					<input type="hidden" name="rollback_version" id="rollback_version" />
				</div>
			</div>
			<div class="pop_up_row clearfix" id="rollbackVersionDiv">
				<label class="pop_up_label" id="rollback_label"> </label>
				<div class="pop_up_item">
					<span class="pop_up_item_value"  id="rollback_span">
					</span>
				</div>
			</div>

			<div class="pop_up_row row_even clearfix">
				<label class="pop_up_label pdg_top">Comment<span class="mand_symbol">*</span></label>
				<div class="pop_up_item">
					<textarea name="pop_up_comment_rollback"
						id="rollback_comment_textarea" cols="35" rows="3" maxlength="255"
						onchange="truncCommentLength(this)" onkeyup="truncCommentLength(this)"></textarea>
				</div>
			</div>
			<span class="clearfix rollback_returnMsg" id="rollbackmsg"></span>
			<span class="clearfix rollback_prvVrnMsg" id="prvVrnWithRollBackSucssMsg"></span>
			<div class="pop_up_main_action_btn">
				<button class="pop_up_upload_btn mrg_rollback" id="rollback_pop_up_btn">OK</button>
			</div>
			<div>&nbsp;</div>
		</div>
	</div>
	</form>
	<!-- THE MODAL ROLLBACK CONTENT POPUP -->

	<!-- THE MODAL CONTENT DETAILS POPUP -->
	<div class="modal_pop_up content_details_pop_up">
		<p class="modal_pop_up_header">
			Content Details <span class="close_pop_up"></span>
		</p>
		<div class="pop_up_content_container pdg_rollback_cont clearfix">
			<div class="pop_up_row clearfix">
				<label class="pop_up_label content_details_pop_label ">Content
					Name</label>
				<div class="pop_up_item">
					<span class="pop_up_item_value">Desert.jpg</span>
				</div>
			</div>
			<div class="pop_up_row row_even clearfix">
				<label class="pop_up_label content_details_pop_label">Content
					Type</label>
				<div class="pop_up_item">
					<span class="pop_up_item_value">Image</span>
				</div>
			</div>
			<div class="pop_up_row clearfix">
				<label class="pop_up_label content_details_pop_label">Content
					Version</label>
				<div class="pop_up_item">
					<span class="pop_up_item_value">2</span>
				</div>
			</div>
			<div class="pop_up_row row_even clearfix">
				<label class="pop_up_label content_details_pop_label">Added
					On </label>
				<div class="pop_up_item">
					<span class="pop_up_item_value">12/08/2013</span>
				</div>
			</div>
			<div class="pop_up_row clearfix">
				<label class="pop_up_label content_details_pop_label">Added
					By</label>
				<div class="pop_up_item">
					<span class="pop_up_item_value">Smith</span>
				</div>
			</div>
			<div class="pop_up_row row_even clearfix">
				<label class="pop_up_label content_details_pop_label">Last
					Updated On </label>
				<div class="pop_up_item">
					<span class="pop_up_item_value">14/08/2013</span>
				</div>
			</div>
			<div class="pop_up_row clearfix">
				<label class="pop_up_label content_details_pop_label">Last
					Updated By</label>
				<div class="pop_up_item">
					<span class="pop_up_item_value">Anderson</span>
				</div>
			</div>
			<div class="pop_up_row row_even clearfix">
				<label class="pop_up_label content_details_pop_label">Last
					Updated Comments </label>
				<div class="pop_up_item">
					<span class="pop_up_item_value comment_text">Lorem ipsum
						twseonfnjsdnfoais aisdasdnas asdhoiasndklsand asodhisaondksandpisa
						asojdpiosandksanid asjhdioasjdsa</span>
				</div>
			</div>
		</div>

	</div>
	<!-- THE MODAL CONTENT DETAILS POPUP -->
	
	<!-- THE MODAL TAG/UNTAG CONTEXT POPUP - Begin -->
	<form action="tagUntagContext" name="tagUntagForm" method="post" id="tagUntagForm">
	<div class="modal_pop_up tag_Untag_Context_pop_up" id="TagUntagContextPopUp">
		<p class="modal_pop_up_header">
			Edit Content<span class="close_pop_up"></span>
		</p>
		
		<div class="pop_up_content_container pdg_update_cont clearfix">
			<div class="pop_up_row clearfix">
				<label class="pop_up_label">Content Name</label>
			
				<div class="pop_up_item">&nbsp;
					<span class="pop_up_item_value" id="tagUntagContentFile" name="tagUntagContentFile"></span>
					<input type="hidden" name="tagUntagContentFileName" id="tagUntagContentFileName" />
					<input type="hidden" name="tagUntagSelectedType" id="tagUntagSelectedType" />
				</div>
			</div>
			<div class="pop_up_row row_even clearfix">
				<label class="pop_up_label pdg_top" for="filter_pop_up_content_sel">Language</label>
				<div class="pop_up_item">&nbsp;
					<select name="Language" id="tagUntagLanguage">
						
						<%
						    for (ContentLanguageDTO item1 : contentLanguageDTOs) {
						        if (item1.getLanguage() != null) {
						%>
						<option value="<%=item1.getId()%>"><%=item1.getLanguage()%></option>
						<%
						    }
						    }
						%>
					</select>
				</div>
			</div>
			<div class="pop_up_row clearfix">
				<label class="pop_up_label ">Application Context </label>
				<div class="pop_up_item">
					<!-- <table id="contextDataTable"></table> -->
					<span class="pop_up_item_value" id="contextDataTable" name="contextDataTable"></span>
					<input type="hidden" name="selectedAppContext" id="selectedAppContext" />
				</div>
			</div>
			
			<div class="pop_up_row row_even clearfix" id="is_mandatory_update">
				<label class="pop_up_label pdg_top">Mandatory Content</label>
				<div class="pop_up_item">
					<!-- <table id="app_context_id"></table> -->
					<span class="pop_up_item_value"  name="is_mandatory">
						<table>
							<tr>
								<td><input type="checkbox" id="doc_req_udt" name="doc_req_udt" value="true" />  </td>
							</tr>
						</table>
						<input type="hidden" id="doc_opt_udt_val" name="doc_opt_udt_val"/> 
						<input type="hidden" id="edt_slctd_lng" name="edt_slctd_lng"/> 
						<input type="hidden" id="is_disabled" name="is_disabled"/> 
					</span>
				</div>
			</div>
			<span class="clearfix upload_message" id="tagUntagMsg" style="width: 40%;"></span>
		</div>

		<div class="pop_up_main_action_btn">
			<button class="tagUntag_pop_up_btn" id="tagUntag_pop_up_btn">Save</button>
		</div>
		<div>&nbsp;</div>

	</div>
	</form>
	<!-- THE MODAL TAG/UNTAG CONTEXT POPUP - End -->
	
	
		<!-- THE MESSAGE POPUP -->
	<div class="modal_pop_up message_details_pop_up">
		<p class="modal_pop_up_header">
			Status <span class="close_pop_up"></span>
		</p>
			<div class="pop_up_row row_even clearfix">
				<label class="pop_up_label content_details_pop_label">Last
					Updated Comments </label>
				<div class="pop_up_item">
					<span class="pop_up_item_value comment_text" id="msg_popup"></span>
				</div>
			</div>
		<div class="pop_up_main_action_btn">
			<span class="mand_statement"></span>
			<button class="pop_up_upload_btn mrg_update" id="msg_pop_up_btn" >OK</button>
		</div>
	</div>
	<!-- THE MESSAGE POPUP end -->

	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/jquery.jtruncate.js"></script>
</body>
</html>
