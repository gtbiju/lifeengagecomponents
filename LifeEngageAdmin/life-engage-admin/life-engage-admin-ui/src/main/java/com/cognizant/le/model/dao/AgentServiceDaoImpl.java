package com.cognizant.le.model.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.cognizant.le.model.dto.Achievement;
import com.cognizant.le.model.dto.AchievementCompKey;
import com.cognizant.le.model.dto.AchievementDetails;
import com.cognizant.le.model.dto.AchievementDetailsDTO;
import com.cognizant.le.model.dto.Agent;
import com.cognizant.le.model.dto.AgentAchievmentDTO;
import com.cognizant.le.model.dto.AgentDTO;
import com.cognizant.le.model.dto.Login;
import com.cognizant.le.model.dto.User;
import com.cognizant.le.util.CommonUtils;
import com.cognizant.le.util.Constants;
import com.cognizant.le.util.HibernateUtil;
import com.cognizant.le.util.LoggerUtil;

public class AgentServiceDaoImpl implements AgentServiceDAO {
	static Logger logger = Logger.getLogger(AgentServiceDaoImpl.class);

	public boolean isValidAgent(String agentId) throws Exception {
		logger.debug(LoggerUtil.getLogMessage(
				"Method Entry : AgentServiceDaoImpl.isValidAgent:", agentId));
		boolean validAgent = false;
		Session session = null;
		List<User> userList = null;
		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query queryToFetchUser = session
					.createQuery("from Agent where agentCode = :agentId ");
			queryToFetchUser.setParameter("agentId", agentId);
			logger.debug("from Agent where agentCode =" + agentId);
			userList = queryToFetchUser.list();

			if (userList != null && userList.size() > 0) {
				validAgent = true;
			} else {
				validAgent = false;
			}

			session.getTransaction().commit();

		} catch (Exception e) {
			logger.error(
					LoggerUtil
							.getLogMessage(
									"Error while getting User Profile in AgentServiceDaoImpl for Agent:",
									agentId), e);
			throw new Exception(e);
		} finally {
			session.close();
		}
		logger.debug(LoggerUtil
				.getLogMessage(
						"Method Exit : AgentServiceDaoImpl.isValidAgent in AgentServiceWS for Agent:",
						agentId));
		return validAgent;
	}

	public boolean isValidUser(String userid, String password) throws Exception {
		boolean validUser = false;
		logger.debug("[METHOD ENTRY: AgentServiceDaoImpl.isValidUser"
				+ "for the user" + userid);
		Session session = HibernateUtil.getSession();
		// String
		// sqlQuery="select password from MLI_schema.agentapp_login where userid='"+userid+"' and password=hashbytes('"+md5+"','"+password+"')")";
		try {
			Query query = session.getNamedQuery("fetchLoginDtls");
			query.setParameter("userid", userid);
			String dbPassword = "";
			Login login = null;
			/*
			 * SQLQuery query = session
			 * 
			 * .createSQLQuery("select password from AGENT_APP_LOGIN where userid='"
			 * +userid+"'");
			 */
			List<Login> loginList = query.list();
			if (null != loginList && loginList.size() > 0) {
				login = (Login) query.list().get(0);
			}
			if (login != null) {
				dbPassword = login.getPassword();
			}
			String hashOfPw = CommonUtils.getMD5(password);
			if (dbPassword.equals(hashOfPw)) {
				validUser = true;
			} else {
				validUser = false;
			}

		} catch (Exception e) {
			logger.error(
					"Error while retrieving password  in AgentServiceDaoImpl.isValidUser",
					e);
			throw new Exception(e);
		} finally {
			session.close();
		}
		logger.debug("[METHOD : AgentServiceDaoImpl.isValidUser flagfor the user"
				+ validUser + "");
		logger.debug("[METHOD EXIT: AgentServiceDaoImpl.isValidUser"
				+ "for the user" + userid);

		return validUser;

	}



	public Set<AgentDTO> saveToDatabase(
			Set<AgentDTO> agents, List<AgentAchievmentDTO> achievements, Set<AchievementDetailsDTO> achievementDtls, String mode) throws Exception {
		
		logger.debug("METHOD ENTRY: AgentServiceDaoImpl.insertObservations in AgentApp");
		Session session = null;
		session = HibernateUtil.getSession();
		session.beginTransaction();
		
		Set<AchievementDetails> achListDb = getAchievementDtl(session);
		Set<AchievementDetails> achInTot = new HashSet<AchievementDetails>();
		
		if(achievementDtls!=null){
			for(AchievementDetailsDTO achievementDetailsDTO : achievementDtls){
				AchievementDetails achievementToSave = new AchievementDetails();
				for(AchievementDetails achievement : achListDb){
					if(achievementDetailsDTO.getTitle().equals(achievement.getTitle())){
						achievementToSave = achievement;
						break;
					}
				}
				achievementToSave.setImage(achievementDetailsDTO.getImage());
				achievementToSave.setDescription(achievementDetailsDTO.getDescription());
				achievementToSave.setTitle(achievementDetailsDTO.getTitle());
				achInTot.add(achievementToSave);
			}
		}
		
			if (agents != null) {
				boolean valid = true;
				for (AgentDTO agent : agents) {
					try {
						Agent agentDB = new Agent();
						if(mode.equals("REPLACE")){
							agentDB = deleteAgentMappings(session, agent.getAgentCode());
						}else if(mode.equals("BACKDATE")){
							backDateAgent(session, agent.getAgentCode());
						}
						//preserveOldAchDetails(session, agent.getAgentCode());
						List<Achievement> achievementsDList = new ArrayList<Achievement>();
						agentDB.setUserid(agent.getUserid());
						agentDB.setAgentCode(agent.getAgentCode());
						agentDB.setFullname(agent.getFullname());
						agentDB.setEmployeeType(agent.getEmployeeType());
						agentDB.setRole(agent.getRole());
						agentDB.setSupervisorCode(agent.getSupervisorCode());
						agentDB.setOffice(agent.getOffice());
						agentDB.setUnit(agent.getUnit());
						agentDB.setGroup(agent.getGroup());
						agentDB.setBriefWriteUp(agent.getBriefWriteUp());
						agentDB.setYearsOfExperience(agent
								.getYearsOfExperience());
						agentDB.setBusinessSourced(agent.getBusinessSourced());
						agentDB.setNumOfCustServiced(agent
								.getNumOfCustServiced());
						agentDB.setLicenseNumber(agent.getLicenseNumber());
						agentDB.setLicenseIssueDate(agent.getLicenseIssueDate());
						agentDB.setLicenseExpiryDate(agent
								.getLicenseExpiryDate());
						agentDB.setMobileNumber(agent.getMobileNumber());
						agentDB.setEmailId(agent.getEmailId());

						agentDB.setName_of_excelsheet(agent
								.getName_of_excelsheet());
						agentDB.setUploaded_timestamp(new Date());
						agentDB.setActive(true);
						agentDB.setChannelId(agent.getChannelId());
						session.saveOrUpdate(agentDB);
						
						for(AgentAchievmentDTO achievement : achievements){
							if(achievement.getAgentCode().equals(agent.getAgentCode())){
								Achievement achievementDB = new Achievement();
								// An agent can't have multiple achievements with same id, as of now. So UPDATE functionality is provided. In case multiple
								//achievements with same id can be tagged to the user, comment out this block
						/*		if(agentExists){
									List<Achievement> achieveList = agentDB.getAchievements();
									for(Achievement achmntDb : achieveList){
										if(achievement.getTitleRef().equals(achmntDb.getAchievementCode())){
											achievementDB = achmntDb;
										}
									}
								}*/
								AchievementDetails achievementDetails = new AchievementDetails();
								for(AchievementDetailsDTO achievementDetailsDTO : achievementDtls){
									//gets ui bean matching the selected title
									if(achievementDetailsDTO.getTitle().equals(achievement.getTitleRef())){
										for(AchievementDetails achDetail : achInTot){
											//gets bean from persistence if already exists in db.
											if(achDetail.getTitle().equals(achievementDetailsDTO.getTitle())){
												achDetail.setActive(true);
												achievementDetails = achDetail;
												break;
											}
										}
									}
								}
								achievementDB.setAgent(agentDB);
								achievementDB.setAchievementDetails(achievementDetails);
								achievementsDList.add(achievementDB);
							}
						}
						agentDB.setAchievements(achievementsDList);
						agent.setStatus(Constants.AGENT_STATUS_SUCCESS);
						agent.setMessage(Constants.AGENT_PROF_SUCCESS);
						logger.debug("[METHOD : AgentServiceDaoImpl.insertAgents  Insert/Update Successfully done for the agent"
								+ agent.getAgentCode() + "");

					}
					/*
					 * catch (ConstraintViolationException cve){ if
					 * (cve.getMessage
					 * ().contains("Cannot insert duplicate key")){
					 * agent.setStatus("D"); agent.setMessage(
					 * "Agent already exists in Agent table .Please check the excel"
					 * ); } }
					 */

					catch (Exception e) {
						session.getTransaction().rollback();
						agent.setStatus(Constants.AGENT_STATUS_FAILURE);
						agent.setMessage(e.getMessage());

						logger.debug("[METHOD : AgentServiceDaoImpl.insertObservations  Error  for the agent"
								+ agent.getAgentCode() + "");
						logger.error(
								"Error while inserting agents in AgentServiceDaoImpl",
								e);
						session.close();
						valid = false;
						break;

					}

				}
				if(valid){
				//deleteOrphanAchDetails(achInTot, session);
				session.flush();
				session.getTransaction().commit();
				session.close();
				}
			}else{
				logger.debug("Empty agents : AgentServiceManagerImpl.saveAgentProfile");
			}

		logger.debug(LoggerUtil
				.getLogMessage(
						"Method Exit : AgentServiceManagerImpl.saveAgentProfile to UI  for Agent:",
						agents.size() + ""));

		return agents;
	}

	@Override
	public List<AgentAchievmentDTO> insertAchievments(
			List<AgentAchievmentDTO> achievments) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<Agent> retrieveAllAgents() throws Exception {
		logger.debug(LoggerUtil.getLogMessage(
				"Method Entry : AgentServiceDaoImpl.retrieveAllAgents:"));
		Session session = null;
		List<Agent> agentList = null;
		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query queryToFetchAgents = session
					.createQuery("from Agent");
			logger.debug("from Agent ");
			agentList = queryToFetchAgents.list();
			session.getTransaction().commit();

		} catch (Exception e) {
			logger.error(
					LoggerUtil
							.getLogMessage(
									"Error while getting Agents in AgentServiceDaoImpl for Agent:",
									""), e);
			throw new Exception(e);
		} finally {
			session.close();
		}
		logger.debug(LoggerUtil
				.getLogMessage(
						"Method Exit : AgentServiceDaoImpl.retrieveAllAgents in AgentServiceWS for Agent:",
						""));
		return agentList;
	}
	
	
	public Boolean backDateAgent(Session session, String agentCode) throws SQLException{
		Boolean status =false;
		if(null!=agentCode && !agentCode.equals("")){
			Query query = session.createQuery("update Agent set invalidateDate = :invalidDate , active =:active " +
					" where agentCode = :agentCode and active = :oldactive");
			query.setParameter("invalidDate", new Date());
			query.setParameter("active", false);
			query.setParameter("agentCode", agentCode);
			query.setParameter("oldactive", true);
			int result = query.executeUpdate();
			if (result > 0) {
			    logger.info("Agent was removed");
			    status = true;
			}
		}else{
			logger.error("Error : Null or empty AgentCode. Updation Failed.");
		}
		return status;
	}
	
	
	public Agent deleteAgentMappings(Session session, String agentCode) throws SQLException{
		Boolean status = false;
		List<Agent> agentList = null;
		Agent agent = new Agent();
		if(null!=agentCode && !agentCode.equals("")){
			Query query = session.createQuery("from Agent where agentCode = :agentCode and active = :active");
			query.setParameter("agentCode", agentCode);
			query.setParameter("active", true);
			agentList = query.list();
			for(Agent agentIt: agentList){
				agent = agentIt;
				for(Achievement achievement: agent.getAchievements()){
					achievement.setAchievementDetails(null);
				}
				agent.setAchievements(new ArrayList<Achievement>());
			}
		}else{
			logger.error("Error : Null or empty AgentCode. Deletion Failed.");
		}
		return agent;
	}
		
	
	public Set<AchievementDetails> getAchievementDtl(Session session) throws SQLException{
		Boolean status =false;
		List<AchievementDetails> achList = null;
		Query query = session.createQuery("from AchievementDetails");
		achList = query.list();
		return new HashSet<AchievementDetails>(achList);
	}

	
	public void deleteOrphanAchDetails(Set<AchievementDetails> achList, Session session){
		for(AchievementDetails achDetails : achList){
			if(!achDetails.getActive()){
				session.delete(achDetails);
				logger.info("Achievement Detail with title "+achDetails.getTitle()+ " is removed since it is no longer referenced.");
			}
		}
		session.flush();
	}
	
	public String getChannelID(String channel){
		List<String> channelIDList = null;
		String id = null;
		Session session = HibernateUtil.getSession();
		session.beginTransaction();
		Query lookupIdQuery = session
				.createSQLQuery("SELECT ID FROM CORE_PROD_CODE_TYPE_LOOK_UP WHERE DESCRIPTION = :description");
		lookupIdQuery.setParameter("description", channel);
		channelIDList = lookupIdQuery.list();
		if(null!=channelIDList && channelIDList.size() > 0){
			id = channelIDList.get(0);
		}
		session.getTransaction().commit();
		session.close();
		return id;
		
	}
}
