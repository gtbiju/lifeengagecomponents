package com.cognizant.le.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import com.cognizant.le.mdu.exception.ValidationException;
import com.cognizant.le.model.dao.LookupDAO;
import com.cognizant.le.model.dto.CodeLocaleLookup;
import com.cognizant.le.model.dto.CodeLookup;
import com.cognizant.le.model.dto.CodeRelation;
import com.cognizant.le.model.dto.CodeTypeLookup;

/**
 * The Class MasterDataValidator.
 */
@Component
public class MasterDataValidator {

	/** The acceptable types. */
	List<Integer> acceptableTypes = new ArrayList<Integer>();

	/** The acceptable carriers. */
	List<Integer> acceptableCarriers = new ArrayList<Integer>();

	/** The acceptable codes. */
	List<Integer> acceptableCodes = new ArrayList<Integer>();
	
	/** The lookup dao. */
	@Autowired
	private LookupDAO lookupDAO;
	
	/** The environment. */
	@Autowired
	private Environment environment;
	
	private String schemaName;

	/**
	 * Validate lookups.
	 * 
	 * @param codeTypeLookups
	 *            the code type lookups
	 * @param codeLookups
	 *            the code lookups
	 * @return the boolean
	 */
	public Boolean validateLookups(List<CodeTypeLookup> codeTypeLookups,
			List<CodeLookup> codeLookups) {

		for (CodeTypeLookup codeTypeLookup : codeTypeLookups) {
			acceptableTypes.add(codeTypeLookup.getId());
		}

		for (CodeLookup codeLookup : codeLookups) {
			if (!acceptableTypes.contains(codeLookup.getTypeId())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Validate relations.
	 *
	 * @param codeLookups the code lookups
	 * @param codeRelations the code relations
	 * @param codeTypeLookups the code type lookups
	 * @return the boolean
	 */
	public Boolean validateRelations(List<CodeLookup> codeLookups,
			List<CodeRelation> codeRelations,
			List<CodeTypeLookup> codeTypeLookups) {
		for (CodeLookup codeLookup : codeLookups) {
			acceptableCodes.add(codeLookup.getId());
		}
		for (CodeRelation codeRelation : codeRelations) {
			if (!acceptableCodes.contains(codeRelation.getParentCode())
					|| !acceptableCodes.contains(codeRelation.getChildCode())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Validate locales.
	 *
	 * @param codeLookups the code lookups
	 * @param codeLocaleLookups the code locale lookups
	 * @return the boolean
	 */
	public Boolean validateLocales(List<CodeLookup> codeLookups,
			List<CodeLocaleLookup> codeLocaleLookups) {
		for(CodeLocaleLookup codeLocaleLookup : codeLocaleLookups){
			if(!acceptableCodes.contains(codeLocaleLookup.getCodeId())){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Check for tables.
	 *
	 * @return the boolean
	 * @throws ValidationException 
	 * @throws IOException 
	 */
	public void syncTables() throws DataAccessException, ValidationException, IOException{
		List<String> tableList = new ArrayList<String>();
		tableList.add("CODE_TYPE_LOOKUP");
		tableList.add("CODE_LOOKUP");
		tableList.add("CODE_LOCALE_LOOKUP");
		tableList.add("CODE_RELATION");
		
		String dbType = environment.getProperty("le.mdu.app.jdbc.dbtype");
		dbType =dbType.toUpperCase();
		if(dbType!=null && (dbType.equalsIgnoreCase("MYSQL") || (dbType.equalsIgnoreCase("MSSQL")))){
			for(String table : tableList){
				String schemaName = environment.getProperty("le.mdu.app.jdbc.schema");
				Boolean tblExists = lookupDAO.checkTable(schemaName, table);
				if(!tblExists){
					InputStream in = this.getClass().getClassLoader().getResourceAsStream(table+"_"+dbType+".sql");
					if(dbType.equalsIgnoreCase("MSSQL")){
						String theString = IOUtils.toString(in);
						theString= theString.replaceAll("~~~",schemaName);
						in = IOUtils.toInputStream(theString, "UTF-8");
					}
					lookupDAO.createTable(new InputStreamReader(in));
				}
			}
			
		}else{
			throw new ValidationException("Invalid DB Type!");
		}
	}


}
