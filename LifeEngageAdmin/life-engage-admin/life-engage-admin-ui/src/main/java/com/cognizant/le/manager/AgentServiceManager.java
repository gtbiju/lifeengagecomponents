package com.cognizant.le.manager;

import java.util.List;
import java.util.Set;

import com.cognizant.le.model.dto.AchievementDetailsDTO;
import com.cognizant.le.model.dto.AgentAchievmentDTO;
import com.cognizant.le.model.dto.AgentDTO;
import com.cognizant.le.model.dto.AgentProfile;

public interface AgentServiceManager {

	
	
	public boolean isValidUser(String userid,String password ) throws Exception;
    
	public Set<AgentDTO> saveToAgentDB(Set<AgentDTO> agents, List<AgentAchievmentDTO> achievments,  Set<AchievementDetailsDTO> achievementDtls, String opMode) throws Exception;
	
	public List<AgentAchievmentDTO> saveToAchievmentsDB(List<AgentAchievmentDTO> achievments) throws Exception;
	
	public AgentProfile saveAgentProfile(AgentProfile agentProfile, String opMode) throws Exception;

	public String getChannelIDForName(String name);

}
