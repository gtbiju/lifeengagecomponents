package com.cognizant.le.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.cognizant.le.manager.AgentServiceManager;
import com.cognizant.le.manager.AgentServiceManagerImpl;
import com.cognizant.le.model.dao.ContentAdminDAO;
import com.cognizant.le.model.dao.ContentAdminDAOImpl;


/**
 * Servlet implementation class LoginAction
 */
@WebServlet("/LoginAction")
public class LoginAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = Logger.getLogger(LoginAction.class);
	
	private ContentAdminDAO contentAdminServiceDAO = new ContentAdminDAOImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 *//*
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}*/

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.debug("Method Entry : LoginAction.doPost");
		boolean validUser = false;
		boolean  isNotLogin =false;
		Properties properties = new Properties();
		String propertiesFile = getServletContext().getInitParameter("CONFIG_PROPERTIES");
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream stream = classLoader.getResourceAsStream("/" + propertiesFile);
		if(null == stream) {
			stream = classLoader.getResourceAsStream(propertiesFile);
		}
		properties.load(stream);
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		AgentServiceManager service = new AgentServiceManagerImpl();
	    HttpSession session = request.getSession(true);
	    
	    String defaultLanguage = properties.getProperty("DEFAULT_LANGUAGE");
	    
		if((null==defaultLanguage) || "".equals(defaultLanguage.trim())) {
			LOGGER.error("Property DEFAULT_LANGUAGE is not defined");
		} 
		session.setAttribute("DEFAULT_LANGUAGE", defaultLanguage);

	    try {
			//request.getSession().setAttribute("Login", "failure");
			validUser = service.isValidUser(username, password);
			if (validUser) {
				request.getSession().setAttribute("Login", "success");
				session.setAttribute("username", username);
				session.setAttribute("extensions", contentAdminServiceDAO.getContentTypes());
				session.setAttribute("appContexts", contentAdminServiceDAO.getApplicationContexts());
				session.setAttribute("contentPreference", contentAdminServiceDAO.getContentPreference());
				request.getRequestDispatcher("jsp/ContentList.jsp").forward(request, response);
				
			} else {
				
				isNotLogin = true;
				request.setAttribute("isNotLogin", isNotLogin);
				request.setAttribute("requestMsg", "Username/Password is incorrect!");
		        request.getRequestDispatcher("/index.jsp").forward(request, response);
		       
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.debug("Method Exit : LoginAction.doPost");		
	}
}
