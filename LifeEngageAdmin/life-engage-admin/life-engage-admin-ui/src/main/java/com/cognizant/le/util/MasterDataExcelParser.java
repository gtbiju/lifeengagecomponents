package com.cognizant.le.util;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import com.cognizant.le.mdu.exception.ValidationException;
import com.cognizant.le.model.dao.LookupDAO;
import com.cognizant.le.model.dto.Carrier;
import com.cognizant.le.model.dto.CodeLocaleLookup;
import com.cognizant.le.model.dto.CodeLookup;
import com.cognizant.le.model.dto.CodeRelation;
import com.cognizant.le.model.dto.CodeTypeLookup;
import com.cognizant.le.model.dto.ResponseBean;

/**
 * The Class MasterDataExcelParser.
 */
@Component
public class MasterDataExcelParser extends ExcelParser {

	@Autowired
	LookupDAO lookupDAO;

	@Autowired
	MasterDataValidator masterDataValidator;

	/**
	 * Parses the excel file.
	 * 
	 * @param file
	 *            the file
	 * @throws Exception
	 *             the exception
	 */
	public List<ResponseBean> parseExcelFile(File file) throws Exception {
		List<ResponseBean> responseBeans = new ArrayList<ResponseBean>();
		FileInputStream fileInputStream = new FileInputStream(file);
		XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
		try {
			masterDataValidator.syncTables();
		} catch (DataAccessException da) {
			throw new ValidationException(
					"Could not connect to database. Please contact administrator");
		}
		lookupDAO.deleteExistingEntries();
		/*
		 * List<Carrier> carriers = parseCarriers(workbook); if (carriers.size()
		 * > 0) { //lookupDAO.insertCarriers(carriers); ResponseBean
		 * responseBean = new ResponseBean();
		 * responseBean.setDataCount(carriers.size());
		 * responseBean.setTableName("CORE_CARRIER");
		 * responseBean.setMasterDbEntries(carriers);
		 * responseBeans.add(responseBean); }else{ throw new
		 * ValidationException(
		 * "No carriers recognized. Please fix the excel sheet "); }
		 */
		List<CodeTypeLookup> codeTypeLookups = parseCodeTypeLookups(workbook);
		if (codeTypeLookups.size() > 0) {
			lookupDAO.insertTypes(codeTypeLookups);
			ResponseBean responseBean = new ResponseBean();
			responseBean.setDataCount(codeTypeLookups.size());
			responseBean.setTableName("CODE_TYPE_LOOKUP");
			responseBean.setMasterDbEntries(codeTypeLookups);
			responseBeans.add(responseBean);
		} else {
			throw new ValidationException(
					"No code-types recognized. Please fix the excel sheet");
		}

		List<CodeLookup> codeLookups = parseCodeLookups(workbook);
		if (!masterDataValidator.validateLookups(codeTypeLookups, codeLookups)) {
			throw new ValidationException(
					"Orphan code-lookups present. Please fix the excel sheet");
		}
		if (codeLookups.size() > 0) {
			lookupDAO.insertLookUp(codeLookups);
			ResponseBean responseBean = new ResponseBean();
			responseBean.setDataCount(codeLookups.size());
			responseBean.setTableName("CODE_LOOKUP");
			responseBean.setMasterDbEntries(codeLookups);
			responseBeans.add(responseBean);
		} else {
			throw new ValidationException(
					"No code-lookups recognized. Please fix the excel sheet");
		}
		List<CodeRelation> codeRelations = parseCodeRelations(workbook);
		if (!masterDataValidator.validateRelations(codeLookups, codeRelations,
				codeTypeLookups)) {
			throw new ValidationException(
					"Orphan code-relations present. Please fix the excel sheet");
		}
		if (codeRelations.size() > 0) {
			lookupDAO.insertRelations(codeRelations);
			ResponseBean responseBean = new ResponseBean();
			responseBean.setDataCount(codeRelations.size());
			responseBean.setTableName("CODE_RELATION");
			responseBean.setMasterDbEntries(codeRelations);
			responseBeans.add(responseBean);
		} else {
			throw new ValidationException(
					"No code-relations recognized. Please fix the excel sheet");
		}
		List<CodeLocaleLookup> codeLocaleLookups = parseCodeLocaleLookups(workbook);
		if (!masterDataValidator
				.validateLocales(codeLookups, codeLocaleLookups)) {
			throw new ValidationException(
					"Orphan code-locales present. Please fix the excel sheet");
		}
		if (codeLocaleLookups.size() > 0) {
			lookupDAO.insertLocale(codeLocaleLookups);
			ResponseBean responseBean = new ResponseBean();
			responseBean.setDataCount(codeLocaleLookups.size());
			responseBean.setTableName("CODE_LOCALE_LOOKUP");
			responseBean.setMasterDbEntries(codeLocaleLookups);
			responseBeans.add(responseBean);
		} else {
			throw new ValidationException(
					"No code-locales recognized. Please fix the excel sheet");
		}
		fileInputStream.close();
		return responseBeans;
	}

	/**
	 * Parses the carriers.
	 * 
	 * @param workbook
	 *            the workbook
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	public List<Carrier> parseCarriers(final XSSFWorkbook workbook)
			throws Exception {
		XSSFSheet worksheet = workbook.getSheet(Constants.CARRIER_XL);
		List<Carrier> carriers = new ArrayList<Carrier>();
		if (worksheet != null) {
			Iterator<Row> rowsIterator = worksheet.rowIterator();
			rowsIterator.next();
			rowsIterator.next();
			while (rowsIterator.hasNext()) {
				XSSFRow xssfRow = (XSSFRow) rowsIterator.next();
				if (!isRowEmpty(xssfRow)) {
					Iterator<Cell> cellsIterator = xssfRow.cellIterator();
					Carrier carrier = new Carrier();
					carrier.setCarrierId(returnNumber(cellsIterator.next()));
					carrier.setCarrierName(returnString(cellsIterator.next()));
					carriers.add(carrier);
				}
			}
		}
		return carriers;

	}

	/**
	 * Parses the code locale lookups.
	 * 
	 * @param workbook
	 *            the workbook
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	public List<CodeLocaleLookup> parseCodeLocaleLookups(
			final XSSFWorkbook workbook) throws Exception {
		XSSFSheet worksheet = workbook.getSheet(Constants.LOCALE_XL);
		List<CodeLocaleLookup> codeLocaleLookups = new ArrayList<CodeLocaleLookup>();
		if (worksheet != null) {
			Iterator<Row> rowsIterator = worksheet.rowIterator();
			rowsIterator.next();
			rowsIterator.next();
			while (rowsIterator.hasNext()) {
				XSSFRow xssfRow = (XSSFRow) rowsIterator.next();
				if (!isRowEmpty(xssfRow)) {
					Iterator<Cell> cellsIterator = xssfRow.cellIterator();
					cellsIterator.next();
					CodeLocaleLookup codeLocaleLookup = new CodeLocaleLookup();
					codeLocaleLookup.setCodeId(returnNumber(cellsIterator
							.next()));
					codeLocaleLookup.setLanguage(returnString(cellsIterator
							.next()));
					String country = returnString(cellsIterator.next());
					codeLocaleLookup.setCountry(country);
					String value = returnString(cellsIterator.next());
					codeLocaleLookup.setValue(value);
					codeLocaleLookups.add(codeLocaleLookup);
				}
			}
		}
		return codeLocaleLookups;
	}

	/**
	 * Parses the code lookups.
	 * 
	 * @param workbook
	 *            the workbook
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	public List<CodeLookup> parseCodeLookups(final XSSFWorkbook workbook)
			throws Exception {
		XSSFSheet worksheet = workbook.getSheet(Constants.LOOKUP_XL);
		List<CodeLookup> codeLookups = new ArrayList<CodeLookup>();
		if (worksheet != null) {
			Iterator<Row> rowsIterator = worksheet.rowIterator();
			rowsIterator.next();
			rowsIterator.next();
			while (rowsIterator.hasNext()) {
				XSSFRow xssfRow = (XSSFRow) rowsIterator.next();
				if (!isRowEmpty(xssfRow)) {
					Iterator<Cell> cellsIterator = xssfRow.cellIterator();
					CodeLookup codeLookup = new CodeLookup();
					codeLookup.setId(returnNumber(cellsIterator.next()));
					codeLookup.setTypeId(returnNumber(cellsIterator.next()));
					codeLookup.setCode(returnString(cellsIterator.next()));
					codeLookup
							.setDescription(returnString(cellsIterator.next()));
					String isDefault = returnString(cellsIterator.next());
					if (null != isDefault) {
						codeLookup
								.setIsDefault(isDefault.trim().equals("1") ? true
										: false);
					}
					codeLookups.add(codeLookup);
				}
			}
		}
		return codeLookups;

	}

	/**
	 * Parses the code relations.
	 * 
	 * @param workbook
	 *            the workbook
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	public List<CodeRelation> parseCodeRelations(final XSSFWorkbook workbook)
			throws Exception {
		XSSFSheet worksheet = workbook.getSheet(Constants.RELATION_XL);
		List<CodeRelation> codeRelations = new ArrayList<CodeRelation>();
		if (worksheet != null) {
			Iterator<Row> rowsIterator = worksheet.rowIterator();
			rowsIterator.next();
			rowsIterator.next();
			while (rowsIterator.hasNext()) {
				XSSFRow xssfRow = (XSSFRow) rowsIterator.next();
				if (!isRowEmpty(xssfRow)) {
					Iterator<Cell> cellsIterator = xssfRow.cellIterator();
					CodeRelation codeRelation = new CodeRelation();
					codeRelation.setChildCode(returnNumber(cellsIterator
							.next()));
					codeRelation
							.setParentCode(returnNumber(cellsIterator.next()));
					codeRelation
							.setCodeType(returnNumber(cellsIterator.next()));
					codeRelations.add(codeRelation);
				}
			}
		}
		return codeRelations;
	}

	/**
	 * Parses the code type lookups.
	 * 
	 * @param workbook
	 *            the workbook
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	public List<CodeTypeLookup> parseCodeTypeLookups(final XSSFWorkbook workbook)
			throws Exception {
		XSSFSheet worksheet = workbook.getSheet(Constants.CODE_TYPE_XL);
		List<CodeTypeLookup> codeTypeLookups = new ArrayList<CodeTypeLookup>();
		if (worksheet != null) {
			Iterator<Row> rowsIterator = worksheet.rowIterator();
			rowsIterator.next();
			rowsIterator.next();
			while (rowsIterator.hasNext()) {
				XSSFRow xssfRow = (XSSFRow) rowsIterator.next();
				if (!isRowEmpty(xssfRow)) {
					Iterator<Cell> cellsIterator = xssfRow.cellIterator();
					CodeTypeLookup codeTypeLookup = new CodeTypeLookup();
					codeTypeLookup.setId(returnNumber(cellsIterator.next()));
					codeTypeLookup.setCarrier_id(returnNumber(cellsIterator
							.next()));
					codeTypeLookup.setTypeName(returnString(cellsIterator
							.next()));
					codeTypeLookups.add(codeTypeLookup);
				}
			}
		}
		return codeTypeLookups;

	}

}
