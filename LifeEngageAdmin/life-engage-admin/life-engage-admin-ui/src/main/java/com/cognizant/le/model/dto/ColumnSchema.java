/**
 *
 * � Copyright 2012, Cognizant
 *
 * @author        : 287304
 * @version       : 0.1, May 16, 2014
 */
package com.cognizant.le.model.dto;

// TODO: Auto-generated Javadoc
/**
 * The Class ColumnSchema.
 *
 * @param <T> the generic type
 */
public class ColumnSchema {

	/** The column name. */
	private String columnName;
	
	/** The column type. */
	private String columnType;
	
	/** The length. */
	private int length;
	
	/** The is nullable. */
	public boolean isNullable;
	
	/** The default value. */
	public String defaultValue;
	
	/** The is identity. */
	public Boolean isIdentity;
	
	/** The is case sensitive. */
	public Boolean isCaseSensitive;
	

	/**
	 * Gets the column name.
	 *
	 * @return the column name
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * Sets the column name.
	 *
	 * @param columnName the new column name
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * Gets the column type.
	 *
	 * @return the column type
	 */
	public String getColumnType() {
		return columnType;
	}

	/**
	 * Sets the column type.
	 *
	 * @param columnType the new column type
	 */
	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	/**
	 * Gets the length.
	 *
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * Sets the length.
	 *
	 * @param length the new length
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * Checks if is nullable.
	 *
	 * @return true, if is nullable
	 */
	public boolean isNullable() {
		return isNullable;
	}

	/**
	 * Sets the nullable.
	 *
	 * @param isNullable the new nullable
	 */
	public void setNullable(boolean isNullable) {
		this.isNullable = isNullable;
	}

	/**
	 * Gets the default value.
	 *
	 * @return the default value
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Sets the default value.
	 *
	 * @param defaultValue the new default value
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * Gets the checks if is identity.
	 *
	 * @return the checks if is identity
	 */
	public Boolean getIsIdentity() {
		return isIdentity;
	}

	/**
	 * Sets the checks if is identity.
	 *
	 * @param isIdentity the new checks if is identity
	 */
	public void setIsIdentity(Boolean isIdentity) {
		this.isIdentity = isIdentity;
	}

	/**
	 * Gets the checks if is case sensitive.
	 *
	 * @return the checks if is case sensitive
	 */
	public Boolean getIsCaseSensitive() {
		return isCaseSensitive;
	}

	/**
	 * Sets the checks if is case sensitive.
	 *
	 * @param isCaseSensitive the new checks if is case sensitive
	 */
	public void setIsCaseSensitive(Boolean isCaseSensitive) {
		this.isCaseSensitive = isCaseSensitive;
	}

}
