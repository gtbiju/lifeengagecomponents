package com.cognizant.le.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

// TODO: Auto-generated Javadoc
/**
 * Servlet Filter implementation class RequestFilter.
 */
public class RequestFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public RequestFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Destroy.
	 * 
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		HttpServletResponse hsr = (HttpServletResponse) res;
		HttpServletRequest hreq = (HttpServletRequest) req;
		hsr.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP
																				// 1.1.
		hsr.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		hsr.setDateHeader("Expires", 0); // Proxies.
		HttpSession session = hreq.getSession(false);
		String path = hreq.getServletPath();
		if (!excludeFromFilter(path)) {
			if (session != null && !session.isNew() && session.getAttribute("username")!=null) {
				chain.doFilter(req, res);
			} else {
				req.getRequestDispatcher("/index.jsp").forward(hreq, hsr);
			}
		}else{
			chain.doFilter(req, res);
		}
		// don't create if it doesn't exist
		// if(session != null && !session.isNew()) {
		//chain.doFilter(req, res);
		/*
		 * }else{ req.getRequestDispatcher("/index.jsp").forward(hreq, hsr); }
		 */
	}

	/**
	 * Inits the.
	 * 
	 * @param fConfig
	 *            the f config
	 * @throws ServletException
	 *             the servlet exception
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

	private boolean excludeFromFilter(String path) {
		if (path.startsWith("/rest") || path.contains("downloadContent") || path.contains("/img") || path.contains("/Login") || path.endsWith(".css") || path.endsWith(".js"))
			return true; // add more page to exclude here
		else
			return false;
	}

}
