package com.cognizant.le.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.cognizant.le.model.dao.ContentAdminDAO;
import com.cognizant.le.model.dao.ContentAdminDAOImpl;
import com.cognizant.le.model.dto.ContentFileDTO;
import com.cognizant.le.model.dto.FileInfo;
import com.cognizant.le.util.LoggerUtil;
import com.google.gson.Gson;

/**
 * Servlet implementation class
 */
@WebServlet("/ContentListServlet")
public class ContentListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = Logger.getLogger(ContentListServlet.class);
	private String contentFolder="";
	private String serverURL="";
	private String defaultLanguage="";


	static Logger logger = Logger.getLogger(ContentListServlet.class);
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ContentListServlet() {
		super();
	}
	List<ContentFileDTO> contentInfoList;
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		LOGGER.debug("Method Entry : ContentListServlet.doPost");
		try {
			String option = request.getParameter("option");
			ContentAdminDAO contentAdminServiceDAO = new ContentAdminDAOImpl();
			
			String propertyFileName =  getServletContext().getInitParameter("CONFIG_PROPERTIES");
			
			Properties properties = new Properties();
			try {
				ClassLoader classLoader = Thread.currentThread()
						.getContextClassLoader();
				InputStream stream = classLoader.getResourceAsStream("/"
						+ propertyFileName);
				if (null == stream) {
					stream = classLoader.getResourceAsStream(propertyFileName);
				}
				properties.load(stream);
			} catch (Exception e) {
				logger.debug(LoggerUtil.getLogMessage(e.getMessage()));
			}
			
			String serverURL = properties.getProperty("SERVER_URL");
			if ((null == serverURL) || "".equals(serverURL.trim())) {
				logger.error("Property SERVER_URL is not defined");
			}
			
			FileInfo fileInfo = new FileInfo();
			fileInfo.setServerUrl(serverURL);
			fileInfo.setContextPath(getServletContext().getContextPath());
			
			if (option == null || option.equals("")) {
				contentInfoList = contentAdminServiceDAO.retriveContent(fileInfo);
			} else {
				contentInfoList = contentAdminServiceDAO.retriveContent(option,fileInfo);
			}
			String json = new Gson().toJson(contentInfoList);
			response.setContentType("application/json");
			response.getWriter().write(json);
			LOGGER.debug("Method Exit : ContentListServlet.doPost");
		} catch (Exception e) {
			LOGGER.error("Error in ContentListServlet.doPost", e);
			throw new ServletException(e);
		}
	}
	
	@Override
	public void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{
		request.getRequestDispatcher("jsp/ContentList.jsp").forward(request, response);
	}
}
