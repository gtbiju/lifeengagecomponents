/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 287304
 * @version       : 0.1, May 16, 2014
 */
package com.cognizant.le.util;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.cognizant.le.enums.DType;
import com.cognizant.le.model.dto.ColumnSchema;
import com.cognizant.le.model.dto.ForeignKeySchema;
import com.cognizant.le.model.dto.IndexColumn;
import com.cognizant.le.model.dto.IndexSchema;
import com.cognizant.le.model.dto.TableSchema;


/**
 * The Class MSSQL2SQLite.
 */
public class MSSQLConverter {

	/** The pkey. */
	static Boolean pkey = false;

	/**
	 * Gets the db type of column.
	 * 
	 * @param cs
	 *            the cs
	 * @return the d type
	 */
	private static DType GetDbTypeOfColumn(ColumnSchema cs) {
		if (cs.getColumnType().equals("tinyint"))
			return DType.Byte;
		if (cs.getColumnType().equals("int"))
			return DType.Int32;
		if (cs.getColumnType().equals("smallint"))
			return DType.Int16;
		if (cs.getColumnType().equals("bigint"))
			return DType.Int64;
		if (cs.getColumnType().equals("bit"))
			return DType.Binary;
		if (cs.getColumnType().equals("nvarchar")
				|| cs.getColumnType().equals("varchar")
				|| cs.getColumnType().equals("text")
				|| cs.getColumnType().equals("ntext"))
			return DType.String;
		if (cs.getColumnType().equals("float"))
			return DType.Double;
		if (cs.getColumnType().equals("real"))
			return DType.Single;
		if (cs.getColumnType().equals("blob"))
			return DType.Binary;
		if (cs.getColumnType().equals("numeric"))
			return DType.Double;
		if (cs.getColumnType().equals("timestamp")
				|| cs.getColumnType().equals("datetime"))
			return DType.DateTime;
		if (cs.getColumnType().equals("date"))
			return DType.Date;
		if (cs.getColumnType().equals("datetime2"))
			return DType.DateTime2;
		if (cs.getColumnType().equals("nchar")
				|| cs.getColumnType().equals("char"))
			return DType.String;
		if (cs.getColumnType().equals("uniqueidentifier")
				|| cs.getColumnType().equals("guid"))
			return DType.GUID;
		if (cs.getColumnType().equals("xml"))
			return DType.String;
		if (cs.getColumnType().equals("sql_variant"))
			return DType.Object;
		if (cs.getColumnType().equals("integer"))
			return DType.Int64;
		if (cs.getColumnType().equals("time"))
			return DType.Time;
		return null;

	}

	/**
	 * Cast value for column.
	 * 
	 * @param val
	 *            the val
	 * @param columnSchema
	 *            the column schema
	 * @param listOfValuesInRow
	 *            the list of values in row
	 */
	private static void castValueForColumn(Object val,
			ColumnSchema columnSchema, List<Object> listOfValuesInRow) {
		if (val == null)
			listOfValuesInRow.add(val);
		else {
			DType dt = GetDbTypeOfColumn(columnSchema);

			switch (dt) {
			case Int32:
				listOfValuesInRow.add((Integer) val);
				break;
			case Int16:
				if (val instanceof Short) {
					listOfValuesInRow.add((Short) val);
				} else {
					listOfValuesInRow.add(val);
				}
				break;
			case Int64:
				if (val instanceof Long) {
					listOfValuesInRow.add((Long) val);
				} else {
					listOfValuesInRow.add(val);
				}
				break;
			case Single:
			case Double:
				if (val instanceof Double) {
					listOfValuesInRow.add((Double) val);
				} else if (val instanceof BigDecimal) {
					listOfValuesInRow.add((BigDecimal) val);
				} else {
					listOfValuesInRow.add(val);
				}
				break;
			case String:
				listOfValuesInRow.add("\"" + val.toString() + "\"");
				break;
			case Time:
			case DateTime:
			case Date:
			case DateTime2:
				listOfValuesInRow.add("\"" + val + "\"");
				break;
			case Binary:
			case Boolean:
				if(val instanceof Boolean){
					Boolean value = (Boolean)val;
					if(value.equals(true)){
						listOfValuesInRow.add(1);
					}else{
						listOfValuesInRow.add(0);
					}
				}
				break;

			default:
			} // switch
		}
	}

	/**
	 * Builds the column statement.
	 * 
	 * @param col
	 *            the col
	 * @param ts
	 *            the ts
	 * @return the string
	 */
	private static String buildColumnStatement(ColumnSchema col, TableSchema ts) {
		StringBuilder sb = new StringBuilder();
		sb.append("\t\"" + col.getColumnName() + "\"\t\t");
		boolean isAdjustedDataType = false;
		// Special treatment for IDENTITY columns
		if (col.getIsIdentity()) {
			if (null != ts.getPrimaryKey()
					&& ts.getPrimaryKey().size() == 1
					&& (col.getColumnType().equals("tinyint")
							|| col.getColumnType().equals("int")
							|| col.getColumnType().equals("smallint")
							|| col.getColumnType().equals("bigint") || col
							.getColumnType().equals("integer"))) {
				sb.append("integer PRIMARY KEY AUTOINCREMENT");
				pkey = true;
			} else
				sb.append("integer");
		} else {
			if (null != col.getColumnType()
					&& col.getColumnType().equals("int"))
				sb.append("integer");
			else if(null != col.getColumnType()
					&& (col.getColumnType().equals("numeric")||col.getColumnType().equals("varchar"))){
				sb.append("text");
				isAdjustedDataType = true;
			}
			else {
				sb.append(col.getColumnType());
			}
			if (col.getLength() > 0 && !isAdjustedDataType)
				sb.append("(" + col.getLength() + ")");
		}
		if (!col.isNullable())
			sb.append(" NOT NULL");

		if (null != col.getIsCaseSensitive() && col.getIsCaseSensitive())
			sb.append(" COLLATE NOCASE");

		String defval = stripParanthesis(col.getDefaultValue());
		defval = discardNational(defval);
		System.out.println("DEFAULT VALUE BEFORE [" + col.getDefaultValue()
				+ "] AFTER [" + defval + "]");
		if (null != defval && defval != StringUtils.EMPTY
				&& defval.toUpperCase().contains("GETDATE")) {
			System.out
					.println("converted SQL Server GETDATE() to CURRENT_TIMESTAMP for column ["
							+ col.getColumnName() + "]");
			sb.append(" DEFAULT (CURRENT_TIMESTAMP)");
		} else if (null != defval && defval != StringUtils.EMPTY
				&& !defval.equalsIgnoreCase("NULL") && !isAdjustedDataType)
			sb.append(" DEFAULT " + defval);

		return sb.toString();
	}

	/**
	 * Strip paranthesis.
	 * 
	 * @param value
	 *            the value
	 * @return the string
	 */
	private static String stripParanthesis(String value) {
		if (null != value) {
			String pattern = "\\(([^\\)]*)\\)";
			Pattern r = Pattern.compile(pattern);
			System.out.println(value);
			Matcher m = r.matcher(value);
			if (m.find()) {
				return (m.group(1));
			}
		}
		return value;
	}

	/**
	 * Adjust default value.
	 * 
	 * @param value
	 *            the value
	 * @return the string
	 */
	private static String adjustDefaultValue(String value) {
		if (null != value) {
			String pattern = "\\(N(\\'.*\\')\\)";
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(value);
			if (m.find()) {
				return (m.group(1));
			}
		}
		return value;
	}

	/**
	 * Discard national.
	 * 
	 * @param value
	 *            the value
	 * @return the string
	 */
	private static String discardNational(String value) {
		if (null != value) {
			String pattern = "N\'([^\']*)\'";
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(value);
			if (m.find()) {
				return (m.group(1));
			}
		}
		return value;
	}

	/**
	 * Creates the table schema.
	 * 
	 * @param conn
	 *            the conn
	 * @param tableName
	 *            the table name
	 * @param tschma
	 *            the tschma
	 * @return the table schema
	 * @throws Exception
	 *             the exception
	 */
	public static TableSchema createTableSchema(Connection conn,
			String tableName, String tschma) throws Exception {

		TableSchema res = new TableSchema();
		res.setTableName(tableName);
		res.setTableSchemaName(tschma);
		List<ColumnSchema> columnSchemas = new ArrayList<ColumnSchema>();
		String sql = "SELECT COLUMN_NAME,COLUMN_DEFAULT,IS_NULLABLE,DATA_TYPE,"
				+ " (columnproperty(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity')) AS [IDENT], "
				+ "CHARACTER_MAXIMUM_LENGTH AS CSIZE "
				+ "FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '"
				+ tableName + "' AND TABLE_SCHEMA='"+tschma+"' ORDER BY " + "ORDINAL_POSITION ASC";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			String columnName = rs.getString("COLUMN_NAME");
			String columnDefault = rs.getString("COLUMN_DEFAULT");
			Boolean isNullable = rs.getString("IS_NULLABLE").equalsIgnoreCase(
					"YES") ? true : false;
			String dataType = rs.getString("DATA_TYPE");
			Boolean isIdentity = rs.getInt("IDENT") == 1 ? true : false;
			int length = rs.getInt("CSIZE");
			if (length == -1) {

			}

			// Note that not all data type names need to be converted because
			// SQLite establishes type affinity by searching certain Strings
			// in the type name. For example - everything containing the String
			// 'int' in its type name will be assigned an INTEGER affinity
			if (dataType.equals("timestamp"))
				dataType = "blob";
			else if (dataType.equals("date"))
				dataType = "text";
			else if (dataType.equals("datetime")
					|| dataType.equals("smalldatetime")
					|| dataType.equals("datetime2"))
				dataType = "text";
			else if (dataType.equals("time"))
				dataType = "text";
			else if (dataType.equals("decimal"))
				dataType = "text";
			else if (dataType.equals("money") || dataType.equals("smallmoney"))
				dataType = "text";
			else if (dataType.equals("binary") || dataType.equals("varbinary")
					|| dataType.equals("image"))
				dataType = "blob";
			else if (dataType.equals("tinyint"))
				dataType = "smallint";
			else if (dataType.equals("bigint"))
				dataType = "integer";
			else if (dataType.equals("sql_variant"))
				dataType = "blob";
			else if (dataType.equals("xml"))
				dataType = "varchar";
			else if (dataType.equals("uniqueidentifier"))
				dataType = "guid";
			else if (dataType.equals("ntext"))
				dataType = "text";
			else if (dataType.equals("nchar"))
				dataType = "char";

			if (dataType.equals("bit") || dataType.equals("int")) {
				if (columnDefault == "('False')")
					columnDefault = "(0)";
				else if (columnDefault == "('True')")
					columnDefault = "(1)";
			}
			columnDefault = fixDefaultValueString(columnDefault);

			ColumnSchema col = new ColumnSchema();
			col.setColumnName(columnName.toUpperCase());
			col.setColumnType(dataType);
			col.setLength(length);
			col.setNullable(isNullable);
			col.setIsIdentity(isIdentity);
			col.setDefaultValue(adjustDefaultValue(columnDefault));
			columnSchemas.add(col);
		}
		res.setColumns(columnSchemas);
		rs.close();
		ps.close();

		// Find PRIMARY KEY information
		String sql2 = new String("EXEC sp_pkeys '"+tableName + "'");
		List<String> primaryKeys = new ArrayList<String>();
		PreparedStatement ps2 = conn.prepareStatement(sql2);
		ResultSet rsPkey = null;
		try{
			rsPkey = ps2.executeQuery();
			while (rsPkey.next()) {
				String colName = rsPkey.getString("COLUMN_NAME");
				primaryKeys.add(colName);
				rsPkey.close();
				ps2.close();
				res.setPrimaryKey(primaryKeys);
			}
		}catch(Exception e){
			System.out.println("EXEC sp_pkeys failed");
		}
		if (!tableName.contains("EXTN") && !tableName.contains("extn")) {
			String sqlIndexes = "exec sp_helpindex '"+tableName + "'";
			PreparedStatement psIndexes = conn.prepareStatement(sqlIndexes);
			try{
			ResultSet rsIndexes = psIndexes.executeQuery();
			List<IndexSchema> indexSchemas = new ArrayList<IndexSchema>();
			while (rsIndexes.next()) {
				String indexName = rsIndexes.getString("index_name");
				String desc = rsIndexes.getString("index_description");
				String keys = rsIndexes.getString("index_keys");

				if (desc.contains("primary key"))
					continue;
				IndexSchema index = buildIndexSchema(indexName, desc, keys);
				indexSchemas.add(index);
			}

			res.setIndexes(indexSchemas);
			}catch(Exception e){
				System.out.println("EXEC sp_helpindex failed");
			}
		}
		return res;
	}

	/**
	 * Builds the index schema.
	 * 
	 * @param indexName
	 *            the index name
	 * @param desc
	 *            the desc
	 * @param keys
	 *            the keys
	 * @return the index schema
	 * @throws Exception
	 *             the exception
	 */
	private static IndexSchema buildIndexSchema(String indexName, String desc,
			String keys) throws Exception {

		IndexSchema res = new IndexSchema();
		res.setIndexName(indexName);
		// Determine if this is a unique index or not.
		String[] descParts = desc.split(",");
		for (String p : descParts) {
			if (p.trim().contains("unique")) {
				res.setIsUnique(true);
				break;
			}
		} // foreach
		String[] keysParts = keys.split(",");
		List<IndexColumn> indexColumns = new ArrayList<IndexColumn>();
		for (String p : keysParts) {
			String pattern = "([a-zA-Z_0-9]+)(\\(\\-\\))?";
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(p);
			if (!m.find()) {
				throw new Exception("invalid index name " + p);
			}
			String key = m.group(1);
			IndexColumn ic = new IndexColumn();
			ic.setColumnName(key);
			indexColumns.add(ic);
		}
		res.setColumns(indexColumns);
		// Get all key names and check if they are ASCENDING or DESCENDING

		return res;
	}

	/**
	 * Gets the db connection.
	 * 
	 * @return the db connection
	 */
	public static Connection getDbConnection() {
		Connection conn = null;
		try {
			String completeUrl = "jdbc:jtds:sqlserver://PC263738:1433/generali5;instance=SQLEXPRESS";

			Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(completeUrl, "sa", "admin123$");
			System.out.println("Connected to the database");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	/**
	 * Fix default value string.
	 * 
	 * @param colDefault
	 *            the col default
	 * @return the string
	 */
	private static String fixDefaultValueString(String colDefault) {
		if (null != colDefault) {
			Boolean replaced = false;
			String resString = colDefault.trim();
			char[] res = resString.toCharArray();
			// Find first/last indexes in which to search
			int first = -1;
			int last = -1;
			for (int i = 0; i < res.length; i++) {
				if (res[i] == '\'' && first == -1)
					first = i;
				if (res[i] == '\'' && first != -1 && i > last)
					last = i;
			} // for

			if (first != -1 && last > first)
				return resString.substring(first, last - first + 1);

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < res.length; i++) {
				if (res[i] != '(' && res[i] != ')') {
					sb.append(res[i]);
					replaced = true;
				}
			}
			if (sb.toString().equalsIgnoreCase("NULL")) {
				return null;
			}
			if (replaced)
				return "(" + sb.toString() + ")";
			else
				return sb.toString();
		}
		return colDefault;
	}

	/**
	 * Creates the foreign key schema.
	 * 
	 * @param ts
	 *            the ts
	 * @param conn
	 *            the conn
	 * @throws SQLException
	 *             the sQL exception
	 */
	public static void createForeignKeySchema(TableSchema ts, Connection conn)
			throws SQLException {
		List<ForeignKeySchema> foreignKeySchemas = new ArrayList<ForeignKeySchema>();
		String sql = "SELECT  ColumnName = CU.COLUMN_NAME,   ForeignTableName  = PK.TABLE_NAME,   ForeignColumnName = PT.COLUMN_NAME, DeleteRule = C.DELETE_RULE, IsNullable = COL.IS_NULLABLE FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS C INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS FK ON C.CONSTRAINT_NAME = FK.CONSTRAINT_NAME INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS PK ON C.UNIQUE_CONSTRAINT_NAME = PK.CONSTRAINT_NAME INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU ON C.CONSTRAINT_NAME = CU.CONSTRAINT_NAME INNER JOIN   (  SELECT i1.TABLE_NAME, i2.COLUMN_NAME  FROM  INFORMATION_SCHEMA.TABLE_CONSTRAINTS i1 INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE i2 ON i1.CONSTRAINT_NAME = i2.CONSTRAINT_NAME  WHERE i1.CONSTRAINT_TYPE = 'PRIMARY KEY'   ) PT ON PT.TABLE_NAME = PK.TABLE_NAME INNER JOIN INFORMATION_SCHEMA.COLUMNS AS COL ON CU.COLUMN_NAME = COL.COLUMN_NAME AND FK.TABLE_NAME = COL.TABLE_NAME WHERE FK.Table_NAME='"
				+ ts.getTableName() + "'";
		PreparedStatement psFk = conn.prepareStatement(sql);
		ResultSet rsFk = psFk.executeQuery();
		while (rsFk.next()) {

			ForeignKeySchema fkc = new ForeignKeySchema();
			fkc.setColumnName(rsFk.getString("ColumnName"));
			fkc.setForeignColumnName(rsFk.getString("ForeignColumnName"));
			fkc.setForeignTableName(rsFk.getString("ForeignTableName"));
			fkc.setCascadeOnDelete(rsFk.getString("DeleteRule").equals(
					"CASCADE"));
			fkc.setIsNullable(rsFk.getString("IsNullable").equals("YES"));
			fkc.setTableName(ts.getTableName());
			foreignKeySchemas.add(fkc);

		}
		ts.setForeignKeys(foreignKeySchemas);
	}

	/**
	 * Builds the create table query.
	 * 
	 * @param ts
	 *            the ts
	 * @return the string
	 */
	public static String buildCreateTableQuery(TableSchema ts) {
		StringBuilder sb = new StringBuilder();

		sb.append("CREATE TABLE [" + ts.getTableName() + "] (\n");

		for (int i = 0; i < ts.getColumns().size(); i++) {
			ColumnSchema col = ts.getColumns().get(i);
			String cline = buildColumnStatement(col, ts);
			sb.append(cline);
			if (i < ts.getColumns().size() - 1)
				sb.append(",\n");
		} // foreach

		// add primary keys...
		if (ts.getPrimaryKey() != null && ts.getPrimaryKey().size() > 0 & !pkey) {
			sb.append(",\n");
			sb.append("    PRIMARY KEY (");
			for (int i = 0; i < ts.getPrimaryKey().size(); i++) {
				sb.append("[" + ts.getPrimaryKey().get(i) + "]");
				if (i < ts.getPrimaryKey().size() - 1)
					sb.append(", ");
			} // for
			sb.append(")\n");
		} else
			sb.append("\n");

		// add foreign keys...
		if (null != ts.getForeignKeys() && ts.getForeignKeys().size() > 0) {
			sb.append(",\n");
			for (int i = 0; i < ts.getForeignKeys().size(); i++) {
				ForeignKeySchema foreignKey = ts.getForeignKeys().get(i);
				String stmt = "    FOREIGN KEY ([" + foreignKey.getColumnName()
						+ "])\n        REFERENCES ["
						+ foreignKey.getForeignTableName() + "](["
						+ foreignKey.getForeignColumnName() + "])";

				sb.append(stmt);
				if (i < ts.getForeignKeys().size() - 1)
					sb.append(",\n");
			} // for
		}

		sb.append("\n");
		sb.append(");\n");

		// Create any relevant indexes
		if (ts.getIndexes() != null) {
			for (int i = 0; i < ts.getIndexes().size(); i++) {
				String stmt = buildCreateIndex(ts.getTableName(), ts
						.getIndexes().get(i));
				sb.append(stmt + ";\n");
			} // for
		} // if

		String query = sb.toString();
		System.out.println(query);
		return query;
	}

	/**
	 * Builds the create index.
	 * 
	 * @param tableName
	 *            the table name
	 * @param indexSchema
	 *            the index schema
	 * @return the string
	 */
	private static String buildCreateIndex(String tableName,
			IndexSchema indexSchema) {
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE ");
		if (null != indexSchema.getIsUnique() && indexSchema.getIsUnique())
			sb.append("UNIQUE ");
		sb.append("INDEX [" + tableName + "_" + indexSchema.getIndexName()
				+ "]\n");
		sb.append("ON [" + tableName + "]\n");
		sb.append("(");
		for (int i = 0; i < indexSchema.getColumns().size(); i++) {
			sb.append("[" + indexSchema.getColumns().get(i).getColumnName()
					+ "]");
			if (null != indexSchema.getColumns().get(i).getIsAscending()
					&& !indexSchema.getColumns().get(i).getIsAscending())
				sb.append(" DESC");
			if (i < indexSchema.getColumns().size() - 1)
				sb.append(", ");
		} // for
		sb.append(")");

		return sb.toString();
	}

	/**
	 * Builds the sql server table query.
	 * 
	 * @param ts
	 *            the ts
	 * @param prodIds
	 *            the prod ids
	 * @param conn
	 *            the conn
	 * @param fromExtn
	 *            the from extn
	 * @return the prepared statement
	 * @throws SQLException
	 *             the sQL exception
	 */
	private static PreparedStatement buildSqlServerTableQuery(TableSchema ts,
			Connection conn)
			throws SQLException {
		StringBuilder sql = new StringBuilder();
		PreparedStatement ps = null;
		String schemaName = ts.getTableSchemaName() != null ? ts
				.getTableSchemaName() + "." : "";
		sql = new StringBuilder("SELECT * FROM " + schemaName
				+ ts.getTableName());
		ps = conn.prepareStatement(sql.toString());
		return ps;
	}

	/**
	 * Copy sql server rows to sq lite db.
	 * 
	 * @param schema
	 *            the schema
	 * @param conn
	 *            the conn
	 * @param prodIds
	 *            the prod ids
	 * @param fromExtn
	 *            the from extn
	 * @return the string
	 * @throws SQLException
	 *             the sQL exception
	 */
	public static String copySqlServerRowsToSQLiteDB(TableSchema schema,
			Connection conn)
			throws SQLException {
		PreparedStatement ps = buildSqlServerTableQuery(schema,  conn);
 		StringBuilder insertSql = new StringBuilder();
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		List<List<Object>> listOfRows = new ArrayList<List<Object>>();
		while (rs.next()) {
			List<Object> listOfValuesInRow = new ArrayList<Object>();
			for (int i = 1; i <= columnsNumber; i++) {
				castValueForColumn(rs.getObject(i),
						schema.getColumns().get(i - 1), listOfValuesInRow);
			}
			listOfRows.add(listOfValuesInRow);
		}
		for (List<Object> listOfRowVals : listOfRows) {
			insertSql.append(buildSQLiteInsert(schema, listOfRowVals));
			insertSql.append("\n");
		}
		listOfRows.size();
		return insertSql.toString();
	}

	/**
	 * Builds the sq lite insert.
	 * 
	 * @param ts
	 *            the ts
	 * @param listOfRowVals
	 *            the list of row vals
	 * @return the string
	 */
	private static String buildSQLiteInsert(TableSchema ts,
			List<Object> listOfRowVals) {

		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO [" + ts.getTableName() + "] (");
		for (int i = 0; i < ts.getColumns().size(); i++) {
			sb.append("[" + ts.getColumns().get(i).getColumnName() + "]");
			if (i < ts.getColumns().size() - 1)
				sb.append(", ");
		} // for
		sb.append(") VALUES (");

		for (int i = 0; i < ts.getColumns().size(); i++) {
			if (listOfRowVals.get(i) instanceof String) {
				sb.append((String) listOfRowVals.get(i));
			} else {
				sb.append(listOfRowVals.get(i));
			}
			if (i < ts.getColumns().size() - 1)
				sb.append(", ");

		}
		sb.append(");");
		System.out.println(sb);
		return sb.toString();
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws Exception
	 *             the exception
	 */
	public static void main(String[] args) throws Exception {
		Connection conn = getDbConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try{
		String sql = "SELECT * FROM dbo.CORE_PRODUCT_COMPONENT";
		 ps =  conn.prepareStatement(sql.toString());
		 rs = ps.executeQuery();
		while(rs.next()){
			for(int i =1 ;i<25; ){
			System.out.println(rs.getObject(i++));
			}
		}
		
		}finally{
		rs.close();
		ps.close();
		conn.close();
		}
		//TableSchema ts = createTableSchema(conn, "EXTN_FM_CHARGE_TBL", "");
		//createForeignKeySchema(ts, conn);
		//buildCreateTableQuery(ts);
		//copySqlServerRowsToSQLiteDB(ts, conn, null, true);
	}
}
