CREATE TABLE `CODE_LOCALE_LOOKUP` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `basicDataCompleteCode` int(11) DEFAULT NULL,
  `contextId` int(11) DEFAULT NULL,
  `creationDateTime` date DEFAULT NULL,
  `transactionId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lookUpCode_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA3A3342DAD7F8980` (`lookUpCode_id`),
  CONSTRAINT `FKA3A3342DAD7F8980` FOREIGN KEY (`lookUpCode_id`) REFERENCES `CODE_LOOKUP` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
