CREATE TABLE [~~~].[CODE_LOCALE_LOOKUP] (
  [id] BIGINT NOT NULL IDENTITY,
  [basicDataCompleteCode] INTEGER DEFAULT NULL,
  [contextId] INTEGER DEFAULT NULL,
  [creationDateTime] DATE DEFAULT NULL,
  [transactionId] varchar(255) DEFAULT NULL,
  [typeName] varchar(255) DEFAULT NULL,
  [Country] varchar(255) DEFAULT NULL,
  [Language] varchar(255) DEFAULT NULL,
  [Value] varchar(255) DEFAULT NULL,
  [lookUpCode_id] BIGINT DEFAULT NULL FOREIGN KEY REFERENCES [~~~].[CODE_LOOKUP] ([id]) ,
  PRIMARY KEY ([id])
  );