CREATE TABLE `CODE_TYPE_LOOKUP` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `basicDataCompleteCode` int(11) DEFAULT NULL,
  `contextId` int(11) DEFAULT NULL,
  `creationDateTime` date DEFAULT NULL,
  `transactionId` varchar(255) DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `carrier_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2C4EE6CD7A2D17FF` (`carrier_id`),
  CONSTRAINT `FK2C4EE6CD7A2D17FF` FOREIGN KEY (`carrier_id`) REFERENCES `CORE_CARRIER` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;