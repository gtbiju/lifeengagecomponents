CREATE TABLE `CODE_LOOKUP` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `basicDataCompleteCode` int(11) DEFAULT NULL,
  `contextId` int(11) DEFAULT NULL,
  `creationDateTime` date DEFAULT NULL,
  `transactionId` varchar(255) DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `Code` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `codeType_id` bigint(20) DEFAULT NULL,
  `is_default` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKE401E00CCF8C239A` (`codeType_id`),
  CONSTRAINT `FKE401E00CCF8C239A` FOREIGN KEY (`codeType_id`) REFERENCES `CODE_TYPE_LOOKUP` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
