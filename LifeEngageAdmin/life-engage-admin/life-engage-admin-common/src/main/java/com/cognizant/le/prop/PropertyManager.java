package com.cognizant.le.prop;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import org.apache.log4j.Logger;
import com.cognizant.le.util.LoggerUtil;

public class PropertyManager {
	static Logger logger = Logger.getLogger(PropertyManager.class);  
	/**
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public Properties loadProperties(String path) throws Exception {
		logger.debug(LoggerUtil.getLogMessage("Method Entry: PropertyManager.loadProperties() in LifeEngageAdminWS"));
		Properties prop = new Properties();
		File f1 = new File(System.getProperty("catalina.base"), path);
		prop.load(new FileInputStream(f1));
		logger.debug(LoggerUtil.getLogMessage("Method Exit: PropertyManager.loadProperties() in LifeEngageAdminWS"));
		return prop;
	}
}
