package com.cognizant.le.model.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


// TODO: Auto-generated Javadoc
/**
 * The Class ContentFileDTO.
 */
public class ContentFileDTO {
	
	/** The id. */
	private Integer id;
	
	/** The content type. */
	private ContentTypeDTO contentType;
	
	/** The file name. */
	private String fileName;
	
	/** The version. */
	private Integer version;
	
	/** The last updated. */
	private Date lastUpdated;
	
	/** The public url. */
	private String publicURL;
	
	/** The app context. */
	private List<AppContext> appContexts;
	
	/** The app context. */
	private String appContext;
	
	/** The comments. */
	private String comments;
	
	/** The content language. */
	private ContentLanguageDTO contentLanguage;
	
	/** The is required. */
	private Boolean isMandatory;
	
	/** The download url. */
	private String downloadUrl;
	
	/** The file size. */
	private Long fileSize;
	
	/** The file desc. */
	private String fileDesc;
	
	/**
	 * Instantiates a new content file dto.
	 */
	public ContentFileDTO() {
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Gets the content type.
	 *
	 * @return the contentType
	 */
	public ContentTypeDTO getContentType() {
		return contentType;
	}

	/**
	 * Sets the content type.
	 *
	 * @param contentType the contentType to set
	 */
	public void setContentType(ContentTypeDTO contentType) {
		this.contentType = contentType;
	}

	/**
	 * Gets the file name.
	 *
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public Integer getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version the version to set
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

	/**
	 * Gets the last updated.
	 *
	 * @return the lastUpdated
	 */
	public Date getLastUpdated() {
		return lastUpdated;
	}

	/**
	 * Sets the last updated.
	 *
	 * @param lastUpdated the lastUpdated to set
	 */
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	/**
	 * Gets the public url.
	 *
	 * @return the publicURL
	 */
	public String getPublicURL() {
		return publicURL;
	}

	/**
	 * Sets the public url.
	 *
	 * @param publicURL the publicURL to set
	 */
	public void setPublicURL(String publicURL) {
		this.publicURL = publicURL;
	}
	

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the app contexts.
	 *
	 * @return the app contexts
	 */
	public List<AppContext> getAppContexts() {
		return appContexts;
	}

	/**
	 * Sets the app contexts.
	 *
	 * @param appContexts the new app contexts
	 */
	public void setAppContexts(List<AppContext> appContexts) {
		this.appContexts = appContexts;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * Gets the app context.
	 *
	 * @param appContext the app context
	 * @return the app context
	 */
	public String getAppContext(String appContext) {
		return appContext;
	}
	
	/**
	 * Sets the app context.
	 *
	 * @param appContexts the new app context
	 */
	public void setAppContext(List<AppContext> appContexts) {
		String appContextString="";
		for(AppContext appContext : appContexts){
			appContextString+=appContext.getContext()+",";
		}
		if( appContextString != null && appContextString.length() > 0){
			appContextString = appContextString.substring(0, appContextString.length()-1);
		}
		this.appContext = appContextString;
	}
	/**
	 * Gets the content language.
	 * 
	 * @return the content language
	 */
	public ContentLanguageDTO getContentLanguage() {
		return contentLanguage;
	}

	/**
	 * Sets the content language.
	 * 
	 * @param contentLanguage
	 *            the new content language
	 */
	public void setContentLanguage(ContentLanguageDTO contentLanguage) {
		this.contentLanguage = contentLanguage;
	}

	/**
	 * Gets the checks if is required.
	 *
	 * @return the checks if is required
	 */
	public Boolean getIsRequired() {
		return isMandatory;
	}

	/**
	 * Sets the checks if is required.
	 *
	 * @param isRequired the new checks if is required
	 */
	public void setIsRequired(Boolean isRequired) {
		this.isMandatory = isRequired;
	}

	/**
	 * Gets the download url.
	 *
	 * @return the download url
	 */
	public String getDownloadUrl() {
		return downloadUrl;
	}

	/**
	 * Sets the download url.
	 *
	 * @param downloadUrl the new download url
	 */
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	/**
	 * Gets the file size.
	 *
	 * @return the file size
	 */
	public Long getFileSize() {
		return fileSize;
	}

	/**
	 * Sets the file size.
	 *
	 * @param fileSize the new file size
	 */
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * Gets the file desc.
	 *
	 * @return the file desc
	 */
	public String getFileDesc() {
		return fileDesc;
	}

	/**
	 * Sets the file desc.
	 *
	 * @param fileDesc the new file desc
	 */
	public void setFileDesc(String fileDesc) {
		this.fileDesc = fileDesc;
	}
}
