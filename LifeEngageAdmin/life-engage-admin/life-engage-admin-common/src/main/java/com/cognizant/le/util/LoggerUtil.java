package com.cognizant.le.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * The Class LoggerUtil.
 */
public class LoggerUtil {
	private static Logger logger = Logger.getLogger(LoggerUtil.class);
	 private static String versionNumber;
	/**
	 * Gets the log message.
	 * 
	 * @param message
	 *            the message
	 * @param contextOwner
	 *            the context owner
	 * @return the log message
	 */
	
	public static String getLogMessage(String message, String contextOwner) {
		if(versionNumber == null){
			loadProperty();
		}
		return message + contextOwner + ", Version : "+versionNumber;

	}
	
	public static String getLogMessage(String message) {
		if(versionNumber == null){
			loadProperty();
		}
		return message +", Version : "+versionNumber;

	}
	
	private static void loadProperty() {
		/*Properties properties = new Properties();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream stream = loader.getResourceAsStream("/version.properties");
		if (stream == null) {
			stream = loader.getResourceAsStream("version.properties");
		}
		try {
			properties.load(stream);
		} catch (IOException e) {
			logger.error("Error while loading property file to read version",e);
		}
		versionNumber = properties.getProperty(Constants.VERSION);*/
		versionNumber = "2";
	}
	
}
