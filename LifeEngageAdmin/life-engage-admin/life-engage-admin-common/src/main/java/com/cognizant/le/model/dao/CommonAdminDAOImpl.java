package com.cognizant.le.model.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.cognizant.le.model.dto.ContentLanguage;
import com.cognizant.le.model.dto.ContentLanguageDTO;
import com.cognizant.le.model.dto.ContentType;
import com.cognizant.le.model.dto.ContentTypeDTO;
import com.cognizant.le.util.HibernateUtil;

public class CommonAdminDAOImpl implements CommonAdminDAO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8033001517429517173L;

	/**
	 * This method is used to Retrive Content from data base.
	 * 
	 */

	@Override
	public List<ContentLanguageDTO> retriveLanguage() throws Exception {
		Session session = null;
		List<ContentLanguageDTO> contentLanguageList = new ArrayList<ContentLanguageDTO>();
		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query query = session.getNamedQuery("fetchLanguages");
			// Query query = session.createQuery("FROM ContentType");
			List<ContentLanguage> contentLangEntityList = query.list();
			for (ContentLanguage contentLanguage : contentLangEntityList) {
				ContentLanguageDTO contentLanguageDTO = new ContentLanguageDTO();
				contentLanguageDTO.setId(contentLanguage.getId());
				contentLanguageDTO.setLanguage(contentLanguage.getLanguage());
				contentLanguageList.add(contentLanguageDTO);
			}
		} catch (Exception e) {
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return contentLanguageList;
	}

	/**
	 * This method is used to Retrive content type from DB.
	 * 
	 */
	@Override
	public List<ContentTypeDTO> retriveContentType() throws Exception {
		Session session = null;
		List<ContentTypeDTO> contentTypeList = new ArrayList<ContentTypeDTO>();
		try {
			session = HibernateUtil.getSession();
			session.beginTransaction();
			Query query = session.createQuery("FROM ContentType");
			List<ContentType> contentTypeEntityList = query.list();
			for (ContentType contentType : contentTypeEntityList) {
				ContentTypeDTO contentTypeDTO = new ContentTypeDTO();
				contentTypeDTO.setId(contentType.getId());
				contentTypeDTO.setContentType(contentType.getType());
				contentTypeList.add(contentTypeDTO);
			}
		} catch (Exception e) {
			session.getTransaction().rollback();
			throw e;
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return contentTypeList;
	}
}
