package com.cognizant.le.model.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The Class ContentLanguage.
 */
@NamedQueries({
	@NamedQuery(name = "fetchLanguages", query = "from ContentLanguage")
})
@Entity
@Table(name = "CONTENT_LANGUAGES")
public class ContentLanguage {
	
	/** The id. */
	@Id
	@Column(name="id")
	private int id;
	
	/** The language. */
	@Column(name="name")
	private String language;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the language.
	 *
	 * @param language the new language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	
	

}
