package com.cognizant.le.util;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.stat.Statistics;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;

public class HibernateUtil {
	static Logger logger = Logger.getLogger(HibernateUtil.class);
	public static ServiceRegistry serviceRegistry;

	private static final SessionFactory sessionFactory;

	static { // create sessionFactory only once
		try {
			// creating the SessionFactory from hibernate.cfg.xml
			// java.util.Properties properties = new Properties();
			// properties.load(new
			// FileInputStream(Constants.SERVER_BASE+Constants.HIBERNATE_PROPERTY));
			String propertyFileName = Constants.PROPERTY_FILE;
        	StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor(); 
        	encryptor.setPassword(Constants.ENC_VAR+"."+Constants.ENC_VAR_PART);
            Properties properties = new EncryptableProperties(encryptor);
			try {
				ClassLoader classLoader = Thread.currentThread()
						.getContextClassLoader();
				InputStream stream = classLoader.getResourceAsStream("/"
						+ propertyFileName);
				if (null == stream) {
					stream = classLoader.getResourceAsStream(propertyFileName);
				}
				properties.load(stream);
			} catch (Exception e) {
				logger.debug(LoggerUtil.getLogMessage(e.getMessage()));
			}
			Configuration configuration = new Configuration();
			configuration.configure("/hibernate.cfg.xml");
			String dvrClass = properties
					.getProperty("le.mdu.app.jdbc.driverClassName");
			String url = properties.getProperty("le.mdu.app.jdbc.url");
			String uname = properties.getProperty("le.mdu.app.jdbc.username");
			String pwd = properties.getProperty("le.mdu.app.jdbc.password");
			String schema = properties.getProperty("le.mdu.app.jdbc.schema");
			Properties newProps = new Properties();
			newProps.put(Environment.DIALECT,"org.hibernate.dialect.MySQLDialect");
			newProps.put(Environment.HBM2DDL_AUTO, "update");
			newProps.put("hibernate.connection.driver_class", dvrClass);
			newProps.put("hibernate.connection.url", url);
			newProps.put("hibernate.connection.username", uname);
			newProps.put("hibernate.connection.password", pwd);
			newProps.put("hibernate.default_schema", schema);
			configuration.addProperties(newProps);
			// configuration.addProperties(properties);
			serviceRegistry = new ServiceRegistryBuilder().applySettings(
					configuration.getProperties()).buildServiceRegistry();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);

		} catch (Throwable ex) {
			logger.error(
					"****Error while initializing hibernate.cfg.xml file ****",
					ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static Session getSession() {

		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		
		return session;
	}

	public static void getStatistics(SessionFactory sessionFactory2) {
		logger.debug("Method Entry : HibernateUtil.getStatistics in AgentServiceWS");
		Statistics stats = sessionFactory2.getStatistics();
		stats.setStatisticsEnabled(true);
		// Number of connection requests. Note that this number represents
		// the number of times Hibernate asked for a connection, and
		// NOT the number of connections (which is determined by your
		// pooling mechanism).
		System.out.println("ConnectCount :" + stats.getConnectCount());
		// Number of flushes done on the session (either by client code or
		// by hibernate).
		System.out.println("FlushCount :" + stats.getFlushCount());
		// The number of completed transactions (failed and successful).
		System.out.println("TransactionCount :" + stats.getTransactionCount());
		// The number of transactions completed without failure
		System.out.println("SuccessfulTransactionCount :"
				+ stats.getSuccessfulTransactionCount());
		// The number of sessions your code has opened.
		System.out.println("SessionOpenCount :" + stats.getSessionOpenCount());
		// The number of sessions your code has closed.
		System.out
				.println("SessionCloseCount :" + stats.getSessionCloseCount());
		// Total number of queries executed.
		System.out.println("QueryExecutionCount() :"
				+ stats.getQueryExecutionCount());
		// Time of the slowest query executed.
		System.out.println("QueryExecutionMaxTime :"
				+ stats.getQueryExecutionMaxTime());
		logger.debug("Method Exit : HibernateUtil.getStatistics in AgentServiceWS");
	}
}
