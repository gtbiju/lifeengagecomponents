package com.cognizant.le.model.dao;

import java.util.List;
import com.cognizant.le.model.dto.ContentLanguageDTO;
import com.cognizant.le.model.dto.ContentTypeDTO;

public interface CommonAdminDAO {
	
	public List <ContentLanguageDTO> retriveLanguage() throws Exception;
	
	public List <ContentTypeDTO> retriveContentType() throws Exception;

}
