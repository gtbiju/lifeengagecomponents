package com.cognizant.le.model.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
	@NamedQuery(
	name = "fetchLoginDtls",
	query = "from Login where userid= :userid"
	)
})
@Entity
@Table(name = "AGENT_APP_LOGIN")
public class Login {
	@Id
	@Column(name = "userid")
	private String userid;
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAgentcode() {
		return active;
	}
	public void setAgentcode(String agentcode) {
		this.active = agentcode;
	}
	@Column(name = "password")
	private String password;
	@Column(name = "active")
	private String active;

}
