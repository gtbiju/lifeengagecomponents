package com.cognizant.le.model.dto;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;



// TODO: Auto-generated Javadoc
/**
 * The Class Achievement.
 */
@Entity
@Table(name = "ACHIEVEMENT")
public class Achievement implements Serializable {

	private static final long serialVersionUID = 7800713878318462684L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "agent_id", nullable=false)
	private Agent agent;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private AchievementDetails achievementDetails;


	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public AchievementDetails getAchievementDetails() {
		return achievementDetails;
	}

	public void setAchievementDetails(AchievementDetails achievementDetails) {
		this.achievementDetails = achievementDetails;
	}

}
