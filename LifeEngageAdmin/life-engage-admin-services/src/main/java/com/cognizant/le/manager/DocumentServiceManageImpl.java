package com.cognizant.le.manager;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.cognizant.le.model.dao.CommonAdminDAO;
import com.cognizant.le.model.dao.CommonAdminDAOImpl;
import com.cognizant.le.model.dao.DocumentServiceDAO;
import com.cognizant.le.model.dao.DocumentServiceDAOImpl;
import com.cognizant.le.model.dto.AppContext;
import com.cognizant.le.model.dto.ContentFile;
import com.cognizant.le.model.dto.ContentLanguageDTO;
import com.cognizant.le.model.dto.ContentTypeDTO;
import com.cognizant.le.model.dto.Documents;
import com.cognizant.le.model.dto.FileInfo;
import com.cognizant.le.model.dto.RequestInfo;
import com.cognizant.le.util.Constants;
import com.cognizant.le.util.LoggerUtil;

public class DocumentServiceManageImpl implements DocumentServiceManager {
	static Logger logger = Logger.getLogger(DocumentServiceManageImpl.class);

	CommonAdminDAO commonAdminDAO = new CommonAdminDAOImpl();

	@Override
	public List<ContentFile> getUpdatedDocuments(
			List<ContentFile> inputContentFiles) throws Exception {
		logger.info(LoggerUtil
				.getLogMessage("Method Entry 2: DocumentServiceManageImpl.getUpdatedDocuments in LifeEngageAdminWS"));
		/*
		 * List<ContentFile> inputFile = new ArrayList<ContentFile>(); inputFile
		 * = inputContentFiles;
		 * 
		 * for(ContentFile conFile : inputFile){
		 * 
		 * String fileName = conFile.getFileName(); String tabletFullPath =
		 * conFile.getTabletFullPath();
		 * System.out.println("******File Name******" + fileName);
		 * System.out.println("******Tablet Fullpath******" + tabletFullPath); }
		 */

		List<ContentFile> updatedDocuments = new ArrayList<ContentFile>();
		try {
			DocumentServiceDAO documentServiceDAO = new DocumentServiceDAOImpl();
			updatedDocuments = documentServiceDAO
					.getUpdatedDocs(inputContentFiles);
		} catch (Exception e) {
			logger.error(
					LoggerUtil
							.getLogMessage("Exception in DocumentServiceManageImpl.getUpdatedDocuments in LifeEngageAdminWS"),
					e);
			throw e;
		}

		logger.info(LoggerUtil
				.getLogMessage("Method Exit 2: DocumentServiceManageImpl.getUpdatedDocuments in LifeEngageAdminWS"));
		// TODO Auto-generated method stub
		return updatedDocuments;
	}

	@Override
	public List<ContentFile> getUpdatedDocumentsForContext(
			List<ContentFile> inputContentFiles, RequestInfo requestInfo, FileInfo fileInfo)
			throws Exception {
		logger.info(LoggerUtil
				.getLogMessage("Method Entry 2: DocumentServiceManageImpl.getUpdatedDocumentsForContext in LifeEngageAdminWS"));

		List<ContentFile> newAndUpdatedDocumentst = new ArrayList<ContentFile>();
		try {
			DocumentServiceDAO documentServiceDAO = new DocumentServiceDAOImpl();
			newAndUpdatedDocumentst = documentServiceDAO
					.getUpdatedDocumentsForContext(inputContentFiles,
							requestInfo, fileInfo);
		} catch (Exception e) {
			logger.error(
					LoggerUtil
							.getLogMessage("Exception in DocumentServiceManageImpl.getUpdatedDocumentsForContext in LifeEngageAdminWS"),
					e);
			throw e;
		}

		logger.info(LoggerUtil
				.getLogMessage("Method Exit 2: DocumentServiceManageImpl.getUpdatedDocumentsForContext in LifeEngageAdminWS"));
		// TODO Auto-generated method stub
		return newAndUpdatedDocumentst;
	}

	public AppContext getRequestInfo(RequestInfo requestInfo) throws Exception {
		logger.debug(LoggerUtil
				.getLogMessage("Method Entry : DocumentServiceManageImpl.getRequestInfo in LifeEngageAdminWS"));
		AppContext appContext = new AppContext();
		if (requestInfo != null) {
			if (requestInfo.getAppContext() != null) {
				appContext.setContext(requestInfo.getAppContext());
			}

		}
		logger.debug(LoggerUtil
				.getLogMessage("Method Entry : DocumentServiceManageImpl.getRequestInfo in LifeEngageAdminWS"));
		return appContext;
	}

	public List<ContentFile> convertJSONTOJava(
			List<Documents> inputDocumentFiles) throws Exception {
		logger.debug(LoggerUtil
				.getLogMessage("Method Entry: DocumentServiceManageImpl.convertJSONTOJava in LifeEngageAdminWS"));
		List<ContentFile> updatedDocumnetsRequest = new ArrayList<ContentFile>();
		try {

			if (!inputDocumentFiles.isEmpty()) {
				for (Documents inputDocuments : inputDocumentFiles) {
					ContentFile tempContentFiles = new ContentFile();
					tempContentFiles.setFileName(inputDocuments.getFileName());
					tempContentFiles.setTabletFullPath(inputDocuments
							.getTabletFullPath());
					tempContentFiles
							.setPublicURL(inputDocuments.getPublicURL());
					/*
					 * SimpleDateFormat lastUpdateDateFormat = new
					 * SimpleDateFormat("dd-MMM-yyyy"); String lastUpdatedStr =
					 * inputDocuments.getLastUpdated(); java.util.Date
					 * lastUpdatedDate =
					 * (java.util.Date)lastUpdateDateFormat.parse
					 * (lastUpdatedStr); SimpleDateFormat lastUpdateDateFormat2
					 * = new SimpleDateFormat("yyyy-MM-dd"); lastUpdatedStr =
					 * lastUpdateDateFormat2.format(lastUpdatedDate);
					 * lastUpdatedDate =
					 * (java.util.Date)lastUpdateDateFormat2.parse
					 * (lastUpdatedStr);
					 * tempContentFiles.setLastUpdated(lastUpdatedDate);
					 */
					tempContentFiles.setVersion(Integer.parseInt(inputDocuments
							.getVersion()));
					updatedDocumnetsRequest.add(tempContentFiles);
				}
			}
		} catch (Exception e) {
			logger.error(
					LoggerUtil
							.getLogMessage("Exception in DocumentServiceManageImpl.convertJSONTOJava in LifeEngageAdminWS"),
					e);
			throw e;
		}
		logger.debug(LoggerUtil
				.getLogMessage("Method Exit: DocumentServiceManageImpl.convertJSONTOJava in LifeEngageAdminWS"));
		return updatedDocumnetsRequest;
	}

	public List<Documents> convertJavaTOJSON(
			List<ContentFile> updatedDocumentFiles) throws Exception {
		logger.debug(LoggerUtil
				.getLogMessage("Method Entry: DocumentServiceManageImpl.convertJavaTOJSON in LifeEngageAdminWS"));
		List<Documents> updatedDocumnetsResponse = new ArrayList<Documents>();
		if (!updatedDocumentFiles.isEmpty()) {
			for (ContentFile updatedContentFiles : updatedDocumentFiles) {
				Documents tempOutputDocuments = new Documents();
				tempOutputDocuments.setFileName(updatedContentFiles
						.getFileName());
				tempOutputDocuments.setTabletFullPath(updatedContentFiles
						.getTabletFullPath());
				tempOutputDocuments.setPublicURL(updatedContentFiles
						.getPublicURL());
				tempOutputDocuments.setType(updatedContentFiles
						.getContentType().getType());

				if (updatedContentFiles.getIsRequired() == true) {
					tempOutputDocuments.setIsMandatory("Yes");
				} else {
					tempOutputDocuments.setIsMandatory("No");
				}
				tempOutputDocuments.setLanguage(updatedContentFiles
						.getContentLanguage().getLanguage());
				/*
				 * SimpleDateFormat lastUpdateDateFormat = new
				 * SimpleDateFormat("dd-MMM-yyyy");
				 * tempOutputDocuments.setLastUpdated
				 * (lastUpdateDateFormat.format
				 * (updatedContentFiles.getLastUpdated()));
				 */
				tempOutputDocuments.setVersion(Integer
						.toString(updatedContentFiles.getVersion()));
				tempOutputDocuments.setFileSize(updatedContentFiles.getFileSize());
				tempOutputDocuments.setDescription(updatedContentFiles.getDescription());
				updatedDocumnetsResponse.add(tempOutputDocuments);
			}
		}

		logger.debug(LoggerUtil
				.getLogMessage("Method Exit: DocumentServiceManageImpl.convertJavaTOJSON in LifeEngageAdminWS"));

		return updatedDocumnetsResponse;
	}

	public RequestInfo validateModes(RequestInfo reqInfo) {
		try {
			Boolean contentFilterFlag = false;
			Boolean contentLanguageFlag = false;
			Boolean contentPrefFlag = false;

			Properties properties = new Properties();
			String propertiesFile = Constants.PROPERTY_FILE;
			ClassLoader classLoader = Thread.currentThread()
					.getContextClassLoader();
			InputStream stream = classLoader.getResourceAsStream("/"
					+ propertiesFile);
			if (null == stream) {
				stream = classLoader.getResourceAsStream(propertiesFile);
			}
			properties.load(stream);

			String defaultLanguage = properties.getProperty("DEFAULT_LANGUAGE");

			if ((null == defaultLanguage) || "".equals(defaultLanguage.trim())) {
				defaultLanguage = "English";
			}

			List<ContentTypeDTO> contentFiles = commonAdminDAO
					.retriveContentType();
			for (ContentTypeDTO contentTypeDTO : contentFiles) {
				if (null != reqInfo.getContentFilter()
						&& reqInfo.getContentFilter().equalsIgnoreCase(
								contentTypeDTO.getContentType())) {
					contentFilterFlag = true;
					break;
				}
			}

			List<ContentLanguageDTO> contentLangs = commonAdminDAO
					.retriveLanguage();
			for (ContentLanguageDTO contentLanguageDTO : contentLangs) {
				if (null != reqInfo.getLanguage()
						&& reqInfo.getLanguage().equalsIgnoreCase(
								contentLanguageDTO.getLanguage())) {
					contentLanguageFlag = true;
					break;
				}
			}
			//checks whether default language is in ContentLanguages
			if (null == reqInfo.getLanguage()) {
				Boolean defaultLangExists = false;
				if ((null != defaultLanguage)
						&& !"".equals(defaultLanguage.trim())) {
					for (ContentLanguageDTO contentLanguageDTO : contentLangs) {
						if (defaultLanguage.equalsIgnoreCase(contentLanguageDTO
								.getLanguage())) {
							defaultLangExists = true;
							break;
						}
					}
				}
				if (defaultLangExists) {
					reqInfo.setLanguage(defaultLanguage);
					contentLanguageFlag = true;
				} 
				else{
					logger.error("Invalid Default Language in the property file");
				}
			}

			if (null != reqInfo.getContentPreference()
					&& (reqInfo.getContentPreference().equalsIgnoreCase(
							"optional") || reqInfo.getContentPreference()
							.equalsIgnoreCase("mandatory"))) {
				contentPrefFlag = true;
			}

			if (!contentPrefFlag) {
				logger.warn(LoggerUtil
						.getLogMessage("Invalid Content Preference. Defaulting to \"All\""));
				reqInfo.setContentPreference("All");
			}

			if (!contentLanguageFlag && reqInfo.getLanguage()!=null && !reqInfo.getLanguage().equalsIgnoreCase("all")) {
				logger.warn(LoggerUtil
						.getLogMessage("Invalid Content Language."));
				reqInfo.setLanguage("Invalid");
			}

			if (!contentFilterFlag) {
				logger.warn(LoggerUtil
						.getLogMessage("Invalid Content Filter. Defaulting to \"All\""));
				reqInfo.setContentFilter("All");
			}

			String syncMode = null != reqInfo.getSyncMode() ? reqInfo
					.getSyncMode() : "";
			if ((!syncMode.equalsIgnoreCase("all"))
					&& (!syncMode.equalsIgnoreCase("update"))
					&& (!syncMode.equalsIgnoreCase("new"))) {
				logger.warn(LoggerUtil
						.getLogMessage("Invalid Sync Mode. Defaulting to \"All\""));
				reqInfo.setSyncMode("All");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reqInfo;

	}

}
