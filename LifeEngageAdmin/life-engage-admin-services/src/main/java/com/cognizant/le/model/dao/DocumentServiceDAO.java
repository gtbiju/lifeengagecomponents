package com.cognizant.le.model.dao;

import java.util.List;

import com.cognizant.le.model.dto.ContentFile;
import com.cognizant.le.model.dto.FileInfo;
import com.cognizant.le.model.dto.RequestInfo;

public interface DocumentServiceDAO {
	
	public List<ContentFile> getUpdatedDocs(List<ContentFile> inputFiles) throws Exception;
	List<ContentFile> getUpdatedDocumentsForContext(List<ContentFile> inputContentFileList,RequestInfo requestInfo, FileInfo fileInfo) throws Exception;

}
