package com.cognizant.le.manager;

import java.util.List;

import com.cognizant.le.model.dto.AppContext;
import com.cognizant.le.model.dto.ContentFile;
import com.cognizant.le.model.dto.Documents;
import com.cognizant.le.model.dto.FileInfo;
import com.cognizant.le.model.dto.RequestInfo;

public interface DocumentServiceManager {

	List<ContentFile> convertJSONTOJava(List<Documents> inputDocumentFiles)
			throws Exception;

	List<Documents> convertJavaTOJSON(List<ContentFile> updatedDocumentFiles)
			throws Exception;

	List<ContentFile> getUpdatedDocuments(List<ContentFile> inputContentFilest)
			throws Exception;

	List<ContentFile> getUpdatedDocumentsForContext(
			List<ContentFile> inputContentFiles, RequestInfo requestInfo, FileInfo fileInfo)
			throws Exception;
	AppContext getRequestInfo(RequestInfo requestInfo) throws Exception;
	
	RequestInfo validateModes(RequestInfo reqInfo);
	

}
