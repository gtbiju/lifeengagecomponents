package com.cognizant.le.action;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class ContentDownloadServlet.
 */
public class ContentDownloadServlet extends HttpServlet {

	/** The logger. */
	private static Logger LOGGER = Logger
			.getLogger(ContentDownloadServlet.class);

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant BUFFER_SIZE. */
	private static final int BUFFER_SIZE = 4096;

	/** The content folder. */
	private String contentFolder = "";

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		LOGGER.debug("Method Entry : ContentDownloadServlet.init");
		super.init(config);
		String propertiesFile = getServletContext().getInitParameter(
				"CONFIG_PROPERTIES");
		Properties properties = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread()
					.getContextClassLoader();
			InputStream stream = classLoader.getResourceAsStream("/"
					+ propertiesFile);
			if (null == stream) {
				stream = classLoader.getResourceAsStream(propertiesFile);
			}
			properties.load(stream);
			contentFolder = properties.getProperty("SHARED_CONTENT_FOLDER");
			LOGGER.debug("Upload directory for contents is " + contentFolder);
			LOGGER.debug("Method Exit : ContentDownloadServlet.init");
		} catch (IOException e) {
			LOGGER.error(propertiesFile + " is missing", e);
			throw new ServletException(e.getMessage(), e);
		}
	}

	/**
	 * Do get.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		LOGGER.debug("Method Entry : ContentDownloadServlet.doGet");
		String fileName = request.getParameter("fileName");
		if ((null != fileName) && !("".equals(fileName.trim()))) {
			fileName = getFileName(fileName);

			File file = new File(contentFolder + File.separator + fileName);
			if (fileName != null && file.exists()) {
				int length = 0;
				ServletOutputStream outStream = response.getOutputStream();
				ServletContext context = getServletConfig().getServletContext();
				String mimetype = context.getMimeType(file.getCanonicalPath());

				// sets response content type
				if (mimetype == null) {
					mimetype = "application/octet-stream";
				}
				response.setContentType(mimetype);
				response.setContentLength((int) file.length());
				// String fileName = (new File(filePath)).getName();

				// sets HTTP header
				response.setHeader("Content-Disposition",
						"attachment; filename=\"" + fileName + "\"");

				byte[] byteBuffer = new byte[BUFFER_SIZE];
				DataInputStream in = new DataInputStream(new FileInputStream(
						file));

				// reads the file's bytes and writes them to the response stream
				while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
					outStream.write(byteBuffer, 0, length);
				}

				in.close();
				outStream.close();
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);

			}
			LOGGER.debug("Method Exit : ContentDownloadServlet.doGet");
		}

	}

	/**
	 * Gets the file name.
	 * 
	 * @param filePath
	 *            the file path
	 * @return the file name
	 */
	private String getFileName(String filePath) {
		String fileSeperator = getFileSeperator(filePath);
		String fileName = null;
		if (null != fileSeperator) {
			String[] filePathArray = filePath.split(fileSeperator);
			fileName = filePathArray[filePathArray.length - 1];
		} else {
			fileName = filePath;
		}
		return fileName;
	}

	/**
	 * Gets the file seperator.
	 * 
	 * @param filePath
	 *            the file path
	 * @return the file seperator
	 */
	private String getFileSeperator(String filePath) {
		String seperator = null;
		if (filePath.contains("\\")) {
			seperator = "\\\\";
		} else if (filePath.contains("/")) {
			seperator = "/";
		}
		return seperator;
	}
}
