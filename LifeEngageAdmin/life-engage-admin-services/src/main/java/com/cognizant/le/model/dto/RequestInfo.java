package com.cognizant.le.model.dto;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

// TODO: Auto-generated Javadoc
/**
 * The Class RequestInfo.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestInfo {
	
	/** The app context. */
	private String appContext;

	/** The sync mode. */
	private String syncMode;
	
	/** The content filter. */
	private String contentFilter;
	
	/** The language. */
	private String language;
	
	/** The document preference. */
	private String contentPreference;
	
	
	/**
	 * Gets the app context.
	 *
	 * @return the app context
	 */
	public String getAppContext() {
		return appContext;
	}

	/**
	 * Sets the app context.
	 *
	 * @param appContext the new app context
	 */
	public void setAppContext(String appContext) {
		this.appContext = appContext;
	}

	/**
	 * Gets the sync mode.
	 *
	 * @return the sync mode
	 */
	public String getSyncMode() {
		return syncMode;
	}

	/**
	 * Sets the sync mode.
	 *
	 * @param syncMode the new sync mode
	 */
	public void setSyncMode(String syncMode) {
		this.syncMode = syncMode;
	}

	/**
	 * Gets the content filter.
	 *
	 * @return the content filter
	 */
	public String getContentFilter() {
		return contentFilter;
	}

	/**
	 * Sets the content filter.
	 *
	 * @param contentFilter the new content filter
	 */
	public void setContentFilter(String contentFilter) {
		this.contentFilter = contentFilter;
	}

	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the language.
	 *
	 * @param language the new language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	public String getContentPreference() {
		return contentPreference;
	}

	public void setContentPreference(String contentPreference) {
		this.contentPreference = contentPreference;
	}

}
