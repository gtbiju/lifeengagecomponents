/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 270722
 * @version       : 0.1, Oct 22, 2013
 */
package com.cognizant.icr.pdf.util;

/**
 * The Class class Constants.
 */
public final class Constants {

	/** The Constant OUTFILENAME. */
	public static final String OUTFILENAME = "generatedPdf.pdf";

	/** The Constant REPO_IMPL. */
	public static final String REPO_IMPL = "REPOSITORY_IMPLEMENTATION";

	/** The Constant TIMESTAMP_FMT. */
	public static final String TIMESTAMP_FMT = "yyyyMMddhhmmSS";

	/** The Constant EXT_PDF. */
	public static final String EXT_PDF = ".pdf";

	/** The Constant INPUT_TEMPLATE_LOC. */
	public static final String INPUT_TEMPLATE_LOC = "le.pdf.template.location";

	/** The Constant URL. */
	public static final String URL = "le.pdf.jdbc.url";

	/** The Constant USER. */
	public static final String USER = "le.pdf.jdbc.userName";

	/** The Constant PASSWORD. */
	public static final String PASSWORD = "le.pdf.jdbc.password";

	/** The Constant PASSWORD. */
	public static final String SERVERNAME = "serverName";

	/** The Constant DRIVER_CLASS. */
	public static final String DATABASENAME = "databaseName";

	/** The Constant DRIVER_CLASS. */
	public static final String DRIVER_CLASS = "le.pdf.jdbc.driver";

	/** The Constant CONFIG_PROPERTY_FILE. */
	public static final String CONFIG_PROPERTY_FILE = "omni-channel-qa-pdf-generator-config-coc.properties";

	/** The Constant RE_URL. */
	public static final String RE_URL = "le.pdf.re.url";

	/** The Constant RE_USER. */
	public static final String RE_USER = "le.pdf.re.userName";

	/** The Constant RE_PASSWORD. */
	public static final String RE_PASSWORD = "le.pdf.re.password";

	/** The Constant RE_DRIVER_CLASS. */
	public static final String RE_DRIVER_CLASS = "le.pdf.re.driver";

	/** The Constant RULE_CLIENT_ID. */
	public static final String RULE_CLIENT_ID = "le.pdf.re.rule_client_id";

	/** The Constant RULE_CATEGORY. */
	public static final String RULE_CATEGORY = "le.pdf.re.rule_category";

	/** The Constant IS_ENABLED. */
	public static final String IS_ENABLED = "le.pdf.re.isEnabled";
	
	public static final String PDF_IMAGE_LOC = "le.pdf.image.location";

	/**
	 * Instantiates a new constants.
	 */
	private Constants() {

	}
}
