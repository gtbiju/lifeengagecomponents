package com.cognizant.icr.pdf.util;


/**
 * A factory for creating Connection objects.
 */
public interface ConnectionFactory {
	
	/**
	 * Creates a new Connection object.
	 *
	 * @param <T> the generic type
	 * @return the t
	 */
	<T> T createConnection();
	

}
