/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291430
 * @version       : 0.1, Dec 30, 2013
 */
package com.cognizant.icr.pdf.file.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.cognizant.icr.pdf.file.FileNameGenerator;
import com.cognizant.icr.pdf.util.Constants;

/**
 * The Class class EAPPFileNameGenerator.
 */
public class EAPPFileNameGenerator implements FileNameGenerator {
       
    /**
     * Generate file name.
     *
     * @param <Transactions> the generic type
     * @param transaction the transaction
     * @return the string
     */
    public final <T> String generateFileName(final T transaction) {
        String timeStamp = new SimpleDateFormat(Constants.TIMESTAMP_FMT).format(new Date());
        String fileName = "generated-file" + timeStamp;
        return fileName;
    }

}
