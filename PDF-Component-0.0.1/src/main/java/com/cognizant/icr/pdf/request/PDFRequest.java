/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270722
 * @version       : 0.1, Oct 22, 2013
 */
package com.cognizant.icr.pdf.request;

import java.util.Map;

/**
 * The Interface interface PDFRequest.
 * 
 * @param <T>
 *            the generic type
 */
public interface PDFRequest<T> {

    /**
     * Gets the template id.
     * 
     * @return the template id
     */
    String getTemplateID();

    /**
     * Sets the template id.
     * 
     * @param templateID
     *            the template id to set.
     */
    void setTemplateID(String templateID);

    /**
     * Gets the data objs map.
     *
     * @return the data objs map
     */
    Map<String, T> getDataObjsMap();

    /**
     * Sets the data objs map.
     * 
     * @param dataObjsMap
     *            the data objs map
     */
    void setDataObjsMap(Map<String, T> dataObjsMap);

    /**
     * Gets the template version.
     * 
     * @return the template version
     */
    String getTemplateVersion();

    /**
     * Sets the template version.
     * 
     * @param templateVersion
     *            the new template version
     */
    void setTemplateVersion(String templateVersion);

    /**
     * Gets the user id.
     * 
     * @return the user id
     */
    String getUserId();

    /**
     * Sets the user id.
     * 
     * @param userId
     *            the new user id
     */
    void setUserId(String userId);

    /**
     * Gets the template type.
     * 
     * @return the template type
     */
    String getTemplateType();

    /**
     * Sets the template type.
     * 
     * @param templateType
     *            the new template type
     */
    void setTemplateType(String templateType);

}
