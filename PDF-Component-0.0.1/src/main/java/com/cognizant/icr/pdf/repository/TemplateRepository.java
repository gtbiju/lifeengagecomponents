/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270722
 * @version       : 0.1, Oct 22, 2013
 */
package com.cognizant.icr.pdf.repository;

import java.io.File;

import net.sf.jasperreports.engine.JRException;

/**
 * The Interface interface DataRepository.
 */
public interface TemplateRepository  {

	/**
	 * Generate pdf.
	 *
	 * @param templateName the template name
	 * @return the file
	 * @throws JRException the jR exception
	 */
    File getTemplate(String templateName)  throws JRException;
    
    /**
     * Gets the template name.
     *
     * @param templateId the template id
     * @return the template name
     */
    String getTemplateName(String templateId);

}
