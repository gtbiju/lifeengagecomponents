/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270722
 * @version       : 0.1, Oct 22, 2013
 */
package com.cognizant.icr.pdf.repository;


/**
 * The Interface interface DatabaseRepository.
 */
public interface DatabaseRepository extends TemplateRepository {


}
