/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291430
 * @version       : 0.1, Dec 30, 2013
 */
package com.cognizant.icr.pdf.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A factory for creating Connection objects.
 */
public final class DriverManagerFactory implements ConnectionFactory{

    /** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DriverManagerFactory.class);

    /** The instance. */
    private static DriverManagerFactory instance = new DriverManagerFactory();

    /** The url. */
    private String url;

    /** The user. */
    private String user;

    /** The password. */
    private String password;

    /** The driver_class. */
    private String driverClass;

    /**
     * Instantiates a new connection factory.
     */
    public DriverManagerFactory() {
        LOGGER.debug("Constructor Entry : ConnectionFactory");
        try {
            url = PropertyFileReader.fetchProperty(Constants.URL, Constants.CONFIG_PROPERTY_FILE);
            user = PropertyFileReader.fetchProperty(Constants.USER, Constants.CONFIG_PROPERTY_FILE);
            password = PropertyFileReader.fetchProperty(Constants.PASSWORD, Constants.CONFIG_PROPERTY_FILE);
            driverClass = PropertyFileReader.fetchProperty(Constants.DRIVER_CLASS, Constants.CONFIG_PROPERTY_FILE);
            Class.forName(driverClass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        LOGGER.debug("Constructor Exit : ConnectionFactory");
    }

    /**
     * Creates a new Connection object.
     * 
     * @return the connection
     */
     public Connection createConnection() {
        LOGGER.debug("Method Entry : ConnectionFactory-> createConnection");
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            LOGGER.error("ERROR: Unable to Connect to Database.");
        }
        LOGGER.debug("Method Exit : ConnectionFactory-> createConnection");
        return connection;
    }

    /**
     * Gets the connection.
     * 
     * @return the connection
     */
    public static Connection getConnection() {
        return instance.createConnection();
    }
}
