/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 270722
 * @version       : 0.1, Oct 22, 2013
 */
package com.cognizant.icr.pdf.repository.impl;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.icr.pdf.dao.CommonDAOImpl;
import com.cognizant.icr.pdf.repository.FileRepository;
import com.cognizant.icr.pdf.util.Constants;
import com.cognizant.icr.pdf.util.PropertyFileReader;

/**
 * The Class class FileRepositoryImpl.
 */
public class FileRepositoryImpl implements FileRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileRepositoryImpl.class);
	/**
	 * Gets the jasper.
	 * 
	 * @param templateId
	 *            the template id
	 * @return the jasper
	 */
	public File getTemplate(String templateName) {
	    LOGGER.debug("Method Entry : FileRepository-> getTemplate");
		String templateLocation = PropertyFileReader
				.fetchProperty(Constants.INPUT_TEMPLATE_LOC,Constants.CONFIG_PROPERTY_FILE);
		LOGGER.debug("Method Exit : FileRepository-> getTemplate");
		return new File(templateLocation + templateName);
	}

	public String getTemplateName(String templateId) {
	    LOGGER.debug("Method Entry : FileRepository-> getTemplateName");
	    String templateName = new CommonDAOImpl().getTemplateName(templateId);
		LOGGER.debug("Method Exit : FileRepository-> getTemplateName");
		return templateName;
	}

}
