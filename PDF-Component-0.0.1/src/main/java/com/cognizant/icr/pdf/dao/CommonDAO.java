package com.cognizant.icr.pdf.dao;

/**
 * The Interface CommonDAO.
 */
public interface CommonDAO {
	
	/**
	 * Checks if is rule available.
	 *
	 * @param ruleName the rule name
	 * @return the boolean
	 */
	Boolean isRuleAvailable(String ruleName);
	
	/**
	 * Gets the template name.
	 *
	 * @param templateId the template id
	 * @return the template name
	 */
	String getTemplateName(String templateId);

}
