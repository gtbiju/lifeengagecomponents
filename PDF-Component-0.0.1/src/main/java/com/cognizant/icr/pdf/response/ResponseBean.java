/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291430
 * @version       : 0.1, Dec 30, 2013
 */
package com.cognizant.icr.pdf.response;

/**
 * The Class ResponseBean.
 */
public class ResponseBean {
	
	/** The file name. */
	private String fileName;
	
	/** The file location. */
	private String fileLocation;

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public final String getFileName() {
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName the new file name
	 */
	public final void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the file location.
	 *
	 * @return the file location
	 */
	public final String getFileLocation() {
		return fileLocation;
	}

	/**
	 * Sets the file location.
	 *
	 * @param fileLocation the new file location
	 */
	public final void setFileLocation(final String fileLocation) {
		this.fileLocation = fileLocation;
	}

}
