package com.cognizant.icr.pdf.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.icr.pdf.util.ConnectionFactory;
import com.cognizant.icr.pdf.util.DataSourceFactory;
import com.cognizant.icr.pdf.util.DriverManagerFactory;

/**
 * The Class CommonDAOImpl.
 */
public class CommonDAOImpl implements CommonDAO {
	
	/** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonDAOImpl.class);
	
	/**
	 * Checks if is rule available.
	 *
	 * @param ruleName the rule name
	 * @return the boolean
	 */
	public Boolean isRuleAvailable(String ruleName){
		Boolean isAvailable =false;
		ConnectionFactory connection = new DataSourceFactory();
		DataSource dataSource = connection.createConnection();
		try {				
			Connection con = dataSource.getConnection();
			String sqlString= "SELECT COUNT(*) FROM RULE_TABLE WHERE  RULENAME=?";
			PreparedStatement ps= con.prepareStatement(sqlString);
			ps.setString(1,ruleName);
			ResultSet rs= ps.executeQuery();
			if(rs.next())
			{
				if(rs.getInt(1)== 1)
				{
					isAvailable= true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return isAvailable;
		
	}
	
	
	/**
	 * Gets the template name.
	 *
	 * @param templateId the template id
	 * @return the template name
	 */
	public String getTemplateName(String templateId) {
	    LOGGER.debug("Method Entry : FileRepository-> getTemplateName");
		String templateName = null;
		ConnectionFactory conFactory= new DriverManagerFactory();
		try {
			Connection con = conFactory.createConnection();
			String sql = "SELECT TEMPLATE_NAME FROM CORE_PROD_TEMPLATES WHERE TEMPLATE_ID=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, templateId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				templateName = rs.getString(1);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		LOGGER.debug("Method Exit : FileRepository-> getTemplateName");
		return templateName;
	}

}
