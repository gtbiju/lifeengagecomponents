﻿//
// This is just a function to return some hardcoded test data.
// In real life you'd probably use AJAX to retrieve JSON from a server.
//


function getRuleJson() {
    return {
        "Rules": {
            "Rule": [
                  {
                      "type": "IF",
                      "Conditions": {
                          "Condition": [{ "lhs": "6", "rhs": "SUV", "operator": "==", "connector": "And" },
                                        { "lhs": "7", "rhs": "CAR", "operator": "==", "connector": "Or" },
                                        { "lhs": "8", "rhs": "LUV", "operator": "==", "connector": "" }
                      ]
                      },
                      "Actions": {
                          "Action": [{ "type": "IF", "lhs": "8", "rhs": "LUV", "operator": "==",
                              "Conditions": {
                                  "Condition": [{ "lhs": "6", "rhs": "SUV", "operator": "==", "connector": "And" },
                                                { "lhs": "7", "rhs": "CAR", "operator": "==", "connector": "Or" },
                                                { "lhs": "8", "rhs": "LUV", "operator": "==", "connector": ""}]
                              },
                              "Statement": [{ "lhs": "8", "rhs": "LUV" },
                                            { "lhs": "8", "rhs": "LUV"}]
                          },
                        { "type": "ElseIF", "lhs": "8", "rhs": "LUV", "operator": "==",
                            "Conditions": {
                                "Condition": [{ "lhs": "9", "rhs": "SUV", "operator": "==", "connector": "And" },
                              { "lhs": "10", "rhs": "CAR", "operator": "==", "connector": "Or" },
                              { "lhs": "11", "rhs": "LUV", "operator": "==", "connector": ""}]
                            },
                            "Statement": [{ "lhs": "8", "rhs": "LUV" },
                                           { "lhs": "8", "rhs": "LUV" }
                          ]
                        },
                        { "type": "Loop", "start": "1", "step": "1", "limit": "Age",
                            "Statement": [{ "lhs": "8", "rhs": "LUV" },
                            { "lhs": "8", "rhs": "LUV" },
                            { "lhs": "8", "rhs": "LUV" },
                            { "lhs": "8", "rhs": "LUV" }
                          ]
                        },
                        {
                            "type": "Statement",
                            "Statement": [{ "lhs": "8", "rhs": "LUV" },
                            { "lhs": "8", "rhs": "LUV" },
                            { "lhs": "8", "rhs": "LUV" },
                            { "lhs": "8", "rhs": "LUV" }
                          ]
                        }
                      ]
                      }
                  },
                  {
                      "type": "ElseIF",
                      "Conditions": {
                          "Condition": [{ "lhs": "6", "rhs": "SUV", "operator": "==", "connector": "And" },
                        { "lhs": "7", "rhs": "CAR", "operator": "==", "connector": "Or" },
                        { "lhs": "8", "rhs": "LUV", "operator": "==", "connector": "" }
                      ]
                      },
                      "Actions": {
                          "Action": [{ "type": "IF", "lhs": "8", "rhs": "LUV", "operator": "==",
                              "Conditions": {
                                  "Condition": [{ "lhs": "6", "rhs": "SUV", "operator": "==", "connector": "And" },
                              { "lhs": "7", "rhs": "CAR", "operator": "==", "connector": "Or" },
                              { "lhs": "8", "rhs": "LUV", "operator": "==" }
                            ]
                              },
                              "Statement": [{ "lhs": "8", "rhs": "LUV" },
                                            { "lhs": "8", "rhs": "LUV" }
                          ]
                          },
                        {
                            "type": "ElseIF", "lhs": "8", "rhs": "LUV", "operator": "==", "Conditions": {
                                "Condition": [{ "lhs": "9", "rhs": "SUV", "operator": "==", "connector": "And" },
                              { "lhs": "10", "rhs": "CAR", "operator": "==", "connector": "Or" },
                              { "lhs": "11", "rhs": "LUV", "operator": "==" }
                            ]
                            },
                            "Statement": [{ "lhs": "8", "rhs": "LUV" },
                                          { "lhs": "8", "rhs": "LUV" }
                          ]
                        },
                        { "type": "Loop", "start": "1", "step": "1", "limit": "Age",
                            "Statement": [{ "lhs": "8", "rhs": "LUV" },
                            { "lhs": "8", "rhs": "LUV" },
                            { "lhs": "8", "rhs": "LUV" },
                            { "lhs": "8", "rhs": "LUV" }
                          ]
                        },
                        { "type": "Statement", "Statement": [{ "lhs": "8", "rhs": "LUV" }, { "lhs": "8", "rhs": "LUV" }, { "lhs": "8", "rhs": "LUV" }, { "lhs": "8", "rhs": "LUV"}]
                        }
                      ]
                      }
                  },
                  {
                      "type": "Else",
                      "Actions": {
                          "Action": {
                              "type": "Statement", "Statement": [{ "lhs": "8", "rhs": "LUV" }, { "lhs": "8", "rhs": "LUV"}]
                          }
                      }
                  },
                  {
                      "type": "Loop", "start": "0", "step": "1", "limit": "Age",
                      "Actions": {
                          "Action": {
                              "type": "Statement",
                              "Statement": [{ "lhs": "8", "rhs": "LUV" }, { "lhs": "8", "rhs": "LUV"}]
                          }
                      }
                  },
                  {
                      "type": "Statement",
                      "Actions": {
                          "Action": {
                              "type": "Statement", "Statement": [{ "lhs": "8", "rhs": "LUV" }, { "lhs": "8", "rhs": "LUV"}]
                          }
                      }
                  }
                ]
        }
    }
};


function test_data_get_controls() {
    return [
      {
          "control_name": "Textbox",
          "QuestionText": "what is ur name",
          "value": "555-9623",
          "template": "tmpl-textbox",
          "Options": [{ "option": "a" }, { "option": "b"}],
          "required": "true",
          "defaults": "true"
      },
      {
          "control_name": "Radiobutton",
          "QuestionText": "Sex",
          "value": "M",
          "template": "tmpl-radio",
          "Options": [{ "option": "a" }, { "option": "b"}],
          "required": "true",
          "defaults": "false"
      },
      {
          "control_name": "Dropdown",
          "QuestionText": "Address",
          "value": "www",
          "template": "tmpl-dropdown",
          "Options": [{ "option": "a" }, { "option": "b"}],
          "required": "false",
          "defaults": "true"
      },

  ];
};
function test_data_get_controlsTypes() {
    return [
      {
          "control_name": "TextBox",
          "template": "tmpl-textbox"

      },
      {
          "control_name": "RadioButtonList",
          "template": "tmpl-radio"

      },
      {
          "control_name": "DropdownList",
          "template": "tmpl-dropdown"

      },

             {
                 "control_name": "CheckBoxList",
                 "template": "tmpl-check"
             },

      {
          "control_name": "Label",
          "template": "tmpl-label"
      },
      {
          "control_name": "DatePicker",
          "template": "tmpl-dtp"
      },
            {
                "control_name": "Group Break",
                "template": "tmpl-group"
            },
             {
                 "control_name": "Page Break",
                 "template": "tmpl-page"
             }
  ];
};
