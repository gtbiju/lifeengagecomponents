﻿//first

var arr = [];
var previousArray = [];

var selectedTxtBox = '';

var keywordsTitle = [];

var keywordsValue = [];

var keywordsReplace = [];
keywordsinitialization();
var functionCount = keywordsTitle.length;

function RefreshCloneList(variableID, values) {
	var values_arr = [];

	var keys = Object.keys(values);
	keys.forEach(function(key) {
		values_arr.push(values[key]);
	});
	for (i = functionCount, j = 0; i < functionCount + values_arr.length,
			j < values_arr.length; i++, j++) {
		if (keywordsValue.indexOf(values_arr[j]) == -1) {
			keywordsTitle.splice(i, 0, variableID);
			keywordsValue.splice(i, 0, values_arr[j]);
			if(variableID == "SingleOutput" || variableID == "MultiOutput" || variableID == "FunctionInput"){
				keywordsReplace.splice(i, 0, values_arr[j]);
			}
			else{
				keywordsReplace.splice(i, 0, 'inoutMap.' + values_arr[j]);
			}
		}

	}

}
function getIntellesense(position) {
	var keywordsFoundObj = {};
	keywordsFoundObj.keywordfound = false;
	var dotFoundObj = {};
	dotFoundObj.dotfound = false;

	var str = arr[position].split(".");
	var div = $("#intelliDiv");
	if (str[0] == '' || str[0] == ' ' || str[0] == '(' || str[0] == ')'
			|| str[0] == ';' || str[0] == '\n' || str[0] == '='
			|| str[0] == ']' || str[0] == '[' || str[0] == '.' || str[0] == ',') {
		div.empty();
		$(div).hide();
		return;
	}

	for (i = 0; i < keywordsValue.length; i++) {
		len = 0;
		if (str.length == 1) {
			len = 0;
		} else {
			len = str.length - 2;
		}
		
		if(keywordsValue[i] !== null){
			if (keywordsValue[i].search(new RegExp(str[len], 'i')) === 0) {
				keywordsFoundObj.keywordfound = true;
				if (!keywordsFoundObj.hasOwnProperty(keywordsTitle[i])) {
					keywordsFoundObj[keywordsTitle[i]] = {};
					keywordsFoundObj[keywordsTitle[i]].string = [];
					keywordsFoundObj[keywordsTitle[i]].replacement = [];
				}

				keywordsFoundObj[keywordsTitle[i]].string.push(keywordsValue[i]);			
				keywordsFoundObj[keywordsTitle[i]].replacement
						.push(keywordsReplace[i]);
			}	
		}
	}

	if (str.length > 1 && keywordsFoundObj.keywordfound) {
		dotFoundObj.dotfound = true;
		for (i = 0; i < keywordsValue.length; i++) {
			if (keywordsValue[i].search(new RegExp(str[str.length - 1], 'i')) === 0) {
				if (!dotFoundObj.hasOwnProperty(keywordsTitle[i])) {
					dotFoundObj[keywordsTitle[i]] = {};
					dotFoundObj[keywordsTitle[i]].string = [];
					dotFoundObj[keywordsTitle[i]].replacement = [];
				}

				dotFoundObj[keywordsTitle[i]].string.push(keywordsValue[i]);
				dotFoundObj[keywordsTitle[i]].replacement
						.push(keywordsReplace[i]);
			}
		}
	}

	if (!keywordsFoundObj.keywordfound) {
		div.empty();
		$(div).hide();
		return;
	}

	div.empty();
	$(div).hide();

	if (!dotFoundObj.dotfound) {
		for (prop in keywordsFoundObj) {
			if (prop == "SingleInput") {
				tempStr = "<div class='ttl_bg' id='SingleValuedInputs'>Single Valued Inputs";
				len = keywordsFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ keywordsFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">"
							+ keywordsFoundObj[prop].string[i] + "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}

			if (prop == "SingleOutput") {
				tempStr = "<div class='ttl_bg' id='SingleValuedOutputs'>Single Valued Outputs";
				len = keywordsFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ keywordsFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">"
							+ keywordsFoundObj[prop].string[i] + "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}

			if (prop == "MultiInput") {
				tempStr = "<div class='ttl_bg' id='MultiValuedInputs'>Multi Valued Inputs";
				len = keywordsFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ keywordsFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">"
							+ keywordsFoundObj[prop].string[i] + "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}

			if (prop == "MultiOutput") {
				tempStr = "<div class='ttl_bg' id='MultiValuedOutputs'>Multi Valued Outputs";
				len = keywordsFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ keywordsFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">"
							+ keywordsFoundObj[prop].string[i] + "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}

			if (prop == "StandardInput") {
				tempStr = "<div class='ttl_bg' id='StandardInput'>Standard Input";
				len = keywordsFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ keywordsFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">"
							+ keywordsFoundObj[prop].string[i] + "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}

			if (prop == "Functions") {
				tempStr = "<div class='ttl_bg' id='Functions'>Functions";
				len = keywordsFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ keywordsFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">"
							+ keywordsFoundObj[prop].string[i] + "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}
			if (prop == "FunctionInput") {
				tempStr = "<div class='ttl_bg' id='FunctionInput'>Function Arguments";
				len = keywordsFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ keywordsFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">"
							+ keywordsFoundObj[prop].string[i] + "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}
		}
	} else {
		str.pop();
		for (prop in dotFoundObj) {
			if (prop == "SingleValuedInputs") {
				tempStr = "<div class='ttl_bg' id='SingleValuedInputs'>SingleValuedInputs";
				len = dotFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ str.join(".") + "."
							+ dotFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">" + dotFoundObj[prop].string[i]
							+ "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}

			if (prop == "SingleOutput") {
				tempStr = "<div class='ttl_bg' id='SingleValuedOutputs'>Single Valued Outputs";
				len = keywordsFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ keywordsFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">"
							+ keywordsFoundObj[prop].string[i] + "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}

			if (prop == "MultiInput") {
				tempStr = "<div class='ttl_bg' id='MultiValuedInputs'>Multi Valued Inputs";
				len = keywordsFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ keywordsFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">"
							+ keywordsFoundObj[prop].string[i] + "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}

			if (prop == "MultiOutput") {
				tempStr = "<div class='ttl_bg' id='MultiValuedOutputs'>Multi Valued Outputs";
				len = keywordsFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ keywordsFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">"
							+ keywordsFoundObj[prop].string[i] + "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}

			if (prop == "StandardInput") {
				tempStr = "<div class='ttl_bg' id='StandardInput'>Standard Input";
				len = keywordsFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ keywordsFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">"
							+ keywordsFoundObj[prop].string[i] + "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}

			if (prop == "Functions") {
				tempStr = "<div class='ttl_bg' id='Functions'>Functions";
				len = dotFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ str.join(".") + "."
							+ dotFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">" + dotFoundObj[prop].string[i]
							+ "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}
			if (prop == "FunctionInput") {
				tempStr = "<div class='ttl_bg' id='FunctionInput'>Function Arguments";
				len = dotFoundObj[prop].string.length;
				for (i = 0; i < len; i++) {
					tempStr = tempStr
							+ "<div class='intelli_val_bg' onclick=\"insertText(\'"
							+ str.join(".") + "."
							+ dotFoundObj[prop].replacement[i] + "\',"
							+ position + ")\">" + dotFoundObj[prop].string[i]
							+ "</div>";
				}
				tempStr = tempStr + "</div>"
				div.append(tempStr);
			}			
			
		}
	}
	$('#intelliDiv').css({
		"left" : selectedTxtBox.offsetLeft + 5,
		"top" : selectedTxtBox.offsetTop + 20
	});
	$('#intelliDiv').show();
}

function insertText(text, position) {
	var scope;
	arr[position] = text;
	str = arr.join("");

	arr = [];
	getTokens(str);
	previousArray = arr.slice();
	arr = [];
	scope=angular.element($(selectedTxtBox)).scope();
	eval("scope."+$(selectedTxtBox).attr("ng-model")+"="+"'"+str+"'")
	$(selectedTxtBox).val(str);
	var div = $("#intelliDiv");
	div.empty();
	$(div).hide();
}

function checkText(textarea) { 

	if (textarea.value == '') {
		$('#intelliDiv').empty();
		$('#intelliDiv').hide();
		previousArray[0] = '';
	}
			
	arr = [];
	getTokens(textarea.value);
	selectedTxtBox = textarea;
	if (previousArray.length == 0) {
		previousArray = arr.slice();
		// console.log(arr[0]);
		getIntellesense(0);
		return;
	}
	var prlength = previousArray.length;
	for (i = 0; i < arr.length; i++) {
		if (i < prlength) {
			if (arr[i] != previousArray[i]) {
				// console.log(arr[i]);
				previousArray = arr.slice();
				getIntellesense(i);
				return;
			}
		}
	}

	if (arr.length > prlength) {
		// console.log(arr[prlength]);
		previousArray = arr.slice();
		getIntellesense(prlength);
		return;
	}

}

function getTokens(data) {

	arr = [];
	str = data;
	aray = str.split("\n");
	for (i = 0; i < aray.length; i++) {
		if (aray[i] != "")
			arr.push(aray[i]);
		if (i != (aray.length - 1)) {
			arr.push("\n");
		}
	}
	breakInToTokens(";");
	breakInToTokens("(");
	// breakInToTokens(".");
	breakInToTokens(")");
	breakInToTokens("=");
	breakInToTokens(",");
	breakInToTokens("[");
	breakInToTokens("]");
	breakInToTokens(",");
	breakInToTokens(" ");

	// console.log(arr);

}

function breakInToTokens(character) {
	tempArr = [];
	for (i = 0; i < arr.length; i++) {
		tmp = arr[i];
		if (arr[i] == '' || arr[i] == ' ' || arr[i] == '(' || arr[i] == ')'
				|| arr[i] == ';' || arr[i] == '\n' || arr[i] == '='
				|| arr[i] == ']' || arr[i] == '[' || arr[i] == '.'
				|| arr[i] == ',') {
			tempArr.push(arr[i]);
		} else {
			tmp = tmp.split(character);
			for (j = 0; j < tmp.length; j++) {
				if (!(tmp[j] == ""))
					tempArr.push(tmp[j]);
				if (j != (tmp.length - 1)) {
					tempArr.push(character);
				}
			}
		}
	}

	arr = tempArr.slice();
}

// second

jQuery(function($) {
	var multiinput;
	var multioutput;
	var message = "";
	var el = $('textarea');

	el.blur(function(e) {
		keywordsinitialization();
		if (e.target.value == '')
			e.target.value = e.target.defaultValue;
		else {
			var singleInputs = $('#SingleInput').val().split(/\n\r?/g);
			var singleOutputs = $('#SingleOutput').val().split(/\n\r?/g);
			var multiInputs = $('#MultiInput').val().split(/\n\r?/g);
			var multiOutputs = $('#MultiOutput').val().split(/\n\r?/g);
			var standardInputs = $('#StandardInput').val().split(/\n\r?/g);
			var functionInputs = $('#FunctionInput').val().split(/\n\r?/g);
			var splitters = [];
			
			singleInputs !=''? splitters.push(singleInputs):"";
			singleOutputs !=''? splitters.push(singleOutputs) : "";
			multiInputs !=''? splitters.push(multiInputs):"";
			multiOutputs !=''? splitters.push(multiOutputs):"";
			standardInputs !=''? splitters.push(standardInputs):"";
			functionInputs !=''? splitters.push(functionInputs):""; 
			
			var isDuplicate = duplicateKeyValueCheck(splitters);
		
			if(!isDuplicate){
				if(singleInputs !=''){
				createIntellisenseValues(singleInputs,"SingleInput");
				}
				if(singleOutputs !=''){
				createIntellisenseValues(singleOutputs,"SingleOutput");
				}
				if(multiInputs !=''){
				createIntellisenseValues(multiInputs,"MultiInput");
				}
				if(multiOutputs !=''){
				createIntellisenseValues(multiOutputs,"MultiOutput");
				}
				if(standardInputs !=''){
				createIntellisenseValues(standardInputs,"StandardInput");
				}
				if(functionInputs !=''){
				createIntellisenseValues(functionInputs,"FunctionInput");
				}
			} else {
				alert (message +"are reserved key words.");
				e.currentTarget.focus();
			}
		}
	});
	
	function duplicateKeyValueCheck(splitres){
		message = "";
		var keyWordsArray = [];
		var isDuplicate = false;
       /*  for (var i in splitres) {
			for( var j in splitres[i]){
				if($.inArray(splitres[i][j].toString().toLowerCase(), keywordsValue) !== -1){
					isDuplicate = true;
					keyWordsArray.push(splitres[i][j]);
				}
			}
        } */
		$.each(splitres, function(i, v) {
			$.each(v, function(j, value) {
			if($.inArray(value.toString().toLowerCase(), keywordsValue) !== -1){
					isDuplicate = true;
					keyWordsArray.push(value);
				}
			
			
		});
			
		});
		
		
		if(keyWordsArray.length > 0){
			$.each(keyWordsArray, function(j, value) {
				message += value+",";
			});
		}
		return isDuplicate;
	}
	
	function createIntellisenseValues(inputArrayValue,inputType){
		var val = {};
		$.each(inputArrayValue, function(i, v) {
			val[v] = v;
		});
		
		RefreshCloneList(inputType, val);
	}
	
});

function keywordsinitialization(){
	  keywordsTitle = [ "Functions", "Functions", "Functions", "Functions",
			"Functions", "Functions", "Functions", "Functions","Functions",
			"Functions", "Functions", "Functions", "Functions","Functions",
			"Functions", "Functions", "Functions", "Functions", "Functions",
			"Functions", "Functions", "Functions", "Functions", "Functions",
			"Functions", "Functions", "Functions", "Functions", "Functions",
			"Functions", "Functions", "Functions", "Functions", "Functions",
			"Functions", "Functions", "Functions", "Functions", "Functions",
			"Functions", "Functions", "Functions", "Functions", "Functions",
			"Functions", "Functions", "Functions", "Functions", "Functions"];

	 keywordsValue = [ "add", "sub", "mul", "div", "pow", "getAge",
			"getDurationOfYear", "split", "format", "min", "max", "sum",
			"Number", "typeof","length", "lookupAsJson",
			"abs", "ceil", "floor", "sqrt", "cbrt", "exp", "formatDecimal",
			"maxOfNValues", "minOfNValues", "splitByRegex", "splitByRegexByLimit",
			"substring", "substringWithEnd", "toUpperCase", "toLowerCase", "trim",
			"ltrim", "rtrim", "indexOf", "indexOfWithStart", "lastIndexOf",
			"lastIndexOfWithStart", "equlasIgnoreCase", "isEmpty","isNull","isEmptyOrNull","isNaN","isUndefined",
			"slice", "sliceWithEnd", "pushArray", "createTable", "createObject"];

	 keywordsReplace = [ "sum(arg1,arg2,argn)", "sub(arg1,arg2,argn)",
			"mul(arg1,arg2,argn)", "div(arg1,arg2,argn)", "pow(a,n)",
			"getAge(childDOB)", "getDurationOfYear(yearsFromNowFundRequired)",
			"split(value)", "format(value)",
			"min(value1,value2)", "max(value1,value2)", "sum(arg1,arg2,argn)",
			"Number(value)", "typeof(value)","length(string)", "lookupAsJson(tablename,columname,condtions)",
			"abs(arg)",
			"ceil(arg)", "floor(arg)", "sqrt(arg)", "cbrt(arg)", "exp(arg)",
			"formatDecimal(number,index)", "maxOfNValues(number1,number2,numbern)",
			"minOfNValues(number1,number2,numbern)",
			"splitByRegex(string,seperator)",
			"splitByRegex(string,seperator,limit)", "substring(string,start)",
			"substring(string,start,end)", "toUpperCase(string)",
			"toLowerCase(string)", "trim(string)", "ltrim(string)",
			"rtrim(string)", "indexOf(string,searchvalue)",
			"indexOf(string,searchvalue,start)", "lastIndexOf(string,searchvalue)",
			"lastIndexOf(string,searchvalue,start)", "equlasIgnoreCase(arg1,arg2)",
			"isEmpty(value)","isNull(value)","isEmptyOrNull(value)","isNaN(value)","isUndefined(string)", "slice(array,start)",
			"slice(array,start,end)", "pushArray(arg1,arg2)",
			"createTable(inoutMap,arrayName,index,key,value)",
			"createObject(inoutMap,objectName,key,value)"];
	 }
// third

$(document).ready(function() {

});

// fix for IE8

$(document).ready(
		function() {
			$(".intelli").keyup(function(e) {
		var code = e.which;
		console.log(code); // recommended to use e.which, it's normalized
							// across browsers

		if (code == 13)
			e.preventDefault();

		if (code == 8) {
			var textboxVal = $(this)[0].value;
			if (textboxVal == '') {
				$(this).empty();
				previousArray[0] = '';
			} else {
				checkText($(this));
			}
		}

		// missing closing if brace
	});

			if (!('map' in Array.prototype)) {
				Array.prototype.map = function(mapper, that /* opt */) {
					var other = new Array(this.length);
					for ( var i = 0, n = this.length; i < n; i++)
						if (i in this)
							other[i] = mapper.call(that, this[i], i, this);
					return other;
				};
			}
			if (!Array.prototype.indexOf) {
				Array.prototype.indexOf = function(elt /* , from */) {
					var len = this.length >>> 0;

					var from = Number(arguments[1]) || 0;
					from = (from < 0) ? Math.ceil(from) : Math.floor(from);
					if (from < 0)
						from += len;

					for (; from < len; from++) {
						if (from in this && this[from] === elt)
							return from;
					}
					return -1;
				};
			}
			
			if (!Array.prototype.forEach) {
				Array.prototype.forEach = function(fun /* , thisp */) {
					var len = this.length;
					if (typeof fun != "function")
						throw new TypeError();

					var thisp = arguments[1];
					for ( var i = 0; i < len; i++) {
						if (i in this)
							fun.call(thisp, this[i], i, this);
					}
				};
			}
			if (!('filter' in Array.prototype)) {
				Array.prototype.filter = function(filter, that /* opt */) {
					var other = [], v;
					for ( var i = 0, n = this.length; i < n; i++)
						if (i in this
								&& filter.call(that, v = this[i], i, this))
							other.push(v);
					return other;
				};
			}


			var customItemTemplate = "<div><span />&nbsp;<small /></div>";

			function elementFactory(element, e) {
				var template = $(customItemTemplate).find('span').text(
						'@' + e.val).end().find('small').text(
						"(" + (e.meta || e.val) + ")").end();
				element.append(template);
			}
			;



			// $('.editable').sew({unique: true, values: repeatedElements,
			// elementFactory: elementFactory});
			// $('.editable-repeat').sew({repeat: false, unique: true, values:
			// repeatedElements, elementFactory: elementFactory});
			// $('.editable-position').sew({repeat: false, unique: true, values:
			// values, elementFactory: elementFactory});
		});