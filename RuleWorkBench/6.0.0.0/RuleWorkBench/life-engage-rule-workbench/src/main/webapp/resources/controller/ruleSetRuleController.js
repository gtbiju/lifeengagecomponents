var groupBOList =[];
var ruleSetID;
var ruleTypeID;
var fromClone = false;
var standardVariableJson;
var canonicalFileName;

var myapp = angular.module('myapp', ['ui.bootstrap','popupdirective']);
myapp.controller('RuleSetController', ['$scope','$modal', '$log','$timeout', function ($scope, $modal,$log,$timeout) {
	$scope.lePopupCtrl = {
            
    }
	
	$scope.showErrorPopupOnPageLoad = function(data) {
		if (data.status==412) {
			$scope.$watch( "lePopupCtrl.showError" , function(n,o){
				if(n==o) {
					return
				};
				$timeout(function(){
					$scope.lePopupCtrl.showError('Rule Workbench',JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details,'OK');
				},0);	
			});
		}				
	}
    $scope.showErrorPopup = function(data) {
    	if (data.status==412) {
			$timeout(function(){
				$scope.lePopupCtrl.showError('Rule Workbench',JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details,'OK');
			},0);	
		}
    }	
	// Special character check for RuleSet Name
	$scope.alpha = function(e) {
		var k;
		document.all ? k = e.keyCode : k = e.which;
		if(!((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57)|| (k==95) || (k==45))){
			e.currentTarget.blur();
			$timeout(function(){
	            $scope.lePopupCtrl.showError('Rule Workbench','Special characters are not allowed.','Ok');

	},0);
		}
		return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57) || (k==95) || (k==45));
	
	};

	if (window.location.href.indexOf("?") != -1) {
		var pairs = window.location.href.substring(window.location.href.indexOf("?")+1).split("&")
		for (var i = 0, len = pairs.length; i < len; i++) {
			pair = pairs[i].split('=');
			if (pair[0] == 'ruleSetId') {
				ruleSetID = pair[1];
			} else if (pair[0] == 'fromClone') {
				fromClone = (pair[1] === 'true');
			} else if (pair[0] == 'ruleType') {
				ruleTypeID = pair[1];
			}
		}
	}
	
	
	getData().done(handleData);
	function getData() {
	    return $.ajax({
						type: "POST",
						url: "../listGroupTable",
						dataType : "json",
						data : { },
						async : false,
						cache : false,
	    });
	}

	function handleData(data) {
		var groupId ;

		groupBOList = data.groupList;
		$scope.debug = data.debugConf;
		canonicalLookupType = data.canonicalLookupType;
		if(data.filedownloadURL) {
			filedownloadURL = data.filedownloadURL;
		}
		
		
		for(var i = 0; i < groupBOList.length; i++){
			if(i==0){
				firstGroupID = groupBOList[i].groupID;
			}
			var groupName = groupBOList[i].groupName;
			
			 groupId = groupBOList[i].groupID;
			
			$(("#groupSel")  + "").append("<option value='"+groupId+"'>" + groupName+ "</option>");
			
			
		}
		
			
		if(ruleSetID == null){		
			getRuleNamesForruleSet(firstGroupID);
		}
	}
	
	function getRuleNamesForruleSet(groupId){
		
		 $.ajax({
				type: "POST",
				url: "../listRuleName",
				dataType: "json", 
				data: {firstGroupID : groupId},
				async: false ,
				cache: false ,
				success: function(data){
								json = data;
							if(json != null)	{ 
							  $('#lstBox1').empty()
							  $.each(json, function(i,j){
								$('#lstBox1').append($('<option></option>').val(j.ruleID).html(j.ruleName));
									 });
									 }
				},
				error : function(data) {
					$scope.showErrorPopupOnPageLoad(data);			
   			}
	       });
		
	}
	
	$( "#groupSel" ).change(function() {
		var groupId = $("#groupSel").val();
		getRuleNamesForruleSet(groupId);
		$('#lstBox2').empty();
		});
	if(ruleSetID != null){
 $.ajax({
  				type: "POST",
  				url: "../listRuleSet",
  				dataType: "json", 
				data: { ruleSetID : ruleSetID, firstGroupID : firstGroupID},
  				async: false ,
  				cache: false ,
  				success: function(data){
  								json = data;
  								for(var i = 0; i < groupBOList.length; i++){
										
										if(data.ruleSetBOList[0].ruleSetGroupName == groupBOList[i].groupID)
											{
												var editGroupID = groupBOList[i].groupID;
												$scope.editGroupName = groupBOList[i].groupName;											
												$("#groupSel option").each(function() {
												if($(this).text() == $scope.editGroupName) {
													$(this).attr('selected', 'selected');            
												}                        
											});
											}
									}
  								$scope.commentTextArea = data.ruleSetBOList[0].comments;
  								if(fromClone==true){
  									$('#ruleSetName').val("CopyOf"+data.ruleSetBOList[0].ruleSetName);
  									$('#ruleSetGroupName').val(data.ruleSetBOList[0].ruleSetGroupName);
									getRuleNamesForruleSet(data.ruleSetBOList[0].ruleSetGroupName);
									$('#groupSel').prop("disabled", true);
  	  							} else {
									ruleSetNameEditMode = data.ruleSetBOList[0].ruleSetName;
  									$('#ruleSetName').val(data.ruleSetBOList[0].ruleSetName);									
									getRuleNamesForruleSet(data.ruleSetBOList[0].ruleSetGroupName);
  	  							}
      						$('#ruleSetId').val(data.ruleSetBOList[0].ruleSetID);
      						if(data.ruleBOList != null)	{
  							  $.each(data.ruleBOList, function(i,j){
  								$('#lstBox2').append($('<option></option>').val(j.ruleID).html(j.ruleName));
  									 });
  									 }
      						
							
 				},
 				error : function(data) {
 					$scope.showErrorPopupOnPageLoad(data);
    				/*
					 * if (data.status==412) {
					 * alert(JSON.parse(data.responseText).message + " : " +
					 * JSON.parse(data.responseText).details); }
					 */				
    			}
	       });
	}
	
	if(ruleSetID != null){
    	$('#lstBox2 option').each(function(i){
    		}); 
    	BuildRuleSetJavascript(); 
    }
	
    $('#btnUp').click(function(e) {
        
        var selectedOpts = $('#lstBox2 option:selected');
        var prevOption = $('#lstBox2 option:selected').prev("option");
        if (selectedOpts.length == 0) {
            alert("Nothing to move.");
            e.preventDefault();
        }
        
        if ($(prevOption).text() != "") {
            $(selectedOpts).remove();
            $(prevOption).before($(selectedOpts));
        }
        e.preventDefault();
    });
    
    $('#btnDown').click(function(e) {
        var selectedOpts = $('#lstBox2 option:selected');
        var nextOption = $('#lstBox2 option:selected').next("option");
        if (selectedOpts.length == 0) {
             alert("Nothing to move.");
             e.preventDefault();
        }
        if ($(nextOption).text() != "") {             
            $(selectedOpts).remove();
            $(nextOption).after($(selectedOpts));
        }
        e.preventDefault();
    });
			
		    
			
		   /*
			 * if(ruleSetID != null){ $('#lstBox2 option').each(function(i){ });
			 * BuildRuleSetJavascript(); }
			 */

		  /*
			 * for(var key in groupBOList){
			 * if($.trim(groupBOList[key].groupName) == $scope.groupName1){
			 * $scope.canonicalFileName =
			 * groupBOList[key].canonicalJsonFileName; } }
			 */
		function getCanonicalJSON(){
			for(var key in groupBOList){
				if($.trim(groupBOList[key].groupName) == $.trim($("#groupSel option:selected").text())){
					canonicalFileName = groupBOList[key].canonicalJsonFileName;
				}
			}
		
			 if(canonicalLookupType == "Remote"){
					var canonicalFileDownloadUrl = filedownloadURL + canonicalFileName;
					$.ajax({
						type: "GET",
						url: canonicalFileDownloadUrl,
						dataType : "json",
						data : { },
						async : false,
						cache : false,
						contentType: 'application/json; charset=UTF-8',
						success : function(data) {
							// standardVariableJson = JSON.stringify(data);
							standardVariableJson = data;
						},
						error : function(data) {
							$scope.showErrorPopupOnPageLoad(data);				
						}
					});
					
				} else if(canonicalLookupType == "Local"){
					$.ajax({
						type: "POST",
						url: "../getCanonicalJSON",
						dataType : "json",
						data : {fileName : canonicalFileName},
						async : false,
						cache : false,
						// contentType: 'application/json; charset=UTF-8',
						success : function(data) {
							// standardVariableJson = JSON.stringify(data);
							standardVariableJson = data;
						},
						error : function(data) {
							$scope.showErrorPopupOnPageLoad(data);				
						}
					});
				}  
		}
		$("#btnSave").live('click',function () {
			var id;
			var isRuleSetNameChanged = false;
			var ruleNames = [];
			var ruleSetName = $('#ruleSetName').val();
			var ruleSetGroupName = $('#groupSel').val();
			var ruleSetId = $('#ruleSetId').val();
			var listSel = $('#lstBox2').val();
			var listLen = $('#lstBox2 option').length;
			var comments = $scope.commentTextArea;
			
		    	if(fromClone==true){
		    		ruleSetId=parseInt("0");
			    }
				if(window.id != null){
					ruleSetId = window.id ;
					if(($.trim(window.ruleSetNameBefEdit.toUpperCase()) != $('#ruleSetName').val().trim().toUpperCase()) || ($.trim(window.groupNameBefEdit.toUpperCase()) != $('#groupSel').val().trim()) || (window.isRuleSetExists == 'true')){
						isRuleSetNameChanged = true;
					}
				} else if(ruleSetId != null && ruleSetId != "" && ruleSetId != 0){
					if($.trim(ruleSetNameEditMode.toUpperCase()) != $('#ruleSetName').val().trim().toUpperCase() || ($.trim($scope.editGroupName).toUpperCase() != $('#groupSel').val().trim())){
		        		isRuleSetNameChanged = true;
		        	}
				 }
			if ((ruleSetName!= "") && (listLen != 0)){
					if (ruleSetGroupName !=""){
						if(comments != "" && comments != null && comments != undefined){
				
							$('#lstBox2').find('option').map(function() {
								ruleNames.push({ ruleId : $(this).val() } );
							});
							getCanonicalJSON();
								var singleInput = [];
								var mutiInput = [];
								var canonicalVariables = {};
								
							 Object.keys(standardVariableJson).forEach(function(keyCode) {
								 Object.keys(standardVariableJson[keyCode]).forEach(function(key) {
									 if(typeof standardVariableJson[keyCode][key] == 'object'){ 
										mutiInput.push($.trim(keyCode)+"."+$.trim(key)) ;
									 } else if(typeof standardVariableJson[keyCode][key]  == 'string'){
										singleInput.push($.trim(keyCode)+"."+$.trim(key));
									 }
								});
							});
							canonicalVariables["SingleInput"]  = singleInput;
							canonicalVariables["MultiInput"] = mutiInput;
							var ruleSetJS = BuildRuleSetJavascript();
							var ruleSetOfflineJS = BuildOfflineRuleSetJavascript();
							
							var ruleSetData = {ruleSetName :ruleSetName, associatedRules :ruleNames, ruleSetJS : ruleSetJS, ruleSetOfflineJS:ruleSetOfflineJS, ruleSetId : ruleSetId, ruleSetGroupName : ruleSetGroupName,canonicalVariables : canonicalVariables,comments : comments};
				
				
				    $.ajax({
				  				type: "POST",
				  				url: "../createRuleSet",
								dataType : "json",
								data : {ruleSetData : $.toJSON(ruleSetData), ruleType: ruleTypeID, isRuleSetNameChanged : isRuleSetNameChanged},
								async : false,
								cache : false,
								beforeSend: function() {
									$('#dvSaveMsg').hide(800);
								},
								success : function(data) {
								json = data;
								var result = json.result;
								if(json.ruleID != null && json.ruleID != 'undefined' && json.ruleID != ""){
									window.id = json.ruleID;
									window.ruleSetNameBefEdit = $('#ruleSetName').val();
									window.groupNameBefEdit = $('#groupSel').val().trim();
									window.isRuleSetExists = json.isRuleSetExists;
								}
								$("#dvSaveMsg").html(result);
								$('#dvSaveMsg').show(800);
								},
								error : function(data) {
									$scope.showErrorPopupOnPageLoad(data);			
				    			}
								});
						} else {
							$timeout(function(){ $scope.lePopupCtrl.showInformation('Rule Workbench','Please enter valid comments.','OK');},0);
						}
					} else {
						 $timeout(function(){ $scope.lePopupCtrl.showInformation('Rule Workbench','Group name cannot be empty"','OK');},0);
						// alert("Group name cannot be empty..!!");
					}
				} else {
	                 // alert("Select rule set name OR rules..!!");
					 $timeout(function(){ $scope.lePopupCtrl.showInformation('Rule Workbench','Select rule set name OR rules..!!"','OK');},0);
	             }
		
					});
	
	
	
	$('#cancel').click(function () {
    	
    	window.location = "../jsp/listBusinessRule.jsp?ruleTypeId=1&ruleSetTypeId=1";
    });
} ]);


//index of fix for IE8

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

//foreach fix for IE8
if (!Array.prototype.forEach) {
	Array.prototype.forEach = function(fun /* , thisp */) {
		var len = this.length;
		if (typeof fun != "function")
			throw new TypeError();

		var thisp = arguments[1];
		for ( var i = 0; i < len; i++) {
			if (i in this)
				fun.call(thisp, this[i], i, this);
		}
	};
}

//trim fix for IE8
if(typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g, ''); 
	  };
}