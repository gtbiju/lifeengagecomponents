﻿function ScrollToNewlyAddedRow(plholder) {
if ($(plholder).offset() != null) {
	$("html, body").animate({
		scrollTop : $(plholder).offset().top - 150
	}, 1500);

	var highlight_color = "#dceff7";
	var original_color = "#e8f3f9";

	$(plholder).animate({
		'background-color' : highlight_color
	}, 'slow', 'swing').delay(1000).animate({
		'background-color' : original_color
	}, 'slow', 'swing');
}
}

jQuery(function($) {
	$("#dlgPreview").dialog({
		resizable : false,
		autoOpen : false,
		height : 500,
		width : 730,
		modal : true
	});
		$("#groupTreeview").dialog({
		resizable : false,
		autoOpen : false,
		height : 500,
		width : 730,
		modal : true
	});

	$(".flip")
			.click(
					function() {
						$(this).next('.panel').slideToggle("slow");
						
						if($(this).find("div.plus_icon").hasClass("plus_icon")){
							$(this).find("div.plus_icon").removeClass("plus_icon").addClass("minus_icon");
						} else if($(this).find("div.minus_icon").hasClass("minus_icon")){ 
							$(this).find("div.minus_icon").removeClass("minus_icon").addClass("plus_icon");
						}
						/*$(this)
								.html(
										$(this).html() == "<div class='pdg_left' style='padding-top:5px;float:left;'>Declare Variables</div><div class='plus_icon'></div>" ? "<div class='pdg_left' style='padding-top:5px;float:left;'>Declare Variables</div><div class='minus_icon'></div>"
												: "<div class='pdg_left' style='padding-top:5px;float:left;'>Declare Variables</div><div class='plus_icon'></div>");*/
	});

	
	$('.addrule').live('click', function() {
		$('.dvIfmenu').not($(this).parent().find('.dvIfmenu')).hide();
		$(this).parent().find('.dvIfmenu').toggle();
	});
	
	$('.aMenu').live(
			'click',
			function() {
				
				$(this).parent().parent().parent().hide();
	});
	
	$('.Preview').live('click',function(e) {
				defaultTreeStruct();
				$("#tree_view_tab").show();
				$("#code_view_tab").hide();
				$("#dvValidationResults").hide();
				$(".preview_txtarea").height(294);
				$("#jstextArea").hide();
				$("#validJS").hide();
				$(".tree well").show();
				$("#dlgPreview").dialog('open');
				$('.preview_tabs  li').removeClass('selected');
				$('.preview_tabs  li:first').addClass('selected');
				
	});
	
	$('.treeView').live('click',function(e) {
		defaultTreeStruct();
		$("#tree_view_tab").show();
		$("#code_view_tab").hide();
		$("#dvValidationResults").hide();
		$(".preview_txtarea").height(294);
		var parent_li = $(this).parent().parent();
		$('.preview_tabs  li').removeClass('selected');
		$(parent_li).addClass('selected');
				
	});
	
	$('.codeView').live('click',function(e) {
		$("#tree_view_tab").hide();
		$("#code_view_tab").show();
		var parent_li = $(this).parent().parent();
		$('.preview_tabs  li').removeClass('selected');
		$(parent_li).addClass('selected');
	});		
			
	
	function defaultTreeStruct(){
			$('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Expand this branch');
			$('.tree li:has(ul)').find(' > ul > li').hide();
			$('.tree li:has(ul)').find(' > span > i').addClass('icon-plus-sign');
	}
	
});
	
	/*
	function hideshow(object) {
		var obj = $(object).parent();
		if(!$(object).hasClass('treeview_check')){
			var children = $(obj).parent('li.parent_li').find(' > ul > li');
			replaceContentInContainer('selectedNode');
			replaceContentInContainer('selectedSpan');
		    if (children.is(":visible")) {
		            children.hide();
		            $(obj).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
					 $(obj).find(' > label').addClass('selectedNode'); 
					$(obj).addClass('selectedSpan');
		    } else {
		            children.show();
		            $(obj).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
					 $(obj).find(' > label').addClass('selectedNode'); 
					$(obj).addClass('selectedSpan');
		     }
		  }
	}*/	
function hideshowTree(object) {
	var obj = $(object).parent();
	if(!$(object).hasClass('treeview_check')){
		var children = $(obj).parent('li.parent_li').find(' > ul > li');
		//replaceContentInContainer('selectedNode');
		//replaceContentInContainer('selectedSpan');
		$('.tree li span').removeClass('selectedSpan');
		
	    if (children.is(":visible")) {
	            children.hide();
	            $(obj).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
				 //$(obj).find(' > label').addClass('selectedNode'); 
				$(obj).addClass('selectedSpan');
	    } else {
	            children.show();
	            $(obj).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
				// $(obj).find(' > label').addClass('selectedNode'); 
				$(obj).addClass('selectedSpan');
	     }
	  }
}

//oldOne
	
	function hideshow(obj) {
		var children = $(obj).parent('li.parent_li').find(' > ul > li');
		replaceContentInContainer('selectedNode');
	    if (children.is(":visible")) {
	            children.hide();
	            $(obj).attr('title', 'Expand this branch').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
				$(obj).find(' > label').addClass('selectedNode');
	    } else {
	            children.show();
	            $(obj).attr('title', 'Collapse this branch').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
				$(obj).find(' > label').addClass('selectedNode');
	        }
	    }
	function replaceContentInContainer(matchClass) {
	    var elems = document.getElementsByTagName('*'), i;
	    for (i in elems) {
	        if((' ' + elems[i].className + ' ').indexOf(' ' + matchClass + ' ')
	                > -1) {
	            elems[i].className="inline";
	        }
	    }
	}
	
	function onPass(dvMessages, result) {
		var output = $("#" + dvMessages);
		output.removeClass("fail");
		output.addClass("pass");
		$("#" + dvMessages).show();
		output.html(result);
	
	}
	
	function onFail(dvMessages) {
		var output = $("#" + dvMessages), template = $("#failOutput");
		output.removeClass("pass");
		output.addClass("fail");
		$("#" + dvMessages).show();
		output.html(template.tmpl(JSHINT.data()));
	}
	
	function BuildRuleSetJavascript() {
		var strJs = "function " + $('#ruleSetName').val().replace(/ /g, '')
				+ "(inoutMap){";
		$('#lstBox2 option').each(function(i) {
			strJs = strJs.concat($(this).text() + "(inoutMap);");
		});
		strJs = strJs.concat("return JSON.stringify(inoutMap);");
		strJs = strJs.concat("}");
		var output = js_beautify(strJs);
		return output;
	}
	
	//commenting old ruleset format
	
	/*function BuildOfflineRuleSetJavascript() {
		var rulesetObj = new Object();
	
		var rule = new Array();
		rulesetObj["type"] = "ruleset";
		var strJs = "function " + $('#ruleSetName').val().replace(/ /g, '')
				+ "(inoutMap, successCB){";
		$('#lstBox2 option').each(function(i) {
	
			rule[i] = $(this).text();
	
			// strJs = strJs.concat($(this).text() + "(inoutMap);");
		});
	
		rulesetObj["rule"] = rule;
		strJs = strJs + "var ruleset = ";
	
		strJs = strJs.concat(JSON.stringify(rulesetObj));
		strJs = strJs.concat("return successCB(ruleset);");
		strJs = strJs.concat("}");
		var output = js_beautify(strJs);
		return output;
	}*/
	
	function BuildOfflineRuleSetJavascript() {
	
		var strJs = "function " + $('#ruleSetName').val().replace(/ /g, '')
				+ "(inoutMap, successCB){ processRuleSet(inoutMap,[";
		$('#lstBox2 option').each(function(i) {
			strJs = strJs.concat('"' + $(this).text() + '",');
		});
		strJs = strJs.concat("successCB]");
		strJs = strJs.concat(");");
		strJs = strJs.concat("}");
		var output = js_beautify(strJs);
		return output;
	}
	
	function checkNumber(value) {
		var legal_chars = /[1-9]+/
		if (value.match(legal_chars))
			return true;
		return false;
	}




