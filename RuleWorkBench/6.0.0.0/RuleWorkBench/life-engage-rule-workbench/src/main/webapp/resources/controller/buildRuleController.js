var DBDetails={};
var ruleJson;
var ruleName;
var groupName;
var ruleNameEditMode;
var standardVariableJson_Illustration;
var standardVariableJson_Req;
var groupBOList =[];
var lllustrationArray = [];
var reqArray = [];
var standardArray = [];
var keywordsValue = [ "add", "sub", "mul", "div", "pow", "getAge",
		"getDurationOfYear", "split", "format", "min", "max", "sum",
		"Number", "typeof","length", "lookup", "lookupWhere", "lookupAsJson",
		"abs", "ceil", "floor", "sqrt", "cbrt", "exp", "formatDecimal",
		"maxOfNValues", "minOfNValues", "splitByRegex", "splitByRegexByLimit",
		"substring", "substringWithEnd", "toUpperCase", "toLowerCase", "trim",
		"ltrim", "rtrim", "indexOf", "indexOfWithStart", "lastIndexOf",
		"lastIndexOfWithStart", "equlasIgnoreCase", "isEmpty","isNull","isEmptyOrNull","isNaN","isUndefined",
		"slice", "sliceWithEnd", "pushArray", "createTable", "createObject"];

var keywordsValueLowerCase = convertToLowerCase(keywordsValue);

function convertToLowerCase(keywordsValue){
	var sorted = [];
	for (var i = 0; i < keywordsValue.length; i++) {
		  sorted.push(keywordsValue[i].toLowerCase());
	}
	return sorted;
}
function setRuleName(name){
	ruleName= name;
}
	
function replaceContentInContainer(matchClass) {
    var elems = document.getElementsByTagName('*'), i;
    for (i in elems) {
        if((' ' + elems[i].className + ' ').indexOf(' ' + matchClass + ' ')
                > -1) {
            elems[i].className="inline";
        }
    }
}	


function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}


//validate rule

function validateJS(ruleJavaScript){
	var options = {
		smarttabs:true,	
		bitwise : true,
		curly : false,
		eqeqeq : false,
		forin : true,
		immed : true,
		latedef : true,
		newcap : false,
		noarg : true,
		noempty : true,
		nonew : true,
		regexp : true,
		undef : false,
		strict : false,
		trailing : true,
		node : true,
		sub : true,
		unused : false,
		expr : false
	};
	var globals = {
		describe : false,
		it : false,
		beforeEach : false,
		afterEach : false
	};

	if (JSHINT(ruleJavaScript, options)) {
		$("#dvValidationResults").removeClass("fail");
		$("#dvValidationResults").addClass("pass");
		$("#dvValidationResults").show();
		$("#dvValidationResults").html("Code checking passed!!");
		$(".preview_txtarea").height(150);
	} else {
		$(".preview_txtarea").height(150);
		onFail("dvValidationResults");
	}


}

		

	


var lookupTable = "";
var constantType = "";

function getPosition(str, m, i) {
   return str.split(m, i).join(m).length;
}

var myapp = angular.module('myapp', ['ui.bootstrap','popupdirective']);

myapp.controller('DialogDemoCtrl', ['$scope','$modal', '$log','$timeout', function ($scope, $modal,$log,$timeout) {
                $scope.lePopupCtrl = {
            
    }

	
	$scope.mulvalStandardInput=[];
	$("a.topopupst1").click(function() {
					loading(); // loading
					loadPopupst(); 
					var htmlstring;
					var htmlString= '';
					
					var jsonObj=jQuery.parseJSON(standardVariableJson);
					$('.accordion').html("");
					for (var key in jsonObj) {
							var obj=jsonObj[key];
							var keyCopy=key;
							$('.accordion').append('<dt class="accordion_elmnt"><a href="" class="accordion_anchor_elmnt">'+key+'<span class="plus_icon"></span></a></dt>');
							htmlString='<dd class="dd_cont">';
							for(key in obj){
							if(obj[key].constructor === Array){
									$scope.mulvalStandardInput.push(keyCopy+"."+key);
							}
							
								htmlString+='<p class="dd_elemnt"><input type="checkbox" value = '+keyCopy+'.'+key+'>'+key+'</input></p>';
							}
							htmlString+='</dd>';
							$('.accordion').append(htmlString);
						}
					
					//make valus in the StandardInput text area selected in the accordian
					var standardInputs = $('#StandardInput').val().split(/\n\r?/g);
					if(standardInputs != ""){
						var chkBoxValueArray =[];
						$.each(standardInputs, function(i, value) {
							chkBoxValueArray.push(value);
						});
						$('#toPopupst1').find($('input[type="checkbox"]')).each(function() {
							 if($.inArray($(this).val(),chkBoxValueArray) !== -1){
								$(this).attr('checked','checked');
							}  
						});
					}
					
					setTimeout(function(){ // then show popup, deley in .5 second
						// function show popup
					}, 500); // .5 second
					  var allPanels = $('.accordion > dd').hide();
					    
					  $('.accordion > dt > a').click(function() {
					    allPanels.slideUp();
					    
					    //plus,minus icon show hide function
					    if($(this).find('span').hasClass("plus_icon")){
							$('.accordion > dt > a').find('span').removeClass("minus_icon").addClass("plus_icon");
							$(this).find("span").removeClass("plus_icon").addClass("minus_icon");
						} else if($(this).find('span').hasClass("minus_icon")) {
							$(this).find("span").removeClass("minus_icon").addClass("plus_icon");
						}
					    
					    $(this).parent().next().toggle();
					    return false;
					  });
			return false;
			});

			$(this).keyup(function(event) {
				if (event.which == 27) { // 27 is 'Ecs' in the keyboard
					disablePopupst();  // function close pop up
				}
			});

		        $("div#backgroundPopupst1").click(function() {
				disablePopupst();  // function close pop up
			});

			$('a.livebox').click(function() {
			return false;
			});

			 /************** start: functions. **************/
			function loading() {
				$("div.loaderst1").show();
			}
			function closeloading() {
				$("div.loaderst1").fadeOut('normal');
			}
			function loadPopupst() {
				
				closeloading(); // fadeout loading
				$("#toPopupst1").fadeIn(0500); // fadein popup div
				$("#backgroundPopupst1").css("opacity", "0.7"); // css opacity, supports IE7, IE8
				$("#backgroundPopupst1").fadeIn(0001);					
			}

			function disablePopupst() {						
					$("#toPopupst1").fadeOut("normal");
					$("#backgroundPopupst1").fadeOut("normal");
					
			}
			/************** end: functions. **************/
			
			$('.close_pop_up').click(function(){	 
				disablePopupst();
				});
	
	//Special character check for Rule Name
	$scope.alpha = function(e) {
		var k;
		document.all ? k = e.keyCode : k = e.which;
		if(!((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57)|| (k==95) || (k==45))){
			e.currentTarget.blur();
			$timeout(function(){
	            $scope.lePopupCtrl.showError('Rule Workbench','Special characters are not allowed.','Ok');

	},0);
		}
		return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57) || (k==95) || (k==45));
	
	};
	
	$scope.variableSpecCharCheck = function(e) {
		var k;
		document.all ? k = e.keyCode : k = e.which;
		if(!((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57)|| (k==95) || (k==45) || k==13)){
			e.currentTarget.blur();
			$timeout(function(){
	            $scope.lePopupCtrl.showError('Rule Workbench','Special characters are not allowed.','Ok');

	},0);
		}
		return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57) || (k==95) || (k==45) || k==13);
	
	};
	
	$scope.showErrorPopupOnPageLoad = function(data) {
		if (data.status==412) {
			$scope.$watch( "lePopupCtrl.showError" , function(n,o){
				if(n==o) {
					return
				};
				$timeout(function(){
					$scope.lePopupCtrl.showError('Rule Workbench',JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details,'OK');
				},0);	
			});
		}				
	}
    $scope.showErrorPopup = function(data) {
    	if (data.status==412) {
			$timeout(function(){
				$scope.lePopupCtrl.showError('Rule Workbench',JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details,'OK');
			},0);	
		}
    }
	
    $scope.getCanonicalJson = function() {
	    if($.inArray($scope.groupName1, lllustrationArray) > -1){
						standardVariableJson = standardVariableJson_Illustration;
					} else if($.inArray($scope.groupName1, reqArray) > -1) {
						standardVariableJson = standardVariableJson_Req;
					} else {
						standardVariableJson = standardVariableJson_Illustration;
					}
	 }
	
	$scope.changeData = function() {
		$scope.paintStandardVaribles();
    };
    
    
    $scope.paintStandardVaribles = function(){
    	for(var key in groupBOList){
			if($.trim(groupBOList[key].groupName) == $scope.groupName1){
				$scope.canonicalFileName = groupBOList[key].canonicalJsonFileName;
			}
		}
	    if($scope.canonicalLookupType == "Remote"){
	    	
	    	
	    	
	    	var canonicalFileDownloadUrl = $scope.filedownloadURL+$scope.canonicalFileName;
	    	$.ajax({
				type: "GET",
			    url: canonicalFileDownloadUrl,
				dataType : "json",
				data : { },
				async : false,
				cache : false,
				contentType: 'application/json; charset=UTF-8',
				success : function(data) {
					standardVariableJson = JSON.stringify(data);
				},
				error : function(data) {
					$scope.showErrorPopupOnPageLoad(data);				
				}
			});
	    	
			
		} else if(($scope.canonicalLookupType == "Local")){
			
			$.ajax({
				type: "POST",
			    url: "../getCanonicalJSON",
				dataType : "json",
				data : {fileName : $scope.canonicalFileName},
				async : false,
				cache : false,
				//contentType: 'application/json; charset=UTF-8',
				success : function(data) {
					standardVariableJson = JSON.stringify(data);
				},
				error : function(data) {
					$scope.showErrorPopupOnPageLoad(data);				
				}
			});
		}
    
	};
    
    
	// List Group
	$scope.listGroupTable = function(){
		var groupListArr=[];
		$.ajax({
			type: "POST",
		    url: "../listGroupTable",
			dataType : "json",
			data : { },
			async : false,
			cache : false,												
			success : function(data) {			
				groupBOList = data.groupList;
				$scope.debug = data.debugConf;
				$scope.canonicalLookupType = data.canonicalLookupType;
				if(data.filedownloadURL) {
					$scope.filedownloadURL = data.filedownloadURL;
				}
				
				//standardVariableJson_Illustration = data.standardVariable_Illustration;
				//standardVariableJson_Req = data.standardVariable_Req;
				for(var i = 0; i < groupBOList.length; i++){
					groupListArr.push(groupBOList[i].groupName);	
				}
				if(ruleId == null || ruleId == ''){		
					$scope.defaultGrpName=groupListArr[0];
					$scope.groupName1=$scope.defaultGrpName;
					$scope.paintStandardVaribles();
				} else {
					$scope.paintStandardVaribles();
				}
			},
			error : function(data) {
				$scope.showErrorPopupOnPageLoad(data);				
			}
		});
			
		return groupListArr;
	}
	$scope.RuleJsonLookup = function(ruleId,clone){	
		if(clone!=null){
	    	var fromClone = clone;			               	
	       }
		if(ruleId != null){
		$.ajax({
			type: "POST",
		    url: "../editRule",
		    dataType : "json",
			data : { ruleID : ruleId, fromCopy : fromClone },
			async : false,
			cache : false,							
			success : function(data) {						
				var ruleBO = data;
				$scope.defaultGrpName=ruleBO.groupName;
				$scope.groupName1=$scope.defaultGrpName;
				$scope.commentTextArea = data.comments;
				//$scope.groupName1=ruleBO.groupName;	
				groupName=ruleBO.groupName;
				
				ruleName = ruleBO.ruleName;
	            if(fromClone==true || fromClone=="true"){
	            	ruleName = "CopyOf"+ruleBO.ruleName;            
	            }	
				ruleNameEditMode = 	ruleName;				
				$scope.ruleName1=ruleName;		
				//var tempruleJson = (ruleBO.ruleJson).replace(/'/g,'"');
				ruleJson = JSON.parse(ruleBO.ruleJson);
			},
			error : function(data) {
				$scope.showErrorPopupOnPageLoad(data);				
			}
			});
		}
	}
	
	
	
	$scope.allLookupTableDetails = function() {
		var tableList = [];
	    $.ajax({
				type: "POST",
			    url: "../tableLoader",
				async : false,
				cache : false,							
				success : function(data) {					
					for(var i=0; i<data.length;i++){
						tableList[i] = data[i].tableName;
					}
					DBDetails = data;
				},
				error : function(data) {
    				$scope.showErrorPopupOnPageLoad(data);				
    			}
				});
			
			 return tableList;
	}

	
	ruleId ="";
	clone =false;
	ruleTypeId =false;
	var pair;
	if (window.location.href.indexOf("?") != -1) {
		var pairs = window.location.href.substring(window.location.href.indexOf("?")+1).split("&")
		for (var i = 0, len = pairs.length; i < len; i++) {
			pair = pairs[i].split('=');
			if (pair[0] == 'ruleId') {
				ruleId = pair[1];
			} else if (pair[0] == 'fromClone') {
				clone = (pair[1] === 'true');
			} else if (pair[0] == 'ruleTypeId') {
				ruleTypeId = pair[1];
			}
		}
	}

	if(window.location.href.indexOf('flag')==-1){
		$scope.RuleJsonLookup(ruleId,clone);
		if($scope.groupName1 == undefined){
			$scope.groupName1 = groupName;
		}
		$scope.rule = ruleJson;
		$scope.listGroup=$scope.listGroupTable();
		//$scope.listGroup.push(groupName);alert("[][][ "+$scope.listGroup);
		//$scope.groupName=groupName;
	}else{
		$scope.rule={ 'Variables': { 'SingleInput':[],'SingleOutput':[],'MultiInput':[],'MultiOutput':[],'StandardInput':[],'FunctionInput':[]},'Rules': {'Rule': []}};
		$scope.listGroup=$scope.listGroupTable();
	}
	
	build();
    $scope.selectedNode = "";

	
	
	function build() {
		
		
		$scope.rulePart = $scope.rule.Rules.Rule;
		$scope.operators =["","=","==","!=",">","<",">=","<="]; 
		$scope.connector =["And","Or"]; 
		$scope.valueType =["","Variable","Constant","Custom"];
		$scope.startItems = ["IF","Loop","Statement","Code Block","LookUp","Function"];
		$scope.items = ["IF","Else","Else If","Loop","Statement","Code Block","LookUp","Function"]; 
		$scope.AddActionLoopItems = ["IF","Else","Else If","Statement","Code Block","LookUp","Function"];
		$scope.choice1="0";
		var TableName = "";
		$scope.tableList=$scope.allLookupTableDetails();
		$scope.ruleName1="";
		$scope.groupName1="";
		
		$scope.DBDetailsList=DBDetails;
		$scope.singleInputValue="";
		var singleInputObj = {};
		for(var i=0;i<$scope.rule.Variables.SingleInput.length;i++){
			$scope.singleInputValue+=$scope.rule.Variables.SingleInput[i]+"\n";
			singleInputObj[$scope.rule.Variables.SingleInput[i]]=$scope.rule.Variables.SingleInput[i];
		}	
		RefreshCloneList('SingleInput', singleInputObj);
		$scope.singleOutputValue="";
		var SingleOutputObj = {};
		for(var i=0;i<$scope.rule.Variables.SingleOutput.length;i++){
			$scope.singleOutputValue+=$scope.rule.Variables.SingleOutput[i]+"\n";
			SingleOutputObj[$scope.rule.Variables.SingleOutput[i]]=$scope.rule.Variables.SingleOutput[i];
		}
		RefreshCloneList('SingleOutput', SingleOutputObj);
		$scope.multiValuedInput="";
		var MultiInputObj = {};
		for(var i=0;i<$scope.rule.Variables.MultiInput.length;i++){
			$scope.multiValuedInput+=$scope.rule.Variables.MultiInput[i]+"\n";
			MultiInputObj[$scope.rule.Variables.MultiInput[i]]=$scope.rule.Variables.MultiInput[i];
		}
		RefreshCloneList('MultiInput', MultiInputObj);		
		$scope.multiOutputValue="";
		var MultiOutputObj = {};
		for(var i=0;i<$scope.rule.Variables.MultiOutput.length;i++){
			$scope.multiOutputValue+=$scope.rule.Variables.MultiOutput[i]+"\n";
			MultiOutputObj[$scope.rule.Variables.MultiOutput[i]]=$scope.rule.Variables.MultiOutput[i];
		}
		RefreshCloneList('MultiOutput', MultiOutputObj);		
		$scope.functionInputValue="";
		var FunctionInputObj = {};
		if($scope.rule.Variables.FunctionInput != undefined){
		for(var i=0;i<$scope.rule.Variables.FunctionInput.length;i++){
			$scope.functionInputValue+=$scope.rule.Variables.FunctionInput[i]+"\n";
			FunctionInputObj[$scope.rule.Variables.FunctionInput[i]]=$scope.rule.Variables.FunctionInput[i];
			}
		}
		RefreshCloneList('FunctionInput', FunctionInputObj);	
		$scope.standardInputValue="";
		var StandardInputObj = {};
		if($scope.rule.Variables.StandardInput != undefined){
			if(typeof $scope.rule.Variables.StandardInput[0] == 'string') {
				for(var i=0;i<$scope.rule.Variables.StandardInput.length;i++){
					$scope.standardInputValue+=$scope.rule.Variables.StandardInput[i]+"\n";
					StandardInputObj[$scope.rule.Variables.StandardInput[i]]=$scope.rule.Variables.StandardInput[i];
				}
			} else if(typeof $scope.rule.Variables.StandardInput[0] == 'object') {
				$.each($scope.rule.Variables.StandardInput[0], function (key, value) {
				  $scope.standardInputValue += key+"\n";
				  StandardInputObj[key]=key;
				});
			}
		}
		RefreshCloneList('StandardInput', StandardInputObj);			
		if($scope.ruleName1 == ""){
            $scope.ruleName1=ruleName;
		}
		if($scope.groupName1 == ""){
           $scope.groupName1 = groupName;
		}
    }
	

	$scope.del = function(leaf,branch) {
		branch = branch||$scope.tree;
        var index = branch.indexOf(leaf);
        if(index!=-1){
            branch.splice(index,1);
        }
    };
	$scope.js="";
	//$scope.js1="";
	$scope.displayTree ="";
	
	/////////////////////////////////////////
$scope.showNode = function(data) {
		return data.type;
    };
	
$scope.constructRuleTree = function(ruleJson) {	
	var treeNode =[];
	$(ruleJson).each(function (i, v) {
		if(this.type =='Statement'){
			var statementsNode = [];  
			if(this.Actions != undefined) {
				$(this.Actions.Action).each(function (i, v) {
					statementsNode = constructNodeTree(v);
					
				});
				
			}
			treeNode.push(statementsNode);
		} else { 
			var blockCond = constructNodeTree(v);	
			treeNode.push(blockCond);
			
		} 
	

	});
		
		$scope.displayTree = [{"type":$scope.ruleName1,"nodes":treeNode}];
		return $scope.displayTree;
}
	
	 	function constructNodeTree(rule){
	
		if(rule.type =='Statement'){
			if(rule.Statement){
					var statements = [];
					$(rule.Statement).each(function (i, v) {
						var dispalyText = "";
						dispalyText = v.lhs+" = "+v.rhs;
						var statementNode = {"type":dispalyText,"showTextBox":false,"value":"","nodes":[]};
						statements.push(statementNode);
					
					});
				}
			statementBlock = {"type":"Statement Block","showTextBox":false,"value":"","nodes":statements};
			return statementBlock;
		
		} else if(rule.type =='IF'){
			var ifNode = [];
			if(rule.Conditions != undefined){
				var conditionText = "";
				$(rule.Conditions.Condition).each(function (i, v) {	
					var ifConnector;
					if(v.connector == 'And'){
						ifConnector = '&&';
					} else if(v.connector == 'Or'){
						ifConnector = '||';
					} else {
						ifConnector = "";
					};
					conditionText = conditionText+"("+v.lhs+v.operator+v.rhs+")"+ifConnector;
				
				});				
			}
			
			if(rule.Actions != undefined){
				var actions = [];
					$(rule.Actions.Action).each(function (i, v) {
						var actionNode = constructNodeTree(v);
						ifNode.push(actionNode);
						
					});
			}
			if(rule.Statement){
				var statements = [];
				$(rule.Statement).each(function (i, v) {
					var displayText = "";
					dispalyText = v.lhs+" = "+v.rhs;
					var statementNode = {"type":dispalyText,"showTextBox":false,"value":"","nodes":[]};
					statements.push(statementNode);
				
				});
				
				var statementBlockNode = {"type":"Statement Block","showTextBox":false,"value":"","nodes":statements};
				ifNode.push(statementBlockNode);
			
			}
			var displayText = "if ("+conditionText+")";
			var ifBlock= {"type":displayText,"showTextBox":false,"value":"","nodes":ifNode};
			
		 return ifBlock;
		 
		} else if(rule.type =='ElseIF'){
			var elseIfNode =[];
			if(rule.Conditions != undefined){
				var conditionText = "";
				$(rule.Conditions.Condition).each(function (i, v) {	
					var elseIfConnector;
					if(v.connector == 'And'){
						elseIfConnector = '&&';
					} else if(v.connector == 'Or'){
						elseIfConnector = '||';
					} else {
						elseIfConnector = "";
					}
					
					conditionText = conditionText+"("+v.lhs+v.operator+v.rhs+")"+ elseIfConnector;
				
				});
			}
			
			if(rule.Actions != undefined){
				var actions = [];
					$(rule.Actions.Action).each(function (i, v) {
						var actionNode = constructNodeTree(v);
						elseIfNode.push(actionNode);
						
					});
			}
			if(rule.Statement){
				var statements = [];
				$(rule.Statement).each(function (i, v) {
					var dispalyText = "";
					dispalyText = v.lhs+" = "+v.rhs;
					var statementNode = {"type":dispalyText,"showTextBox":false,"value":"","nodes":[]};
					statements.push(statementNode);
				
				});
				var statementBlockNode = {"type":"Statement Block","showTextBox":false,"value":"","nodes":statements};
				elseIfNode.push(statementBlockNode);
			}
			var displayText = "else if ("+conditionText+")";
			var elseIfBlock = {"type":displayText,"showTextBox":false,"value":"","nodes":elseIfNode};
			
		 return elseIfBlock;			
			
		} else if(rule.type =='Else'){
			var elseNode = [];
			if(rule.Actions != undefined){
					var actions = [];
						$(rule.Actions.Action).each(function (i, v) {
							var actionNode = constructNodeTree(v);
							elseNode.push(actionNode);
							
						});
				}
			if(rule.Statement){
				var statements = [];
				$(rule.Statement).each(function (i, v) {
					var dispalyText = "";
					dispalyText = v.lhs+" = "+v.rhs;
					var statementNode = {"type":dispalyText,"showTextBox":false,"value":"","nodes":[]};
					statements.push(statementNode);
				
				});
				var statementBlockNode = {"type":"Statement Block","showTextBox":false,"value":"","nodes":statements};
				elseNode.push(statementBlockNode);
			}
				
				var elseBlock = {"type":"else","showTextBox":false,"value":"","nodes":elseNode};
			return elseBlock;
				
		} else if(rule.type == 'Loop'){
			var loopNode =[];
			var displayText = "for(i="+rule.start+";i<="+rule.limit+";i=i+"+rule.step+")";
			if(rule.Actions != undefined){
				var actions = [];
				$(rule.Actions.Action).each(function (i, v) {
					var actionNode = constructNodeTree(v);
					loopNode.push(actionNode);
				});
			}
			if(rule.Statement){
				var statements = [];
				$(rule.Statement).each(function (i, v) {
					var displayText = "";
					dispalyText = v.lhs+" = "+v.rhs;
					var statementNode = {"type":dispalyText,"showTextBox":false,"value":"","nodes":[]};
					statements.push(statementNode);
				
				});
				
				var statementBlockNode = {"type":"Statement Block","showTextBox":false,"value":"","nodes":statements};
				loopNode.push(statementBlockNode);
			
			}
			var loopBlock = {"type":displayText,"showTextBox":false,"value":"","nodes":loopNode};
			return loopBlock;
		
		} else if(rule.type == 'CodeBlock'){
			var codeBlockNode = [{"type":"Code","showTextBox":false,"showTextArea":true,"value":rule.Code,"nodes":[]}];
			var codeBlock = {"type":"Code Block","showTextBox":false,"value":rule.Code,"nodes":codeBlockNode};
			return codeBlock;
		
		} else if(rule.type == 'Function'){
			if(rule.Functions != undefined){
				var functionsNode =[];
				$(rule.Functions).each(function (i, v) {
					var displayText = "";
					displayText = "function "+v.functionName+"(inoutMap";
					if(v.params != undefined && v.params != ""){
						displayText = displayText + ", " +v.params;
					}
					displayText = displayText + ")";
					var functionNode = {"type":displayText,"showTextBox":false,"value":"","nodes":[]};
					functionsNode.push(functionNode);
					
				});
			}
			var functionBlock = {"type":"Function Block","showTextBox":false,"value":"","nodes":functionsNode};		
			return functionBlock;
			
		} else if(rule.type == 'LookUp'){
			var looKupBlkNode = [];
			var lhs = {"type":"lhs","showTextBox":true,"value":rule.lhs,"nodes":[]};
			var tableName ={"type":"tableName","showTextBox":true,"value":rule.tableName,"nodes":[]};
			var columnName;
			var columnVal = lookupKeyWordCheck(rule.columnName);
			if (rule.dbFunction == "" || typeof rule.dbFunction  == "undefined") {
			
				//columnName = {"type":"columnName","showTextBox":true,"value":rule.columnName,"nodes":[]};
				columnName = {"type":"columnName","showTextBox":true,"value":columnVal,"nodes":[]};
			} else { 
				//column = rule.dbFunction+ "(" + rule.columnName + ")";
				column = rule.dbFunction+ "(" + columnVal + ")";
				columnName = {"type":"columnName","showTextBox":true,"value":column,"nodes":[]};
			}
			looKupBlkNode.push(lhs);
			looKupBlkNode.push(tableName);
			looKupBlkNode.push(columnName);
			var conditionsNode = {};
			if(rule.Conditions != undefined){
					var conditions = [];
					$(rule.Conditions.Condition).each(function (i, v) {
						var conditionText = "";
						var columnVal = lookupKeyWordCheck(v.columnName);
						var columnName = {"type":"columnName","showTextBox":true,"value":columnVal,"nodes":[]};
						var operator = {"type":"operator","showTextBox":true,"value":v.operator,"nodes":[]};
						var valueTypeField = {"type":"valueType","showTextBox":true,"value":v.valueTypeField,"nodes":[]};
						var value = {"type":"value","showTextBox":true,"value":v.value,"nodes":[]};
						//conditionText = v.columnName+" "+ v.operator+" "+v.value;
						conditionText = columnVal+" "+ v.operator+" "+v.value;
						var conditionNode = {"type":conditionText,"showTextBox":false,"value":"","nodes":[]};
						conditions.push(conditionNode);
					});
					conditionsNode = {"type":"Conditions","showTextBox":false,"value":"","nodes":conditions};
				}
				looKupBlkNode.push(conditionsNode);
				var orderBy ={};
				if(rule.orderBy != null && rule.orderBy != undefined && rule.orderBy !=""){
					var orderByValue = lookupKeyWordCheck(rule.orderBy);
					var orderBy = {"type":"orderBy","showTextBox":true,"value":orderByValue,"nodes":[]};
					//var orderBy = {"type":"orderBy","showTextBox":true,"value":rule.orderBy,"nodes":[]};
					looKupBlkNode.push(orderBy);
				}
				
				var lookupBlock = {"type":"LookUp Block","showTextBox":false,"value":"","nodes":looKupBlkNode};
			return lookupBlock;
			}

}
	
	function lookupKeyWordCheck(columnVal){
		var keyWordArray =["ORDER"];
		if($.inArray(columnVal.toUpperCase(), keyWordArray) != -1) {
			columnVal = "`"+columnVal+"`";
		}
	 return columnVal;
	}
	
	////////////////////////////////////
	
	
	

	$scope.construct = function(node, isOnline)
    {	
		$scope.isLoopExists =false;
		$scope.js1="";
		if(typeof isOnline == "undefined") {
			isOnline = true;
		}
		if($scope.ruleName1 == ""){
						$scope.ruleName1=ruleName;
		}
		if($scope.groupName1 == ""){
						$scope.groupName1 = groupName;
		}
		$scope.js = "function "+$scope.ruleName1+"(inoutMap"; //rule testing
		//$scope.js = "function "+$scope.ruleName1+"(inoutMap";
		if($scope.rule.Variables.FunctionInput != undefined){
			if($scope.rule.Variables.FunctionInput.length>0){
							for(var i=0;i<$scope.rule.Variables.FunctionInput.length;i++){
											if($scope.rule.Variables.FunctionInput[i].trim().length>0){
															$scope.js+= ", "+$scope.rule.Variables.FunctionInput[i];
											}
							}
			}
		}
		if (!isOnline) {			
			$scope.js = $scope.js+",successCB"; //rule testing
		}
		$scope.js+="){ ";
		for(var i=0;i<$scope.rule.Variables.SingleOutput.length;i++){
			if ($scope.rule.Variables.SingleOutput[i] != "") {
				$scope.js+= "var "+$scope.rule.Variables.SingleOutput[i]+";";
			}
		}
		for(var i=0;i<$scope.rule.Variables.MultiOutput.length;i++){
			if ($scope.rule.Variables.MultiOutput[i] != "") {
				$scope.js+= "var "+$scope.rule.Variables.MultiOutput[i]+" = [];";
			}
		}
		
		
		constructJs(node, isOnline);
		if($scope.isLoopExists) {
			$scope.js+= "var i;";
		}
		$scope.js+=$scope.js1;
		for(var i=0;i<$scope.rule.Variables.SingleOutput.length;i++){
			if ($scope.rule.Variables.SingleOutput[i] != "") {
				$scope.js+= "updateJSON(inoutMap, '"+$scope.rule.Variables.SingleOutput[i]+"',"+$scope.rule.Variables.SingleOutput[i]+");";
			}
		}
		
		for(var i=0;i<$scope.rule.Variables.MultiOutput.length;i++){
			if ($scope.rule.Variables.MultiOutput[i] != "") {
				$scope.js+= "updateJSON(inoutMap, '"+$scope.rule.Variables.MultiOutput[i]+"',"+$scope.rule.Variables.MultiOutput[i]+");";
			}
		}
		//$scope.js += "return JSON.stringify(inoutMap);}";
		if (isOnline) {
			$scope.js += "return JSON.stringify(inoutMap);}";	//rule testing
		} else{
			$scope.js += "successCB(inoutMap);}";	//rule testing
		}
		$scope.js = js_beautify($scope.js, null);
		//$("#dlgPreview").dialog('open');
		
		return $scope.js;	//for save rule function
    }


	function constructJs(node, isOnline) {
			for(var i = 0;i<node.length;i++){
				if(node[i].type=='Statement'){
					if(node[i].Actions!=undefined){
						//$scope.js=$scope.js+"\n"+tab+node[i].Actions.Action[0].Statement[0].lhs+" = "+node[i].Actions.Action[0].Statement[0].rhs+";";
						constructJs(node[i].Actions.Action, isOnline);
					}else if(node[i].Statement!=undefined){
						for(j=0;j<node[i].Statement.length;j++){
							$scope.js1=$scope.js1+node[i].Statement[j].lhs+" = "+node[i].Statement[j].rhs+";";
						}
					}
				}else if(node[i].type=='IF'){
					$scope.js1=$scope.js1+"if(";
					if(node[i].Conditions!=undefined){
						for(j=0;j<node[i].Conditions.Condition.length;j++){
							var ifConnector;
							var connectorVal = node[i].Conditions.Condition[j].connector;
							if(connectorVal == 'And'){
								ifConnector = "&&";
							} else if(connectorVal == 'Or'){
								ifConnector = "||";
							} else {
								ifConnector = "";
							}
							
							$scope.js1=$scope.js1+node[i].Conditions.Condition[j].lhs+node[i].Conditions.Condition[j].operator+node[i].Conditions.Condition[j].rhs+" "+ifConnector+" ";
						}
					}
					$scope.js1=$scope.js1+"){"
					if(node[i].Actions!=undefined){
						constructJs(node[i].Actions.Action, isOnline);
					}
					if(node[i].Statement){
						$(node[i].Statement).each(function (i, v) {
							$scope.js1=$scope.js1+v.lhs+" = "+v.rhs+";";
						});
					}
					$scope.js1=$scope.js1+"}";
				}else if(node[i].type=='ElseIF'){
					$scope.js1=$scope.js1+"else if(";
					if(node[i].Conditions!=undefined){
						for(j=0;j<node[i].Conditions.Condition.length;j++){
							var elseIfConnector;
							var connectorVal = node[i].Conditions.Condition[j].connector;
							if(connectorVal == 'And'){
								elseIfConnector = "&&";
							} else if(connectorVal == 'Or'){
								elseIfConnector = "||";
							} else {
								elseIfConnector ="";
							}
							$scope.js1=$scope.js1+node[i].Conditions.Condition[j].lhs+node[i].Conditions.Condition[j].operator+node[i].Conditions.Condition[j].rhs+" "+elseIfConnector+" ";
						}
					}
					$scope.js1=$scope.js1+"){"
					if(node[i].Actions!=undefined){
						constructJs(node[i].Actions.Action, isOnline);
					}
					if(node[i].Statement){
						$(node[i].Statement).each(function (i, v) {
							$scope.js1=$scope.js1+v.lhs+" = "+v.rhs+";";
						});
					}
					$scope.js1=$scope.js1+"}";
				}else if(node[i].type=='Else'){
					$scope.js1=$scope.js1+"else {";
					if(node[i].Actions!=undefined){
						constructJs(node[i].Actions.Action, isOnline);
					}
					if(node[i].Statement){
						$(node[i].Statement).each(function (i, v) {
							$scope.js1=$scope.js1+v.lhs+" = "+v.rhs+";";
						});
					}
					$scope.js1=$scope.js1+"}"
				}else if(node[i].type=='Loop'){
					$scope.isLoopExists = true;
					$scope.js1=$scope.js1+"for(i="+node[i].start+";i<="+node[i].limit+";i=i+"+node[i].step+"){";
					if(node[i].Actions!=undefined){
						
						constructJs(node[i].Actions.Action, isOnline);
					}
					if(node[i].Statement){
						$scope.js1=$scope.js1+node[i].Statement[0].lhs+" = "+node[i].Statement[0].rhs+";";
					}
					$scope.js1=$scope.js1+"}";
				}else if(node[i].type=='CodeBlock'){
					$scope.js1=$scope.js1+node[i].Code;
				}else if(node[i].type=='LookUp'){
					$scope.js1=$scope.js1+node[i].lhs+" = 0;";
					if (isOnline) {
						$scope.js1=$scope.js1+node[i].lhs+" = "
					}
					$scope.js1=$scope.js1+"lookupAsJson(\"";
					if (node[i].dbFunction !="" && typeof node[i].dbFunction  != "undefined") {
						$scope.js1=$scope.js1+node[i].dbFunction + "(";
					}
					//$scope.js1=$scope.js1 +node[i].columnName
					var columnName = lookupKeyWordCheck(node[i].columnName);
					$scope.js1=$scope.js1 + columnName;
					if (node[i].dbFunction !="" && typeof node[i].dbFunction  != "undefined") {
						$scope.js1=$scope.js1+ ")";
					}
					$scope.js1 = $scope.js1+"\",\""+node[i].tableName+"\"";
					for(j=0;j<node[i].Conditions.Condition.length;j++){
						var columnName = lookupKeyWordCheck(node[i].Conditions.Condition[j].columnName);
						//$scope.js1=$scope.js1+",\""+node[i].Conditions.Condition[j].columnName+node[i].Conditions.Condition[j].operator;
						$scope.js1=$scope.js1+",\""+columnName+node[i].Conditions.Condition[j].operator;
						if(node[i].Conditions.Condition[j].valueTypeField=="Variable"){
							$scope.js1=$scope.js1+"'\"+"+node[i].Conditions.Condition[j].value+"+\"\'";
						}else if(node[i].Conditions.Condition[j].valueTypeField=="Constant"){
							if (checkNumber(node[i].Conditions.Condition[j].value)) {
								$scope.js1=$scope.js1+node[i].Conditions.Condition[j].value;
							}
							else{
								$scope.js1=$scope.js1+"'"+node[i].Conditions.Condition[j].value+"'";
							}
						} else if(node[i].Conditions.Condition[j].valueTypeField=="Custom"){
							$scope.js1=$scope.js1+" "+node[i].Conditions.Condition[j].value;
						}
						if(j == node[i].Conditions.Condition.length-1 && node[i].orderBy!=0){
							var orderByVal = lookupKeyWordCheck(node[i].orderBy);
							$scope.js1=$scope.js1+" ORDER BY "+orderByVal;
						}
						$scope.js1=$scope.js1+"\"";
					}
					
					if (isOnline) {
						$scope.js1=$scope.js1+");"
					} else{
						$scope.js1=$scope.js1+",cont("+node[i].lhs+",err));"						
					}
				}else if(node[i].Statement!=undefined){
					$scope.js1=$scope.js1+node[i].Statement[0].lhs+" = "+node[i].Statement[0].rhs+";";
				}else if(node[i].Functions!=undefined){
					for(j=0;j<node[i].Functions.length;j++){
						$scope.js1=$scope.js1+node[i].Functions[j].functionName+"(inoutMap";
						if(node[i].Functions[j].params != undefined && node[i].Functions[j].params != ""){
							$scope.js1 = $scope.js1 + ", " + node[i].Functions[j].params;
						}
						if (!isOnline) {
							$scope.js1 = $scope.js1 + ",cont(inoutMap,err)";
						}
						$scope.js1 = $scope.js1 + ");";
					}
				}
				//alert("node[i] "+angular.toJson(node[i])+" node[i].Statement "+node[i].Statement);
			}
			
    };

    $scope.addActionDropDownList =function (data) {
    	var items=[];
    	if(data=="IF" || data=="Else" || data=="ElseIF"){
			items = ["IF","Else If","Else","Loop","Statement","Code Block","LookUp","Function"];  
		} else if(data=="Loop"){
			items = ["IF","Else If","Else","Statement","Code Block","LookUp","Function"];
		}
    	
    	return items;
    }
    
    $scope.dbFunctions =function (data) {    	
    	return ["MAX","COUNT"];
    }

	$scope.dropDownList =function (data) {
		if(data=="Add"){
			$scope.items = ["IF","Else","Else If","For","Statement","Code Block","lookup"]; 
		}else if(data.type=="IF" || data.type=="ElseIF"){
			$scope.items = ["Condition"]; 
		}else if(data.type=="Condition Block"){
			$scope.items = ["Lookup Condition"]; 
		}else if(data=="Start"){
			$scope.items = ["IF","Else","Else If"]; 
		}else{
			$scope.items = ["IF","Else","Else If","Loop","Statement","Code Block","lookup"]; 
		}
	}
	
	$scope.addCondition =function (data) {//AddIntellisense();  
		if(data.type=="IF" || data.type=="ElseIF"){
		if(data.Conditions.Condition.length>0)
		{
			data.Conditions.Condition[data.Conditions.Condition.length-1].connector="And";
		}
			data.Conditions.Condition.push({
              'rhs': '',
              'operator': '',
              'lhs': '',
              'connector': ''
            });
		}else if(data.type=="Condition Block"){
			
		}else{
			
		}
	}
	
	$scope.removeCondition =function (data,data1) {
	var parent=null;
		if(data.type=="IF" || data.type=="ElseIF"){
				for(var i=0;i<data.Conditions.Condition.length;i++){
					if(data.Conditions.Condition[i]==data1){
						if(parent!=null && data.Conditions.Condition[data.Conditions.Condition.length-1]==data1){
							parent.connector="";
						}
						data.Conditions.Condition.splice(i,1);
					}
					parent=data.Conditions.Condition[i];
				}

	
		}else if(data.type=="Condition Block"){
			
		}else{
			
		}
	}
	
$scope.deleteLookUpCondition =function (condition,data) {
	var parent=null;
		for(var i=0;i<data.Conditions.Condition.length;i++){
			if(data.Conditions.Condition[i]==condition){
				data.Conditions.Condition.splice(i,1);
			}
			parent=data.Conditions.Condition[i];
		}

}
$scope.addLookUpCondition =function (condition,data) {
	var parent=null;
		for(var i=0;i<data.Conditions.Condition.length;i++){
			if(data.Conditions.Condition[i]==condition){
				data.Conditions.Condition.splice(i+1,0,{'columnName': '','operator': '','valueTypeField': '','value': ''});
			}
			parent=data.Conditions.Condition[i];
		}

}
	
	
	$scope.LookUpTableSelected =function (data) {
		data.columnName = "";
		for(var i=0;i<data.Conditions.Condition.length;i++){
			data.Conditions.Condition[i].columnName = "";
		}
		$(".myselect > option").each(function(currSelect){ this.text+=""});
	}

	
// to be romoved	
	$scope.deleteNode =function (data,parent) { 
    var index =0;
    if(parent.Actions){
        index= parent.Actions.Action.indexOf(data); 
			if(index!=-1){
				parent.Actions.Action.splice(index,1);
			}
	}else{
            index = $scope.rule.Rules.Rule.indexOf(data); 
            $scope.rule.Rules.Rule.splice(index,1);
    }
    }
	
	$scope.addStatementNode =function (data,parent,index) {
		if(parent!=$scope.rule){
			if(!data.type){
				if(parent.Statement){
					for(var i=0;i<parent.Statement.length;i++){
						if(parent.Statement[i]==data){
							parent.Statement.splice(i+1,0,{'rhs': '','lhs': ''});
						}
					}
				}else{
					for(var i=0;i<parent.Actions.Action.length;i++){
						if(parent.Actions.Action[i].type=="Statement"){
							
								if(parent.Actions.Action[i].Statement[0]==data){
									parent.Actions.Action.splice(i+1,0,{'type': 'Statement','Statement': [{'rhs': '','lhs': ''}]});
								}	
								
						}	
					}
				}
	//parent.Actions.Action.push({'type': 'Statement','type': 'Statement','Actions': {'Action': [{'type': 'Statement','Statement': [{'rhs': '','lhs': ''}]}]}});				
			}else{
					for(var i=0;i<parent.Actions.Action.length;i++){
						if(parent.Actions.Action[i]==data){
								statementIndex = parent.Actions.Action.indexOf(data);
								parent.Actions.Action.splice(statementIndex+1,0,{'type': 'Statement','Statement': [{'rhs': '','lhs': ''}]});
								/*if(parent.Actions.Action[i].Statement[0]==data){
									parent.Actions.Action.splice(i+1,0,{'type': 'Statement','Statement': [{'rhs': '','lhs': ''}]});
								}*/	
								
						}	
					}
			
				
			}
			
		}else{
			for(var i=0;i< $scope.rule.Rules.Rule.length; i++){
					if($scope.rule.Rules.Rule[i].type=="Statement"){
						if($scope.rule.Rules.Rule[i].Actions.Action[0]==data){
							$scope.rule.Rules.Rule.splice(i+1,0,{'type': 'Statement','type': 'Statement','Actions': {'Action': [{'type': 'Statement','Statement': [{'rhs': '','lhs': ''}]}]}});
						}	
					}
				}
		}
	}
	
	$scope.deleteStatementNode =function (data,parent) {
		var statementIndex=-1;
		if(parent.Actions){
			for(var i=0;i<parent.Actions.Action.length;i++){
				if(parent.Actions.Action[i].type=="Statement"){
					if(parent.Actions.Action[i].Statement){
						index= parent.Actions.Action[i].Statement.indexOf(data);
						statementIndex=i;
						if(statementIndex!=-1 && index!=-1)
							break;
					}else{
						index= parent.Actions.Action.indexOf(data);
						statementIndex=i;
						if(statementIndex!=-1 && index!=-1)
							break;
					}
				}
			}
			if(parent.type == "Else" || parent.type == "IF" || parent.type == "ElseIF"|| parent.type == "Loop" || parent.Actions.Action[statementIndex].Statement){
				if(statementIndex!=-1){
					parent.Actions.Action.splice(statementIndex,1);
				}	
			}else{
				if(index!=-1){
					parent.Actions.Action[statementIndex].Statement.splice(index,1);
				}
			}
		} else if(parent.Statement){
			index= parent.Statement.indexOf(data);
			parent.Statement.splice(index,1);
		} 
		if(parent == $scope.rule){
			for(var i=0;i<$scope.rule.Rules.Rule.length;i++){
				if($scope.rule.Rules.Rule[i].type=="Statement"){
					index= $scope.rule.Rules.Rule[i].Actions.Action.indexOf(data);	
					statementIndex=i;
					if(statementIndex!=-1)
						break;
				}
			}
			$scope.rule.Rules.Rule.splice(statementIndex,1);
		}		
	}
	
	
	$scope.addFunctionNode =function (data,parent){
		index = parent.Functions.indexOf(data);
		parent.Functions.splice(index+1,0,{'functionName': '','params': ''});

	}
	
	$scope.deleteFunctionNode =function (data,parent){
		index = parent.Functions.indexOf(data);
		parent.Functions.splice(index,1);
	}
	
	
	
                
    $scope.addNode =function (data,type,parent) {
    if(type.choice1=="IF"){
        if(data.type=="IF" || data.type=="Loop" || data.type=="ElseIF" || data.type=="Else"){
            if(data.Actions.Action){
                 data.Actions.Action.push({'type': 'IF','Conditions': {'Condition': [{'rhs': '','operator': '','lhs': '','connector': ''}]},'Statement': [{'rhs': '','lhs': ''}]});
            } 
        }else if(data.Actions.Action){
            data.Rules.Rule.push({'type': 'IF','Conditions': {'Condition': [{'rhs': '','operator': '','lhs': '','connector': ''}]}});
        }
    }else if(type.choice1=="Else If"){ 
        if(data.type=="IF" || data.type=="Loop" || data.type=="ElseIF" || data.type=="Else"){
            if(data.Actions.Action){
                data.Actions.Action.push({'type': 'ElseIF','Conditions': {'Condition': [{'rhs': '','operator': '','lhs': '','connector': ''}]},'Statement': [{'rhs': '','lhs': ''}]});
            } 
        }else {
            data.Rules.Rule.push({'type': 'ElseIF','Conditions': {'Condition': [{'rhs': '','operator': '','lhs': '','connector': ''}]}});
        }
    } else if(type.choice1=="Else"){ 
	
        if(data.type=="IF" || data.type=="Loop" || data.type=="ElseIF" || data.type=="Else"){
            if(data.Actions.Action){
                data.Actions.Action.push({'type': 'Else','Statement': [{'rhs': '','lhs': ''}]});
            } 
        }

    } else if(type.choice1=="Statement"){
        if(data.type=="IF" ||  data.type=="ElseIF" || data.type=="Loop" || data.type=="Else"){
            if(data.Statement){
                data.Statement.push({'rhs': '','lhs': ''});
            } else if(parent==$scope.rule || data.type=="Loop") {
				data.Actions.Action.push({'type': 'Statement','Statement': [{'rhs': '','lhs': ''}]});
			}else {
				data["Statement"] = [{'rhs': '','lhs': ''}]
			}
        }
    } else if(type.choice1=="Loop"){
        if(data.type=="IF" || data.type=="ElseIF" || data.type=="Else"){
            if(data.Actions.Action){
                 data.Actions.Action.push({'type': 'Loop','limit': '','step': '','start': '','Statement': [{'rhs': '','lhs': ''}]});
            } 
        }
    } else if(type.choice1=="LookUp"){
        if(data.type=="IF" || data.type=="ElseIF" || data.type=="Loop" || data.type=="Else"){
		
            if(data.Actions.Action){
                 data.Actions.Action.push({'type': 'LookUp','lhs': '','tableName': '','dbFunction': '','columnName': '','Conditions': {'Condition': [{'columnName': '','operator': '','valueTypeField': '','value': ''}]},'orderBy': ''});
            } 
        }
    }else if(type.choice1=="Code Block"){
        if(data.type=="IF" || data.type=="ElseIF" || data.type=="Loop" || data.type=="Else"){
		
            if(data.Actions.Action){
                 data.Actions.Action.push({'type': 'CodeBlock','Code': ''});
            } 
        }
    }else if(type.choice1=="Function"){
        if(data.type=="IF" || data.type=="ElseIF" || data.type=="Loop" || data.type=="Else"){
            if(data.Actions.Action){
                 data.Actions.Action.push({'type': 'Function','Functions': [{'functionName': '','params': ''}]});
            } 
        }
    }		
	type.choice1 =0;
    }

	
	
$scope.addStartNode =function (type) { 
    if(type.choice1=="IF"){
		$scope.rule.Rules.Rule.splice(0,0,{'type': 'IF','Conditions': {'Condition': [{'rhs': '','operator': '','lhs': '','connector': ''}]},'Actions': {'Action': []}});
    }else if(type.choice1=="Else If"){ 
		$scope.rule.Rules.Rule.splice(0,0,{'type': 'ElseIF','Conditions': {'Condition': [{'rhs': '','operator': '','lhs': '','connector': ''}]},'Actions': {'Action': []}});
    } else if(type.choice1=="Else"){ 
		$scope.rule.Rules.Rule.splice(0,0,{'type': 'Else','Actions': {'Action': [{'type': 'Statement','Statement': [{'rhs': '','lhs': ''}]}]}});
    } else if(type.choice1=="Statement"){
		$scope.rule.Rules.Rule.splice(0,0,{'type': 'Statement','type': 'Statement','Actions': {'Action': [{'type': 'Statement','Statement': [{'rhs': '','lhs': ''}]}]}});
    } else if(type.choice1=="Loop"){
        $scope.rule.Rules.Rule.splice(0,0,{'type': 'Loop','limit': '','step': '','start': '','Actions': {'Action': []}});
    } else if(type.choice1=="LookUp"){
       $scope.rule.Rules.Rule.splice(0,0,{'type': 'LookUp','lhs': '','tableName': '','dbFunction': '','columnName': '','Conditions': {'Condition': [{'columnName': '','operator': '','valueTypeField': '','value': ''}]},'orderBy': ''});
    }else if(type.choice1=="Code Block"){
		$scope.rule.Rules.Rule.splice(0,0,{'type': 'CodeBlock','Code': ''});
    }else if(type.choice1=="Function"){
		$scope.rule.Rules.Rule.splice(0,0,{'type': 'Function','Functions': [{'functionName': '','params': ''}]});
	}
	type.choice1 =0;
}	
	
	
	
	$scope.addMainNode =function (choice,data){
		if(choice=="IF"){
			for(var i=0;i<$scope.rule.Rules.Rule.length;i++){
				if($scope.rule.Rules.Rule[i]==data){
					$scope.rule.Rules.Rule.splice(i+1,0,{'type': 'IF','Conditions': {'Condition': [{'rhs': '','operator': '','lhs': '','connector': ''}]},'Actions': {'Action': []}});
				}
			}
		}else if(choice=="Else If"){
			for(var i=0;i<$scope.rule.Rules.Rule.length;i++){
				if($scope.rule.Rules.Rule[i]==data){
					$scope.rule.Rules.Rule.splice(i+1,0,{'type': 'ElseIF','Conditions': {'Condition': [{'rhs': '','operator': '','lhs': '','connector': ''}]},'Actions': {'Action': []}});
				}
			}
		}else if(choice=="Else"){
			for(var i=0;i<$scope.rule.Rules.Rule.length;i++){
				if($scope.rule.Rules.Rule[i]==data){
					$scope.rule.Rules.Rule.splice(i+1,0,{'type': 'Else','Actions': {'Action': []}});
				}
			}
		}else if(choice=="Loop"){
			for(var i=0;i<$scope.rule.Rules.Rule.length;i++){
				if($scope.rule.Rules.Rule[i]==data){
					$scope.rule.Rules.Rule.splice(i+1,0,{'type': 'Loop','limit': '','step': '','start': '','Actions': {'Action': []}});
				}
			}
		}else if(choice=="LookUp"){
			for(var i=0;i<$scope.rule.Rules.Rule.length;i++){
				if($scope.rule.Rules.Rule[i]==data){
					$scope.rule.Rules.Rule.splice(i+1,0,{'type': 'LookUp','lhs': '','tableName': '','dbFunction': '','columnName': '','Conditions': {'Condition': [{'columnName': '','operator': '','valueTypeField': '','value': ''}]},'orderBy': ''});
				}
			}
		}else if(choice=="Code Block"){
			for(var i=0;i<$scope.rule.Rules.Rule.length;i++){
				if($scope.rule.Rules.Rule[i]==data){
					$scope.rule.Rules.Rule.splice(i+1,0,{'type': 'CodeBlock','Code': ''});
				}
			}
		}else if(choice=="Statement"){
			for(var i=0;i<$scope.rule.Rules.Rule.length;i++){
				if($scope.rule.Rules.Rule[i]==data){
					$scope.rule.Rules.Rule.splice(i+1,0,{'type': 'Statement','type': 'Statement','Actions': {'Action': [{'type': 'Statement','Statement': [{'rhs': '','lhs': ''}]}]}});
				}
			}
		}else if(choice=="Function"){
			for(var i=0;i<$scope.rule.Rules.Rule.length;i++){
				if($scope.rule.Rules.Rule[i]==data){
					$scope.rule.Rules.Rule.splice(i+1,0,{'type': 'Function','Functions': [{'functionName': '','params': ''}]});
				}
			}
		}
	}
	
	$scope.delMainNode =function (data){
		for(var i=0;i<$scope.rule.Rules.Rule.length;i++){
			if($scope.rule.Rules.Rule[i]==data){
				$scope.rule.Rules.Rule.splice(i, 1);
			}
		}		
			//$scope.rule.Rules.Rule.pop(data); alert("++++++++++++"+angular.toJson($scope.rule.Rules.Rule)+ "++++++++++++"+angular.toJson(data));
			/*$scope.rule={};
			var obj = clone($scope.ruleCurrent);
			$scope.rule = new Object(obj);
			$scope.rulePart = $scope.rule.Rules.Rule; */
			//$scope.rule.Rules.Rule = new Array($scope.ruleCurrent.Rules.Rule);alert("1"+angular.toJson($scope.rule.Rules.Rule));
	}
	
	
	$scope.populateSingleInputtoJson =function (textValue){
		$scope.rule.Variables.SingleInput = [];
		$scope.rule.Variables.SingleInput=textValue.split("\n");
	}
	
	$scope.populateSingleOutputtoJson =function (textValue){
		$scope.rule.Variables.SingleOutput = [];
		$scope.rule.Variables.SingleOutput=textValue.split("\n");
	}
	
	$scope.populateMultiInputtoJson =function (textValue){
		$scope.rule.Variables.MultiInput = [];
		$scope.rule.Variables.MultiInput=textValue.split("\n");
	}
	
	$scope.populateMultiOutputtoJson =function (textValue){
		$scope.rule.Variables.MultiOutput = [];
		$scope.rule.Variables.MultiOutput=textValue.split("\n");
	}
	
	$scope.populateStandardInputtoJson =function (textValue){
		$scope.rule.Variables.StandardInput = [];
		var standardInputArray ={};
		var standardInputs = textValue.split("\n");
		for(standardInput in standardInputs){
			if(standardInputs[standardInput] !=''){
				if($.inArray(standardInputs[standardInput],$scope.mulvalStandardInput) !== -1){
					standardInputArray[standardInputs[standardInput]] ="MultiInput";
				} else {
					standardInputArray[standardInputs[standardInput]] ="SingleInput";
				}
			}
		}
		$scope.rule.Variables.StandardInput.push(standardInputArray);
		
	}
	
	$scope.populateFunctionInputtoJson =function (textValue){
		$scope.rule.Variables.FunctionInput = [];
		$scope.rule.Variables.FunctionInput=textValue.split("\n");
	}
	
	//Save Rule
	$scope.SaveRule =function(){
		var js =$scope.construct($scope.rulePart, true);
		var offlineJs =$scope.construct($scope.rulePart, false);
		$scope.saveRuleJson(ruleId,clone,js,offlineJs,$scope.rule,$scope.ruleName1,$scope.groupName1);
	};
	
	$scope.validation = function(){
		if ($('#txtRuleName').val() == "") {
			$scope.errorMessage = "Please enter the rule name.";
			return false;
		} else if ($('#groupSel').val() == "") { 
			$scope.errorMessage = "Please enter the group name.";
			return false;
		} else if($.inArray($('#txtRuleName').val().toLowerCase(), keywordsValueLowerCase) != -1){
			$scope.errorMessage = $('#txtRuleName').val() +' is a keyword.Please give another name.';
			return false;
		} else if($scope.commentTextArea === undefined || $scope.commentTextArea ==""){
			$scope.errorMessage = "Please enter a valid comment.";
			return false;
		} else {
			$scope.errorMessage = "";
			return true;
		}
	};
	
	//Save Rule
	$scope.saveRuleJson = function(ruleId,clone,ruleJavascript,offlineRuleJavascript,ruleJson,ruleName,groupName1){
		ruleJsonString= JSON.stringify(ruleJson);
        	var id;
			var isDupChkRequired = false;
        	if(clone != null){
            	var fromClone = clone;	               	
               }


        	if(fromClone == true){	
				ruleId = "";
			}
        	
        	/*
			 * if(window.id != null){ ruleId = window.id ; }
			 */
			;
        	//if ($('#txtRuleName').val() != "") {
        		//if ($('#groupSel').val() != "") { 
        			//if($.inArray($('#txtRuleName').val().toLowerCase(), keywordsValueLowerCase) == -1){
					if($scope.validation()){
	        			if(window.id != null){
	        				ruleId = window.id ;
		        			if(($.trim(window.ruleNameBefEdit.toUpperCase()) != $('#txtRuleName').val().trim().toUpperCase()) ||
							($.trim(window.groupNameBefEdit.toUpperCase()) != $scope.groupName1.trim().toUpperCase()) || (window.isRuleExists == 'true')){
								isDupChkRequired = true;
		        			}
	        			} else if(ruleId != null && ruleId != ""){
							if($.trim(ruleNameEditMode.toUpperCase()) != $('#txtRuleName').val().trim().toUpperCase() ||
							($.trim(groupName) != $scope.groupName1.trim())){
								isDupChkRequired = true;
		        			}
						}
		                var options = { smarttabs:true,newcap: false, bitwise: true, curly: false, eqeqeq: false, forin: true, immed: true, latedef: true, newcap: false, noarg: true, noempty: true, nonew: true, regexp: true, undef: false, strict: false, trailing: true, node: true, unused: false };
		                var globals = { describe: false, it: false, beforeEach: false, afterEach: false };
		
		                // Get ruleName, ruleJavascript and ruleJson and save to
						// DB
		                // var ruleJavascript = BuildRuleJavascript();
		                // var ruleJson = BuildRuleJson();
		                // ruleJson = JSON.stringify(ruleJson);
		                if (JSHINT(ruleJavascript, options) && JSHINT(offlineRuleJavascript, options)) {
	                    // save ruleName, ruleJavascript and ruleJson to DB
						
							var loading;	
							
							$.ajax({
									type: "POST",
									url: "../createRule",
									dataType : "json",
									data : {		ruleID : ruleId,
													ruleJson : ruleJsonString,
													ruleJS : ruleJavascript,
													offlineRuleJs : offlineRuleJavascript,
													ruleName : ruleName,
													groupName : groupName1,
													fromCopy : fromClone,
													ruleComment : $scope.commentTextArea,
													isDupChkRequired : isDupChkRequired
												},
									async : false,
									cache : false,
									beforeSend : function() {

												},
									success : function(data) {
													json = data;
													var result = json.createRuleResult;
													if(json.ruleID != null && json.ruleID != undefined && json.ruleID != ""){
														window.id = json.ruleID;
														window.ruleNameBefEdit = $('#txtRuleName').val();
														window.groupNameBefEdit = $scope.groupName1;
														window.isRuleExists = json.isRuleExists;
													}
													flagUnload = true;
													$('#dvMessages').hide();
													$('#dvMessages').show(
															400);
													onPass("dvMessages",
															result);
												},
										error : function(data) {
								    				$scope.showErrorPopup(data);
								    			},
										complete : function() {

													// $('#dvMessages').hide(4000);
													clearTimeout(loading);
													$("#spinner")
															.hide(3500);
												}
											});

								} else {
									onFail("dvMessages");
								}
							} else {
								$timeout(function(){
									$scope.lePopupCtrl.showError('Rule Workbench',$scope.errorMessage,'Ok');
								},0);
							}
	        			/* } else {
							$timeout(function(){
		                        $scope.lePopupCtrl.showError('Rule Workbench',$('#txtRuleName').val() +' is a keyword.Please give another name','Ok');
								},0);
						} */
							/* } else {
								// alert("Please enter the group name.");
								$timeout(function(){
	                                $scope.lePopupCtrl.showError('Rule Workbench','Please enter the group name.','Ok');
								},0);							
							} */
						/* } else {
							// alert("Please enter the rule name.");
							$timeout(function(){
								$scope.lePopupCtrl.showError('Rule Workbench','Please enter the rule name.','Ok');

							},0);	
														
						} */

					
	}
	
	$scope.SaveStaValues = function(e) {
		$('#StandardInput').val('');
		var stdInputStr = '';
			var flag = false;
			//e.preventDefault();
			var oldString = $('#StandardInput').val();
			var oldStringArr = oldString.split('\n');
			standInpArray = [];
			for ( var i = 0; i < oldStringArr.length; i++) {
				if (oldStringArr[i] != '') {
					standInpArray.push(oldStringArr[i]);

				}
			}
			$('input[type="checkbox"]').each(function() {
				if ($(this).is(':checked')) {
					var tempVar = $(this).val();
					for ( var i = 0; i < standInpArray.length; i++) {
						if (tempVar == standInpArray[i]) {
							flag = true;

						}
					}
					if (flag == false) {
						standInpArray.push(tempVar);
					}
					flag = false;
				}
			});
			for ( var i = 0; i < standInpArray.length; i++) {
				if(standInpArray[i] != ''){
					stdInputStr = stdInputStr + standInpArray[i] + '\n';
				}
			}
			$('#StandardInput').val(stdInputStr);
			//AddIntellisense();
			$scope.populateStandardInputtoJson(stdInputStr);
			$scope.disablePopupst();
			$('#StandardInput').focus();
	}
	

	$scope.disablePopupst = function() {
				$("#toPopupst1").fadeOut("normal");
					$("#backgroundPopupst1").fadeOut("normal");
					
			}
	
	$scope.validate =function(){
		validateJS($scope.js);
	}
	
	
	$scope.AddIntellisense =function(){
		var TextArea = $('.intelli'); // TEXT AREA IN WHICH THE USER TYPES THE FORMULA
		var MenuDiv = $('#selectionMenu'); // DIV WHICH CONTAINS THE LISTBOX
		var MenuListBox = $('#selectionList'); // LIST BOX WHICH CONTAINS THE ITEMS
		var MenuListBoxCopy = $('#selectionListCopy'); // LIST BOX WHICH CONTAINS THE
		$(this).AddIntelliSense(TextArea, MenuDiv, MenuListBox, MenuListBoxCopy,
					null,$(this));			
	}
	

  $scope.delMainNodeAlertBox = function (data) {
	
    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: ModalInstanceCtrl,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
		$scope.delMainNode(data);
		$scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };


  $scope.removeConditionAlertBox = function (data,data1) {
	
    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: ModalInstanceCtrl,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
		$scope.removeCondition(data,data1);
		$scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };  
  
  $scope.deleteNestedIfAlertBox = function (data,parent) {
		
	    var modalInstance = $modal.open({
	      templateUrl: 'myModalContent.html',
	      controller: ModalInstanceCtrl,
	      resolve: {
	        items: function () {
	          return $scope.items;
	        }
	      }
	    });

	    modalInstance.result.then(function (selectedItem) {
			$scope.deleteNode(data,parent);
			$scope.selected = selectedItem;
	    }, function () {
	      $log.info('Modal dismissed at: ' + new Date());
	    });
	  };  
	  
   $scope.deleteStatementNodeAlertBox = function (data5,parent1) {
	
    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: ModalInstanceCtrl,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
		$scope.deleteStatementNode(data5,parent1);
		$scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };  
  
   $scope.deleteNodeAlertBox = function (data,parent) {
	
    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: ModalInstanceCtrl,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
		$scope.deleteNode(data,parent);
		$scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };   

   $scope.deleteStatementNodeAlertBox = function (data5,parent1) {	
    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: ModalInstanceCtrl,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
		$scope.deleteStatementNode(data5,parent1);
		$scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  }; 

   $scope.deleteLookUpConditionAlertBox = function (data1,data) {	
    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: ModalInstanceCtrl,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
		$scope.deleteLookUpCondition(data1,data);
		$scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };  

   $scope.deleteFunctionNodeAlertBox = function (data1,parent) {	
    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: ModalInstanceCtrl,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function () {
		$scope.deleteFunctionNode(data1,parent);
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };  


} ]);

/////////////////
var ModalInstanceCtrl = function ($scope, $modalInstance, items) {



  $scope.ok = function () {
    $modalInstance.close();
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
};

//index of fix for IE8

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

if (!Array.prototype.forEach) {
	Array.prototype.forEach = function(fun /* , thisp */) {
		var len = this.length;
		if (typeof fun != "function")
			throw new TypeError();

		var thisp = arguments[1];
		for ( var i = 0; i < len; i++) {
			if (i in this)
				fun.call(thisp, this[i], i, this);
		}
	};
}

if(typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g, ''); 
	  };
}

