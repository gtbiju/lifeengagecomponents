
//$scope.ruleList=allRuleDetails();





var myapp = angular.module('myapp', ['ui.bootstrap','popupdirective']);

myapp.controller('allRuleDetails', ['$scope','$modal', '$log','$timeout', function ($scope, $modal,$log,$timeout) {
	
    $scope.lePopupCtrl = {
            
    }
    //showLoader('Please Wait...');
    $scope.showErrorPopupOnPageLoad = function(data) {
		if (data.status==412) {
			$scope.$watch( "lePopupCtrl.showError" , function(n,o){
				if(n==o) {
					return
				};
				$timeout(function(){
					$scope.lePopupCtrl.showError('Rule Workbench',JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details,'OK');
				},0);	
			});
		}				
	}
    $scope.showErrorPopup = function(data) {
    	if (data.status==412) {
			$timeout(function(){
				$scope.lePopupCtrl.showError('Rule Workbench',JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details,'OK');
			},0);	
		}
    }
    $scope.listRule = function(){
    	var ruleTypeId = 1;
    	var ruleSetTypeId = 1;
    	var ruleList = [];
        $.ajax({
    			type: "GET",
    		    url: "../listRuleAndRulSet",
    		    data : { ruleTypeId : ruleTypeId, ruleSetTypeId : ruleSetTypeId},
    			async : false,
    			cache : false,							
    			success : function(data) {
    					ruleList = data;
    					//$("#flash").hide();
    				
    			},
    		    error : function(data) {
    		    	$scope.showErrorPopupOnPageLoad(data);		
    			}
    			});
    		
    		 return ruleList;
    }
	$scope.listRules = $scope.listRule();
	$scope.deleteRule = function(id, ruleType) {
	

		var ruleID = id;

		//var c =confirm("Are you sure you want to delete?");
		$timeout(function(){
            $scope.lePopupCtrl.showQuestion('Rule Workbench','Are you sure you want to delete?','Delete',$scope.deleteCall,'Cancel');
},0);
		$scope.deleteCall = function(){
			    	$.ajax({
		    			type: "POST",
		    		    url:"../deleteRule",
						dataType : "json",
						data : { ruleID : ruleID, ruleTypeId : ruleType},
						async : false,
						cache : false,												
						success : function(data) {
							if(data.message) {
								 $timeout(function(){
				                      $scope.lePopupCtrl.showInformation('Rule Workbench',data.message,'OK');
								 },0);	
							} else {
								location.reload(); 
							}
						},
						error : function(data) {
							$scope.showErrorPopup(data);				
		    			}
			    	});
			    }


	}
	
	 function showLoader(txt) {
         $("#flash").show();
         //'<img src="http://localhost:8080/life-engage-rule-workbench/images/ajax-loader.gif" 
         var loader = '<img src="../jsp/images/ajax-loader.gif" align="absmiddle">&nbsp;<span class="loading">' + txt + '...</span>';
         $("#flash").fadeIn(400).html(loader);
     };
	
} ]);
