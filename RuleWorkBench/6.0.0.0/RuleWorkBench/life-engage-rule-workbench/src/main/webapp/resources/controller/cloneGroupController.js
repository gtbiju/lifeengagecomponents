var ruleBOListCopy = [];
var groupCloneName;
var groupID = "";
var groupTypeID = "";
var  ruleBOList = [];
var myapp = angular.module('myapp', ['ui.bootstrap','popupdirective']);
//angular.module('myapp', ['ui.bootstrap','popupdirective']);
//angular.module('myapp', ['ui.bootstrap','myapp.popupService']);
//function CloneGroupController($scope, $modal, $log) {
myapp.controller('CloneGroupController', ['$scope','$modal', '$log','$timeout', function ($scope, $modal,$log,$timeout) {
	
	//Special character check for Clone Group Name
	$scope.alpha = function(e) {
		var k;
		document.all ? k = e.keyCode : k = e.which;
		if(!((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57)|| (k==95))){
			e.currentTarget.blur();
			$timeout(function(){
            $scope.lePopupCtrl.showError('Rule Workbench','Special characters are not allowed.','Ok');

},0);
		}
		return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57) || (k==95));
	
	};          
	
	
	$scope.lePopupCtrl = {
            
    }
	
	$scope.showErrorPopupOnPageLoad = function(data) {
		if (data.status==412) {
			$scope.$watch( "lePopupCtrl.showError" , function(n,o){
				if(n==o) {
					return
				};
				$timeout(function(){
					$scope.lePopupCtrl.showError('Rule Workbench',JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details,'OK');
				},0);	
			});
		}				
	}
    $scope.showErrorPopup = function(data) {
    	if (data.status==412) {
			$timeout(function(){
				$scope.lePopupCtrl.showError('Rule Workbench',JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details,'OK');
			},0);	
		}
    }

	groupName = "";
	
	if (window.location.href.indexOf("?") != -1) {
		var pairs = window.location.href.substring(window.location.href.indexOf("?")+1).split("&")
		for (var i = 0, len = pairs.length; i < len; i++) {
			pair = pairs[i].split('=');
			if (pair[0] == 'groupID') {
				groupID = pair[1];
			} else if (pair[0] == 'groupName') {
				groupName = pair[1];
			} else if (pair[0] == 'typeId') {
				groupTypeID = pair[1];
			}
		}
	}
	$("#txtGroupCopyName").blur(function() {
		if($.trim($('#txtGroupCopyName').val().toUpperCase()) == groupName.toUpperCase()){
			//alert("Plaese give another name");
			$timeout(function(){
                $scope.lePopupCtrl.showWarning('Rule Workbench','Please give another name','OK');
},0);
			$('#txtGroupCopyName').val("");
			$('#txtGroupCopyName').focus();
			}
		});
	
	
	$('#groupLabel').text(groupName);
	$('#txtGroupName').val(groupName);
	$('#txtGroupName').attr("readonly", true);
	
	
	$scope.listRulesByGroup = function(groupID){
		var ruleBOList =[];
		$.ajax({
			type: "POST",
		    url: "../listRuleByGroup",
			dataType : "json",
			data : { groupID : groupID},
			async : false,
			cache : false,												
			success : function(data) {
				ruleBOList = data;
				ruleBOListCopy = ruleBOList;
			},
			error : function(data) {
				$scope.showErrorPopupOnPageLoad(data);
			}
		});
				
		return ruleBOList;

	};
	
	
	$scope.listRulesByGroup = $scope.listRulesByGroup(groupID); 
	
	var ruleBOList = [];
	var groupCloneName;
	$scope.saveGroupCopy = function() {
		var selectedRules = [];
		ruleBOList = [];
			
		$('input[type="checkbox"]').each(function() {
			if($(this).is(':checked')) {
				selectedRules.push($(this).val());
			} 
		});
			
		$.each(ruleBOListCopy, function (i, v) {
			var id = ruleBOListCopy[i].ruleID;
			if( $.inArray(String(id),selectedRules) > -1 ){
				ruleBOList.push(ruleBOListCopy[i]);
			}    		
		});
		  
		groupCloneName = $('#txtGroupCopyName').val();
			
		if(groupCloneName != "") {
			if($.trim($('#txtGroupCopyName').val().toUpperCase()) != groupName.toUpperCase()){
				if(selectedRules.length !== 0){
					$scope.getData().done($scope.handleData);
					
				} else {
					//alert("Please select a rule.");
                    $timeout(function(){ $scope.lePopupCtrl.showInformation('Rule Workbench','Please select a rule.','OK');},0);
				}
			} else {
				$timeout(function(){$scope.lePopupCtrl.showWarning('Rule Workbench','Please give another name','OK');},0);
				
			}
		} else {
				//alert("Please enter a group name.");
				$('#txtGroupCopyName').blur();
			      $timeout(function(){ $scope.lePopupCtrl.showInformation('Rule Workbench','Please enter a group name.','OK'); },0);
     
				//$('#txtGroupCopyName').focus();
		}
		
    };
    
    $scope.getData=function() {
	    return $.ajax({
	    	type: "POST",
 		    url: "../cloneGroup",
			dataType : "json",
			data : { ruleBOList : $.toJSON(ruleBOList),groupCloneName : groupCloneName,groupID : groupID, groupTypeID : groupTypeID },
			async : false,
			cache : false,
			success : function(data) {
				json = data;
				var result = json.createGroupResult;
				if (typeof result != 'undefined') {
					 $timeout(function(){
	                      $scope.lePopupCtrl.showInformation('Rule Workbench',result,'OK');
					 },0);
				}
			},
			error : function(data) {
				$scope.showErrorPopup(data);
			}
	    });
	}
	$scope.handleData=function(data) {
	    if(data.groupexists){
			//var c = false;
			// var c =confirm("Group Name already exists.Are you sure you want to merge it?");
			
			$timeout(function(){
                $scope.lePopupCtrl.showQuestion('Rule Workbench','Group Name already exists.Are you sure you want to merge it?','Confirm',$scope.mergeGroup,'Cancel');
},0);
			    $scope.mergeGroup=function(){
				
				
				$.ajax({
			 			type: "POST",
			 		    url: "../mergeGroup",
						dataType : "json",
						data : { ruleBOList : $.toJSON(ruleBOList), groupCloneName : groupCloneName,groupID : data.groupID , groupTypeID : groupTypeID },
						async : false,
						cache : false,												
						success : function(data) {
									var resultMap = data;
				
									if(resultMap.isMergeSuccess){
							    		window.location="../jsp/AddGroup.jsp";
									} else {
										if(resultMap.hasOwnProperty('isSameGroup')){
											//alert(resultMap.message);
											 $timeout(function(){
							                      $scope.lePopupCtrl.showInformation('Rule Workbench',resultMap.message,'OK');
							      },0);	
										}
										else if(resultMap.hasOwnProperty('isRuleNameExists')){
										//alert(resultMap.message);
											$timeout(function(){
							                      $scope.lePopupCtrl.showError('Rule Workbench',resultMap.message,'OK');
							      },0);	
										}
									}
							
							
						},
						error : function(data) {
							$scope.showErrorPopup(data);
						}
						  
					});
					
					
				    }//if
			} else {
				window.location="../jsp/AddGroup.jsp";
				}
	}
	
	
	$scope.cancel=function() {
		window.location="../jsp/AddGroup.jsp";
	};
	
	
} ]);


