'use strict';
var app = angular.module('popupdirective', []);
app.directive('lepopup', function() {
	return {
		restrict : 'E',
		templateUrl : 'template/lePopUp.html',
		scope : {
			control : '='
		},
		replace : true, // Replace with the template below
		//transclude: true, // we want to insert custom content inside the directive
		link : function(scope, element, attrs) {
			scope.control.show = false;
			scope.control.showWarning = function(header, message, button1Name,
					button1Function, button2Name, button2Function) {
				scope.control.icon = "warning";
				scope.control.setProperties(header, message,button1Name,button1Function);				
				scope.control.setButton2Properties(button2Name, button2Function);
				scope.control.button3Show = false;
				scope.control.textAreaShow = false;
				
			};
			scope.control.showQuestion = function(header, message, button1Name,
					button1Function, button2Name, button2Function,button3Name,button3Function) {
				scope.control.textAreaShow = false;
				scope.control.icon = "question";
				scope.control.setProperties(header, message,button1Name,button1Function);
				scope.control.setButton2Properties(button2Name, button2Function);
				scope.control.setButton3Properties(button3Name, button3Function);
				
			};
			scope.control.showError = function(header, message, button1Name,
					button1Function) {
				scope.control.icon = "error";
				scope.control.setProperties(header, message,button1Name,button1Function);
				scope.control.button2Show = false;
				scope.control.button3Show = false;
				scope.control.textAreaShow = false;
				
			};
			scope.control.showInformation = function(header, message, button1Name,
					button1Function,button2Name, button2Function) {
				scope.control.icon = "information";
				scope.control.setProperties(header, message,button1Name,button1Function);
				if(typeof button2Name =='undefined' ) {
					scope.control.button2Show = false;
					scope.control.button3Show = false;
					scope.control.textAreaShow = false;
				} else {
					scope.control.textAreaShow = false;
					scope.control.setButton3Properties(button2Name, button2Function);
				}
				
				
				
			};

			scope.control.setProperties = function(header,message,button1Name,button1Function) {
				if(typeof button1Name =='undefined' ) {
					button1Name = "OK";
				}
				if(typeof button1Function =='undefined' ) {
					button1Function = scope.control.hideModal;
				}
				scope.control.header = header;
				scope.control.message = message;
				scope.control.button1Label = button1Name;
				scope.control.show = true;
				var dynamic_height = $(window).height();
				$('.ng-modal-overlay').css("display", "block");
				$('.ng-modal-overlay').css('height', dynamic_height)
						.fadeIn(300);
				var top = ($(window).height() - $('.le_pop_up').height()) / 2;
				var left = ($(window).width() - $('.le_pop_up').width()) / 2;
				$('.le_pop_up').css({
					'margin-top' : top,
					'margin-left' : left
				});
				scope.control.callBackPositive = function() {
					button1Function();
					scope.control.show = false;
						$('.le_pop_up').height(175);
				}
				
				
			}

			scope.control.setButton2Properties = function(button2Name, button2Function){
				if (typeof button2Name =='undefined') {	
					scope.control.button2Show = false;
				} else {	
					scope.control.button2Label = button2Name;
					scope.control.button2Show = true;
					if (typeof button2Function =='undefined' ) {	
						scope.control.callBackNegative = scope.control.hideModal;
					} else {						
						scope.control.callBackNegative = function() {
							$('.le_pop_up').height(175);
							button2Function();
							scope.control.show = false;
						}
					}					
				}
			}
			scope.control.setButton3Properties = function(button3Name, button3Function){
				if (typeof button3Name =='undefined') {	
					scope.control.button2Show = false;
				} else {	
					scope.control.button3Label = button3Name;
					scope.control.button3Show = true;
					if (typeof button3Function =='undefined' ) {	
						scope.control.showDetails = scope.control.hideModal;
					} else {				
						scope.control.showDetails = function() {
							
						scope.control.details = button3Function();
							if(scope.control.textAreaShow){
								scope.control.textAreaShow = false;
								$('.le_pop_up').height(175);
							} else {
								scope.control.textAreaShow = true;
								$('.le_pop_up').height(250);
							} 
							
						}
					}					
				}
			}
			
			scope.control.hideModal = function() {
				scope.control.show = false;
				$('.le_pop_up').height(175);
			};
		}
	}
});
