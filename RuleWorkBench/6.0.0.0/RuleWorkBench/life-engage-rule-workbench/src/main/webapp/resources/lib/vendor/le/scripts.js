$(document).ready(function () {

		
	
		$('.cust_tbl tr:visible:odd').addClass('row_odd');
		$('.cust_tbl tr:visible:even').addClass('row_even');
		

	/* main nav bar naviation */
		 $('.main_nav_bar_list li').click(function(){
			$('.main_nav_bar_list li').removeClass('active');
			$(this).addClass('active');
		}); 
/* 		$('li.inactiveList').click(function(){
			$(this).removeClass('inactiveList').addClass('activeList');
			$('.submenu_container').slideUp('2000');
		});
		 $('li.activeList').click(function(){
		
		 if($(this).hasClass('activeList')){
			console.log(this);
			$('.TopSlideMenu ul li').removeClass('inactiveList').addClass('activeList');
			$(this).removeClass('activeList').addClass('inactiveList');
			var activeTab = $(this).find('a').attr('rel');
			$('.submenu_container').slideUp('2000');
			$(activeTab).slideDown('2000');
		}); */
		
		/* $('li.inactiveList').click(function(){
			$(this).removeClass('inactiveList').addClass('activeList');
			$('.submenu_container').slideUp('2000');
		});
		 */
		 $('.TopSlideMenu ul li').click(function(){
		 if($(this).hasClass('activeList')){
				$('.TopSlideMenu ul li').removeClass('inactiveList').addClass('activeList');
				$(this).removeClass('activeList').addClass('inactiveList');
				var activeTab = $(this).find('a').attr('rel');
				$('.submenu_container').slideUp('2000');
				$(activeTab).slideDown('2000');
			}
			else{
				$(this).removeClass('inactiveList').addClass('activeList');
				$('.submenu_container').slideUp('2000');
			}
		});
		
		 $('.submenu_link ul li').click(function(){
			 $('.submenu_link ul li').removeClass('activated');
			 $(this).addClass('activated');
			$('.TopSlideMenu ul li').removeClass('inactiveList').addClass('activeList');
			$('.submenu_container').slideUp('2000');
		 });
		
	/* main nav bar naviation */
	
	/* BUTTON DISABLING AND ENABLING */
		$('input[name="selectionRadio"]').click(function(){ 
		$('button.action_btn').attr('disabled',false);
			$('button.action_btn').removeClass('disabled_btn');
		});
	/* BUTTON DISABLING AND ENABLING */

});

/* MODAL POP_UP OPENING CLOSING */
	$('#add_more_btn').click(function(){
		//document.getElementById('upload_message_span').innerHTML = "";
	    $('#page_loader').empty();
		var dynamic_height = $('body').height();
		$('#page_loader').css('height',dynamic_height).fadeIn(300);
		addMore();
	});
	$('#rollback_btn').click(function(){
	    $('#page_loader').empty();
		var dynamic_height = $('body').height();
		$('#page_loader').css('height',dynamic_height).fadeIn(300);
		rollbackVersion();
	});
	$('#update_btn').click(function(){
		document.getElementById('update_message_span').innerHTML = "";
	    $('#page_loader').empty();
		var dynamic_height = $('body').height();
		$('#page_loader').css('height',dynamic_height).fadeIn(300);
		updateContent();
	});
	
	function addMore(){
	 var flag = $('#page_loader').css("display");
	  if(flag == "block"){	
	    $('.modal_pop_up.add_more_pop_up').css("display","block");
	    var top = ($(window).height() - $('.modal_pop_up.add_more_pop_up').height()) / 2;	   
        var left = ($(window).width() - $('.modal_pop_up.add_more_pop_up').width()) / 2;	  
		$('.modal_pop_up.add_more_pop_up').css({ 'top': top + $(document).scrollTop(),'left': left});
		$("body").removeClass("hide_scroll").addClass("hide_scroll");		
	}
	else{
		$("body").removeClass("hide_scroll");		 
	}
	}
	function rollbackVersion(){
	 var flag = $('#page_loader').css("display");
	  if(flag == "block"){	
	    $('.modal_pop_up.rollback_content_pop_up').css("display","block");
	    var top = ($(window).height() - $('.modal_pop_up.rollback_content_pop_up').height()) / 2;	   
        var left = ($(window).width() - $('.modal_pop_up.rollback_content_pop_up').width()) / 2;	  
		$('.modal_pop_up.rollback_content_pop_up').css({ 'top': top + $(document).scrollTop(),'left': left});
		$("body").removeClass("hide_scroll").addClass("hide_scroll");		
	}
	else{
		$("body").removeClass("hide_scroll");		 
	}
	}
	function updateContent(){
	 var flag = $('#page_loader').css("display");
	  if(flag == "block"){	
	    $('.modal_pop_up.update_content_pop_up').css("display","block");
	    var top = ($(window).height() - $('.modal_pop_up.update_content_pop_up').height()) / 2;	   
        var left = ($(window).width() - $('.modal_pop_up.update_content_pop_up').width()) / 2;	  
		$('.modal_pop_up.update_content_pop_up').css({ 'top': top + $(document).scrollTop(),'left': left});
		$("body").removeClass("hide_scroll").addClass("hide_scroll");		
	}
	else{
		$("body").removeClass("hide_scroll");		 
	}
	}
	
	$('.close_pop_up').click(function(){	 
	  $('.modal_pop_up').css("display","none");
	  $("body").removeClass("hide_scroll"); 
	  $('#page_loader').css("display","none");
	});
/* MODAL POP_UP OPENING CLOSING */


$('.custom-upload input[type=file]').change(function(){
    $(this).next().find('input').val($(this).val());
});