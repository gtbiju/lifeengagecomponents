var myapp = angular.module('myapp', ['ui.bootstrap']);
myapp.controller('login', ['$scope','$modal', '$log','$timeout','$http', function ($scope, $modal,$log,$timeout,$http) {
	if(localStorage.getItem( 'loginDetails' ) != null && localStorage.getItem( 'loginDetails' ) != 'undefined' &&  localStorage.getItem( 'loginDetails' ) != '' ){
		var loginParam = JSON.parse(localStorage.getItem('loginDetails' ));
		$scope.userName = loginParam.uname;
		$scope.password = loginParam.pwd;
	} else {
		$scope.userName = '';
		$scope.password = '';
	}
    $scope.validate = function(){
		if ($('#userName').val() == '') {
			alert('UserName is required');
			$('#userName').focus();
			return false;
		} else if ($('#password').val() == '') {
			alert('Password is required');
			$('#password').focus();
			return false;
		}
			if($scope.remember == true) {
					loginDetails = { 'uname': $scope.userName, 'pwd': $scope.password};
					localStorage.setItem('loginDetails', JSON.stringify(loginDetails));
            } else {
            	localStorage.removeItem('loginDetails');
            }
		document.forms["login"].submit();
    }
    $(document).keyup(function(event){
		if(event.keyCode==13){
			$scope.validate();
		}
	});
	
}]);