var groupBOTypeList = [];
var myapp = angular.module('myapp', ['ui.bootstrap','popupdirective']);
myapp.controller('groupController', ['$scope','$modal', '$log','$timeout', function ($scope, $modal,$log,$timeout) {
	//Special character check for Group Name
	$scope.alpha = function(e) {
		var k;
		document.all ? k = e.keyCode : k = e.which;
		if(!((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57)|| (k==95))){
			var message = "Special characters are not allowed.";
			e.currentTarget.blur();
			$('#dvMessages').hide();
			$('#dvMessages').show(400);
			$('#dvMessages').html(message);
			$('#dvMessages').addClass('fail');
		} else {
			$('#dvMessages').hide();
		}
		return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57) || (k==95));
	
	};	
	$scope.lePopupCtrl = {
            
    }

	$scope.showErrorPopupOnPageLoad = function(data) {
		if (data.status==412) {
			$scope.$watch( "lePopupCtrl.showError" , function(n,o){
				if(n==o) {
					return
				};
				$timeout(function(){
					$scope.lePopupCtrl.showError('Rule Workbench',JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details,'OK');
				},0);	
			});
		}				
	}
    $scope.showErrorPopup = function(data) {
    	if (data.status==412) {
			$timeout(function(){
				$scope.lePopupCtrl.showError('Rule Workbench',JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details,'OK');
			},0);	
		}
    }
	
 $scope.showNode = function(data) {
		return data.type;
    }; 

$scope.displayTree ="";
	function defaultTreeStruct(){
		$(".tree").show();
		$("#tree_view_tab").show();
		$("#groupTreeview").dialog('open');
		$('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Expand this branch');
		$('.tree li:has(ul)').find(' > ul > li').hide();
		$('.tree li:has(ul)').find(' > span > i').addClass('icon-plus-sign');
	}	
	
	$scope.createGroupTree = function() {
		var treeNode = [];
		var groupName = $scope.resultMap.group.groupName;
		$scope.commonRuleList =[];
		var existingElmClass = "";
		if($scope.resultMap.ruleList && $scope.resultMap.ruleList != undefined){
			$scope.commonRuleList = $scope.resultMap.commonRuleList;
			$scope.ruleSetRuleMap = $scope.resultMap.ruleSetRuleMap;
			
			$($scope.resultMap.ruleList).each(function (i, v) {
				var ruleNode = [];
				existingElmClass = "";
				var showCheckBox = false;
				if($.inArray(v.ruleName, $scope.commonRuleList) > -1){
						existingElmClass = "existingGrp";
						showCheckBox = true;
				}
				if(v.ruleTypeID == 1){
					ruleNode = {"type":v.ruleName,"showCheckBox":showCheckBox,"duplicateClass": existingElmClass,"nodes":[]};
				} else if(v.ruleTypeID == 3 ){
					/*var ruleSetRuleNodes = [];
					if($scope.ruleSetRuleMap != undefined) {
						var rulsetRuleList = $scope.ruleSetRuleMap[v.ruleName];
						$(rulsetRuleList).each(function (i, ruleName) {
							var ruleSetRuleNode = {"type":ruleName,"showCheckBox":false,"duplicateClass":existingElmClass,"nodes":[]};
							ruleSetRuleNodes.push(ruleSetRuleNode);
						
						});
					}	*/
					ruleNode = {"type":v.ruleName,"showCheckBox":showCheckBox,"duplicateClass": existingElmClass,"nodes":[]};	
				}
				treeNode.push(ruleNode); 
			});
		}
		
			$scope.displayTree = [{"type":groupName,"showCheckBox":false,"duplicateClass": existingElmClass,"nodes":treeNode}];
			return $scope.displayTree;
	
	};
	$scope.createGroupTreeForCompressedRules = function() {
		
		var treeNode = [];
		var existingElmClass = "";
		var groupName = $scope.resultMap.group.groupName;
		 var rule = $scope.resultMap.rule;
		 var showCheckBox = false;
		if($scope.resultMap.hasOwnProperty("isRuleNameSame")){
			if($scope.resultMap.isRuleNameSame){
				existingElmClass = "existingGrp";
				showCheckBox = true;
				$scope.message ="Group "+groupName+ " already exist with the below highlighted rules/rulesets. If you want to overwrite, please select the rules/rulesets and click proceed button.";
			} else {
				$scope.showProceed = false;
				$scope.messageImp = "mandatory_red";
				$scope.message ="Group "+groupName+ " already exist and the rule name given is not matching.";
			}
		} else {
			$scope.message = groupName+ " group structure is given below. Click proceed to import the rules/rulesets.";
		}
		var ruleNode = {"type":rule.ruleName,"showCheckBox":showCheckBox,"duplicateClass":existingElmClass,"nodes":[]};
		treeNode.push(ruleNode);
		$scope.displayTree = [{"type":groupName,"showCheckBox":false,"duplicateClass":"","nodes":treeNode}];
		return $scope.displayTree;
	}
	
	
	$scope.getOverwritingRules = function() {
		$scope.overWritingRuleList = [];
		$('input:checkbox.treeview_check').each(function() {
			if($(this).is(':checked')) {
				$scope.overWritingRuleList.push($(this).attr("id"));
			 } 
		});
		//$scope.excludedRuleList = $scope.commonRuleList.filter(function(obj) { return $scope.overWritingRuleList.indexOf(obj) == -1; });
	};
	$scope.excludedRules = function() {
		$scope.excludedRuleList = $scope.resultMap.commonRuleList.filter(function(obj) { return $scope.overWritingRuleList.indexOf(obj) == -1; });
	}
	
	$scope.getRuleAndRulesetList = function() {
		$scope.ruleSetIdList = [];
		$scope.selectedRuleList = [];
		$scope.ruleSetRuleMapping = JSON.parse(JSON.stringify($scope.resultMap.ruleSetRuleMap));
		$($scope.resultMap.ruleList).each(function (i, v) {
			if($.inArray(v.ruleName, $scope.excludedRuleList) > -1){
				if(v.ruleTypeID == 3){
					$scope.ruleSetIdList.push(v.ruleName);
				} 
			} else {
				$scope.selectedRuleList.push(v);
			}
		});
		if($scope.ruleSetIdList != null && $scope.ruleSetIdList.length > 0){
			$($scope.ruleSetIdList).each(function (i, v) {
				delete $scope.ruleSetRuleMapping[v];
			});
		
		}
	};
	
	$scope.reloadPage = function() {
		location.reload();
	};
	
	$scope.showMessage = function() {
	$scope.message = "Selected ruleset contains below new rules. Please include those rules for a successfull import. ";
	$($scope.newRuleList).each(function (i, v) {
		$scope.ruleNamemsg =i+1+"."+v+'\n';
	});
		$scope.message = $scope.message+"\n"+$scope.ruleNamemsg ;
		return $scope.message;
	};
	
	$scope.importGroup = function() {
		var importGroupData ={};
		if(!$scope.resultMap.isCompressed){
			if($scope.resultMap.isGroupExists){
				$scope.getOverwritingRules();
				$scope.excludedRules();
				$scope.getRuleAndRulesetList();
				
			} else {
				$scope.selectedRuleList = $scope.resultMap.ruleList;
				$scope.overWritingRuleList = [];
				$scope.ruleSetRuleMapping = $scope.resultMap.ruleSetRuleMap;
			}
			importGroupData = {group : $scope.resultMap.group,ruleList : $scope.selectedRuleList,commonRules : $scope.overWritingRuleList, ruleSetRuleMap : $scope.ruleSetRuleMapping};
		} else {
			var ruleData = $scope.resultMap.rule;
			if($scope.resultMap.isGroupExists){
				$scope.getOverwritingRules();
				if($scope.overWritingRuleList.length == 0){
					ruleData = null;
				} 
			} 
			importGroupData = {group : $scope.resultMap.group,rule : ruleData};
			
		}
		
		if((typeof $scope.selectedRuleList!== 'undefined' && $scope.selectedRuleList.length > 0) || (typeof $scope.overWritingRuleList!== 'undefined' && $scope.overWritingRuleList > 0) || (typeof ruleData!== 'undefined' && ruleData != null)){
				$.ajax({
					type: "POST",
					url: "../importGroup",
					dataType : "json",
					data : {importGroupData : $.toJSON(importGroupData),isGroupExists : $scope.resultMap.isGroupExists,isCompressed : $scope.resultMap.isCompressed},
					async : false,
					cache : false,
					success : function(data) {
					
						$("#groupTreeview").dialog('close');
						if(data.isSuccess || data.isSuccess == "true"){
							$timeout(function(){
								$scope.lePopupCtrl.showInformation('Rule Workbench','Group imported successfully.','OK',$scope.reloadPage);
							},0); 
						} else {
							$scope.newRuleList = data.newRuleList;
							$timeout(function(){
								$scope.lePopupCtrl.showInformation('Rule Workbench','Ruleset you are trying to import is partial. Click details for more information.','OK',undefined,"Details",$scope.showMessage);
							},0);
						}
					},
					error : function(data) {
							$("#groupTreeview").dialog('close');
							$scope.showErrorPopup(data);
					}
			});
		} else {
			$('#importErrorMsg').show(300);
			$('#importErrorMsg').html("Please select a rule/rulesets.");
			$('#importErrorMsg').addClass('fail');
		}
		
	};
	
	$scope.validateForm  = function() {
		var fileName = $("#file").val(); 
		var password = $("#password").val();
		var extension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
		if(fileName == ''){
			$scope.showErrorMessage("Please upload a zip file.");
			return false;
		} else if(password == ''){
			$scope.showErrorMessage("Please enter the password.");
			return false;
		} else if(extension != "zip"){
			$scope.showErrorMessage("Invalid file extension, please upload a zip file.");
			return false;
		}
		return true;
	};
	
	$scope.showErrorMessage  = function(message) {
		$('#errorMessages').show(400);
		$('#errorMessages').html(message);
		$('#errorMessages').addClass('fail');
	};
	
	 $("#importBtn").click(function(e) {
		 $('#importErrorMsg').hide();
		 var oMyForm = new FormData();
		 oMyForm.append("file", file.files[0]);
		 oMyForm.append("password", $('#password').val());
		 if($scope.validateForm()){
			  $.ajax({
				type: 'POST',
				dataType: 'text',
				url: '../uploadCSV',
				data: oMyForm,
				processData: false,
				contentType: false,
				success: function(data){
					data = JSON.parse(data);
					$scope.resultMap = data;
					 
					if(data.hasOwnProperty('errorMessage')){
						//$('#errorMessages').hide();
						$('#errorMessages').show(400);
						$('#errorMessages').html(data.errorMessage);
						$('#errorMessages').addClass('fail');
					} else {
						var groupName = $scope.resultMap.group.groupName;
						$scope.showProceed = true;
						$scope.messageImp ="";
						if(!data.isCompressed){
							 $scope.createGroupTree();
							 if(data.isGroupExists){
								 $scope.message ="Group "+groupName+ " already exist with the below highlighted rules/rulesets. If you want to overwrite, please select the rules/rulesets and click proceed button.";
							 } else {
								 $scope.message = groupName+ " group structure is given below. Click proceed to import the rules/rulesets.";
							 }
						} else if(data.isCompressed) {
							$scope.createGroupTreeForCompressedRules();
							
						}
						
						 $('.modal_pop_up.import_more_pop_up').css("display","none");
						  $("body").removeClass("hide_scroll"); 
						  $('#page_loader').css("display","none");
						 $timeout(function(){
							defaultTreeStruct();
						},0); 
					}
				},
			  error : function(data) {
					$scope.showErrorPopup(data);
					}
			  
			  });
			}
	});
//}




	 
		
	 
	$scope.listGroup = function() {
		var groupList = [];
		$.ajax({
			type : "POST",
			url : "../listGroupTable",
			dataType : "json",
			data : {},
			async : false,
			cache : false,
			success : function(data) {
				groupList = data.groupList;
			},
			error : function(data) {
 				$scope.showErrorPopupOnPageLoad(data);
 			}
		});

		$.ajax({
			type : "POST",
			url : "../listGroupType",
			dataType : "json",
			data : {},
			async : false,
			cache : false,
			success : function(data) {
				$scope.groupBOTypeList = data;
				$scope.groupType = 1;
				for ( var i = 0; i < groupBOTypeList.length; i++) {
					$(("#groupTypeSel") + "").append(
							"<option value='" + groupBOTypeList[i].groupTypeID
									+ "'>" + groupBOTypeList[i].groupType
									+ "</option>");
								$(("#groupTypeDrpdwn") + "").append(
							"<option value='" + groupBOTypeList[i].groupTypeID
									+ "'>" + groupBOTypeList[i].groupType
									+ "</option>");
				}
			},
			error : function(data) {
				$scope.showErrorPopupOnPageLoad(data);
 			}
		});
		return groupList;
	}

	$scope.listGroups = $scope.listGroup();
	$scope.deleteGroup = function(id) {
			var groupID = id;
			$scope.getData(groupID).done($scope.handleData);		
	
	}
	
	
	$scope.getData=function(groupID) {
	    return $.ajax({
			type : "POST",
			url:"../confirmGroupDelete",
			dataType : "json",
			data : {groupID :groupID},
			async : false,
			cache : false,
			success : function(data) {
				//location.reload(); 
			},
			error : function(data) {
				$scope.showErrorPopup(data);
			}
		});
	}			
	
	$scope.handleData=function(data) {
		var resultMap = data;
		
		var message = "";
		if(resultMap.hasOwnProperty('isRuleAndRuleSetExists')){
			$timeout(function(){
				$scope.lePopupCtrl.showQuestion('Rule Workbench','Are you sure you want to delete it?','Confirm',$scope.deleteGroupCall,'Cancel');
			},0);
		} else {
			if(parseInt(data.ruleCount) > 0){
				message = "Group contains "+data.ruleCount+" rules ";
			}
			if(parseInt(data.ruleSetCount) > 0){
					message += "and "+data.ruleSetCount+" rule sets ";
			}
			message +='.Are you sure you want to delete it?';
			$timeout(function(){
				$scope.lePopupCtrl.showQuestion('Rule Workbench',message,'Confirm',$scope.deleteGroupCall,'Cancel');	
			},0);
		}
		
		$scope.deleteGroupCall =function(){
			$.ajax({
				type : "POST",
				url:"../deleteGroup",
				dataType : "json",
				data : {groupID : data.groupID},
				async : false,
				cache : false,
				success : function(data) {
							location.reload(); 
				},
				error : function(data) {
						$scope.showErrorPopup(data);
				}
			});
		}
		
		
	}
	
	$scope.cloneGroup = function(groupID,groupName,typeId){
		$.ajax({
				type: "POST",
			    url: "../listRuleByGroup",
				dataType : "json",
				data : { groupID : groupID},
				async : false,
				cache : false,												
				success : function(data) {					
					var ruleBOList = data;
					if(ruleBOList!= null && ruleBOList.length != 0){
						window.location="../jsp/cloneGroup.jsp?groupID="+groupID+"&groupName="+groupName+"&typeId="+typeId;
					} else {
						$timeout(function(){
							$scope.lePopupCtrl.showError('Rule Workbench','No rule exist for this group.Please add rule.','Ok');

						},0);
					}
				},
				error : function(data) {
    				$scope.showErrorPopup(data);
    			}
		
			});
		
		}
		
		$scope.addGroup = function(groupID,groupName,groupType,comments){
			$scope.groupID = groupID;
			if(groupType !=''){
				$scope.popupHeaderName = "Edit Group";
				$scope.groupType = groupType;
				$('#groupTypeId').prop("disabled", true);
				$scope.commentTextArea = comments;
			} else {
				$scope.popupHeaderName = "Add Group";
				$scope.groupType = 1;
				$scope.commentTextArea = "";
			}
			$scope.groupName = groupName;
			$scope.groupNameBeforEdit = groupName;
			$('#dvMessages').hide();	
			$('#dvMessage').hide();
		    $('#page_loader').empty();
			var dynamic_height = $('body').height();
			$('#page_loader').css('height',dynamic_height).fadeIn(300);
			$scope.add();
		
		}
		
		$scope.clearFileField =function () {
			var control = $("#file");
			control.replaceWith( control = control.clone( true ) );
		}
		
		$scope.importGroupPopUp = function(){
			$('#errorMessages').hide();
			$scope.clearFileField();
			$('#password').val('');
			
		    $('#page_loader').empty();
			var dynamic_height = $('body').height();
			$('#page_loader').css('height',dynamic_height).fadeIn(300);
			$scope.importGP();
		
		}
		$scope.importGP = function(){
			var flag = $('#page_loader').css("display");
			 if(flag == "block"){	
			    $('.modal_pop_up.import_more_pop_up').css("display","block");
			    var top = ($(window).height() - $('.modal_pop_up.import_more_pop_up').height()) / 2;	   
		        var left = ($(window).width() - $('.modal_pop_up.import_more_pop_up').width()) / 2;	  
				$('.modal_pop_up.import_more_pop_up').css({ 'top': top + $(document).scrollTop(),'left': left});
				$("body").removeClass("hide_scroll").addClass("hide_scroll");		
			}
			else{
				$("body").removeClass("hide_scroll");		 
			}
		}
		
		
		$scope.add = function(){
			var flag = $('#page_loader').css("display");
			 if(flag == "block"){	
			    $('.modal_pop_up.add_more_pop_up').css("display","block");
			    var top = ($(window).height() - $('.modal_pop_up.add_more_pop_up').height()) / 2;	   
		        var left = ($(window).width() - $('.modal_pop_up.add_more_pop_up').width()) / 2;	  
				$('.modal_pop_up.add_more_pop_up').css({ 'top': top + $(document).scrollTop(),'left': left});
				$("body").removeClass("hide_scroll").addClass("hide_scroll");		
			}
			else{
				$("body").removeClass("hide_scroll");		 
			}
		}
		
		$scope.cancelImport = function(){
			$("#groupTreeview").dialog('close');
		};
	
	$(".SaveGroupName").click(function(e) {
		e.preventDefault();
		var groupName = $('#txtGroupName').val();
		var comments = $('#commentTextArea').val();
		if(groupName != null && groupName != ''){
			if(comments != null && comments != '') {
					var groupTypeID = $scope.groupType;
					if($scope.groupID > 0) {
						var isGrpNameChanged = false;
						if($.trim(groupName.toUpperCase()) != $scope.groupNameBeforEdit.toUpperCase()){
							isGrpNameChanged = true;
						}
						$.ajax({
							type : "POST",
							url: "../updateGroupAdd",
							dataType : "json",
							data : { groupID: $scope.groupID , groupName : groupName, groupTypeID : groupTypeID, isGrpNameChanged : isGrpNameChanged, comments : comments},
							async : false,
							cache : false,
							success : function(data) {
								var result = data.updateGroupResult;
								if(result  == "false" || result  == false){
									$('#dvMessages').hide();
									$('#dvMessages').show(400);
									$('#dvMessages').html("Group aleardy exists");
									$('#dvMessages').addClass('fail');
								} else {
									$('.modal_pop_up').css("display","none");
									$("body").removeClass("hide_scroll"); 
									$('#page_loader').css("display","none");
									$timeout(function(){
										$scope.lePopupCtrl.showInformation('Rule Workbench','Group updated successfully.','OK',$scope.reloadPage);
									},0); 
								}
							},
							error : function(data) {
								$scope.showErrorPopup(data);
							}
					});
						
					} else {
						$.ajax({
								type : "POST",
								url: "../groupAdd",
								dataType : "json",
								data : { groupName : groupName, groupTypeID : groupTypeID, comments : comments},
								async : false,
								cache : false,
								success : function(data) {
									json = data;
									var result = json.createGroupResult;
									if(result == "false" || result == false){
										$('#dvMessages').hide();
										$('#dvMessages').show(400);
										$('#dvMessages').html("Group aleardy exists");
										$('#dvMessages').addClass('fail');
									} else {
										$('.modal_pop_up').css("display","none");
										$("body").removeClass("hide_scroll"); 
										$('#page_loader').css("display","none");
										$timeout(function(){
											$scope.lePopupCtrl.showInformation('Rule Workbench','Group created successfully.','OK',$scope.reloadPage);
										},0);  
									}
								},
								error : function(data) {
									$scope.showErrorPopup(data);
								}
							});
					}
			} else {
				$('#dvMessages').hide();
				$('#dvMessages').show(400);
				$('#dvMessages').html("Please enter valid comments.");
				$('#dvMessages').addClass('fail');
			}
		} else {
			$('#dvMessages').hide();
			$('#dvMessages').show(400);
			$('#dvMessages').html("Please Enter the Group Name");
			$('#dvMessages').addClass('fail');
		}
		
	});

} ]);

