var ruleBOListCopy = [];
var groupCloneName;
var groupID = "";
var groupTypeID = "";
var myapp = angular.module('myapp', ['ui.bootstrap','popupdirective']);
myapp.controller('DownloadGroupController', ['$scope','$modal', '$log','$timeout', function ($scope, $modal,$log,$timeout) {
	
	//Special character check for Clone Group Name
	$scope.alpha = function(e) {
		var k;
		document.all ? k = e.keyCode : k = e.which;
		if(!((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57)|| (k==95))){
			var message = "Special characters are not allowed.";
			e.currentTarget.blur();
			$('#dvMessages').hide();
			$('#dvMessages').show(400);
			$('#dvMessages').html(message);
			$('#dvMessages').addClass('fail');
		} else {
			$('#dvMessages').hide();
		}
		return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57) || (k==95));
	
	};         
	
	
	$scope.lePopupCtrl = {
            
    }
	
	$scope.showErrorPopupOnPageLoad = function(data) {
		if (data.status==412) {
			$scope.$watch( "lePopupCtrl.showError" , function(n,o){
				if(n==o) {
					return
				};
				$timeout(function(){
					$scope.lePopupCtrl.showError('Rule Workbench',JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details,'OK');
				},0);	
			});
		}				
	}
    $scope.showErrorPopup = function(data) {
    	if (data.status==412) {
			$timeout(function(){
				$scope.lePopupCtrl.showError('Rule Workbench',JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details,'OK');
			},0);	
		}
    }
    $scope.isDownload = false;
	
	if (window.location.href.indexOf("?") != -1) {
		var pairs = window.location.href.substring(window.location.href.indexOf("?")+1).split("&")
		for (var i = 0, len = pairs.length; i < len; i++) {
			pair = pairs[i].split('=');
			if (pair[0] == 'groupID') {
				groupID = pair[1];
			}  else if (pair[0] == 'groupName') {
				$scope.groupName = pair[1];
			} 
		}
	}

	
$scope.listRulesByGroup = function(groupID){
	$scope.ruleBOList =[];
		$.ajax({
    			type: "POST",
    		    url: "../listGroupDownloadRules",
				dataType : "json",
				data : { groupID : groupID },
				async : false,
				cache : false,												
				success : function(data) {
					$scope.ruleBOList = data;
							$scope.groupID = groupID;
						},
				error : function(data) {
							$scope.showErrorPopupOnPageLoad(data);			
	    			}

				});
return $scope.ruleBOList;
	};
		$scope.listRulesByGroup = $scope.listRulesByGroup(groupID); 
		
		$scope.download = function(){
			var path = "../downloadGroupRules";
			window.location.href = path+"?ruleNameList="+$.toJSON($scope.ruleNameList)+"&groupId="+$scope.groupID+"&fileName="+$scope.fileName;
		}
		
		
		$scope.findReferedRules=function() {
		    return $.ajax({
		    	type: "POST",
	 		    url: "../findReferedRules",
				dataType : "json",
				data : {ruleNameList : $.toJSON($scope.ruleNameList),groupId : $scope.groupID},
				async : false,
				cache : false,
				success : function(data) {
					
				if(data.isNonMatch == true || data.isNonMatch == "true"){
					$scope.isDownload = false ;
					$scope.referedRules= data.referedRules;
					$scope.message = data.message;
					$timeout(function(){
	                      //$scope.lePopupCtrl.showInformation('Rule Workbench',data.message,'OK');
						 $scope.lePopupCtrl.showQuestion('Rule Workbench','Selected rules contain other rules.Do you want to include them?','Yes',$scope.downloadReferedRules,'No',$scope.excludeReferedRules,'Details',$scope.showReferedRuleMeassage);
	      },0);	
				} else {
					$scope.download();
				}
				
				},
				error : function(data) {
					$scope.showErrorPopup(data);
				}
		    });
		}
		$scope.showReferedRuleMeassage= function(){
		
			return $scope.message;
			
			
		}
		$scope.downloadReferedRules = function() { 
			$scope.ruleNameList = $scope.ruleNameList.concat($scope.referedRules);
			$scope.findReferedRules();
		}
		
		$scope.excludeReferedRules = function() { 
			//$scope.isDownload = true ;
			var path = "../downloadGroupRules";
					window.location.href = path+"?ruleNameList="+$.toJSON($scope.ruleNameList)+"&groupId="+$scope.groupID+"&fileName="+$scope.fileName;
		}
		
		 $('.dwnLoad_btn').click(function() {
			$('#dvMessages').hide();
			$('#txtFileName').val("");
			$scope.fileName = "";
			var ruleList = getRulesForDownload();
			var ruleIDList = ruleList.ruleIDList;
			if($scope.ruleBOList != undefined && $scope.ruleBOList.length !== 0){
				if(ruleIDList.length !== 0){
					var getvalue = $(this).attr('rel');
					$('#page_loader').empty();
					var dynamic_height = $("body").height();
					$('#page_loader').css('height',dynamic_height).fadeIn(300);
					edit();
				} else {
				  
				  	$timeout(function(){
							 $scope.lePopupCtrl.showInformation('Rule Workbench','Please select a rule.','OK');
				  	},0);	
				   
				}
			} else {
				  
				$timeout(function(){
					//	$scope.lePopupCtrl.showInformation('Rule Workbench','Please add a rule.','OK');
				},0); 
			}	
			
		});
		function edit(){
			 var flag = $('#page_loader').css("display");
			  if(flag == "block"){	
			    $('.modal_pop_up.edit_more_pop_up').css("display","block");
			    var top = ($(window).height() - $('.modal_pop_up.edit_more_pop_up').height()) / 2;	   
		        var left = ($(window).width() - $('.modal_pop_up.edit_more_pop_up').width()) / 2;	  
				$('.modal_pop_up.edit_more_pop_up').css({ 'top': top + $(document).scrollTop(),'left': left});
				$("body").removeClass("hide_scroll").addClass("hide_scroll");		
			}
			else{
				$("body").removeClass("hide_scroll");		 
			}
		}
 
		function getRulesForDownload(){
			var ruleIDList = [];
			var ruleNameList = [];
            var ruleList = [];
            $('input:checkbox.rule_select_chkbox').each(function() {
                  if($(this).is(':checked')) {
                	  ruleIDList.push($(this).attr("id"));
					  ruleNameList.push($(this).val());
                  } 
            });
			ruleList["ruleIDList"] = ruleIDList;
			ruleList["ruleNameList"] = ruleNameList;
			return ruleList;
		}
		
		$("#downloadButton").click(function(e) {
			e.preventDefault();
           // var fileName = $('#txtFileName').val();
            if($scope.fileName != null && $scope.fileName != ''){
            	$scope.ruleList = getRulesForDownload();
            	$scope.ruleNameList = $scope.ruleList.ruleNameList;
				$scope.findReferedRules();
				$('.modal_pop_up').css("display","none");
				$("body").removeClass("hide_scroll"); 
				$('#page_loader').css("display","none");
				
            } else {
				$('#dvMessages').show(400);
				$('#dvMessages').html("Please enter the file name.");
				$('#dvMessages').addClass('fail');
			}
            });
			
		 // add multiple select / deselect functionality
	    
		$("table.fix_head").on("click", "#selectall", function(e) {
	          $('.rule_select_chkbox').attr('checked', this.checked);
	    });
	 
	    // if all checkbox are selected, check the selectall checkbox
	    // and viceversa
		$("table.cust_tbl").on("click", "td", function(event) {
			if ($(event.target).is('input[type=checkbox]')) {
				if($(".rule_select_chkbox").length == $(".rule_select_chkbox:checked").length) {
					$("#selectall").attr("checked", "checked");
				} else {
					$("#selectall").removeAttr("checked");
				}
			}
		});	
	
} ]);


