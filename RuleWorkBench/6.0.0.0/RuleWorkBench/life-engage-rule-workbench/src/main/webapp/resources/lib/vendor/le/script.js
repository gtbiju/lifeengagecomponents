// JavaScript Document
/* select box rounded corner code starts*/

(function($){
 $.fn.extend({
 
 	customStyle : function(options) {
	  if(!$.browser.msie || ($.browser.msie&&$.browser.version>6)){
	  return this.each(function() {
	 
			var currentSelected = $(this).find(':selected');
			$(this).after('<span class="customStyleSelectBox"><span class="customStyleSelectBoxInner" style="font-size:1.0em">'+currentSelected.text()+'</span></span>').css({position:'absolute',marginLeft:"10px",height:"27px",opacity:0,fontSize:"1.0em"});
			var selectBoxSpan = $(this).next();
			
			var selectBoxWidth = parseInt($(this).width());			
			var selectBoxSpanInner = selectBoxSpan.find(':first-child');
			selectBoxSpan.css({display:'inline-block'});
			selectBoxSpanInner.css({width:selectBoxWidth,display:'inline-block'});
		
			var selectBoxHeight = parseInt(selectBoxSpan.height()) + parseInt(selectBoxSpan.css('padding-top')) + parseInt(selectBoxSpan.css('padding-bottom'));
			$(this).height(selectBoxHeight).change(function(){
				// selectBoxSpanInner.text($(this).val()).parent().addClass('changed');   
			selectBoxSpanInner.text($(this).find(':selected').text()).parent().addClass('changed');
				
			});
			
	  });
	  }
	}
 });
})(jQuery);
/* select box rounded corner code ends*/

/*popup code starts*/



var popupStatus = 0;


function loadPopup(){
	
	if(popupStatus==0){
		$(".overlay").css({
			"opacity": "0.7"
		});
		$(".overlay").fadeIn("slow");
		$(".popup").fadeIn("slow");
		popupStatus = 1;
	}
}

function disablePopup(){
	
	if(popupStatus==1){
		$(".overlay").fadeOut("slow");
		$(".popup").fadeOut("slow");
		popupStatus = 0;
	}
}

function centerPopup(){
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $(".popup").height();
	var popupWidth = $(".popup").width();
	
	$(".popup").css({
		"position": "absolute",
		"top": windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2
	});
	
	$("#overlay").css({
		"height": windowHeight
	});
	
}




/*popup code ends*/


$(document).ready(function(){
	$('select').customStyle();
	
	/*script to hide the file upload control in ie alone starts */
	if ( $.browser.msie ) {
	 $(".file").css('display','none');
	
 }
 	/*script to hide the file upload control in ie alone ends */
	
	/* file upload  script starts */



	 $(".upload").click(function(){	
if ( $.browser.msie ) {
	
	  $(".file").val('');
	  $(".file").trigger('click')
	 	if($(".file").val()!='')
		{
		$(".uploadText").val($(".file").val());
	
		}
}
else
{

	  $(".file").trigger('click');
	  $('.file').change(function(){
	 if(($(".file").val()))
	{
		$(".uploadText").val($(".file").val());
			
	}
	});	

}
	  });
	 
/* file upload  script ends  */
	
	$('.radioClk').click(function()
	{
		$('.radioClk').attr('src',"images/Img_radio.png");
		if($(this).attr('src')=="images/Img_radio.png")
		{
			$(this).attr('src',"images/Img_radio_sel.png");
		}
		else
		{
			$(this).attr('src',"images/Img_radio.png");
		}
	});
	
	$('.checkClk').click(function()
	{
		
		if($(this).attr('src')=="images/checkBoxImg.png")
		{
			$(this).attr('src',"images/checkBoxSelImg.png");
		}
		else
		{
			$(this).attr('src',"images/checkBoxImg.png");
		}
	});
	
	
	$(".groupDetailsSearchLeft").click(function(){
	
		centerPopup();
		loadPopup();
	});
		$(".popupHeader .popupHeaderCenter img").click(function(){
		disablePopup();
	});
	$(".searchAttributes").click(function(){
		centerPopup();
		loadPopup();
	});
	
	/* MODAL POP_UP OPENING CLOSING */
	$('#add_more_btn').click(function(){
		document.getElementById('upload_message_span').innerHTML = "";
	    $('#page_loader').empty();
		var dynamic_height = $('body').height();
		$('#page_loader').css('height',dynamic_height).fadeIn(300);
		addMore();
	});
	
	function addMore(){
		 var flag = $('#page_loader').css("display");
		  if(flag == "block"){	
		    $('.modal_pop_up.add_more_pop_up').css("display","block");
		    var top = ($(window).height() - $('.modal_pop_up.add_more_pop_up').height()) / 2;	   
	        var left = ($(window).width() - $('.modal_pop_up.add_more_pop_up').width()) / 2;	  
			$('.modal_pop_up.add_more_pop_up').css({ 'top': top + $(document).scrollTop(),'left': left});
			$("body").removeClass("hide_scroll").addClass("hide_scroll");		
		}
		else{
			$("body").removeClass("hide_scroll");		 
		}
		}
	
	/* EDIT POP_UP OPENING CLOSING */
	$(".edit_btn").live("click",function(){
		alert("hiii");
		document.getElementById('upload_message_span').innerHTML = "";
	    $('#page_loader').empty();
		var dynamic_height = $('body').height();
		$('#page_loader').css('height',dynamic_height).fadeIn(300);
		edit();
	});
	function edit(){
		 var flag = $('#page_loader').css("display");
		  if(flag == "block"){	
		    $('.modal_pop_up.edit_more_pop_up').css("display","block");
		    var top = ($(window).height() - $('.modal_pop_up.edit_more_pop_up').height()) / 2;	   
	        var left = ($(window).width() - $('.modal_pop_up.edit_more_pop_up').width()) / 2;	  
			$('.modal_pop_up.edit_more_pop_up').css({ 'top': top + $(document).scrollTop(),'left': left});
			$("body").removeClass("hide_scroll").addClass("hide_scroll");		
		}
		else{
			$("body").removeClass("hide_scroll");		 
		}
		}
	
});


