﻿var myvar='';
var test;
(function($) {
	
	var FormulaTextArea;
	var functionlist = Get_LIB_Functions();
	var fTxtPrt1_gbl, fTxtPrt2_gbl; // Global Scope variables to pat the string
	// if some formula is going to be inserted
	// inside
	var lastCharacterDispDiv = document.createElement("div"); // TO STORE AND
	// CAPTURE WHAT
	// IS THE LAST
	// KEYED IN
	// STRING IN THE
	// TXTAREA
	lastCharacterDispDiv.name = "lastCharacterDispDiv";
	lastCharacterDispDiv.Id = "lastCharacterDispDiv"
	var LastCharacterDiv = lastCharacterDispDiv; // TO STORE AND CAPTURE WHAT
	// IS THE LAST KEYED IN
	// STRING IN THE TXTAREA
	var intBrHighlightTmOt = 500;
	var selectedTextBox;
	var inteliMenuListClone = functionlist;

	$.fn.RefreshCloneList = function(variableID, values) {
		inteliMenuListClone[variableID] = values;

	};
	
	CloneList = function(variableID, values) {
		inteliMenuListClone[variableID] = values;

	};

	$.fn.AddIntelliSense = function(txtAreaControl, optionsHolder, optionsList,
			optionsListCopy, hiddenTextBox,data) {
		
		// / <summary>
		// / Adds the intellisense feature to the prescribed text area where the
		// options will be poped out on key up.
		// / </summary>
		// / <param name="txtAreaControl" type="textBox control">
		// / Area where in the user types the formula
		// / </param>
		// / <param name="optionsHolder" type="Div control">
		// / Div where selection list resides.
		// / </param>
		// / <param name="optionsList" type="Options control">
		// / selection list where the list of params,functions and all other
		// things are bound
		// / </param>
		// / <returns />

		FormulaTextArea = txtAreaControl; // TEXT AREA IN WHICH THE USER TYPES
		// THE FORMULA
		var SelectionMenuDiv = optionsHolder; // DIV WHICH CONTAINS THE
		// LISTBOX
		var SelectionMenuListBox = optionsList; // LIST BOX WHICH CONTAINS THE
		// ITEMS

		// var cloneList = CreateClone();
		// var jdata = $.toJSON(functionlist);
		// var SelectionMenuListBox = functionlist[0];

		// var inteliMenuListClone = optionsListCopy[0]; // cloneList;

		// alert(Object.values(functionlist));
		// $.each(functionlist, function () {
		// var inteliMenuListClone = Object.keys(this);
		// });

		//       
		//      
		// alert(inteliMenuListClone);

		$(FormulaTextArea).die();

		var CounterBracketIndex = function(strToValidate, startPosition,
				selectedBracket) {
			// / <summary>
			// / Validates and throws error on unmatched braces
			// / </summary>
			// / <param name="strToValidate" type="String">
			// / String which is needed to be validated for braces
			// / </param>
			// / <returns />
			var openBrCount = 0;
			var strArray = strToValidate.split('');
			/*
			 * if selected bracket is nothing but the start position is there
			 * user might most probably clicking on either side of the bracket
			 * than selecting it below logic gets the left and right of the
			 * start position to check wether the brackets are in the visinity
			 */
			if ($.trim(selectedBracket) == ""
					&& startPosition > 0
					&& startPosition <= strToValidate.length
					&& (strArray[startPosition] != ")" && strArray[startPosition] != "(")) {
				// there are no chars between braces selection need not work
				var leftOfIndex = strArray[startPosition - 1];
				selectedBracket = leftOfIndex;
				startPosition--;
			}
			if (selectedBracket == "(") {
				for ( var index = startPosition; index < strArray.length
						&& index >= 0; index++) {
					if (strArray[index] == selectedBracket)
						openBrCount++;
					else if (strArray[index] == ")") {
						openBrCount--;
						if (openBrCount == 0)
							return index;
					}
				}
			} else if (selectedBracket == ')') {
				for ( var index = startPosition; index <= strArray.length
						&& index >= 0; index--) {
					if (strArray[index] == selectedBracket)
						openBrCount++;
					else if (strArray[index] == "(") {
						openBrCount--;
						if (openBrCount == 0)
							return (index);
					}
				}
			}
		}

		var GetCharet = function() {
			// /Get the character position of the clicked character
			var iCaretPos = 0;
			var el = selectedTextBox.get(0)// FormulaTextArea.get(0);//get the
			// selected textbox instead
			// IE Support : for FF its not Document.selection, create a range
			// explicit for the same.
			if (document.selection) {
				el.focus();

				var range = document.selection.createRange();
				/*if (window.getSelection) { // all browsers, except IE before
					// version 9
					range = window.getSelection();
				} else {
					range = document.getSelection();
				}*/

				if (range == null)
					return 0;
				var re = el.createTextRange();
				rc = re.duplicate();
				re.moveToBookmark(range.getBookmark());
				rc.setEndPoint('EndToStart', re);
				iCaretPos = rc.text.length;
				return [ iCaretPos, re.text ];
			}
			
			else {
				/*
                el.focus();
                var range = document.getSelection();

                if (range == null) return 0;
                //var re = el.createTextRange();
                var rangeObj = document.createRange();
                rangeObj.selectNodeContents(el);
                re = rangeObj;
                rc = re.cloneRange;
                // re.moveToBookmark(range.getBookmark());
                // rc.setEndPoint('EndToStart', re);
                iCaretPos = re.START_TO_END; //rc.text.length;
                return [iCaretPos, re.toString]; */
				
				el.focus();
                
                return [el.textLength,""];
            }

		}

		var SelectHighLightCharacter = function(startPos, endPos) {
			// selected charcter start and end position character would be
			// higlighted here
			var intRegex = /^\d+$/; // check numeric

			if (intRegex.test(startPos) && intRegex.test(endPos)) {
				// FormulaTextArea.focus();
				// FormulaTextArea.select();
				selectedTextBox.focus();
				selectedTextBox.select();
				var range = document.selection.createRange();
				range.collapse(true);
				range.moveEnd("character", endPos);
				range.moveStart("character", startPos);
				range.select();
			}
		}

		FormulaTextArea.keydown(function(event) {
			selectedTextBox = $(this);
			// To prevent the basic function of putting a line break when enter
			// is pressed
			switch (event.keyCode) {
			case 27: // esc
				return (event.keyCode != 27);
				break;

			case 13: // ENTER
				return (event.keyCode != 13);
				break;

			}

		});

		FormulaTextArea
				.keyup(function(event) {
					// EVENT TRIGGERED WHEN THE
					// FORMULA IS TYPED IN TEXT
					// AREA.
					// retain only single space character in the string
					selectedTextBox = $(this);
					DisplayInteliDivArea(true);
					// GETTING THE LAST STRING TO COMPARE IT WITH THE DIV
					var stringPositionSubstring = $(this).val();
					var charcterPos = GetCharet()[0];
					var tempStr = stringPositionSubstring;
					fTxtPrt1_gbl = tempStr.substring(0, charcterPos);
					fTxtPrt2_gbl = tempStr.substring(charcterPos,
							tempStr.length);
					// Get the substring from the charecter position
					var txtAreaStringArray = fTxtPrt1_gbl.split(' ');
					var last_Character = txtAreaStringArray[txtAreaStringArray.length - 1]
					SetlastTypedValue((last_Character ? last_Character : ''));

					// FILTERING THE LIST BOX ACCORDING TO THE CHARCTER TYPED
					var inteliMenuList = SelectionMenuListBox;
					var keys = [];
					var values = [];
					var itemCount = 0;
					var filter = (last_Character ? last_Character : '');
					// Filtercharacter is started with operators trim is eg: matching
					// str would be Test but user entry would be (T so this will skip.to
					// avoid this condition
					var opsArray = [ '(', ')', '+', '*', '/', '-', ',' ];
					$
							.each(
									opsArray,
									function(index, option) {
										if (filter.split(option).length > 1) {
											var filtArray = filter
													.split(option);
											var aLgth = filtArray.length - 1;
											fTxtPrt1_gbl = fTxtPrt1_gbl
													.substring(
															0,
															(fTxtPrt1_gbl.length - filtArray[aLgth].length))
													+ ' ' + filtArray[aLgth];
											filter = filtArray[aLgth];
											SetlastTypedValue((filter ? filter
													: ''));
										}
									});

					// STORING THE KEYS AND VALUES IN AN ARRAY TO COMPARE THE LAST TYPED
					// STRING AND GET A MATCH
					$.each(inteliMenuListClone, function(index, option) {
						// var ItemString = option.innerHTML;
						// keys.push(option.outerHTML);
						// values.push(ItemString);

						keys.push(index);
						values.push(option);
					});

					switch (event.keyCode) {// CHECKING FOR THE KEY CODE AND TAKE ACTION
					// ACCORDINGLY

					// case 32: // SPACE
					case 13: // ENTER
						DoListBoxFilter(inteliMenuList, filter, keys, values,
								true);
						DisplayInteliDivArea(false);
						break;
					case 8: // BACKSPACE
						if ($(this).val().length <= 0) // IF TEXT AREA IS EMPTY
							// HIDE THE POP UP DIV
							DisplayInteliDivArea(false);
						else
							DoListBoxFilter(inteliMenuList, filter, keys,
									values, false);
						break;
					case 40: // DOWN
						DoListBoxFilter(inteliMenuList, filter, keys, values,
								true);
						inteliMenuList.focus();
						break;
					case 27: // Escape

						DisplayInteliDivArea(false); // Hide the inteli div
						// FormulaTextArea
						selectedTextBox.focus();
						break;
					case 46: // DELETE

						break;
					case 37: // Left arrow
					case 39: // Right arrow

						/*
						 * FormulaTextArea.val(FormulaTextArea.val().replace(/\s{2,}/g, '
						 * '));
						 */// retain
						// only
						// single
						// space
						// character
						// in
						// the
						// string
						// var s = setTimeout(callHighlightCounterBracket,
						// intBrHighlightTmOt);// Uncomment if the highlighiting of
						// the braces are needed
						DoListBoxFilter(inteliMenuList, filter, keys, values,
								true);
						DisplayInteliDivArea(false);
						break;
					default:

						DoListBoxFilter(inteliMenuList, filter, keys, values,
								false);
						// DisplayInteliDivArea(true);
					}

				});

		FormulaTextArea.click(function(event) {// SELECTING A PART OF STRING
			// AND GIVE ITS POSITION
			selectedTextBox = $(this);
			// var s = setTimeout(callHighlightCounterBracket,
			// intBrHighlightTmOt);// Uncomment if the highlighiting of the
			// braces are needed
		});

		// WRAPPER FUNCTION FOR GIVING THE SELECTION AN ANIMATION/TIME OUT
		var callHighlightCounterBracket = function() {
			HighlightCounterBracket()
		};

		var HighlightCounterBracket = function() {// Function to highlight the
			// counter bracket
			var charPost = GetCharet();
			var startPosition = charPost[0];
			var selectedBracket = charPost[1];
			var counterPartPosition = CounterBracketIndex(
					selectedTextBox.val(), startPosition, selectedBracket);
			SelectHighLightCharacter(counterPartPosition,
					counterPartPosition + 1);

		}

		var DoListBoxFilter = function(listBox, filter, keys, values, resetFlag) {
			// / <summary>
			// / Filtering of the List Box with the last typed in string in the
			// formula text area.
			// / </summary>
			// / <param name="listBox" type="control">
			// / listBox controls which will be filtered for the passed filter
			// criteria
			// / </param>
			// / <param name="filter" type="string">
			// / filter criteria
			// / </param>
			// / <param name="keys" type="Array">
			// / Array of string which are option tags
			// / </param>
			// / <param name="values" type="Array">
			// / the Options in the list box
			// / </param>
			// / <param name="resetFlag" type="boolean">
			// / When enter/tab/escape keys are keyed in depending on this flag
			// the list box will be reset and ready for next filtration
			// / </param>
			// / <returns />
			var list = listBox;
			var i = values.length;
			var tempGeneralOptions = [];
			var tempFunctionOptions = [];
			var tempAttributeOptions = [];
			var tempBuiltInFunctionOptions = [];
			var tempMathOperatorsOptions = [];
			var tempInsuredDetailsFunctionOptions = [];
			var tempSingleInputOptions = [];
			var tempStandardInputOptions = [];
			var tempSingleOutputOptions = [];
			var tempMultiInputOptions = [];
			var tempMultiOutputOptions = [];
			var tempMultiValuedOutputVariablesFAS = [];
			var getOptGroupString = function(typeName) {
				return "<optgroup class='intelisenseShade'  label='" + typeName
						+ "'>"
			}

			var optGroupFunction = getOptGroupString('Function');
			var optGroupGeneral = getOptGroupString('Options');
			var optGroupAttribute = getOptGroupString('Attribute');
			var optGroupBuiltInFunction = getOptGroupString('BuiltInFunction');
			var optInsuredDetailsFunction = getOptGroupString('InsuredDetails');
			var optGroupMathOperators = getOptGroupString('Operators');
			var optGroupSingleOutput = getOptGroupString('SingleValuedOutputVariables');
			var optGroupSingleInput = getOptGroupString('SingleValuedInputVariables');
			var optGroupStandardInput = getOptGroupString('StandardValuedInputVariables');
			var optGroupMultiInput = getOptGroupString('MultiValuedInputVariables');
			var optGroupMultiOutput = getOptGroupString('MultiValuedOutputVariables');

			var optGroupMultiValuedOutputVariables = getOptGroupString('FASOutputVariables');

			var optGroupClose = "</optgroup>";
			var option, value;

			list.empty(); // clearing the list box to filter.

			while (i--) {

				value = values[i];
				$
						.each(
								value,
								function(index, option) {
									if (resetFlag != 'true'
											&& value
											&& strStartsWith(option
													.toString().toLowerCase(), filter
													.toString().toLowerCase())) {
										var attributeType = keys[i].substring(
												keys[i].indexOf('value='),
												keys[i].indexOf('>')).replace(
												'value=', '');

										switch (keys[i]) {
										case 'MultiValuedOutputVariablesFAS':
											tempMultiValuedOutputVariablesFAS
													.push('<option value="MultiValuedOutputVariablesFAS">'
															+ index
															+ '</option>');
											break;
										case 'Function':
											tempFunctionOptions
													.push('<option value="Function">'
															+ index
															+ '</option>');
											break;
										case 'Attribute':
											tempAttributeOptions
													.push('<option value="Attribute">'
															+ index
															+ '</option>');
											break;
										case 'BuiltInFunction':
											tempBuiltInFunctionOptions
													.push('<option value="BuiltInFunction">'
															+ index
															+ '</option>');
											break;
										case 'InsuredDetails':
											tempInsuredDetailsFunctionOptions
													.push('<option value="InsuredDetails">'
															+ index
															+ '</option>');
											break;
										case 'Operators':
											tempMathOperatorsOptions
													.push('<option value="Operators">'
															+ index
															+ '</option>');
											break;
										case 'SingleInput':
											tempSingleInputOptions
													.push('<option value="SingleInput">'
															+ index
															+ '</option>');
											break;
										case 'SingleOutput':
											tempSingleOutputOptions
													.push('<option value="SingleOutput">'
															+ index
															+ '</option>');
											break;
										case 'StandardInput':
											tempStandardInputOptions
													.push('<option value="StandardInput">'
															+ index
															+ '</option>');
											break;
										case 'MultiInput':
											tempMultiInputOptions
													.push('<option value="MultiInput">'
															+ index
															+ '</option>');
											break;
										case 'MultiOutput':
											tempMultiOutputOptions
													.push('<option value="MultiOutput">'
															+ index
															+ '</option>');
											break;
										default:
											tempGeneralOptions
													.push('<option value="General">'
															+ index
															+ '</option>');
											break;
										}
									}
								});
			}
			// Got all the options, now append to DOM according to the OptGrp
			// label
			// //////////////
			var testArray = [
					[ tempFunctionOptions, optGroupFunction ],
					[ tempAttributeOptions, optGroupAttribute ],
					[ tempBuiltInFunctionOptions, optGroupBuiltInFunction ],
					[ tempInsuredDetailsFunctionOptions,optInsuredDetailsFunction ],
					[ tempMathOperatorsOptions, optGroupMathOperators ],
					[ tempSingleInputOptions, optGroupSingleInput ],
					[ tempStandardInputOptions, optGroupStandardInput ],
					[ tempSingleOutputOptions, optGroupSingleOutput ],
					[ tempMultiInputOptions, optGroupMultiInput ],
					[ tempMultiOutputOptions, optGroupMultiOutput ],
					[ tempGeneralOptions, optGroupGeneral ],
					[ tempMultiValuedOutputVariablesFAS,
							optGroupMultiValuedOutputVariables ] ];

			var showSelectionMenuDiv = false;

			$.each(testArray, function(index, optionList) {
				if (!$.isEmptyObject(optionList)) {
					if (optionList[0].length > 0) {

						list.append(optionList[1]);
						list.append(optionList[0].join(''));
						list.append(optGroupClose);
						showSelectionMenuDiv = true;
					}
				}

			});

			// no match is found, hence no need to show the div
			if (!showSelectionMenuDiv)
				SelectionMenuDiv.hide();
		}

		SelectionMenuListBox.dblclick(function(event) {// Mouse click on the
			// Div

			SelctItemAndSetToFormulaText();

		});

		SelectionMenuListBox.keydown(function(event) {
			// To prevent the basic function of putting a line break when
			// backspace is pressed
			switch (event.keyCode) {
			case 8: // bckspace
				return (event.keyCode != 8);
				break;

			}

		});

		SelectionMenuListBox.keyup(function(event) {// When the focus is in
			// the selection
			// menu.select the item
			// selected and pass the
			// value to the text
			// area
			switch (event.keyCode) {// CHECKING FOR THE KEY CODE AND TAKE ACTION
			// ACCORDINGLY
			case 8: // BACKSPACE
			case 17: // Control
			case 16: // SHIFT
				// FormulaTextArea
				selectedTextBox.focus();
				DisplayInteliDivArea(false); // Hide the inteli div
				break;
			case 27: // Escape
				DisplayInteliDivArea(false); // Hide the inteli div
				// FormulaTextArea
				selectedTextBox.focus();
				break;
			case 13: // ENTER
			case 32: // SPACE
			case 9: // TAB
				// when enter is pressed pass the value to the text area.
				SelctItemAndSetToFormulaText();
				break;
			}
		});

		var SelctItemAndSetToFormulaText = function() {// Select the item from
			// Selection menu and
			// set to formula string

			// SelectionMenuListBox
			// var functionlist = Get_LIB_Functions();
			// alert(functionlist);
			// var jdata = $.toJSON(functionlist);
			// alert('to json:' + jdata);
			// var data = $.parseJSON(jdata);
			// alert('parsed json:' + data);
			// alert('keys:' + Object.keys(functionlist[0]));
			// $.each(functionlist, function (i, item) {
			// alert(item.add);
			// });

			var selectedValue = SelectionMenuListBox.val();
			var selectedString;
			if (selectedValue.toString() == "SingleInput"
					|| selectedValue.toString() == "MultiInput"|| selectedValue.toString() == "StandardInput") {
				selectedString = "inoutMap."
						+ SelectionMenuListBox.find("option:selected").text(); // $('#selectionList
				// :selected').text();
			} else {
				selectedString = SelectionMenuListBox.find("option:selected")
						.text(); // $('#selectionList
				// :selected').text();
			}

			// Replace the IF condition with the full text which displays the
			// syntax of the IF condition in our system
			if (functionlist[selectedValue].hasOwnProperty(selectedString)) {
				selectedString = functionlist[selectedValue][selectedString];
			}

			// Setting the Id of the subGroupID into Hiden text box
			// if (SelectionMenuListBox.find("option:selected")[0].value)
			// hiddenTextBox[0].value =
			// SelectionMenuListBox.find("option:selected")[0].value;
			var tArea = selectedTextBox;
			var splitChar = $.trim(LastCharacterDiv.innerText).toString().toLowerCase();
			// Spliting the selected string to append with the textarea string
			// which will have the rest of the selected string part.
			// strStartsWith($.trim(fTxtPrt2_gbl) , ')')
			var insertSpace = ($.trim(fTxtPrt2_gbl) == ''
					|| strStartsWith($.trim(fTxtPrt2_gbl), ')') ? ' ' : '');
			var replaceString = fTxtPrt1_gbl.substring(0, fTxtPrt1_gbl.length
					- splitChar.length)
					+ selectedString + insertSpace;
			// to set the control at the specified pos
			var setCursorPositionat = 0;
			setCursorPositionat = replaceString.length - 1;
			//replaceString = replaceString + $.trim(fTxtPrt2_gbl);
			tArea.val(replaceString);
			myvar=tArea;
			test=angular.element(tArea[0]).scope();
			var scope=angular.element(tArea[0]).scope();
			//var setModel=function(){}
			eval("scope."+tArea.attr("ng-model")+"="+"'"+replaceString+"'")
			//eval("scope."+tArea.attr("ng-model")+"="+replaceString)
			//eval(test="asdasdadadadsa");

			DisplayInteliDivArea(false); // Hide the inteli div
			tArea.focus();
			SelectHighLightCharacter(setCursorPositionat, setCursorPositionat);

		}

		var strStartsWith = function(str, prefix) {// RETURNS BOOL WETHER THE
			// STRING IS STARTED WITH
			// THE PREFIX VALUE
			return str.indexOf(prefix) === 0;
		}

		var DisplayInteliDivArea = function(showDivFlag) {// CHANGING THE
			// POSITION OF THE
			// DIVAREA WHICH
			// DISPLAYS THE
			// INTELLI ACCORDING
			// TO THE CURSOR
			// POSITION IN TEXT
			// AREA
			
			var range;
            if (document.selection) {// Broswers below IE 9
                range = document.selection.createRange();
            }
            else {// Chrome,IE8+
                var sel = document.getSelection();
                //var range = document.selection.createRange();
                range = sel;
            }
			
			var offset = selectedTextBox.offset();
			SelectionMenuDiv.css({
				"position" : 'absolute',
				"left" : offset.left + 5,
				"top" : offset.top + 20,
				"border-style" : 'solid'
			});
			if (showDivFlag)
				SelectionMenuDiv.show();
			else
				SelectionMenuDiv.hide();
		}

		var SetlastTypedValue = function(stringVal) {// Storing the last type
			// string in the
			// Formulatextarea
			LastCharacterDiv.innerHTML = stringVal;
		}

	};

})(jQuery);
function Get_LIB_Functions() {
	
	var obj = {};
	obj = {
		"Function" : {
			"add" : "sum(arg1,arg2...argn)",
			"sub" : "sub(arg1,arg2...argn)",
			"mul" : "mul(arg1,arg2...argn)",
			"div" : "div(arg1,arg2...argn)",
			"pow" : "pow(a,n)",
			"getAge" : "getAge(childDOB)",
			"getDuringYear" : "getDuringYear(yearsFromNowFundRequired)",
			"split" : "split(value)",
			"format" : "format(value)",
			"getMax" : "getMax(initialCapitalArray)",
			"min" : "min(value1,value2)",
			"max" : "max(value1,value2)",
			"sum" : "sum(arg1,arg2...argn)",
			"Number" : "Number(value)",
			"typeof" : "typeof(value)",
			"lookup" : "lookup()",
			"lookupWhere" : "lookupWhere(tableName,condition)",
			"lookupAsJson" : "lookupAsJson(columname,tablename,condtions)",
			"iff" : "if( < CONDITION > ) then( < ACTION >) else( < ACTION >) ",
			"abs" : "abs(arg)",
			"ceil" : "ceil(arg1)",
			"floor": "floor(arg1)" ,
			"sqrt": "sqrt(arg1)" ,
			"cbrt": "cbrt(arg1)" ,
			"exp": "exp(arg1)" ,
			"formatDecimal" : "formatDecimal(number,index)" ,
			"maxOfNValues" : "maxOfNValues(number1,number2,...numbern)" ,
			"minOfNValues" : "minOfNValues(number1,number2,...numbern)" , 
			"splitByRegex" : "splitByRegex(string,seperator)",
			"splitByRegexByLimit" : "splitByRegex(string,seperator,limit)",
			"substring" : "substring(string,start)" ,
			"substringWithEnd" : "substring(string,start,end)" ,
			"toUpperCase" : "toUpperCase(string)" ,
			"toLowerCase" : "toLowerCase(string)" ,
			"trim" : "trim(string)" ,
			"ltrim" : "ltrim(string)" ,
			"rtrim" : "rtrim(string)" ,
			"indexOf" : "indexOf(string,searchvalue)" ,
			"indexOfWithStart" : "indexOf(string,searchvalue,start)" ,
			"lastIndexOf" : "lastIndexOf(string,searchvalue)" ,
			"lastIndexOfWithStart" : "lastIndexOf(string,searchvalue,start)" ,
			"equlasIgnoreCase" : "equlasIgnoreCase(arg1,arg2)" ,
			"isEmpty" : "isEmpty(string)" ,
			"isUndefined" : "isUndefined(string)" ,
			"slice" : "slice(array,start)" ,
			"sliceWithEnd" : "slice(array,start,end)" ,
			"pushArray" : "pushArray(arg1,arg2)" ,
			"createTable" : "createTable(inoutMap,arrayName,index,key,value)",
			"createObject" : "createObject(inoutMap,objectName,key,value)",
			"getBandId" : "getBandId(sumAssured,productCode)",
			"getPremiumRate" : "getPremiumRate(age,PPT,bandID,productCode,policyTerm_optional)"
		},

		"BuiltInFunction" : {
			"substring" : "substring(a,b)"
		}


	}

	return obj;

};
