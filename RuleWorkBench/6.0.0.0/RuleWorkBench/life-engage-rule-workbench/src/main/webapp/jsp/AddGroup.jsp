<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="true"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% 
	if(session.getAttribute("username")== null){
	    response.sendRedirect("../login");
	}
%>
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="myapp">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8;IE=9;IE=10" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META Http-Equiv="Cache-Control" Content="no-cache" />
<META Http-Equiv="Pragma" Content="no-cache" />
<META Http-Equiv="Expires" Content="0" />
<title>Groups</title>
<script type="text/javascript" src="lib/vendor/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery-ui-1.8.24.custom.min.js"></script>
<script src="lib/vendor/angular.min.js"></script>
<script src="lib/vendor/ui-bootstrap-tpls-0.10.0.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link type="text/css" href="css/styles.css" rel="stylesheet" />
<link href="css/jquery-ui-modelpopup.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<script src="lib/vendor/le/BuildRules.js" type="text/javascript"></script>
<script type="text/javascript" src="lib/vendor/json.js"></script>
<script src="directive/lePopup.js"></script>
<link type="text/css" href="css/stylesPopup.css" rel="stylesheet" />
<script src="controller/groupRuleController.js"></script>
<style>
.nav {
	padding-left: 0;
	margin-bottom: 0;
	list-style: none
}
</style>
<script type="text/javascript">
function isEven(value){
    if (value%2 == 0)
        return true;
    else
        return false;
}

function validate() {

	document.forms["logout"].submit();

}
$(function() {
	grid = $('#ruleSetTable');

	// handle search fields key up event
	$('#search-term').keyup(function(e) { 
		text = $(this).val(); // grab search term

		if(text.length > 1) {
			grid.find('tr:has(td)').hide(); // hide data rows, leave header row showing

			// iterate through all grid rows
			grid.find('tr').each(function(i) {
				// check to see if search term matches Name column
				if($(this).find('td:first-child').text().toUpperCase().match(text.toUpperCase()))
					{
					$(this).show();
				 
					}
					 // show matching row
			});
		}
		else 
			grid.find('tr').show(); // if no matching name is found, show all rows
	});
});
	
</script>



</head>

<body>
<div class="blue_bg_header">
<div class="top_header_components clearfix">
	<div class="">
		<div class="icons" style="width:260px;">
		<img src="images/icon1.png" style="vertical-align:bottom;float:left"></img>
		<span class="greeting_header" style = "color: white;float:left">Welcome</span> <span class="login_name_txt"> <%=session.getAttribute("username")%>
		</span>
		</div>
		<div class="icons1" >
			<img src="images/help_icon.png" class="vt_align"></img>
			<a href="javascript:void(0)" title="Help" class="helpLink greeting_header" style= "color : white;">Help</a>
		</div>
		
		<form:form theme="simple" action="../logout" name="logout" method="post"
			id="logout" style="float:left;">
			
			<div class="icons2" ><button class="action_btn"><a class="greeting_header" title="Logout" style= "color : white;" onclick="validate();">Logout</a></button></div>
		</form:form>
	</div>
</div>
</div>
	<center>
		<div class="container" id="ng-app" ng-app ng-controller="groupController">


			<!-- Tree Template Starts->
			<!-- Template should be in single line (IE8 issue fix)-->
			<script type="text/ng-template" id="tree_item_renderer">
				<span class="plus_minus_sign_box "  ng-class="data.duplicateClass" >
					<input type="checkbox" ng-show="data.showCheckBox" id="{{data.type}}" value="{{data.type}}" class="treeview_check">
					<i  onclick="hideshowTree(this)" ng-show="data.nodes.length >0" class="icon-minus-sign inline"></i>
					<label class ="inline">{{showNode(data)}}</label>
				</span>
				<ul class="some"  >
					<li ng-repeat="data in data.nodes" class="parent_li" ng-class="{'last_li':$last}" ng-include="'tree_item_renderer'" ng-init="parent=$parent.data">
					</li>
				</ul>
			</script>
			<!-- Tree Template Ends-->

			<div class="header">
				<div class="headerLeftRule"></div>
				
				<div class="workbench_img_bg">	
					<img src="images/rule_work-bench.png" width="175px" height="35px"/>				
				</div>
			</div>
			<div class="clear" style="height: 5px;"></div>

			<div class="centertop">
				<div class="menucontainer">
					<ul class="nav">
						<li class="menu_left"></li>
						<li class="menu_center"><a
							href="<%=request.getContextPath()%>/jsp/listBusinessRule.jsp?ruleTypeId=1&ruleSetTypeId=1">Rules
						</a></li>
						<li class="menu_right"></li>
						<li class="menu_leftSel"></li>
						<li class="menu_centerSel"><a href="<%=request.getContextPath()%>/jsp/AddGroup.jsp?ruleTypeId=1&ruleSetTypeId=1">Groups</a></li>
						<li class="menu_rightSel"></li>
					</ul>
				</div>
				<div style="border:1px solid #a8d7ea;">
				<div class = "rule_action_btns">
					<table width="400px" border="0" align="right" cellpadding="0"
						cellspacing="0">
						<tr>
							<td width="144"><input type="text" class="searh_box"
								id="search-term" /></td>
							<td width="36" align="center"><img
								src="images/searh_icon.png" width="32" height="30"></td>
									<td><button class="action_btn" width="92" height="30" ng-click="addGroup(0,'','')" id="add_more_btn">Add More</td>
									<td><button class="action_btn" width="92" height="30" ng-click="importGroupPopUp()" title="Import" id="import_Group_btn">Import Group</td>
						</tr>
					</table>
				</div>
				<div class="clear"></div>
				
				<!--<div class="table_wrapper"> -->
				

				<div class="fixed_header_tbl_wrapper">
					<div class="fix_head_wrapper">
						<table class="fix_head">
							<tbody>
								<tr class="tbl_head_bg">
									<th width="301px" min-width="301px" height="30" align="center"
										class="">Group Name</th>
										<th width="100px" min-width="100px" height="30" align="center"
										class="">Comments</th>
									<th width="301px" min-width="301px" height="30" align="center"
										class="">Group Type</th>
									<th width="305px"  height="30" align="center"
										class="last-column">&nbsp;</th>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="main_table_wrapper">
						<table id="ruleSetTable" class="cust_tbl" cellspacing="1" cellpadding="4" border="0" width="100%">
							<tbody id="tblRulesBody">
								<tr ng-repeat = "data in listGroups">
									<td ng-bind="data.groupName"></td>
									<td width="100px" min-width="100px"><div class="data" ng-bind="data.comments"></div></td>
									<td ng-bind="data.groupType"></td>
									<td>
										<div style="width:auto;float:left;text-align:center;margin:5px;"><img src="images/edit_bton.png" width="25" height="25" class="edit_btn" id="edit" ng-click="addGroup(data.groupID,data.groupName,data.typeID,data.comments)"  title="Edit"/></div>
										<div style="width:auto;float:left;text-align:center;margin:5px;"><a ng-href="<%=request.getContextPath()%>/jsp/DownloadGroup.jsp?groupID={{data.groupID}}&groupName={{data.groupName}}"><img src="images/download_bton.png" width="25" height="25" title="Download" /></a></div>
										<div style="width:auto;float:left;text-align:center;margin:5px;"><a ng-click ="deleteGroup(data.groupID)"><img src="images/delete_bton.png" width="25" height="25" title="Delete"/></a></div>
										<div style="width:auto;float:left;text-align:center;margin:5px 5px;"><a ng-click="cloneGroup(data.groupID,data.groupName,data.typeID)" class="copy" ><img src="images/copy_icon.png" width="25" height="25" class="copy_btn" title="Copy"/></a></div>
										<div style="width:auto;float:left;text-align:center;margin:5px 5px;"><a ng-href="<%=request.getContextPath()%>/jsp/ExportGroup.jsp?groupID={{data.groupID}}&groupName={{data.groupName}}&type={{data.typeID}}"><img src="images/export_bton.png" width="25" height="25" class="copy_btn" title="Export"/></a></div>
									</td>
								</tr>
							</tbody>
						</table>	
					</div>	
	
				</div>
				<lepopup control="lePopupCtrl"/>
			</div>
			<div style="height: 122px; width: 100%" class="cognizant_logo">
			</div>
			
			<div id="page_loader"></div>

			<!-- THE MODAL ADDMORE/EDIT POPUP -->
			<div class="modal_pop_up add_more_pop_up">
				<p class="modal_pop_up_header ">
					{{popupHeaderName}} <span class="close_pop_up"></span>
				</p>
					<div class="pop_up_row_left clearfix">
						<label class="pop_up_label_left pdg_top">Group Name <span class="mandatory_red">*</span></label>
						<input type="text" id="txtGroupName" ng-model="groupName" class="txt_box_pop" style="float:left;" size="20" maxlength="20" ng-keypress="alpha($event)"/>
						<label class="pop_up_label min_pdg_left pdg_top" style="padding-left:1px">Type</label>
						<select class="pop_up_select min_mrg_top" ng-options="groupBO.groupTypeID as groupBO.groupType for groupBO in groupBOTypeList"  ng-model="groupType" id="groupTypeId" required></select>
					</div>	
					
					<div class="pop_up_row_left clearfix">
						<label class="pop_up_label_left pdg_top">Comments<span class="mandatory_red">*</span></label>
						<textarea  style="width:400px;resize:none;"  ng-model="commentTextArea" id="commentTextArea" maxlength="250"></textarea>
					</div>
					<div class="pop_up_btn"> <button id="Submit2" style="margin-top:5px;" class="SaveGroupName action_btn_popup">Save</button></div>
					<div id="dvMessages" style="width: 518px; padding: 5px 0px;text-align: left;"></div>
			</div>
			<!-- THE MODAL ADDMORE/EDIT POPUP -->
			
			<!-- IMPORT  POPUP -->
			<div class="modal_pop_up import_more_pop_up">
				<p class="modal_pop_up_header ">
					Import Group <span class="close_pop_up"></span>
				</p>
				<form id="form" method="post" enctype="multipart/form-data" >
					<div class="pop_up_row_left clearfix">
						<label class="pop_up_label_left pdg_top">Upload File:</label>
						<input type="file" name="file" id="file">
					</div>
					<div class="pop_up_row_left clearfix">
						<label class="pop_up_label_left pdg_top">Password :</label>
						<input type="password" id="password" name="password" class="txt_box_pop" size="20" maxlength="20"/>
					</div>
					<div class="pop_up_btn"> <button id="importBtn" value="Submit" style="margin-top:5px;" class="SaveGroupName action_btn_popup">Import</button></div>
					
					<div id="errorMessages" style="width: 518px; padding: 5px 0px;text-align: left;"></div>
				</form>
			</div>
			<!-- IMPORT POPUP -->
			
<!-- TREE VIEW POPUP -->
			
			<div id="groupTreeview" title="Tree View">
				<!-- button for validate-->
				<div class="tableRowDoublebold"
					style="width: 97%; text-align: right;"></div>
				<div class="preview_tabs_message">
					<p ng-class="messageImp">{{message}} </p>
				</div>
				<div class="preview_tab_container">
					<!-- preview_tab_container -->
					<div class="preview_tab" id="tree_view_tab">
						<div class="tree">
							<ul>
								<li ng-repeat="data in displayTree" ng-include="'tree_item_renderer'" class="ng-scope parent_li" ng-class="{'last_li':$last}"></li>
							</ul>
						</div> 
						<!--Tree View-->
					</div>
				</div>
				<!-- preview_tab_container -->
				<div class="preview_button_container">
					<button class="action_btn"   ng-click="cancelImport()">Cancel</button>
					<button class="action_btn"  ng-show="showProceed" ng-click="importGroup()">Proceed</button>
				</div>
				<div id="importErrorMsg" style="width: 100%;padding: 1px 0px 0px 5px;text-align: left;overflow: hidden;margin-top: 40px;"></div>
			</div>
<!-- TREE VIEW POPUP -->

		</div>
	</center>
	
	<script type="text/javascript" src="lib/vendor/le/scripts.js"></script>
	
</body>

</html>
