<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% 
	if(session.getAttribute("username")== null){
	    response.sendRedirect("../login");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="myapp">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META Http-Equiv="Cache-Control" Content="no-cache" />
<META Http-Equiv="Pragma" Content="no-cache" />
<META Http-Equiv="Expires" Content="0" />
<title>Export Group</title>
<link type="text/css" href="css/styles.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" href="css/stylesPopup.css" rel="stylesheet" />
<script type="text/javascript" src="lib/vendor/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery-ui-1.8.24.custom.min.js"></script>
<script type="text/javascript" src="lib/vendor/json.js"></script>
<script src="lib/vendor/angular.min.js"></script>
<script src="lib/vendor/ui-bootstrap-tpls-0.10.0.min.js"></script>
<script src="controller/exportGroupController.js"></script>
<script src="directive/lePopup.js"></script>
<style>
.nav {
	padding-left: 0;
	margin-bottom: 0;
	list-style: none
}
</style>
<script type="text/javascript">
function isEven(value){
    if (value%2 == 0)
        return true;
    else
        return false;
}

function validate() {

	document.forms["logout"].submit();

}

$(function() {
	grid = $('#tblRulesBody');

	// handle search fields key up event
	$('#search-term').keyup(function(e) { 
		text = $(this).val(); // grab search term

		if(text.length > 1) {
			grid.find('tr:has(td)').hide(); // hide data rows, leave header row showing

			// iterate through all grid rows
			grid.find('tr').each(function(i) {
				// check to see if search term matches Name column
				if($(this).find('td:nth-child(2)').text().toUpperCase().match(text.toUpperCase()))
					{
					$(this).show();
				 
					}
					 // show matching row
			});
		}
		else 
			grid.find('tr').show(); // if no matching name is found, show all rows
	});
});

</script>

</head>

<body>
<div class="blue_bg_header">
<div class="top_header_components clearfix">
	<div class="">
		<div class="icons" style="width:260px;">
		<img src="images/icon1.png" style="vertical-align:bottom;float:left"></img>
		<span class="greeting_header" style = "color: white;float:left">Welcome</span> <span class="login_name_txt"> <%=session.getAttribute("username")%>
		</span>
		</div>
		<div class="icons1" >
			<img src="images/help_icon.png" class="vt_align"></img>
			<a href="javascript:void(0)" title="Help" class="helpLink greeting_header" style= "color : white;">Help</a>
		</div>
		
		<form:form theme="simple" action="../logout" name="logout" method="post"
			id="logout" style="float:left;">
			
			<div class="icons2" ><button class="action_btn"><a class="greeting_header" title="Logout" style= "color : white;" onclick="validate();">Logout</a></button></div>
		</form:form>
	</div>
</div>
</div>
	<center>
		<div class="container" id="ng-app" ng-app ng-controller="ExportGroupController">
			
			<div class="header">
				<div class="headerLeftRule"></div>
				
				<div class="workbench_img_bg">	
					<img src="images/rule_work-bench.png" width="175px" height="35px"/>				
				</div>
			</div>
			<div class="clear" style="height: 5px;"></div>

			<div class="centertop">
				<div class="menucontainer">
					<ul class="nav">
						<li class="menu_left"></li>
						<li class="menu_center"><a
							href="<%=request.getContextPath()%>/jsp/listBusinessRule.jsp?ruleTypeId=1&ruleSetTypeId=1">Rules
						</a></li>
						<li class="menu_right"></li>
						<li class="menu_leftSel"></li>
						<li class="menu_centerSel"><a
							href="<%=request.getContextPath()%>/jsp/AddGroup.jsp?ruleTypeId=1&ruleSetTypeId=1">Groups</a></li>
						<li class="menu_rightSel"></li>
						<!--<li class="menu_right"></li>
						<li class="menu_left"></li>
						<li class="menu_center"><a href="/RuleEngineWorkBench/jsp/ListRuleSetName.action">Rule
								Set</a></li>
						<li class="menu_right"></li>-->
					</ul>
				</div>
			<div style="border: #a8d7ea 1px solid; width: 940px;overflow:hidden">
				<div class = "rule_action_btns">
					<span class="rule_set_title pdg_left_title">GROUP :</span>
										<span id="groupLabel" name="groupLabel" class="title_group_lbl" style="padding-top: 13px;width: 450px;text-align: left;line-height: 18px;">{{groupName}}</span>
					<table width="280" border="0" align="right" cellpadding="0"
						cellspacing="0">
						<tr>
							<td width="144"><input type="text" class="searh_box"
								id="search-term" /></td>
							<td width="36" align="center"><img
								src="images/searh_icon.png" width="32" height="30"></td>
							<td><button width="92"
								height="30" class = "dwnLoad_btn action_btn">Export</td>

						</tr>
					</table>
				</div>
				<div class="clear" style="height: 10px;"></div>
				<div id="listRuleTbl" class="fixed_header_tbl_wrapper">
			<div class="fix_head_wrapper">
				<table class="fix_head" id="ruleTable" width="100%" border="0" cellspacing="1"
					cellpadding="4">
					<thead>
						<tr class="tbl_head_bg">
							<th width="80px" height="30" align="center" ><input type="checkbox" id="selectall" align="left"/> Select
								</th>
							<th width="285px" height="30" align="center" >Rule
								Name</th>
							<th width="140px" height="30" align="center" >Type</th>
							<th width="200px" height="30" align="center" >Created
								Date</th>
							<th width="200px" height="30" align="center" >Updated
								Date</th>
							<!-- <th width="22%" height="30" align="center" class="tableRowbold_list">&nbsp;</th> -->
						</tr>
					</thead>
				</table>
			</div>
			<div class="main_table_wrapper">
				<table class="cust_tbl" cellpadding="5px" border="0" width="100%">
					<tbody id="tblRulesBody">
					<tr ng-repeat = "data in listRulesByGroup">
							<td height="30" width="80px" align="left" class="pdg_left"' + bgColor+'><input type="checkbox" class="rule_select_chkbox" id="{{data.ruleID}}" value="{{data.ruleName}}" /> </td>
							<td height="30" class="align_left" align="left" width="237px" ng-bind="data.ruleName"></td>
							<td height="30" width="96px" align="center" ng-show="data.ruleTypeID == 1" ng-bind="'Rule'"></td>
							<td height="30" width="96px" align="center" ng-show="data.ruleTypeID == 3" ng-bind="'RuleSet'"></td>
							<td height="30" width="145px"align="center" ng-bind-template="{{data.createdDate}} {{data.createdTime}}"></td>
							
							<td height="30" width="145px"align="center" ng-show="data.updatedDate == null" ng-bind-template="{{data.createdDate}} {{data.createdTime}}"></td>
							<td height="30" width="145px"align="center" ng-hide="data.updatedDate == null" ng-bind-template="{{data.updatedDate}} {{data.updatedTime}}"></td>
					</tr>
					</tbody>

				</table>
			</div>	
		</div>
			</div>
			<lepopup control="lePopupCtrl"/>
			</div>
			<div style="height: 75px; width: 100%" class="cognizant_logo">
			</div>

			<div id="page_loader"></div>

			<!-- THE FILE NAME POPUP -->
			<div class="modal_pop_up edit_more_pop_up">
				<p class="modal_pop_up_header ">
					EXPORT GROUP <span class="close_pop_up"></span>
				</p>
					<div class="pop_up_row_left clearfix">
						<label class="export_lbl">Select the export type :</label>
						<input type="radio"  ng-model="exportType" ng-value="true" ng-change='exportTypeChange(exportType)' style="padding-left:100px">Compressed
						<input type="radio"  ng-value="false" ng-model="exportType" ng-change='exportTypeChange(exportType)'> Un-Compressed
					</div>	
					<div class="pop_up_row_left clearfix">
						<label class="export_lbl">Password :</label>
						<input type="password" id="pwdText" ng-model="password" class="txt_box_pop" size="30" maxlength="20"/>	
					</div>	
					<div class="pop_up_row_left  ruleSel_div clearfix" style="display :none">
						<label class="export_lbl">Select the Rule name :</label> 
						<select  ng-init="ruleName=listRulesByGroup[0].ruleName" ng-model="ruleName" ng-options="option.ruleName as option.ruleName for option in listRulesByGroup">
						</select> 
						
					</div>
						<!-- <input type="text" id="txtFileName" ng-model="fileName" class="txt_box_pop" style="float:left;" size="36" maxlength="250" ng-keypress="alpha($event)"/>-->
						<button width="43" height="25" id = "exportButton" class="downloadButton action_btn_popup btn_mrg">Export</button>
						<div id="exportErrorMsg" style="width: 518px; padding: 5px 0px;text-align: left;"></div>
					</div>
			</div>
			<!-- THE FILE NAME POPUP  -->
			
		</div>
	</center>

	<script type="text/javascript" src="lib/vendor/le/scripts.js"></script>

</body>

</html>
