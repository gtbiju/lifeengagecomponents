<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="myapp">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8;IE=9;IE=10" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META Http-Equiv="Cache-Control" Content="no-cache" />
<META Http-Equiv="Pragma" Content="no-cache" />
<META Http-Equiv="Expires" Content="0" />
<title>Login Page</title>
<script type="text/javascript" src="lib/vendor/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery-ui-1.8.24.custom.min.js"></script>
<script src="lib/vendor/angular.min.js"></script>
<script src="lib/vendor/ui-bootstrap-tpls-0.10.0.min.js"></script>
<link type="text/css" rel="stylesheet" href="css/styles.css" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<script src="lib/vendor/le/validationScript.js"></script>
<script src="controller/loginController.js"></script>
<script type="text/javascript">

window.history.forward(1);
function noBack() 
{ 
window.history.forward(); 
}
</script>

</head>
<body>
<div class="blue_bg_header">
<div class="top_header_components clearfix">
	<div class="">
		<div class="icons1" >
			<img src="images/help_icon.png" class="vt_align"></img>
			<a href="javascript:void(0)" title="Help" class="helpLink greeting_header" style= "color : white;">Help</a>
		</div>
	</div>
</div>
</div>
<center>
<div  class="container">
	<div class="header">
		<div class="headerLeftRule"></div>
		<div class="workbench_img_bg">	
			<img src="images/rule_work-bench.png" width="175px" height="35px"/>				
		</div>
	</div>
    <div style="width: 442px; " class="logoWhite">
	</div>
    <div class="clear" style="height:5px;"></div>
    <div class="LoginContainer" id="ng-app" ng-app ng-controller="login">
    <form:form theme="simple" action="login" name="login" method="post" id="login" commandName="user">
      <table width="400" border="0" align="center" cellpadding="0" cellspacing="5">
        <tr>
          <td width="141">&nbsp;</td>
          <td width="244">&nbsp;</td>
        </tr>
        <tr>
          <td height="30" align="center" class="blue_heading"></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td height="30" align="right">Username&nbsp;</td>
          <td align="left">	
          <form:input path="userName" ng-model="userName" style="width:200px;" cssClass="searh_box" /></td>
        </tr>
        <tr class="pdg_top">
          <td height="30" align="right" class="pdg_top">Password&nbsp;</td>
          <td align="left" class="pdg_top"> 
          	<form:password path="password"	ng-model="password" style="width:200px;" cssClass="searh_box" />
          </td>
        </tr>
        <tr>
          <td height="30" align="right"></td>
          <td align="right"  class="pdg_top">
         <div style="float: left;vertical-align: bottom;padding-top:5px;"> <input type="checkbox" ng-model="remember" name="remember" value="remember">Remember me</div>
          <button class="action_btn"><a class="greeting_header" title="Logout" style= "color : white;" ng-click="validate()">Login</a></button>
         </td>
        </tr>
        <tr>
        <td colspan="2" style="color:red;"><form:errors /></td>
        </tr>
      </table>
      </form:form>
	</div>
    <div style="height:150px; width:100%" class="cognizant_logo"></div>
</div>
</center>
</body>

</html>
