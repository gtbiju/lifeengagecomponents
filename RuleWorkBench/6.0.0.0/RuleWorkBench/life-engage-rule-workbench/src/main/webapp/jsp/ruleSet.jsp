<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% 
	if(session.getAttribute("username")== null){
	    response.sendRedirect("../login");
	}
%>
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="myapp">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8;IE=9;IE=10" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META Http-Equiv="Cache-Control" Content="no-cache" />
<META Http-Equiv="Pragma" Content="no-cache" />
<META Http-Equiv="Expires" Content="0" />
<title>Rule Set</title>
<link type="text/css" rel="stylesheet" href="css/styles.css" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<script src="lib/vendor/jquery-1.7.1.min.js" type="text/javascript"></script>
<link href="css/jquery-ui-modelpopup.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript" src="lib/vendor/jquery.tmpl.min.js"></script>
<script src="lib/vendor/le/Intellisense.js" type="text/javascript"></script>
<script src="lib/vendor/json.js" type="text/javascript"></script>
<script src="lib/vendor/le/BuildRules.js" type="text/javascript"></script>
<script src="lib/vendor/Beautify.js" type="text/javascript"></script>
<link href="css/Intellisense.css" rel="stylesheet" type="text/css" />
<script src="lib/vendor/jquery-ui-modelpopup.min.js" type="text/javascript"></script>
<script src="lib/vendor/jshint-1.1.0.js" type="text/javascript"></script>
<script src="lib/vendor/jquery.json-2.2.min.js" type="text/javascript"></script>
<script src="lib/vendor/angular.min.js"></script>
<script src="lib/vendor/ui-bootstrap-tpls-0.10.0.min.js"></script>
<script src="directive/lePopup.js"></script>
<link type="text/css" href="css/stylesPopup.css" rel="stylesheet" />
<script src="controller/ruleSetRuleController.js"></script>
<style>
.nav {
	padding-left: 0;
	margin-bottom: 0;
	list-style: none
}
</style>
<script type="text/javascript">
$(document).ready(function() {
$(window).bind('beforeunload', function(){

	<%	
	session.putValue("saveFlag", false);
	%>

});
});
</script>

<script type="text/javascript">
flagUnload= false;
$(document).ready(function() {
$(window).bind('beforeunload', function(){
	if(flagUnload == false){
  		return '';
	}
});
});
</script>

<script type="text/javascript">


</script>

<script type="text/javascript">

function validate() {

	document.forms["logout"].submit();
}

$(document).ready(function() {
	
    $('#btnRight').click(function(e) {
        var selectedOpts = $('#lstBox1 option:selected');
        if (selectedOpts.length == 0) {
            alert("Nothing to move.");
            e.preventDefault();
        }

        $('#lstBox2').append($(selectedOpts).clone());
        
        e.preventDefault();
    });

    $('#btnLeft').click(function(e) {
        var selectedOpts = $('#lstBox2 option:selected');
        if (selectedOpts.length == 0) {
            alert("Nothing to move.");
            e.preventDefault();
        }

        
        $(selectedOpts).remove();
        e.preventDefault();
    });

    $('#btnRightAll').click(function(e) {

    	$('#lstBox1 option').clone().appendTo('#lstBox2');
    	e.preventDefault();
    });

    $('#btnLeftAll').click(function(e) {

    	$('#lstBox2 option').remove();
    	e.preventDefault();
    });
		
		$("#dlgRSPreview").dialog({
			resizable : false,
			autoOpen : false,
			height : 500,
			width : 730,
			modal : true
		});
		
		$('.rsPreview').live('click',function(e) {
			var ruleSetJavascript = BuildRuleSetJavascript();
			$("#dlgRSPreview").dialog('open');
			$("#txtRSPreview").html("<textarea id='taPreview'  cols='115' rows='25' readonly>" + ruleSetJavascript + "</textarea>");
		});
		
		//trim fix in IE8
		if(typeof String.prototype.trim !== 'function') {
			String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, ''); 
			  };
		}

		
		
					});
</script>
</head>
<body onunload = "edit();">
<div class="blue_bg_header">
<div class="top_header_components clearfix">
	<div class="">
		<div class="icons" style="width:260px;">
		<img src="images/icon1.png" style="vertical-align:bottom;float:left"></img>
		<span class="greeting_header" style = "color: white;float:left">Welcome</span> <span class="login_name_txt"> <%=session.getAttribute("username")%>
		</span>
		</div>
		<div class="icons1" >
			<img src="images/help_icon.png" class="vt_align"></img>
			<a href="javascript:void(0)" title="Help" class="helpLink greeting_header" style= "color : white;">Help</a>
		</div>
		
		<form:form theme="simple" action="../logout" name="logout" method="post"
			id="logout" style="float:left;">
			
			<div class="icons2" ><button class="action_btn"><a class="greeting_header" title="Logout" style= "color : white;" onclick="validate();">Logout</a></button></div>
		</form:form>
	</div>
</div>
</div>
	<center>
		<div class="container"  id="ng-app" ng-app ng-controller="RuleSetController">
			
			<div class="header">
				<div class="headerLeftRule"></div>
				
				<div class="workbench_img_bg">	
					<img src="images/rule_work-bench.png" width="175px" height="35px"/>				
				</div>
			</div>
			<div class="clear" style="height: 5px;"></div>

			<div class="centertop">
				<div class="wrapper1">
					<div class="menucontainer">
						<ul class="nav">
							<li class="menu_leftSel"></li>
							<li class="menu_centerSel"><a
								href="<%=request.getContextPath()%>/jsp/listBusinessRule.jsp?ruleTypeId=1&ruleSetTypeId=1">Rules
									</a>
							</li>
							<li class="menu_rightSel"></li>
							<li class="menu_left"></li>
							<li class="menu_center"><a
								href="<%=request.getContextPath()%>/jsp/AddGroup.jsp">Groups
									</a>
							</li>
							<li class="menu_right"></li>
						
						</ul>
					</div>
					<div style="border:1px solid #a8d7ea;">
					<div class = "rule_action_btns">
						<table width="235" border="0" align="right" cellpadding="0" cellspacing="0">
                        <tr>
						  <td width="86" align="center" title="Cancel"><button class="action_btn" width="76" height="30" id="cancel"/>Cancel</td>
                          <td width="96" align="center" title="Shows preview"  ng-show="debug"><button class="rsPreview action_btn  id="rsPreview" width="76" height="30"/>Preview</td>
                          <td width="73" align="center" title="Saves the new rule"><button id='btnSave' class="SaveRule action_btn" width="53" height="30"/>Save</td>
                        </tr>
						</table>
					</div>

					<div class="clear"></div>
			   <div class="clear" style="height:10px;" ></div> 
			   <center>
			   <input type="hidden" id="ruleSetId" name="ruleSetId" value="0" />
			   <div id="dvSaveMsg" style="width: 100%; display: none;"		class="pass"></div>
      <div class="add_new" style="height:50px;width:900px;font-size:14px">
      <span class="rule_set_title">Rule Set Name<span class="mandatory_red">*</span>:</span>
      <input type="text" id="ruleSetName" maxlength="250" class="txt_box"  style="float:left;"  size="45" ng-keypress="alpha($event)"/>
      <span class="rule_set_title"  style="margin-left:20px;">Group:</span>
      <!-- <input type="text" id="ruleSetGroupName" class="txt_box "  style="float:left;"  size="56" /> -->
      <select name="groupSel" id="groupSel" class="min_mrg_top"
			style="width: 150px; text-align: left; vertical-align: bottom; border: 1px solid #000;">
	  </select>
      </div>
      <div class="clear"></div>
		<div id="" class="flip tableRowbold clear"
		style="text-align: left;">
		<div class="pdg_left" style="padding-top: 5px; float: left;">
			Comments<span class="mandatory_red">*</span></div>
			<div class="plus_icon"></div>
		<div class="clear"></div>
	</div>
	<div class="panel"
		style="border: 1px solid #a8d7ea; padding: 15px 0px 15px 0px; margin-bottom: 20px;">
			<textarea cols="" rows="5" ng-model="commentTextArea" class="commments_textarea"></textarea>
		</div>
	<div style="clear: both; height: 10px"></div>
      <div  class="tableRowbold" style="text-align: left;"><div class="pdg_left" style="padding-top:5px;">Manage Rules</div></div>
      <div class="wrapper1" style=" width:900px; border:1px solid #a8d7ea; padding:20px 0 20px 0">
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
      	  <tr>
      	    <td width="45%" align="center"><table width="310" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #a8d7ea;">
      	      <tr>
      	        <td height="35" bgcolor="#E3F6FF" ><span class="blue_text boldText min_pdg_left">&nbsp;Available Rules</span></td>
    	        </tr>
      	      <tr>
      	        <td height="170" align="center" bgcolor="#E8F3F9" style="padding: 2px 7px 7px 7px;"><select
													multiple="multiple" id='lstBox1'
													style="width: 300px; height: 180px; border: 1px solid #000;"D1">
												</select>      	        </td>
    	        </tr>
    	      </table></td>
      	    <td width="10%" align="center" valign="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      	      <tr>
      	        <td height="40" align="center"><button class="action_button" width="76" height="30" id='btnRightAll'>&gt;&gt;</button></td>
    	        </tr>
      	      <tr>
      	        <td height="40" align="center"><button class="action_button" width="76" height="30" id='btnRight'>&gt;</button></td>
    	        </tr>
      	      <tr>
      	        <td height="40" align="center"><button class="action_button" width="76" height="30" id='btnLeft'>&lt;</button></td>
    	        </tr>
      	      <tr>
      	        <td height="40" align="center"><button class="action_button" width="76" height="30" id='btnLeftAll'>&lt;&lt;</button></td>
    	        </tr>
    	      </table></td>
      	    <td width="45%" align="center"><table width="310" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #a8d7ea;">
      	      <tr>
      	        <td height="35" valign="middle" bgcolor="#E3F6FF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      	          <tr>
      	            <td width="184" align="left"><span class="blue_text boldText min_pdg_left">&nbsp;New Rule Set</span></td>
      	            <td width="59" align="center"><button class="action_btn" width="76" height="30" id='btnUp'>Up</button></td>
      	            <td width="65" align="center"><button class="action_btn" width="76" height="30" id='btnDown'>Down</button></td>
    	            </tr>
    	          </table></td>
    	        </tr>
      	      <tr>
      	        <td height="170" align="center" bgcolor="#E8F3F9" style="padding: 2px 7px 7px 7px;"><select	multiple="multiple" id='lstBox2'
													style="width: 300px; height: 180px; border: 1px solid #000;">
												</select>    	          </td>
    	        </tr>
    	      </table></td>
    	    </tr>
    	  </table>
      </div>
      
      </center>
  <div class="clear" style="height:20px;"></div>

							<blockquote>&nbsp;</blockquote>

						</div>
						</div>


					</div>
						<lepopup control="lePopupCtrl"/>
					<!-- internalContainerBodyContent ends here -->
					<div style="height: 100px; width: 100%" class="cognizant_logo">
			</div>
				</div>
				<!-- internalContainerBody ends here -->
				

				<div class="clear"></div>
 
	<div id="dlgRSPreview" title="Preview" style="display: none;">
		<div class="tableRowDoublebold" style="width: 97%; text-align: right;">
		</div>
		<div id="txtRSPreview"></div>

	</div>
	<!-- externalWrapper ends here -->
</center>
</body>
</html>

