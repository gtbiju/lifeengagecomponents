<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="true"%>
	<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% 
	if(session.getAttribute("username")== null){
	    response.sendRedirect("../login");
	}
%>
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="myapp">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8;IE=9;IE=10" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META Http-Equiv="Cache-Control" Content="no-cache" />
<META Http-Equiv="Pragma" Content="no-cache" />
<META Http-Equiv="Expires" Content="0" />
<title>Business Rule</title>
<script type="text/javascript" src="lib/vendor/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery-ui-1.8.24.custom.min.js"></script>
<script src="lib/vendor/angular.min.js"></script>
<script src="lib/vendor/ui-bootstrap-tpls-0.10.0.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link type="text/css" href="css/styles.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<script type="text/javascript" src="lib/vendor/json.js"></script>
<script src="controller/listRuleController.js"></script>
<script src="directive/lePopup.js"></script>
<link type="text/css" href="css/stylesPopup.css" rel="stylesheet" />


<script type="text/javascript">
function isEven(value){
    if (value%2 == 0)
        return true;
    else
        return false;
}

function validate() {

	document.forms["logout"].submit();

}
$(function() {

	grid = $('#tblRulesBody');

	// handle search fields key up event
	$('#search-term').keyup(function(e) { 
		text = $(this).val(); // grab search term

		if(text.length > 1) {
			grid.find('tr:has(td)').hide(); // hide data rows, leave header row showing

			// iterate through all grid rows
			grid.find('tr').each(function(i) {
				// check to see if search term matches Name column
				if($(this).find('td:first-child').text().toUpperCase().match(text.toUpperCase()))
					{
					$(this).show();
				 
					}
					 // show matching row
			});
		}
		else 
			grid.find('tr').show(); // if no matching name is found, show all rows
	});
});


</script>



</head>

<body>
<div class="blue_bg_header">
<div class="top_header_components clearfix">
	<div class="">
		<div class="icons" style="width:260px;">
		<img src="images/icon1.png" style="vertical-align:bottom;float:left"></img>
		<span class="greeting_header" style = "color: white;float:left">Welcome</span> <span class="login_name_txt"> <%=session.getAttribute("username")%> 
		</span>
		</div>
		<div class="icons1" >
			<img src="images/help_icon.png" class="vt_align"></img>
			<a href="javascript:void(0)" title="Help" class="helpLink greeting_header" style= "color : white;">Help</a>
		</div>
		
		<form:form theme="simple" action="../logout" name="logout" method="post"
			id="logout" style="float:left;">
			
			<div class="icons2" ><button class="action_btn"><a class="greeting_header" title="Logout" style= "color : white;" onclick="validate();">Logout</a></button></div>
		</form:form>
	</div>
</div>
</div>
	<center>
	
		<div class="container" id="ng-app" ng-app ng-controller="allRuleDetails">

			<div class="header">
				<div class="headerLeftRule"></div>
				
				<div class="workbench_img_bg">	
					<img src="images/rule_work-bench.png" width="175px" height="35px"/>				
				</div>
			</div>
			<div class="clear" style="height: 5px;"></div>

			<div class="centertop">
				<div class="menucontainer">
					<ul class="nav">
						<li class="menu_leftSel"></li>
						<li class="menu_centerSel"><a
							href="<%=request.getContextPath()%>/jsp/listBusinessRule.jsp?ruleTypeId=1&ruleSetTypeId=1">Rules
								</a>
						</li>
						<li class="menu_rightSel"></li>
						<li class="menu_left"></li>
						<li class="menu_center"><a href="<%=request.getContextPath()%>/jsp/AddGroup.jsp?ruleTypeId=1&ruleSetTypeId=1">Groups</a>
						</li>
						<li class="menu_right"></li>
					</ul>
				</div>
			<div style="border:1px solid #a8d7ea;">
				<div class = "rule_action_btns" style="padding-top:0px;">
					<table width="450" border="0" align="right" cellpadding="0"
						cellspacing="0">
						<tr>
							<td width="144" style=""><input type="text" class="searh_box"
								id="search-term" />
							</td>
							<td width="56" align="center"><img
								src="images/searh_icon.png" width="32" height="30">
							</td>							
							<td width="110" align="center"><a
								class="action_btn" href="<%=request.getContextPath()%>/jsp/buildBusinessRule.jsp?ruleTypeId=1&flag=1" style="float:right">
									Create Rule
							</a>
								</td>
							<td width="120" align="center"><a class="action_btn"
								href="<%=request.getContextPath()%>/jsp/ruleSet.jsp?ruleType=3"> 
								Create Rule Set
							</a></td>
						</tr>
					</table>
				</div>
				<div class="fixed_header_tbl_wrapper" id="rule_list_tbl">
					<div class="fix_head_wrapper">
						<table class="fix_head">
							<tbody>
								<tr class="list_table_head_bg">
									<th width="215px" height="30" align="center" >Rule Name</th>
									<th width="70px" height="30" align="center" >Type</th>
									<th width="94px" height="30" align="center" >Group</th>
									<th width="94px" height="30" align="center" >Comments</th>
									<th width="125px" height="30" align="center" >Created Date</th>
									<th width="125px" height="30" align="center" >Updated Date</th>
									<th height="30" align="center" class="last-column" >&nbsp;</th>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="main_table_wrapper">
						<table class="cust_tbl" cellpadding="5px" border="0" width="100%">
							<tbody id="tblRulesBody">
								<tr height="30"  ng-repeat = "data in listRules" ng-class-odd="'odd'" ng-class-even="'even'">
									<td height="30" class="" align="left" width="215px" ng-bind="data.ruleName"></td>
									<td height="30" width="70px" align="center" ng-show="data.ruleTypeID == 1" ng-bind="'Rule'"></td>
									<td height="30" width="70px" align="center" ng-show="data.ruleTypeID == 3" ng-bind="'RuleSet'"></td>
									<td height="30" class="" width="96px" align="center" ng-bind="data.groupName" ></td>	
									<td height="30" class="" width="96px" align="center"   ><div class="data" ng-bind="data.comments"></div></td>
									<td height="30" width="125px"align="center" ng-bind-template="{{data.createdDate}} {{data.createdTime}}" ></td>
									
									<td height="30" width="125px"align="center" ng-show="data.updatedDate == null" ng-bind-template="{{data.createdDate}} {{data.createdTime}}"></td>
									<td height="30" width="125px"align="center" ng-hide="data.updatedDate == null" ng-bind-template="{{data.updatedDate}} {{data.updatedTime}}"></td>
									
									<td width="160"><div ng-show="data.ruleTypeID == 1" style="width:30px;float:left;text-align:center"><a ng-href="<%=request.getContextPath()%>/jsp/buildBusinessRule.jsp?ruleId={{data.ruleID}}&ruleType={{data.ruleTypeID}}"><img src="images/edit_bton.png" width="25" height="25" title="Edit"/></div></a>
									<div ng-show="data.ruleTypeID == 3" style="width:30px;float:left;text-align:center"><a ng-href="<%=request.getContextPath()%>/jsp/ruleSet.jsp?ruleSetId={{data.ruleID}}&ruleType={{data.ruleTypeID}}"><img src="images/edit_bton.png" width="25" height="25" title="Edit"/></div></a>
									<div style="width:30px;float:left;text-align:center"><a ng-href="<%=request.getContextPath()%>/clone?ruleID={{data.ruleID}}"><img src="images/copy_icon.png" width="25" height="25" title="Copy"/></div></a>
									<div style="width:30px;float:left;text-align:center"><a ng-href="<%=request.getContextPath()%>/jsp/TestBed.jsp?ruleID={{data.ruleID}}&&ruleName={{data.ruleName}}"><img src="images/testcheck.png" width="25" height="25" title="Test"/></div></a>
									<div style="width:30px;float:left;text-align:center"><a ng-href="<%=request.getContextPath()%>/downloadRuleJS?ruleID={{data.ruleID}}"><img src="images/download_bton.png" width="25" height="25" title="Download"/></div></a>
									<div style="width:30px;float:left;text-align:center"><a ng-click ="deleteRule(data.ruleID, data.ruleTypeID)"><img src="images/delete_bton.png" width="25" height="25" title="Delete"/></div></a></td>
								</tr>
							
							</tbody>
						</table>
					</div>	
				</div>

				</div>
			</div>
			<div id="flash" style="display:none;" class="loaderclass"></div>
			<div style="height: 60px; width: 100%" class="cognizant_logo">
			</div>
			<lepopup control="lePopupCtrl"/>
		</div>
	</center>
</body>

</html>
