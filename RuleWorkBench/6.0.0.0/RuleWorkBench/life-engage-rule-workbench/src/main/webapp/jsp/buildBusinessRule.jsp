<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="true"%>
	<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% 
	if(session.getAttribute("username")== null){
	    response.sendRedirect("../login");
	}
%>
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="myapp">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8;IE=9;IE=10" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Build Rule</title>
<script src="lib/vendor/jquery-1.7.1.min.js" type="text/javascript"></script>
<link href="css/jquery-ui-modelpopup.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="lib/vendor/jquery.tmpl.min.js"></script>
<script src="lib/vendor/jquery-ui-modelpopup.min.js" type="text/javascript"></script>
<script src="lib/vendor/jquery.json-2.2.min.js" type="text/javascript"></script>
<!-- Added for angular-->
<script src="lib/vendor/angular.min.js"></script>
<script src="lib/vendor/ui-bootstrap-tpls-0.10.0.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="controller/buildRuleController.js"></script>
<script src="directive/lePopup.js"></script>
<link type="text/css" href="css/stylesPopup.css" rel="stylesheet" />


<!-- <link rel="stylesheet" href="css/AngularStyle.css" /> -->
<!--[if lte IE 8]>
        <script>
          document.createElement('ng-include');
          document.createElement('ng-pluralize');
          document.createElement('ng-view');
 
          // Optionally these for CSS
          document.createElement('ng:include');
          document.createElement('ng:pluralize');
          document.createElement('ng:view');
        </script>
      <![endif]-->

<!-- Added for angular-->
<link type="text/css" rel="stylesheet" href="css/styles.css" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<script src="lib/vendor/le/Intellisense.js" type="text/javascript"></script>
<script type="text/javascript" src="lib/vendor/le/LEIntelliSense.js"></script>
<script src="lib/vendor/json.js" type="text/javascript"></script>
<script src="lib/vendor/le/BuildRules.js" type="text/javascript"></script>
<script src="lib/vendor/Beautify.js" type="text/javascript"></script>
<link href="css/Intellisense.css" rel="stylesheet" type="text/css" />
<script src="lib/vendor/jshint-1.1.0.js" type="text/javascript"></script>
<script type="text/javascript">
        var RuleData = { "rhs": "", "connector": "And", "operator": "==", "lhs": "" , "valueTypeField" : "", "orderBy" : "", "orderByType" : ""  };		//Aneesh
</script>

<script type="text/javascript">
flagUnload= false;

$(document).ready(function() {
$(window).bind('beforeunload', function(){

	<%session.putValue("editFlag", false);%>
	
	if(flagUnload == false){
  		return '';
	}
});
});
</script>

<script type="text/javascript">

/*commenting older edit
jQuery(function ($) {
	var ruleId = <%=request.getParameter("ruleId")%>;
	var groupList;
	if(ruleId == null){
	$.ajax({
		type: "POST",
	    url: "<%=request.getContextPath()%>/listGroupTable",
		dataType : "json",
		data : { },
		async : false,
		cache : false,												
		success : function(data) {
			
			json = data;
			var groupBOList = json.groupBOList;
			
			for(var i = 0; i < groupBOList.length; i++){
				groupList = groupBOList[i].groupName;
				//$(("#groupSel1")  + "").append("<option value='"+groupList+"'>" + groupList+ "</option>");
			}
			
		}
		 
});
	}
});
*/
</script>


<script type="text/javascript">

     function validate() {

      		document.forms["logout"].submit();
    	}
       
   </script>
<script type="text/javascript">

        jQuery(function ($) {
            
            $('#cancel').click(function () {
            	
            	window.location = "<%=request.getContextPath()%>/jsp/listBusinessRule.jsp?ruleTypeId=1&ruleSetTypeId=1";
            });
           
	/* $(document).ready(function() {

		$('#spinner').bind("ajaxSend", function() {
			$(this).show();
		}).bind("ajaxComplete", function() {
			$(this).hide();
		});

	});
	 */
	//popup

		
			
		}); // jQuery End
</script>



<script id="failOutput" type="text/x-jquery-tmpl">
      <p>There are some errors in the generated javascript.</p>

      {{if errors}}
        <label>Errors:</label>

        <ul>
        {{each(i, error) errors}}
          {{if error}}
            <li>
              <p>Line ${error.line}:<code>${error.evidence}</code></p>
              <p>${error.reason}</p>
            </li>
          {{/if}}
        {{/each}}
        </ul>
      {{/if}}
    
    </script>
<script id="passOutput" type="text/x-jquery-tmpl">    
      <p>Successfully saved </p>    
    
    </script>
    
     
    
    

</head>
<body onunload="leavePage();">
<div id="intelliDiv"></div>
<div class="blue_bg_header">
<div class="top_header_components clearfix">
	<div class="">
		<div class="icons" style="width:260px;">
		<img src="images/icon1.png" style="vertical-align:bottom;float:left"></img>
		<span class="greeting_header" style = "color: white;float:left">Welcome</span> <span class="login_name_txt"> <%=session.getAttribute("username")%> 
		</span>
		</div>
		<div class="icons1" >
			<img src="images/help_icon.png" class="vt_align"></img>
			<a href="javascript:void(0)" title="Help" class="helpLink greeting_header" style= "color : white;">Help</a>
		</div>
		
		<form:form theme="simple" action="../logout" name="logout" method="post"
			id="logout" style="float:left;">
			
			<div class="icons2" ><button class="action_btn"><a class="greeting_header" title="Logout" style= "color : white;" onclick="validate();">Logout</a></button></div>
		</form:form>
	</div>
</div>
</div>
	<center>
		<div class="container">
			
			<div class="header">
				<div class="headerLeftRule"></div>
				
				<div class="workbench_img_bg">	
					<img src="images/rule_work-bench.png" width="175px" height="35px"/>				
				</div>
			</div>
			<div class="clear" style="height: 5px;"></div>

			<div class="centertop">
				<div class="">
					<div class="menucontainer">
						<ul class="nav">
							<li class="menu_leftSel"></li>
							<li class="menu_centerSel"><a
								href="<%=request.getContextPath()%>/jsp/listBusinessRule.jsp?ruleTypeId=1&ruleSetTypeId=1">Rules
							</a></li>
							<li class="menu_rightSel"></li>
							<li class="menu_left"></li>
							<li class="menu_center"><a
								href="<%=request.getContextPath()%>/jsp/AddGroup.jsp">
							Groups</a></li>
							<li class="menu_right"></li>

						</ul>
					</div>
					
									
	<!-- Angular	-->								
									<div  id="ng-app" ng-app ng-controller="DialogDemoCtrl" style="border: #a8d7ea 1px solid; width: 940px;overflow:hidden">
										<div class="span12 article-tree">
										
										
										<!-- Tree Template Starts	Vinitha-->
										<!-- Template should be in single line (IE8 issue fix)-->											
										<script type="text/ng-template"  id="tree_item_renderer"><span class="plus_minus_sign_box" onclick="hideshow(this)" ng-click="showAll(data)" ><i ng-show="data.nodes.length >0" class="icon-plus-sign inline"></i><label class ="inline">{{showNode(data)}}</label></span><span class="action ng-binding"  ng-show="data.count!=0 && data.showMinus"> - </span><input type="text" ng-model="data.value" style="display : inline-block; width : 100px;margin-left: 10px;margin-top: 2px;" ng-show="data.showTextBox  && data.type!='operator' && data.type!='connector' " readonly /><textarea ng-show="data.showTextArea != 'undefined' && data.showTextArea" ng-model="data.value" style="display : inline-block; margin-left: 10px;margin-top: 2px;" readonly></textarea><select style="display : inline-block; width : 70px" ng-show="data.showTextBox && data.type=='operator'" ng-model="data.value" disabled><option ng-repeat="choice in operators">{{choice}}</option></select><select style="display : inline-block; width : 70px" ng-show="data.showTextBox && data.type=='connector'" ng-model="data.value" disabled><option ng-repeat="choice in connector">{{choice}}</option></select><ul class="some"  ><li ng-repeat="data in data.nodes" class="parent_li" ng-class="{'last_li':$last}" ng-include="'tree_item_renderer'" ng-init="parent=$parent.data"></li></ul></script>
										<!-- Tree Template Ends Vinitha	-->	

												<script type="text/ng-template"  id="item_renderer"><div ng-show="data.type=='IF' || data.type=='ElseIF' || data.type=='Else'""  class="box">
												
													<div style="text-align:right;vertical-align:center;padding-right:8px" class="ruleIf" > 
													<img src="images/close_red.png" id="deleteAction" class="deleteAction" align="right" title="Delete Action" ng-show="parent!=rule" ng-click="deleteNestedIfAlertBox(data,parent)">	
													<div class="title_input_container">
														<div class="block_title">
															<div  class="tableRowDoublebold blue_txt_color" style="width:30px;padding-left:10px;padding-top:3px;text-align: left;" ng-show="data.type=='IF'">If</div>
															<div  class="tableRowDoublebold blue_txt_color" style="width:80px;padding-left:10px;padding-top:3px;text-align: left;" ng-show="data.type=='ElseIF'" >Else If</div>
															<div  class="tableRowDoublebold blue_txt_color" style="width:80px;padding-left:10px;padding-top:3px;text-align: left;" ng-show="data.type=='Else'" >Else</div>
														</div>
														
														<div ng-repeat="data1 in data.Conditions.Condition"  class="inputs_template_block" >			
															<div class="inner_input_container">
																<div  class="attrInputWrapper" style="float:left;"> 
																	<input id="txtLHS" type="text" ng-model="data1.lhs" class="intelli" onkeyup="checkText(this);" required/>
																</div>
															
																<select name="operend" ng-model="data1.operator" style="float:left;width:40px;margin: 8px 3px 10px 3px;">
																	<option ng-repeat="choice in operators">{{choice}}</option>
																</select>
																<div class="attrInputWrapper" style="float:left;"> 
																	<input id="txtRHS" type="text" ng-model="data1.rhs" class="intelli" onkeyup="checkText(this);" required /> 
																</div>
																<div class="inner_add_delete_btn">
																	<img  src="images/add.png" id='addstatement' height ="20px" width="20px" class="addstatement" align="left"  title="Rule Operand" ng-click="addCondition(data)"/>
																	<img  src="images/delete.png" id='deleteStatement' height ="20px" width="20px" class="" align="left"  title="Delete condition" ng-click="removeConditionAlertBox(data,data1)" ng-show="data.Conditions.Condition[0]!=data1"/>
																</div>
															</div>
															<select  ng-model="data1.connector" ng-show="data1.connector.length>0" style="float:left;margin: 5px;width:60px;">
																<option ng-repeat="choice in connector">{{choice}}</option>
															</select>
															
															
														</div>
													</div>
													<div class="add_delete_btns" ng-show="parent==rule">
														<img src="images/minus.png" class=''  title="Delete this condition" ng-click="delMainNodeAlertBox(data)" />&nbsp;
														<img src="images/plus_new.png" class='addrule'  title="Add another condition"  />
														<div style="width:100%;text-align:left;margin-top:5px; padding-left:50px;">
															<div class="dvIfmenu" style="width:70px;text-align:left;background-color:#dceff7;display:none;" >
																<ul>
																	<li ng-repeat="choice in items"> <a  ng_click="addMainNode(choice,data)" class="aMenu">{{choice}}</a> </li>
																</ul>
															</div>    
														</div>
													</div>	
														
												<div class="tableRowDoublebold blue_txt_color inner_titles" ng-show="data.Statement">Then</div>
												<div ng-show="data.Statement" style="overflow: hidden;clear: both;" ng-init="parent1=data">
													<div ng-repeat="data5 in data.Statement" style="float:left;">
														<div class="attrInputWrapper" style="float:left;">  
															<input id="txtLHS" type="text" ng-model="data5.lhs" class="intelli" onkeyup="checkText(this);" required/>
														</div>
														<label class="tableRowFontBold" style="float:left;width:40px;margin: 8px 3px 10px 3px;text-align: center;"> = </label>
														<div class="attrInputWrapper" style="float:left;">
															<input id="txtLHS" type="text" ng-model="data5.rhs" class="intelli"  onkeyup="checkText(this);" required/>
														</div>
														<div class="inner_add_delete_btn">
															<img  src="images/add.png" id='addstatement' height ="20px" width="20px" class="addstatement" align="left"  title="Rule Operand" ng-click="addStatementNode(data5,parent1,index)"/>
															<img  src="images/delete.png" id='deleteStatement' height ="20px" width="20px" class="" align="left"  title="Delete condition" ng-click="deleteStatementNodeAlertBox(data5,parent1)" ng-show="data.Statement[0]!=data5"/>
														</div>
													</div>
												</div>	
											</div>  
											

													
												<div class="add_action_container" ng-show="parent==rule">
													<div class="add_action_span">Add Action:</div>     
													<div style="float:left;vertical-align:bottom;">     
															<select style="display : inline-block; width : 80px" style="float:left" ng-model="choice1" ng-change="addNode(data,this,parent)" >
																		<option selected="selected" value="0">Select</option>
																		<option ng-repeat="choice in addActionDropDownList(data.type)">{{choice}}</option>
															</select>
													</div>
												</div>
												
												
													<div ng-repeat="data in data.Actions.Action" ng-include="'item_renderer'" class="ng-scope" ng-init="parent=$parent.data; index= $index + 1" style="margin-left: 10px;">

													</div>
													
													
												
												</div>	
												
												
											<!--	<div ng-show="data.type=='Else'" class="box" > 
													<div style="text-align:right;vertical-align:center;padding-right:8px" class="ruleElseIf" > 
													 
														<img src="images/close_red.png" id="deleteAction" class="deleteAction" align="right" title="Delete Action" ng-show="parent!=rule" ng-click="deleteNodeAlertBox(data,parent)">
														<div class="title_input_container">
															<div class="block_title" style="overflow: hidden;">
																<div  class="tableRowDoublebold blue_txt_color" style="float:left;width:30px;padding-left:10px;padding-top:3px;">Else</div>
															</div>
														
															<div ng-repeat="data in data.Actions.Action" ng-include="'item_renderer'" class="ng-scope" ng-init="parent=$parent.data" style="margin-left: 10px;">
															
															</div>
														
															<div ng-show="data.Statement" style="overflow: hidden;" ng-init="parent1=data">
																<div ng-repeat="data5 in data.Statement" style="float:left;">
																	<div class="attrInputWrapper" style="float:left;">  
																		<input id="txtLHS" type="text" ng-model="data5.lhs" class="intelli" onkeyup="checkText(this);" required/>
																	</div>
																	<label class="tableRowFontBold" style="float:left;width:40px;margin: 8px 3px 10px 3px;text-align: center;"> = </label>
																	<div class="attrInputWrapper" style="float:left;">
																		<input id="txtLHS" type="text" ng-model="data5.rhs" class="intelli" onkeyup="checkText(this);" required/>
																	</div>
																	<div class="inner_add_delete_btn">
																		<img  src="images/add.png" id='addstatement' height ="20px" width="20px" class="addstatement" align="left"  title="Rule Operand" ng-click="addStatementNode(data5,parent1,index)"/>
																		<img  src="images/delete.png" id='deleteStatement' height ="20px" width="20px" class="" align="left"  title="Delete condition" ng-click="deleteStatementNodeAlertBox(data5,parent1)" ng-show="data.Statement[0]!=data5"/>
																	</div>
																</div>
															</div>
														</div>
													
													<div class="add_delete_btns"  ng-show="parent==rule" > 
														<img src="images/minus.png" class=''   title="Delete this condition"  ng_click="delMainNodeAlertBox(data)"/>&nbsp;
														<img src="images/plus_new.png" class='addrule'  title="Add another condition"  />
														<div style="width:100%;text-align:left;margin-top:5px; padding-left:70px;">
															<div class="dvIfmenu" style="width:70px;text-align:left;background-color:#dceff7;display:none;" >
																<ul>
																			<li ng-repeat="choice in items"> <a class="aMenu" ng_click="addMainNode(choice,data)" >{{choice}}</a> </li>
															
																</ul>
															</div>    
														</div>
													 </div>
												</div>
												
												<div class="add_action_container" ng-show="parent==rule">
													<div class="add_action_span">Add Action:</div>     
													<div style="float:left;vertical-align:bottom;">     
															<select style="display : inline-block; width : 80px" style="float:left" ng-model="choice1" ng-change="addNode(data,this,parent)" >
																		<option selected="selected" value="0">Select</option>
																		<option ng-repeat="choice in items">{{choice}}</option>
															</select>
													</div>
												</div>
												
												</div>	-->
												
												
												
												<div ng-show="data.type=='Loop'" class="box">
												
												<div style="text-align:right;vertical-align:center;padding-right:8px" class="ruleLoop" > 
												
												
												<img src="images/close_red.png" id="deleteAction" class="deleteAction" align="right" title="Delete Action" ng-show="parent!=rule" ng-click="deleteNodeAlertBox(data,parent)">
													<div class="title_input_container">
													<span class="tableRowDoublebold min_pdg_top_left blue_txt_color"  style="float:left;padding-left:10px;padding-top:8px;" >Loop Block</span> 
													
													<span class="sub_title_span" style="float:left">Start</span>
													<div class="attrInputWrapper_smallest" style="float:left"><input  class="LoopStart attr_input_smallest intelli" type="text" ng-model="data.start" onkeyup="checkText(this);" required /></div> 
													<span class="sub_title_span" style="float:left">Step</span>
													<div class="attrInputWrapper_smallest" style="float:left"><input  class="LoopStep attr_input_smallest intelli" type="text" ng-model="data.step" onkeyup="checkText(this);" required /></div> 
													<span class="sub_title_span" style="float:left">Limit</span>
													<div class="attrInputWrapper_smallest" style="float:left"><input  class="LoopLimit attr_input_smallest intelli" type="text" ng-model="data.limit" onkeyup="checkText(this);" required /></div>      
													</div>
													
													
													
													<div class="add_action_container" ng-show="parent==rule">
													<img src="images/minus.png" class=''   title="Delete this condition"  ng_click="delMainNodeAlertBox(data)"/>&nbsp;
													<img src="images/plus_new.png" class='addrule'  title="Add another condition"  />
													<div style="width:100%;text-align:left;margin-top:5px; padding-left:70px;">
													<div class="dvIfmenu" style="width:70px;text-align:left;background-color:#dceff7;display:none;" >
													<ul>
													<li ng-repeat="choice in items"> <a class="aMenu" ng_click="addMainNode(choice,data)" >{{choice}}</a> </li>
													
													</ul>
													</div>    
													</div>
												 </div> 
												 <div ng-show="data.Statement" ng-init="parent1=data">
																<div ng-repeat="data5 in data.Statement" style="float:left;">
																	<div class="attrInputWrapper" style="float:left;">  
																		<input id="txtLHS" type="text" ng-model="data5.lhs" class="intelli" onkeyup="checkText(this);" required/>
																	</div>
																	<label class="tableRowFontBold" style="float:left;width:40px;margin: 8px 3px 10px 3px;text-align: center;"> = </label>
																	<div class="attrInputWrapper" style="float:left;">
																		<input id="txtLHS" type="text" ng-model="data5.rhs" class="intelli" onkeyup="checkText(this);" required/>
																	</div>
																	<div class="inner_add_delete_btn">
																		<img  src="images/add.png" id='addstatement' height ="20px" width="20px" class="addstatement" align="left"  title="Rule Operand" ng-click="addStatementNode(data5,parent1,index)"/>
																		<img  src="images/delete.png" id='deleteStatement' height ="20px" width="20px" class="" align="left"  title="Delete condition" ng-click="deleteStatementNodeAlertBox(data5,parent1)" ng-show="data.Statement[0]!=data5"/>
																	</div>
																</div>
												</div>
													
													
												</div>
												<div class="add_action_container" ng-show="parent==rule">
													<div class="add_action_span">Add Action:</div>     
													<div style="float:left;vertical-align:bottom;">     
															<select style="display : inline-block; width : 60px" style="float:left" ng-model="choice1" ng-change="addNode(data,this,parent)">
																<option selected="selected" value="0">Select</option>
																<option ng-repeat="choice in addActionDropDownList(data.type)">{{choice}}</option>
													</select>
													</div>
												</div>
												<div ng-repeat="data in data.Actions.Action" ng-include="'item_renderer'" class="ng-scope" ng-init="parent=$parent.data; index= $index + 1" style="margin-left: 10px;">

													</div>
													
												</div>

												
												</div>
												
											
												<div ng-show="data.type=='LookUp'" class="box">
												<div style="text-align:right;vertical-align:center;padding-right:8px" class="ruleLookUp" > 
												<img src="images/close_red.png" id="deleteAction" class="deleteAction" align="right" title="Delete Action" ng-show="parent!=rule" ng-click="deleteNodeAlertBox(data,parent)">
												
 
												
													<div  class="title_input_container"> 
													<span class="tableRowDoublebold min_pdg_top_left blue_txt_color"  style="float:left;padding-left:10px;padding-top:8px;" >LookUpCall Block</span> 
											 
													<span class="sub_title_span" style="float:left"></span>
													<div class="attrInputWrapper_smallest" style="float:left">
														<input  class="LookUpCallStart attr_input_smallest intelli" type="text" ng-model="data.lhs" onkeyup="checkText(this);" required ></input>
													</div> 
													<span class="sub_title_span" style="float:left">= From</span>
													<div class="ddlStartWith min_mrg_top" style="float:left;padding-top:5px;">
														<select name="tableNameList" id="tableNameList" class="tableNameDropDownClass" style="width:140px;" ng-model="data.tableName" ng-change="LookUpTableSelected(data)">
															<option ng-repeat="choice in tableList" >{{choice}}</option>       						
														</select>
													</div> 
													<span class="sub_title_span" style="float:left;">Select</span>
													<div class="ddlStartWith min_mrg_top" style="float:left;padding-top:5px;color:#006699;">
														<select name="dbFunctionList" id="dbFunctionList" class="dbFunctionList" style="width:70px;" style="float:left" ng-model="data.dbFunction">									  
															<option selected="selected" value=""></option>
															<option ng-repeat="choice in dbFunctions()" >{{choice}}</option>  
														</select>
														( 
														<select name="columnNameList" id="columnNameListId" class="columnNameList" style="width:140px;" style="float:left" ng-model="data.columnName">
									  
															<option ng-repeat="choice in DBDetailsList[tableList.indexOf(data.tableName)].ColumnNames" >{{choice.columnName}}</option>  
														</select>
														)
													</div>
													</div>  
												
													
													
													
													
												<div class="add_action_container" ng-show="parent==rule">
													<img src="images/minus.png" class=''   title="Delete this condition" ng_click="delMainNodeAlertBox(data)" />&nbsp;
													<img src="images/plus_new.png" class='addrule'  title="Add another condition"  />
													<div style="width:100%;text-align:left;margin-top:5px; padding-left:70px;">
													<div class="dvIfmenu" style="width:70px;text-align:left;background-color:#dceff7;display:none;" >
													<ul>
													<li ng-repeat="choice in items"> <a class="aMenu" ng_click="addMainNode(choice,data)" >{{choice}}</a> </li>
													
													</ul>
													</div>    
													</div>
												 </div>	
												 
												 </div>
												 
												 <div ng-repeat="data1 in data.Conditions.Condition" style="text-align:left;padding: 10px 0px 5px 0px;overflow:hidden;" >
														<div ng-show="data1!=data.Conditions.Condition[0]" style="padding-left:10px;margin-bottom:15px;background-color:#e8f3f9;width:110px;" >And</div>
														<span class="tableRowDoublebold blue_txt_color" style="width:80px;padding-left:10px;padding-top:3px;float:left;text-align:left;">Condition</span>
														
														<select  name="columnNameList" class="columnNameList myselect" style="width:140px;padding-left: 10px;;margin-left:20px;margin-bottom:2px;float:left;" ng-model="data1.columnName">  
															<option ng-repeat="choice in DBDetailsList[tableList.indexOf(data.tableName)].ColumnNames">{{choice.columnName}}</option>  
														</select>
														
															<select style="float:left; width : 60px;margin-left: 20px;"  ng-model="data1.operator">
																<option ng-repeat="choice in operators">{{choice}}</option>
															</select>
															<span style="float:left;padding: 5px 5px 5px 20px;">Type: </span>
															<select style="float:left; width : 110px;"  ng-model="data1.valueTypeField" id="valueTypeField" >
																<option ng-repeat="choice in valueType">{{choice}}</option>
															</select>
															<div class="attrInputWrapper_smallest" style="float:left;margin-left: 20px;margin-top: 0px;"> <input type="text" class="intelli attr_input_smallest" onkeyup="checkText(this);" ng-model="data1.value" required=""> </div>
															
															<div style="width: 9%;margin-top: 5px;float:left;" >
																<img  src="images/add.png" id='addstatement' height ="20px" width="20px" class="addstatement" align="left"  title="Rule Operand" ng-click="addLookUpCondition(data1,data)"/>
																<img  src="images/delete.png" id='deleteStatement' height ="20px" width="20px" class="" align="left"  title="Delete condition"  ng-click="deleteLookUpConditionAlertBox(data1,data)" ng-show="data.Conditions.Condition[0]!=data1"/>
															</div>
														
													</div>
													<div style="overflow:hidden;">
														<span class="tableRowDoublebold blue_txt_color" style="width:80px;padding-left:10px;padding-top:3px;float:left;text-align:left;">Order By</span>
														
														<select name="columnNameList" id="columnNameListId" class="columnNameList" style="width:140px;float:left;margin-left:20px;" ng-model="data.orderBy">  
																<option>select</option> 
																<option ng-repeat="choice in DBDetailsList[tableList.indexOf(data.tableName)].ColumnNames">{{choice.columnName}}</option>  
														</select>
													</div>	
												</div>	
												
												
												
												
												<div ng-show="data.type=='Function'" class="box" >
												<div style="text-align:right;vertical-align:center;padding-right:8px" class="functionBlock" > 
												<img src="images/close_red.png" id="deleteAction" class="deleteAction" align="right" title="Delete Action" ng-show="parent!=rule" ng-click="deleteNodeAlertBox(data,parent)">
												
												
												
												<div  class="title_input_container"> 
													<div class="block_title">
														<div class="tableRowDoublebold min_pdg_top_left blue_txt_color"  style="width: 120px; padding-left:10px;padding-top:8px;text-align:left;" >Function</div> 
													</div>
													<div ng-show="data.Functions"  class="inputs_template_block"  >
														<div ng-repeat="data1 in data.Functions" ng-init="parent = data" style="padding-top:3px;">
															<div style="width:100%">
    															<div style="width:310px;float:left;text-align:left;margin-left:10px;color: #0c6689;">Function Name:</div>
    															<div style="width:310px;float:left;text-align:left;margin-left:10px;color: #0c6689;">Parameters:</div>
  															</div>	
															<div class="attrInputWrapper" style="float:left;">  
																<input type="text" class ="intelli" id="txtLhs" ng-model="data1.functionName" onkeyup="checkText(this);" required/> 
															</div>
															<div class="attrInputWrapper" style="float:left;">
																<input type="text" id ="txtRhs" class ="intelli" ng-model="data1.params" onkeyup="checkText(this);" required/> 
															</div>
															<div class="inner_add_delete_btn">
																<img  src="images/add.png" id='addstatement' height ="20px" width="20px" class="addstatement" align="left"  title="Rule Operand" ng-click="addFunctionNode(data1,parent)"/>
																<img  src="images/delete.png" id='deleteStatement' height ="20px" width="20px" class="" align="left"  title="Delete condition" ng-click="deleteFunctionNodeAlertBox(data1,parent)" ng-show="parent.Functions[0]!=data1"/>
															</div>
														</div>
													</div>
												</div>
												<div class="add_action_container" ng-show="parent==rule">
													<img src="images/minus.png" class=''   title="Delete this condition"  ng_click="delMainNodeAlertBox(data)"/>&nbsp;
													<img src="images/plus_new.png" class='addrule'  title="Add another condition"  />
													<div style="width:100%;text-align:left;margin-top:5px; padding-left:70px;">
													<div class="dvIfmenu" style="width:70px;text-align:left;background-color:#dceff7;display:none;" >
													<ul>
													<li ng-repeat="choice in items"> <a class="aMenu" ng_click="addMainNode(choice,data)" >{{choice}}</a> </li>
													
													</ul>
													</div>    
													</div>
												 </div>
												</div>
												</div>

																
												
											<div ng-show="data.type=='Statement'" class="box">
												<div style="text-align:right;vertical-align:center;padding-right:8px" class="StatementBlock" > 
												<img src="images/close_red.png" id="deleteAction" class="deleteAction" align="right" title="Delete Action" ng-show="parent!=rule" ng-click="deleteNodeAlertBox(data,parent)"></img>
													
													 
												<div  class="title_input_container"> 
													<div class="block_title">
														<div class="tableRowDoublebold min_pdg_top_left blue_txt_color"  style="width: 120px; padding-left:10px;padding-top:8px;text-align:left;" >Statement</div> 
													</div>
													
													<div ng-repeat="data0 in data.Actions.Action" class="ng-scope" style="overflow: hidden;" >
														<div ng-show="data0.Statement"  style="overflow: hidden;" >
															<div ng-repeat="data5 in data0.Statement" style="float:left;">
																<div class="attrInputWrapper" style="float:left;">  
																	<input id="txtLHS" type="text" ng-model="data5.lhs" class="intelli" onkeyup="checkText(this);" required/>
																</div>
																<label class="tableRowFontBold" style="float:left;width:40px;margin: 8px 3px 10px 3px;text-align: center;" > = </label>
																<div class="attrInputWrapper" style="float:left;">
																	<input id="txtLHS" type="text" ng-model="data5.rhs" class="intelli" onkeyup="checkText(this);" required/>
																</div>
																<div class="inner_add_delete_btn">
																	<img  src="images/add.png" id='addstatement' height ="20px" width="20px" class="addstatement" align="left"  title="Rule Operand" ng-click="addStatementNode(data5,data0,index)"/>
																	<img  src="images/delete.png" id='deleteStatement' height ="20px" width="20px" class="" align="left"  title="Delete condition" ng-click="deleteStatementNodeAlertBox(data5,data0)" ng-show="data.Actions.Action[0].Statement[0]!=data5"/>
																</div>
															</div>	
														</div>

													</div>
													
													<div ng-show="data.Statement"  style="overflow: hidden;" >
													<div ng-repeat="data5 in data.Statement" style="float:left;">
														<div class="attrInputWrapper" style="float:left;">  
															<input id="txtLHS" type="text" ng-model="data5.lhs" class="intelli" onkeyup="checkText(this);" required/>
														</div>
														<label class="tableRowFontBold" style="float:left;width:40px;margin: 8px 3px 10px 3px;text-align: center;" > = </label>
														<div class="attrInputWrapper" style="float:left;">
															<input id="txtLHS" type="text" ng-model="data5.rhs" class="intelli" onkeyup="checkText(this);" required/>
														</div>
														<div class="inner_add_delete_btn">
															<img  src="images/add.png" id='addstatement' height ="20px" width="20px" class="addstatement" align="left"  title="Rule Operand" ng-click="addStatementNode(data5,data,index)"/>
															<img  src="images/delete.png" id='deleteStatement' height ="20px" width="20px" class="" align="left"  title="Delete condition" ng-click="deleteStatementNodeAlertBox(data5,data)" ng-show="data.Statement[0]!=data5"/>
														</div>
													</div>	
													</div>
												</div>
												<div class="add_delete_btns" ng-show="parent==rule">
														<img src="images/minus.png" class=''   title="Delete this condition"  ng_click="delMainNodeAlertBox(data)"/>&nbsp;
														<img src="images/plus_new.png" class='addrule'  title="Add another condition"  />
														<div style="width:100%;text-align:left;margin-top:5px; padding-left:50px;">
														<div class="dvIfmenu" style="width:70px;text-align:left;background-color:#dceff7;display:none;" >
														<ul>
														<li ng-repeat="choice in items"> <a class="aMenu" ng_click="addMainNode(choice,data)" >{{choice}}</a> </li>
														
														</ul>
														</div>    
														</div>
													 </div> 	
												</div>
											</div>
											
											
												
												
												<!-- Aneesh-->
												
												<div ng-show="data.type=='CodeBlock'" class="box" >
												<div style="text-align:right;vertical-align:center;padding-right:8px" class="ruleCodeBlock" > 
												<img src="images/close_red.png" id="deleteAction" class="deleteAction" align="right" title="Delete Action" ng-show="parent!=rule" ng-click="deleteNodeAlertBox(data,parent)">
												
												
												<div  class="title_input_container"> 
													<span class="tableRowDoublebold min_pdg_top_left blue_txt_color" 
													style="float:left;padding-left:10px;padding-top:8px;">Code Block</span> 
												
													<div class="code_block_textarea_container">
														<textarea rows="10" cols="100" ng-model="data.Code" style="width:98%;height:60px;"></textarea>
													</div>
												</div>	

												<div class="add_action_container" ng-show="parent==rule">
													<img src="images/minus.png" class=''   title="Delete this condition"  ng_click="delMainNodeAlertBox(data)"/>&nbsp;
													<img src="images/plus_new.png" class='addrule'  title="Add another condition"  />
													<div style="width:100%;text-align:left;margin-top:5px; padding-left:70px;">
													<div class="dvIfmenu" style="width:70px;text-align:left;background-color:#dceff7;display:none;" >
													<ul>
													<li ng-repeat="choice in items"> <a class="aMenu" ng_click="addMainNode(choice,data)" >{{choice}}</a> </li>
													
													</ul>
													</div>    
													</div>
												</div> 												
												</div>			
												
												<!-- Aneesh-->	
												
												</script>
												
												
												
												
												
												
												
												
												 <script type="text/ng-template" id="myModalContent.html">
																<div class="modal_header">Delete</div>
																<div class="modal-footer">
																	
																	<div class="modal_txt_content">Delete this Condition</div>
																	<div class="modal_btn_container">
																		<button class="pop_up_modal_btn" ng-click="ok()">OK</button>
																		<button class="pop_up_modal_btn" ng-click="cancel()">Cancel</button>
																	</div>
																</div>
													</script>
												
			
												
								<div >
									
								</div>
								
								<div class = "rule_action_btns">

									<button class = "const_width_btns action_btn"  height="30" id="cancel" >Cancel</button>
									<button height="30" id="btnPreview" class="const_width_btns Preview action_btn" ng-click="constructRuleTree(rulePart)">Preview</button>
									<button  height="30" id="Submit2" class="const_width_btns SaveRule action_btn" ng-click="SaveRule()">Save</button>
									
									
								</div>

					<div class="clear"></div>
					<div style="float: left; width: 100%;">
						<div id="regions" style="width: 100%; float: left;">
							<ul id="movieList">
								<div style="padding-top: 10px;">
									<!-- <div class="tableRowDoublebold"		style="width: 99%; text-align: right;">
										<input type="submit" class="edit" id="Submit1" value="Edit"
											style="vertical-align: middle; padding-left: 10px; border: 0px; display: none;" />
									</div> -->

									<div id="spinner" style="display: none">
										<img src="images/ajax-loader.gif" alt="Loading" />
									</div>				
												
	<div class="add_new_1" style="height: 35px; font-size: 14px;">
										<span class="rule_set_title pdg_left_title">Rule Name<span class="mandatory_red">*</span>:</span>
										<input type="text" id="txtRuleName" class="txt_box" ng-model="ruleName1"
											style="float: left;" size="56" maxlength="250"
											ng-keypress="alpha($event)"/> <span
											class="rule_set_title pdg_left_title">Group:</span>
										<!-- <input type="text" id="txtGroupName" class="txt_box"  style="float:left;" size="56" maxlength="50" onkeypress="return alpha(event)"/> -->
										<select id="groupSel" class="min_mrg_top" ng-model="groupName1" ng-init="groupName1=defaultGrpName" required="required" ng-change="changeData()"
											style="width: 150px; text-align: left; vertical-align: bottom; border: 1px solid #000;">
										<!-- 	<option selected="selected" value=""></option> -->
											<option ng-repeat="choice in listGroup">{{choice}}</option>
										</select>

									</div>

									<div id="dvMessages"
										style="display: none; width: 100%; text-align: left;"></div>
									<div id="Div1" style="width: 100%; height: 10px;">&nbsp;</div>
									<center class="mrg_btm">
									
									<div id="" class="flip tableRowbold clear" style="text-align: left;">
										<div class="pdg_left" style="padding-top: 5px; float: left;">
											Comments<span class="mandatory_red">*</span></div>
											<div class="plus_icon"></div>
										<div class="clear"></div>
									</div>
									<div class="panel" style="border: 1px solid #a8d7ea; padding: 15px 0px 15px 0px; margin-bottom: 20px;">
										<textarea cols="" rows="5" ng-model="commentTextArea" class="commments_textarea"></textarea>
									</div>
									
									<div style="clear: both; height: 10px"></div>
									
										<div id="" class="flip tableRowbold clear"
											style="text-align: left;">
											<div class="pdg_left" style="padding-top: 5px; float: left;">Declare
												Variables</div>
											<div class="plus_icon"></div>
											<div class="clear"></div>
										</div>

										
											
												
												
												
	<!-- Declare section starts-->											
												
											<div class="panel"
											style="border: 1px solid #a8d7ea; padding: 15px 0px 15px 0px; margin-bottom: 20px;">

											<div class="bg_blue"
												style="width: 433px; height: 110px; margin-bottom: 10px; float: left">
												<p style="padding: 5px 5px 5px 10px; text-align: left;"
													class="blue_text">Single Valued Input Variables</p>
												<textarea cols="" rows="5" class="txt_area_1" ng-model="singleInputValue"
													id="SingleInput" ng-change="populateSingleInputtoJson(singleInputValue)" ng-keypress="variableSpecCharCheck($event)"></textarea>
												<div class="ex_info">
													(One variable per line)
													<p>Eg: insuredAge</p>
													<p>gender</p>
												</div>
												<div class="clear"></div>
											</div>

											<div class="bg_blue"
												style="width: 433px; height: 110px; margin-bottom: 10px; float: left">
												<p style="padding: 5px 5px 5px 10px; text-align: left;"
													class="blue_text">Single Valued Output Variables</p>
												<textarea cols="" rows="5" class="txt_area_1"
													id="SingleOutput" ng-model="singleOutputValue" ng-change="populateSingleOutputtoJson(singleOutputValue)" ng-keypress="variableSpecCharCheck($event)"></textarea>
												<div class="ex_info">
													(One variable per line)
													<p>Eg: isSuccess</p>
													<p>isValid</p>
												</div>
												<div class="clear"></div>
											</div>
											<div class="clear"></div>

											<div class="bg_blue"
												style="width: 433px; height: 110px; margin-bottom: 10px; float: left">
												<p style="padding: 5px 5px 5px 10px; text-align: left;"
													class="blue_text">Multi Valued Input Variables</p>
												<textarea cols="" rows="5" class="txt_area_1"
													id="MultiInput" ng-model="multiValuedInput" ng-change="populateMultiInputtoJson(multiValuedInput)" ng-keypress="variableSpecCharCheck($event)"></textarea>
												<div class="ex_info">
													(One variable per line)
													<p>Eg: premiumArray</p>
													<p>rateArray</p>
												</div>
												<div class="clear"></div>
											</div>
											<div class="bg_blue"
												style="width: 433px; height: 110px; margin-bottom: 10px; float: left">
												<p style="padding: 5px 5px 5px 10px; text-align: left;"
													class="blue_text">Multi Valued Output Variables</p>
												<textarea cols="" rows="5" class="txt_area_1"
													id="MultiOutput" ng-model="multiOutputValue" ng-change="populateMultiOutputtoJson(multiOutputValue)" ng-keypress="variableSpecCharCheck($event)"></textarea>
												<div class="ex_info">
													(One variable per line)
													<p>Eg: validationResults</p>
												</div>
												<div class="clear"></div>
											</div>
											<div class="bg_blue"
												style="width: 433px; height: 110px; margin-bottom: 10px; float: left">
												<p style="padding: 5px 5px 5px 10px; text-align: left;"
													class="blue_text">Standard Input Variables</p>
												<textarea cols="" rows="5" class="txt_area_1"
													id="StandardInput" ng-model="standardInputValue" ng-change="populateStandardInputtoJson(standardInputValue)" readonly></textarea>
												<div class="ex_info">
													<a href="#" class="topopupst1">Click to Choose Standard Variables</a>
												</div>
												<div class="clear"></div>
											</div>
											
										<div class="bg_blue"
												style="width: 433px; height: 110px; margin-bottom: 10px; float: left">
												<p style="padding: 5px 5px 5px 10px; text-align: left;"
													class="blue_text">Function Variables</p>
												<textarea cols="" rows="5" class="txt_area_1"
													id="FunctionInput" ng-model="functionInputValue" ng-change="populateFunctionInputtoJson(functionInputValue)" ng-keypress="variableSpecCharCheck($event)"></textarea>
												<div class="ex_info">
													(One variable per line)
													<p>Eg: sample</p>
												</div>
												<div class="clear"></div>
											</div>
										</div>
										<div style="clear: both; height: 10px"></div>

<!-- Declare section ends-->
											<div class="tableRowbold" style="text-align: left;">
												<span style="float: left; padding: 5px 5px 0px 10px">
													Select the condition statement to start writing the rule: </span>
													<select class="ddlStartWith min_mrg_top" style="width: 150px; text-align: left; vertical-align: bottom; border: 1px solid #000;"  ng-model="choice1" ng-change="addStartNode(this)">
														<option selected="selected" value="0">Select</option>
														<option ng-repeat="choice in startItems">{{choice}}</option>
													</select>	
											</div>
											
											<div class="main_div_container">
												<div id="mainDiv" ng-repeat="data in rulePart"  ng-include="'item_renderer'" class="ng-scope" ng-init="parent=rule; index= $index">
												</div>		
											</div>
											<div id="dlgPreview" title="Preview">
											<!-- button for validate-->
											<div class="tableRowDoublebold"
												style="width: 97%; text-align: right;">
												

											</div>
											<div class="preview_tabs">
												<ul class="nav">

													<li class="selected"><span class="tab_left"></span><span class="tab_center"><a class="treeView" ng-click="constructRuleTree(rulePart)" >TreeView</a>
													</span><span class="tab_right"></span></li>
													
													<li class="" ng-show="debug"><span class="tab_left"></span><span class="tab_center"><a class="codeView" ng-click="construct(rulePart,tree)">
													CodeView</a></span><span class="tab_right"></span></li>
												</ul>
											</div>
											<div class="preview_tab_container"><!-- preview_tab_container -->
												<div class="preview_tab" id="tree_view_tab">
													<!--Tree View-->
													<div class="tree" >
														<ul>
															<li ng-repeat="data in displayTree"  ng-include="'tree_item_renderer'"  class="ng-scope" ng-class="{'last_li':$last}"></li> 
														</ul>
													</div>
													<!--Tree View-->
												</div>
												<div class="preview_tab" id="code_view_tab" ng-show="debug">
												<button type="button" ng-click="validate()" class="action_btn preview_val_btn pull-right"> Validate </button>
												
													<div class="prev_txtarea_cont">
														
														<div id="dvValidationResults"></div>
														<textarea class="preview_txtarea" ng-model="js" readonly ></textarea>
													</div>
												</div>
												
												
												
												
											</div><!-- preview_tab_container -->
											</div>
											
											
											
											
										
										</div>
									</div>
<lepopup control="lePopupCtrl"/>
<!--  Angular End-->
										
										
										
										
								</div>
							</ul>
						</div>
						<div style="width: 199px; height: 100%; padding-top: 20px;">
							<div id="selectionMenu" style="display: none;">
								<select class="menuSelectionDiv" id="selectionList"
									multiple="multiple" name="Items">
								</select> <select visible="false" class="menuSelectionDiv"
									id="selectionListCopy" multiple="multiple" name="Items"
									style="display: none;">
								</select>
							</div>
						</div>
						
									<div id="toPopupst1">
			<p class="modal_pop_up_header ">Standard Inputs<span class="close_pop_up"></span></p>
			<div class="acc_cont" >		
				<dl class="accordion" id ="accordionID">


				</dl>
				<div class="pop_up_btn">
					<img src="images/save_bton.png" width="43" height="25" id="Submit2"
						class="SaveStaValues" ng-click="SaveStaValues(eve)">
				</div>
			</div>
			</div>
						
						
					</div>
				</div>
				<div style="clear: both;"></div>
			</div>

			<!-- tritty -->


			<div id="popup_contentst1"></div>
			<!--your content end-->

		</div>
		<!--toPopup end-->

		<div class="loaderst1"></div>
		<div id="backgroundPopupst1"></div>

		<!-- end tritty -->


		<div id="dvForScrollDown" style="display: none;"></div>

		<div style="height: 100px; width: 1000px" class="cognizant_logo">

		</div>
		</div>
		</div>
		<!-- externalContainer ends here -->
		
		
		<!-- externalWrapper ends here -->
		<input type="hidden" id="myField" value=""  />

	</center>
</body>
</html>