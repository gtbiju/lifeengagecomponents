<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="true"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	if (session.getAttribute("username") == null) {
		response.sendRedirect("../login");
	}
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8;IE=9;IE=10" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META Http-Equiv="Cache-Control" Content="no-cache" />
<META Http-Equiv="Pragma" Content="no-cache" />
<META Http-Equiv="Expires" Content="0" />
<title>Excel Upload File</title>
<link type="text/css" href="css/styles.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
<link href="css/jquery-ui-modelpopup.css" rel="stylesheet"
	type="text/css" />
<script src="lib/vendor/jquery-1.7.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="lib/vendor/jquery.dataTables.min.js"></script>
<script src="lib/vendor/jquery-ui-modelpopup.min.js"
	type="text/javascript"></script>

<script type="text/javascript">
     function validate() {
      		document.forms["logout"].submit();
    	}
</script>
<script type="text/javascript">
var validationMsg = "";
$(document).ready(function(){
	
	$('#ruleNameLabel').text("<%=request.getParameter("ruleName")%>");
	jobPopulation();
	//ajax call for populating job details table on button click
	$(document).on('click',".download",function(e){ 
							e.preventDefault();	
							jobPopulation();
	});
	$("#multiform").submit(function(e) {
											var validate = validateForm();
											if(validate == true){
												$("#popupdiv").dialog('open');
												var formObj = $(this);
												//generate a random id
												var iframeId = 'unique'
														+ (new Date().getTime());
												//create an empty iframe
												var iframe = $('<iframe src="javascript:false;" name="'+iframeId+'" />');
												iframe.hide();
												//set form target to iframe
												formObj.attr('target', iframeId);
												//Add iframe to body
												iframe.appendTo('body');
												iframe.load();
											}else{
												alert(validationMsg);
											}
										});
							//dialog box for showing details about refreshing the job list table
							$("#popupdiv").dialog({
								height : 200,
								modal : true,
								autoOpen : false,
								resizable : false,
								draggable: false,
								buttons: [{  text :'Ok',
											click: function() { 
												$("#multifile").val("");
												$("#comment").val("");
												$(this).dialog("close"); },
										 }]
							});
					});
					
//// validates the form by checking whether all required fileds are entered					
function validateForm() {
	var fileName = $("#multifile").val(); 
	var comments = $("#comment").val(); 
	var extension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
	
	if(fileName == ''){
		validationMsg = "Please upload a valid file.";
		return false;
	} else if(comments == ''){
		validationMsg = "Please enter the comments.";
		return false;
	} else if(extension != "xlsx"){
		validationMsg = "Invalid file extension.Please upload a .xlsx file.";
		return false;
	}
	return true ;
}

// populates the job details table using datatable
function jobPopulation(){
	var userId = '<%=session.getAttribute("userId")%>';
	var urlPath = '<%=(String) request.getContextPath()%>/getJobDetails';
	var statusCode;
	var jobId ;
	var filePath;
	var ruleName;
	var startTime;
	var endTime;
	var comment;
	var errorMsg ="";
		$.ajax({
			type: "POST",
			url: urlPath,
		    data : {userId:userId},
		    async : false,
			cache : false,	
			dataType : "json",
	        success : function(data) {
				var jobBOList = data;
				var jobBORow='<table id="jobTable" class="stripe job_list_table"><thead><tr height="30px" class="list_table_head_bg"><th width="75px">Job Id</th><th width="100px" align="left" style="padding-left:30px;">Rule Name</th><th width="100px">Comment</th><th width="125px">Start Time</th><th width="125px">End Time</th><th width="80px">Status</th><th width="125px">Output</th></tr></thead><tbody>';
				// iterate through all job details
				for(var i = 0; i < jobBOList.length; i++){
						statusCode = jobBOList[i].statusCode;
						jobId = jobBOList[i].jobId;
						filePath = jobBOList[i].filePath;
						ruleName = jobBOList[i].ruleName;
						startTime = jobBOList[i].startTime;
						endTime = jobBOList[i].endTime;
						comment = jobBOList[i].comment;
						if(jobBOList[i].errorMsg != null){
							errorMsg = jobBOList[i].errorMsg.replace(/\"/g, "'");
						}
						var tableClass = "odd";
						if(i%2==0){
							tableClass = "even";
						}
						if(statusCode=="COMPLETED"){
							var statusColor ="color:green";
						}else if(statusCode=="COMPLETED WITH ERRORS"){
							var statusColor ="color:orange";
						}
							
						if(statusCode=="COMPLETED" || statusCode=="COMPLETED WITH ERRORS"){
						jobBORow += '<tr  height="30px" class="'+ tableClass +'">'+'<td  width="75px" align="center">' +jobId +
						'</td><td width="100px" align="left" style="padding-left:30px;">' +ruleName +'</td><td width="100px"><div class="data">'+comment+'</div></td><td width="125px">'+startTime+'</td><td width="125px">'+endTime+'</td><td width="80px"align="center" style="'+statusColor+'">' +  statusCode+
					    '</td><td width="125px" align="center">' +'<a href="<%=request.getContextPath()%>/downloadExcelFile?filePath='
										+ filePath
										+ '" class="download_link">View here</a>'
										+ '</td></tr>';
							} else {
								jobBORow += '<tr height="30px" class="'+ tableClass +'">'
								+ '<td  width="75px" align="center">'
								+ jobId
								+ '</td><td  width="100px" align="left" style="padding-left:30px;">'
								+ ruleName
								+ '</td><td width="100px"><div class="data">'+comment+'</div></td><td width="80px">'+startTime+'</td><td width="125px">'+endTime+'</td><td width="80px" align="center" style="color:red" title ="'+ errorMsg +'">'
								+ statusCode
								+ '</td><td width="125px" align="center">-----</td></tr>';
							}

						}
						jobBORow += '</tbody></table>';
						$('#jobList').html(jobBORow);
						//customizing the datatable features
						$('#jobTable')
								.dataTable(
										{
											"sPaginationType" : "full_numbers", //to get pagination that contains page numbers
											"iDisplayLength" : 20, //to set how much rows will be initially displayed per page
											"oLanguage" : {
												"sSearch" : ""
											}, //to remove search filter label
											"bLengthChange" : false, //to disable default items per page drop-down 
											"aaSorting" : [ [ 0, 'desc' ] ], //to set the initial sort order of the columns 
											"aoColumns" : [
													{
														"sType" : "numeric",
														"asSorting" : [ "desc",
																"asc", "desc" ]
													},
													{
														"sType" : "string",
														"asSorting" : [ "desc",
																"asc", "desc" ]
													},
													{
														"bSortable" : false
													},
													{
														"bSortable" : false
													},
													{
														"bSortable" : false
													},
													{
														"sType" : "string",
														"asSorting" : [ "desc",
																"asc", "desc" ]
													}, {
														"bSortable" : false
													} // to set the sorting orders that will be applied when user clicks on the heading 
											],
											"sDom" : '<"datatable_border"<"pagination_links"p><"download"><"table_filter"f>>t<"blue_text"i>',

										});
						$("#jobTable").removeClass("dataTable"); //removing default datatable css class 
						$(".download")
								.html(
										"<input type='button' id='downloadButton' class='refresh_button' value='Refresh jobs' />");
						$("#jobTable_filter").append(
								"<img src='images/searh_icon.png'></img>");
					},
					error : function(data) {
						alert("error");
					}
				});
	}

	function getDoc(frame) {
		var doc = null;
		// IE8 cascading access check
		try {
			if (frame.contentWindow) {
				doc = frame.contentWindow.document;
			}
		} catch (err) {
		}
		if (doc) { // successful getting content
			return doc;
		}
		try { // simply checking may throw in ie8 under ssl or mismatched protocol
			doc = frame.contentDocument ? frame.contentDocument
					: frame.document;
		} catch (err) {
			// last attempt
			doc = frame.document;
		}
		return doc;
	}
	// validates the form by checking whether all required fileds are entered
	/* function validateForm(theform) {
		for ( var i = 0; i < theform.elements.length; i++) {
			var element = theform.elements[i];
			if (element.className.indexOf("required") != -1) {
				element.className = "required";
				if (!isFilled(element)) {
					element.className += " error";
					element.focus();
					return false;
				}
			}
		}
		return true;
	}
	
	function isFilled(field) {
		if (field.value.length < 1) {
			return false;
		} else {
			return true;
		}
	} */
</script>
</head>
<body>
	<div class="blue_bg_header">
		<div class="top_header_components clearfix">
			<div class="">
				<div class="icons" style="width: 260px;">
					<img src="images/icon1.png"
						style="vertical-align: bottom; float: left"></img> <span
						class="greeting_header" style="color: white; float: left">Welcome</span>
					<span class="login_name_txt"> <%=session.getAttribute("username")%>
					</span>
				</div>
				<div class="icons1">
					<img src="images/help_icon.png" class="vt_align"></img> <a
						href="javascript:void(0)" title="Help"
						class="helpLink greeting_header" style="color: white;">Help</a>
				</div>

				<form action="../logout" name="logout" method="post" id="logout"
					style="float: left;">
					<div class="icons2">
						<button class="action_btn">
							<a class="greeting_header" title="Logout" style="color: white;"
								onclick="validate();">Log Out</a>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<center>
		<div class="container">
			<div class="header">
				<div class="headerLeftRule"></div>

				<div class="workbench_img_bg">
					<img src="images/rule_work-bench.png" width="175px" height="35px" />
				</div>
			</div>
			<div class="clear" style="height: 5px;"></div>
			<div class="menucontainer">
				<ul class="nav" style="padding-left: 0px;">
					<li class="menu_leftSel"></li>
					<li class="menu_centerSel"><a
						href="<%=request.getContextPath()%>/jsp/listBusinessRule.jsp?ruleTypeId=1&ruleSetTypeId=1">Rules
					</a></li>
					<li class="menu_rightSel"></li>
					<li class="menu_left"></li>
					<li class="menu_center"><a
						href="<%=request.getContextPath()%>/jsp/AddGroup.jsp">Groups </a></li>
					<li class="menu_right"></li>
				</ul>
			</div>
			<div
				style="border: #a8d7ea 1px solid; width: 940px; overflow: hidden;">
				<div class="add_new" style="height: 10px;"></div>
				<center class="white_bg">
					<div class="tableRowbold "
						style="text-align: left; text-indent: 15px;">
						<div style="padding-top: 5px; float: left">
							Rule Name : <span id="ruleNameLabel" name="ruleNameLabel"
								class="title_rule_lbl"> </span>
						</div>

					</div>
					<div class="wrapper1"
						style="height: auto; width: 900px; border: 1px solid #a8d7ea; padding: 18px 0 20px 0">
						<br> <br>

						<form method="POST" id="multiform" enctype="multipart/form-data" 
							action="<%=request.getContextPath()%>/jobLauncher" onsubmit="return validateForm();"> 
							<center>
								<table class="bg_blue_file_upload">
									<tr>
										<td><span class="blue_text">File to upload:</span><span class="form-required">*</span></td>
										<td><span class="blue_text"><input type="file"
												name="file" id="multifile" value=""></span></td>
									</tr>
									<tr>
										<td><span class="blue_text">Enter the comments :</span><span class="form-required">*</span></td>
										<td><textarea name="comment" id="comment" class="required"  rows="2" cols="30"></textarea></td>
										<td><span class="blue_text"><input type="submit"
												id="submit_btn" class="action_btn" style="float: none;"
												value="Start regression test"></span></td>
									</tr>
								</table>
							</center>
							<input type="hidden" name="ruleName"
								value="<%=request.getParameter("ruleName")%>"> <input
								type="hidden" name="ruleId"
								value="<%=request.getParameter("ruleId")%>"> <input
								type="hidden" name="username"
								value="<%=(String) session.getAttribute("username")%>">
							<input type="hidden" name="userId"
								value="<%=session.getAttribute("userId")%>">
						</form>
						<br> <br>
						<center>
							<div id="jobList" style="width: 850px"></div>
							<div class="clear" style="height: 10px;"></div>
						</center>
					</div>
					<div class="clear" style="height: 10px;"></div>
				</center>
			</div>
			<div class="clear" style="height: 10px;"></div>
		</div>
	</center>
	<div id="popupdiv" style="display: none">
		<span style="color: #069;"> The job has started. Click on
			Refresh Jobs button to know about the job status. If job status is failed, hover on it to know more details. </span>
	</div>
	<div style="height: 100px; width: 100%" class="cognizant_logo"></div>
</body>
</html>