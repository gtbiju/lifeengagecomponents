<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="true"%>
	<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<% 
	if(session.getAttribute("username")== null){
	    response.sendRedirect("../login");
	}
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META Http-Equiv="Cache-Control" Content="no-cache" />
<META Http-Equiv="Pragma" Content="no-cache" />
<META Http-Equiv="Expires" Content="0" />
<title>Test Bed</title>
<link type="text/css" href="css/styles.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<script type="text/javascript" src="lib/vendor/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery-ui-1.8.24.custom.min.js"></script>
<script src="lib/vendor/jquery.json-2.2.min.js" type="text/javascript"></script>
<script src="lib/vendor/jshint-1.1.0.js" type="text/javascript"></script>
<script type="text/javascript" src="lib/vendor/json.js"></script>
<style>
.nav {
	padding-left: 0;
	margin-bottom: 0;
	list-style: none
}
</style>
<script type="text/javascript">
flagUnload= false;

$(document).ready(function() {
$(window).bind('beforeunload', function(){

		
	if(flagUnload == false){
  		return '';
	}
});
});
</script>
<script type="text/javascript">

        jQuery(function ($) {
        	
        	 $('#cancel').click(function () {
             	
             	window.location = "<%=request.getContextPath()%>/jsp/listBusinessRule.jsp?ruleTypeId=1&ruleSetTypeId=1";
             });
        	
        });
</script>

<script>

	var ruleId = "";
	$(document).ready(function() {
		
		<%session.putValue("editFlag", false);%>
		ruleId = <%=request.getParameter("ruleID")%>;
		if(ruleId != null){
		$.ajax({
			type: "POST",
		    url: "<%=request.getContextPath()%>/test",
		    dataType : "json",
			data : { ruleID : ruleId },
			async : false,
			cache : false,							
			success : function(data) {
				
				RuleData = JSON.parse(data.ruleJson);
				window.ruleName=data.ruleName;
				var debugConf = data.debugConf;
				$('#ruleLabel').text(data.ruleName);
				$('#txtTest').val(data.ruleJs);
				if(debugConf == true || debugConf=="true"){
					$("#testBed").css("display","visible");
					$("#testBedClr").css("display","visible");
				} else if(debugConf == false || debugConf=="false"){
					$("#testBed").css("display","none");
					$("#testBedClr").css("display","none");
				}
	            var singinputTable = "<table>";
	            var altrow = 0;
	            var newRow = "";
	            $.each(RuleData.Variables, function (name, value) {
	                switch (name) {
	                    case 'SingleInput':
	                        var strValue = "";
	                        altrow = 0;
	                        $.each(value, function (k, v) {
	                            altrow = altrow + 1;
	                            var cols = "";
	                            strValue = $.trim(v);
	                            if (strValue != "") {
	                            	if (altrow % 2 != 0) {
	                                    newRow = $("<tr>");
	                                }

	                                cols = cols + "<td width='20%' align='right' class='pdg_top_btm'><label id='lblinput'> " + strValue + "</label></td>";
	                                cols = cols + "<td width='30%' align='left' class='pdg_top_btm'><div class='attrInputWrapper_small' style:'float:left;'> <input type='text' name='singleInput' class='attrInput_small' id='" + strValue + "'/></div></td>"
	                                newRow.append(cols);
	                                $('#singInputVariables').append(newRow);
	                            }
	                        });
	                        break;
	                    case 'MultiInput':
	                        var strValue = "";
	                        $.each(value, function (k, v) {
	                            var cols = "";
	                            strValue = $.trim(v);
	                            if (strValue != "") {
	                                var newRow = $("<tr>");
	                                var cols = cols + "<td width='30%' align='right' class='pdg_top_btm'><label id='lblinput'> " + strValue + "</label></td>";
	                                cols = cols + "<td width='70%' align='left' class='pdg_top_btm'><div class='attrInputWrapper'> <input type='text' class = 'attrInput' name='multiInput' id='" + strValue + "'/></div></td>"
	                                newRow.append(cols);
	                                $('#mulitInputVariables').append(newRow);
	                            }
	                        });
	                        break;
	                    case 'SingleOutput':
	                        break;
	                    case 'MultiOutput':
	                        break;
	                    case 'StandardInput':
	                    	var strValue = "";
							if(typeof value[0] == 'string') {
								$.each(value, function (k, v) {
		                            var cols = "";
		                            strValue = $.trim(v);
		                            if(strValue != ""){
			                            //if(v==("SurrenderOption.amount")||("OPPB.premiumValues")||("LoanRepayment.amount")||("Loan.amount")){
			                            if((v=="SurrenderOption.amount")||(v=="OPPB.premiumValues")||(v=="LoanRepayment.amount")||(v=="Loan.amount")||(v=="TopUp.input")){
											var newRow = $("<tr>");
				                            var cols = cols + "<td width='30%' align='right' class='pdg_top_btm'><label id='lblinput'> " + strValue + "</label></td>";
				                            	cols = cols + "<td width='70%' align='left' class='pdg_top_btm'><div class='attrInputWrapper'> <input type='text' class = 'attrInput' name='multiInput' id='" + strValue + "'/></div></td>"
				                            	newRow.append(cols);
				                                $('#mulitInputVariables').append(newRow);
			                            	
			                            } else {
				                            if (strValue != "") {
												newRow = $("<tr>");
				                                cols = cols + "<td width='30%' align='right' class='pdg_top_btm'><label id='lblinput'> " + strValue + "</label></td>";
				                                cols = cols + "<td width='70%' align='left' class='pdg_top_btm'><div class='attrInputWrapper_small' style:'float:left;'> <input type='text' name='standInput' class='attrInput_small' id='" + strValue + "'/></div></td>"
				                                newRow.append(cols);
				                                $('#standInputVariables').append(newRow);
				                            }
			                            }
		                            }
		                        });
							} else if(typeof value[0] == 'object') {
								$.each(value[0], function (key, value) {
									var cols = "";
		                            strValue = $.trim(key);
		                            if(strValue != ""){
			                            //if(v==("SurrenderOption.amount")||("OPPB.premiumValues")||("LoanRepayment.amount")||("Loan.amount")){
			                            if(value == "MultiInput"){
											var newRow = $("<tr>");
				                            var cols = cols + "<td width='30%' align='right' class='pdg_top_btm'><label id='lblinput'> " + strValue + "</label></td>";
				                            	cols = cols + "<td width='70%' align='left' class='pdg_top_btm'><div class='attrInputWrapper'> <input type='text' class = 'attrInput' name='multiInput' id='" + strValue + "'/></div></td>"
				                            	newRow.append(cols);
				                                $('#mulitInputVariables').append(newRow);
			                            	
			                            } else if(value == "SingleInput"){
				                            if (strValue != "") {
												newRow = $("<tr>");
				                                cols = cols + "<td width='30%' align='right' class='pdg_top_btm'><label id='lblinput'> " + strValue + "</label></td>";
				                                cols = cols + "<td width='70%' align='left' class='pdg_top_btm'><div class='attrInputWrapper_small' style:'float:left;'> <input type='text' name='standInput' class='attrInput_small' id='" + strValue + "'/></div></td>"
				                                newRow.append(cols);
				                                $('#standInputVariables').append(newRow);
				                            }
			                            }
		                            }
								});
							}
							
	                    	 
	                }
	            });

				}						
			});
		}
	});
	
$(".delete").click(function(e) {
	e.preventDefault();
	inoutmap = '{"inoutMap" :{';
    $("input[type=text]").each(
	function () {
	    if ($(this).attr('name') == 'multiInput') {
	        inoutmap = inoutmap + '"' + $(this).attr('id') + '"' + ":" + '[' + $(this).val() + ']' + ",";

	    } else {
	        inoutmap = inoutmap + '"' + $(this).attr('id') + '"' + ":" + '"' + $(this).val() + '"' + ",";
	    }
	});
    inoutmap = inoutmap.substring(0, inoutmap.length - 1);
    inoutmap = inoutmap + "}}";
    inoutmapObj = jQuery.parseJSON(inoutmap);

    if(inoutmap!=null){
    	$.ajax({
			type: "POST",
		    url: "<%=request.getContextPath()%>/jsp/ExecuteTest.action",
			dataType : "json",
			data : { inoutMap : inoutmap, ruleName:window.ruleName},
			async : false,
			cache : false,	
			error : function(){
			alert("Error");
			},					
			success : function(data) {
	    }
    	});
    }
});	

function inputCheck() {
	
	count=0;
	
	$("input[type=text]").each(
			function () {
				if ($(this).val() == '') {
			    	count=count+1;
			        }
			}	
	);
	if(count==0){
		excuteRule();
	}
    else
    	{
    	feed = confirm("Some input fields are blank. If u still need to execute click OK or if u need to modify inputs click CANCEL");
     	if(feed == true)
    		{
    		excuteRule();
    		}
    	else
    		{
    		
    		}
    	}
	}


function excuteRule() {
	
    var emptyinoutmap=true;
	inoutmap = "{";
    $("input[type=text]").each(
	function () {
	    if ($(this).attr('name') == 'multiInput') {
	        inoutmap = inoutmap + '"' + $(this).attr('id') + '"' + ":" + '[' + $(this).val() + ']' + ",";
	        
	    } else {
	        inoutmap = inoutmap + '"' + $(this).attr('id') + '"' + ":" + '"' + $(this).val() + '"' + ",";
	    }
	    emptyinoutmap =false;
	});
    if(emptyinoutmap==false)
    {
    	inoutmap = inoutmap.substring(0, inoutmap.length - 1);
    }
    inoutmap = inoutmap + "}";
	inoutmap = inoutmap.replace(/\[/g,'\"\[');
	inoutmap = inoutmap.replace(/\]/g,'\]\"');
	inoutmap=inoutmap.replace(/\"/g,'\'');
	inoutmap = JSON.stringify(deepen(inoutmap));
	inoutmap=inoutmap.replace(/\"\[/g,'\[');
	inoutmap=inoutmap.replace(/\]\"/g,'\]');
	if(inoutmap != null){
		
		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/executeTest",
		    dataType : "json",
		    data : { inoutMap : inoutmap, ruleName:window.ruleName, ruleID : ruleId},
			async : false,
			cache : false,
			beforeSend: function() {
				 
	        },
			success : function(data) {
				
				json=data;
				var errormsg = json.errorMessage;
				if(errormsg != null){
					$('#txtOutput').val(errormsg);
				}else{
					$('#txtOutput').val(json.ruleJson);
					//alert(JSON.stringify(deepen("{'policyDetails.committedPremium':'111','FeeType.coverMultiple':'23','Loan.amount':'[222,1111]'}")));
					//$('#txtOutput').val(JSON.stringify(deepen("{'policyDetails.committedPremium':'111','FeeType.coverMultiple':'23','Loan.amount':'[222,1111]'}")));
				}
				},
				error : function(data) {
    				if (data.status==412) {
    					alert(JSON.parse(data.responseText).message + " : " + JSON.parse(data.responseText).details);
    				}				
    			}
				
			});

		}
	
}

function deepen(deep1) {
    deeporg = $.toJSON(deep1);
	var oo = {}, t, parts, part;
	var keyArr = [];
	var valArr = [];
	deep1 = deep1.substring(0,deep1.indexOf("}"));
	while(deep1.indexOf("'")!=-1){
	deep1 = deep1.substring(deep1.indexOf("'")+1,deep1.length);
	deep = deep1.substring(0,deep1.indexOf("'"));
	keyArr.push(deep);
	deep1 = deep1.substring(deep1.indexOf(":"),deep1.length);
	deep1 = deep1.substring(deep1.indexOf("'")+1,deep1.length);
	deep = deep1.substring(0,deep1.indexOf("'"));
	valArr.push(deep);
	deep1 = deep1.substring(deep1.indexOf("'")+1,deep1.length);
	}
	
	//key =deep.split(':');
	
   for (i=0; i<keyArr.length ;i++) {
		k =keyArr[i];
        t = oo;
        parts = k.split('.');
        var key = parts.pop();
        while (parts.length) {
            part = parts.shift();
            t = t[part] = t[part] || {};
        }
        t[key] = valArr[i]
    }
    return oo;
}



jQuery(function ($) {
	
    $("#testFlip").click(
			function () {
			    $("#testPanel").slideToggle("fast");
				if($(this).find("div.plus_icon").hasClass("plus_icon")){
						$(this).find("div.plus_icon").removeClass("plus_icon").addClass("minus_icon");
					} else if($(this).find("div.minus_icon").hasClass("minus_icon")){ 
						$(this).find("div.minus_icon").removeClass("minus_icon").addClass("plus_icon");
					}
			 });
});
	
</script>


</head>

<body>
<div class="blue_bg_header">
<div class="top_header_components clearfix">
	<div class="">
		<div class="icons" style="width:260px;">
		<img src="images/icon1.png" style="vertical-align:bottom;float:left"></img>
		<span class="greeting_header" style = "color: white;float:left">Welcome</span> <span class="login_name_txt"> <%=session.getAttribute("username")%> 
		</span>
		</div>
		<div class="icons1" >
			<img src="images/help_icon.png" class="vt_align"></img>
			<a href="javascript:void(0)" title="Help" class="helpLink greeting_header" style= "color : white;">Help</a>
		</div>
		
		<form:form theme="simple" action="../logout" name="logout" method="post"
			id="logout" style="float:left;">
			
			<div class="icons2" ><button class="action_btn"><a class="greeting_header" title="Logout" style= "color : white;" onclick="validate();">Logout</a></button></div>
		</form:form>
	</div>
</div>
</div>
	<center>
		<div class="container">
			<div class="header">
				<div class="headerLeftRule"></div>
				
				<div class="workbench_img_bg">	
					<img src="images/rule_work-bench.png" width="175px" height="35px"/>				
				</div>
			</div>
			<div class="clear" style="height: 5px;"></div>

			<div class="centertop">
				<div class="menucontainer">
					<ul class="nav">
						<li class="menu_leftSel"></li>
						<li class="menu_centerSel"><a
								href="<%=request.getContextPath()%>/jsp/listBusinessRule.jsp?ruleTypeId=1&ruleSetTypeId=1">Rules
									</a>
						</li>
						<li class="menu_rightSel"></li>
						<li class="menu_left"></li>
						<li class="menu_center"><a
								href="<%=request.getContextPath()%>/jsp/AddGroup.jsp">Groups
									</a>
						</li>
						<li class="menu_right"></li>
					</ul>
				</div>
				<div style="border: #a8d7ea 1px solid; width: 940px;overflow:hidden">
				<div class = "rule_action_btns">
					<table width="300" border="0" align="right" cellpadding="0"
						cellspacing="0">
						<tr>
													
							<td width="86" align="center"><button class="action_btn" width="66" height="30" id="cancel">Cancel</td>
							<td width="99" align="center" title="Execute the rule"><button class="action_btn" id="execute" width="79" height="30" onclick="inputCheck()"/>Execute</td>
     						<td width="200" align="center" title="Regression Test">
								<a class="action_btn" href="<%=request.getContextPath()%>/jsp/regressionTest.jsp?ruleName=<%=request.getParameter("ruleName")%>&ruleId=<%=request.getParameter("ruleID")%>">Regression Test</a>
							</td>
						</tr>
					</table>
				</div>
				
		<div class="add_new" style="height:10px;"></div>
       <center class="white_bg">
      	<div  class="tableRowbold " style="text-align: left; text-indent:15px;">
     	   <div style="padding-top:5px; float:left">Rule Name :
     	   	<span id="ruleLabel" name="ruleLabel" class="title_rule_lbl"> </span>
     	   </div>
      	 
      </div>
      <div id="dvMessages" style="display: none; width: 100%; text-align:left;"></div>
				<div class="wrapper1" style="height:auto; width:900px; border:1px solid #a8d7ea; padding:18px 0 20px 0">
      	<table id="tblVariables" width="100%" border="0" cellspacing="0" cellpadding="0">
      	  <tr>
      	    <td height="35" align="center" valign="top" >
      	    
      	    	<table id="singInputVariables" width="870" border="0" cellpadding="2" cellspacing="0" class="bg_blue_simple">
      	    	<tr><td height="32" colspan="4" align="left" style="border-bottom:1px #a8d7ea solid"><span class="blue_text" >&nbsp;Single Valued Input Variables</span></td></tr>
      	      	</table>
				</td>
    	    </tr>
			<tr>
			<tr>
				<td height="20" align="center">&nbsp;</td>
    	    </tr>
			<tr>
				<td align="center">
					<table id="standInputVariables" width="870" border="0" cellpadding="2" cellspacing="0" class="bg_blue_simple">
					<tr><td height="32" colspan="2" align="left" style="border-bottom:1px #a8d7ea solid"><span class="blue_text" >&nbsp;Standard Input Variables</span></td></tr>
					</table>
				</td>
    	    </tr>
      	  <tr>
      	    <td height="20" align="center">&nbsp;</td>
    	    </tr>
      	  <tr>
      	    <td align="center">
      	    	<table id="mulitInputVariables" width="870" border="0" cellpadding="2" cellspacing="0" class="bg_blue_simple">
					<tr>
      	        <td height="32" colspan="2" align="left" class="blue_text" style="border-bottom:1px #a8d7ea solid">&nbsp;Multi Valued Input Variables</td>
    	        </tr>

      	      </table>
      	     </td>
    	    </tr>
      	  <tr>
      	    <td height="20" align="center">&nbsp;</td>
    	  </tr>
		  <!-- new row added -->
		  <tr id="testBed">
      	    <td align="center" valign="top"><table width="870" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #a8d7ea;">
      	      <tr>
      	        <td id="testFlip" width="862" height="32" align="left" bgcolor="#E8F3F9" class="blue_text" style="border-bottom:1px #a8d7ea solid">&nbsp;Test<div class="plus_icon"></div></td>
      	        </tr>
      	      <tr id="testPanel" style="display: none;">
      	        <td height="167" align="center" bgcolor="#E8F3F9"><textarea disabled id="txtTest" name="textarea" cols="" rows="2" class="txt_area" style="width:852px;height:150px" ></textarea>      	        </td>
      	        </tr>
    	      </table></td>
      	    </tr>
		  <!-- new row end -->
		  <tr id="testBedClr">
      	    <td height="20" align="center">&nbsp;</td>
    	  </tr>
		  
      	  <tr>
      	    <td align="center" valign="top"><table width="870" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #a8d7ea;">
      	      <tr>
      	        <td width="862" height="32" align="left" bgcolor="#E8F3F9" class="blue_text" style="border-bottom:1px #a8d7ea solid"><span class="boldText">&nbsp;Output</span></td>
      	        </tr>
      	      <tr>
      	        <td height="167" align="center" bgcolor="#E8F3F9"><textarea disabled id="txtOutput" name="textarea" cols="" rows="5" class="txt_area" style="width:852px;height:150px" ></textarea>      	        </td>
      	        </tr>
    	      </table></td>
      	    </tr>
    	  </table>
      </div>
				<div class="clear" style="height: 10px;"></div></div>
				 </center>	
			</div>
			<div style="height: 100px; width: 100%" class="cognizant_logo">
			</div>
			</div>
		</div>
	</center>
</body>

</html>
