<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<% 
	if(session.getAttribute("username")== null){
	    response.sendRedirect("../login");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="myapp">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8;IE=9;IE=10" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META Http-Equiv="Cache-Control" Content="no-cache" />
<META Http-Equiv="Pragma" Content="no-cache" />
<META Http-Equiv="Expires" Content="0" />
<title>Clone Group</title>
<link type="text/css" href="css/styles.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" href="css/stylesPopup.css" rel="stylesheet" />
<script type="text/javascript" src="lib/vendor/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="lib/vendor/jquery-ui-1.8.24.custom.min.js"></script>
<script type="text/javascript" src="lib/vendor/json.js"></script>
<script src="lib/vendor/angular.min.js"></script>
<script src="lib/vendor/ui-bootstrap-tpls-0.10.0.min.js"></script>
<script src="controller/cloneGroupController.js"></script>
<script src="directive/lePopup.js"></script>


<style>
.nav {
	padding-left: 0;
	margin-bottom: 0;
	list-style: none
}
</style>
<script type="text/javascript">

function isEven(value){
    if (value%2 == 0)
        return true;
    else
        return false;
}

function validate() {

	document.forms["logout"].submit();

}


$(function() {

//prevent backspace key from navigating back
var rx = /INPUT|SELECT|TEXTAREA/i;
	$(document).bind("keydown keypress", function(e){
        if( e.which == 8 ){ // 8 == backspace
            if(!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly ){
                e.preventDefault();
            }
        }
    });
	
	
	grid = $('#tblRulesBody');

	// handle search fields key up event
	$('#search-term').keyup(function(e) { 
		text = $(this).val(); // grab search term

		if(text.length > 1) {
			grid.find('tr:has(td)').hide(); // hide data rows, leave header row showing

			// iterate through all grid rows
			grid.find('tr').each(function(i) {
				// check to see if search term matches Name column
				if($(this).find('td:nth-child(2)').text().toUpperCase().match(text.toUpperCase()))
					{
					$(this).show();
				 
					}
					 // show matching row
			});
		}
		else 
			grid.find('tr').show(); // if no matching name is found, show all rows
	});
});
	

	jQuery(function ($) {
	
	
		
		$('#cancel').click(function () {
        	
			window.location="<%=request.getContextPath()%>/jsp/AddGroup.jsp";
        });

		//Check the clone name is similar to parent name or not.
		/* $("#txtGroupCopyName").blur(function() {
			if($.trim($('#txtGroupCopyName').val().toUpperCase()) == groupName.toUpperCase()){
				alert("Plaese give another name");
				$('#txtGroupCopyName').val("");
				$('#txtGroupCopyName').focus();
				}
			}); */


        
	//////////////////////////////////////////////////////////	
		
		
		$(".delete").click(function(e) {
			e.preventDefault();
		    var targetUrl = $(this).attr("href");
		    var c =confirm("Are you sure you want to delete?");
		    if(c){
		    	$.ajax({
	    			type: "POST",
	    		    url: targetUrl,
					dataType : "json",
					data : { ruleTypeId : ruleTypeId, ruleSetTypeId : ruleSetTypeId},
					async : false,
					cache : false,												
					success : function(data) {
						 location.reload(); 
			    }
		    	});
		    }
		});				
	});
	


</script>



</head>

<body >
<div class="blue_bg_header">
<div class="top_header_components clearfix">
	<div class="">
		<div class="icons" style="width:260px;">
		<img src="images/icon1.png" style="vertical-align:bottom;float:left"></img>
		<span class="greeting_header" style = "color: white;float:left">Welcome</span> <span class="login_name_txt"> <%=session.getAttribute("username")%>
		</span>
		</div>
		<div class="icons1" >
			<img src="images/help_icon.png" class="vt_align"></img>
			<a href="javascript:void(0)" title="Help" class="helpLink greeting_header" style= "color : white;">Help</a>
		</div>
		
		<form:form theme="simple" action="../logout" name="logout" method="post"
			id="logout" style="float:left;">
			
			<div class="icons2" ><button class="action_btn"><a class="greeting_header" title="Logout" style= "color : white;" onclick="validate();">Logout</a></button></div>
		</form:form>
	</div>
</div>
</div>
	<center>
		<div class="container" id="ng-app" ng-app ng-controller="CloneGroupController">
			
			<div class="header">
				<div class="headerLeftRule"></div>
				
				<div class="workbench_img_bg">	
					<img src="images/rule_work-bench.png" width="175px" height="35px"/>				
				</div>
			</div>
			<div class="clear" style="height: 5px;"></div>
			<div class="centertop">
				<div class="menucontainer">
					<ul class="nav">
						<li class="menu_left"></li>
						<li class="menu_center"><a
							href="<%=request.getContextPath()%>/jsp/listBusinessRule.jsp?ruleTypeId=1&ruleSetTypeId=1">Rules
								</a>
						</li>
						<li class="menu_right"></li>
						<li class="menu_leftSel"></li>
						<li class="menu_centerSel"><a
							href="<%=request.getContextPath()%>/jsp/AddGroup.jsp?ruleTypeId=1&ruleSetTypeId=1">Groups</a>
						</li>
						<li class="menu_rightSel"></li>
					</ul>
				</div>
				
<div style="border: #a8d7ea 1px solid; width: 940px;overflow:hidden">
					<div class="action_btn_cont" style="	padding-top: 5px;">
					
						<table width="145" border="0" align="right" cellpadding="0"
							cellspacing="0">
							<tr>
														
								<td width="70" align="center"><button class= "action_btn" width="66" height="30"  id="cancel"/>Cancel
								</td>
								<td width="70" align="center"><button width="79" height="30" id="Submit" class="SaveGroupCopy action_btn" ng-click="saveGroupCopy()">Clone</td>
							</tr>
						</table>
											
						</div>
				
				<div class="clear"></div>
				
				
			<div class="add_new_1" style="height:35px;font-size:14px;">
										<span class="rule_set_title pdg_left_title">CLONE FROM :</span>
										<span id="groupLabel" name="groupLabel" class="title_group_lbl"></span>
										<!-- <input type="text" id="txtGroupName" class="txt_box" style="float:left;" size="45" maxlength="50" /> -->
										<span class="rule_set_title pdg_right_title">CLONE TO :</span>
										<input type="text" id="txtGroupCopyName" class="txt_box"  style="float:left;" size="35" maxlength="20" ng-keypress="alpha($event)" />
										<div class="search_container" style="margin-top:10px;">
										<input type="text" class="searh_box" id="search-term" style="vertical-align: top;margin-left: 30px;"/>
										<img src="images/searh_icon.png" width="32" height="30">
										</div>
									</div>
	  <div class="clear" style="height: 0px;"></div>
	  
		<div id="listRuleTbl" class="fixed_header_tbl_wrapper clone_tbl">
			<div class="fix_head_wrapper">
				<table class="fix_head" id="ruleTable" width="100%" border="0" cellspacing="1"
					cellpadding="4">
					<thead>
						<tr class="tbl_head_bg">
							<th width="80px" height="30" align="center" >Select
								</th>
							<th width="285px" height="30" align="center" >Rule
								Name</th>
							<th width="140px" height="30" align="center" >Type</th>
							<th width="200px" height="30" align="center" >Created
								Date</th>
							<th width="200px" height="30" align="center" >Updated
								Date</th>
							<!-- <th width="22%" height="30" align="center" class="tableRowbold_list">&nbsp;</th> -->
						</tr>
					</thead>
				</table>
			</div>
			<div class="main_table_wrapper">
				<table class="cust_tbl" cellpadding="5px" border="0" width="100%">
					<tbody id="tblRulesBody">
					<tr ng-repeat = "data in listRulesByGroup">
							<td height="30" width="80px" align="left" class="pdg_left"' + bgColor+'><input type="checkbox" id="{{data.ruleID}}" value="{{data.ruleID}}" checked=true disabled/> </td>
							<td height="30" class="align_left" align="left" width="237px" ng-bind="data.ruleName"></td>
							<td height="30" width="96px" align="center" ng-show="data.ruleTypeID == 1" ng-bind="'Rule'"></td>
							<td height="30" width="96px" align="center" ng-show="data.ruleTypeID == 3" ng-bind="'RuleSet'"></td>
							<td height="30" width="145px"align="center" ng-bind-template="{{data.createdDate}} {{data.createdTime}}"></td>
							
							<td height="30" width="145px"align="center" ng-show="data.updatedDate == null" ng-bind-template="{{data.createdDate}} {{data.createdTime}}"></td>
							<td height="30" width="145px"align="center" ng-hide="data.updatedDate == null" ng-bind-template="{{data.updatedDate}} {{data.updatedTime}}"></td>
					</tr>
					</tbody>

				</table>
			</div>	
		</div>

			
</div>
<lepopup control="lePopupCtrl"/>
			</div>
			<div style="height: 75px; width: 100%" class="cognizant_logo">
			</div>
			<div id="page_loader"></div>
		</div>
	</center>
	<script type="text/javascript" src="lib/vendor/le/scripts.js"></script>
</body>

</html>
