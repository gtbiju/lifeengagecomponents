/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 10, 2014
 */
package com.cognizant.le.rwb.exception;

/**
 * The Class class ServiceException.
 */
public class ServiceException extends RuntimeException{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 704226031141440104L;

	/** The code. */
	private String code;
	
	/** The description. */
	private String description;

	/**
	 * Instantiates a new service exception.
	 *
	 * @param code the code
	 * @param description the description
	 */
	public ServiceException(String code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the code to set.
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
