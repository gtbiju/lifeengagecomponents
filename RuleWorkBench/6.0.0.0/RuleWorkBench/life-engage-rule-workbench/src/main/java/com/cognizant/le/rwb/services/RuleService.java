/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 7, 2014
 */

package com.cognizant.le.rwb.services;

import java.util.List;
import java.util.Map;

import com.cognizant.le.rwb.domain.Group;
import com.cognizant.le.rwb.domain.GroupType;
import com.cognizant.le.rwb.domain.Rule;

// TODO: Auto-generated Javadoc
/**
 * The Interface interface RuleService.
 * 
 * @author 304003
 */
public interface RuleService {

    /**
     * Creates the rule.
     * 
     * @param Rule
     *            the rule
     * @param groupID
     *            the group id
     * @return the string
     */
    public Map<String,String> createRule(Rule Rule, Integer groupID);

    /**
     * Edits the rule.
     *
     * @param Rule the rule
     * @param groupID the group id
     * @param isRuleNameChanged the is rule name changed
     * @return the string
     */
    public Map<String,String> editRule(Rule Rule, Integer groupID, boolean isDupChkRequired);

    /**
     * List rules.
     * 
     * @param ruleTypeIds
     *            the rule type ids
     * @return the list
     */
    public List<Rule> listRules(List<Integer> ruleTypeIds);

    /**
     * List rules for rule set.
     * 
     * @param ruleTypeId
     *            the rule type id
     * @param groupId
     *            the group id
     * @return the list
     */
    public List<Rule> listRulesForRuleSet(Integer ruleTypeId, Integer groupId);

    /**
     * List rule.
     * 
     * @param ruleId
     *            the rule id
     * @return the rule
     */
    public Rule listRule(Integer ruleId);

    /**
     * Download rule js.
     * 
     * @param ruleId
     *            the rule id
     * @return the rule
     */
    public Rule downloadRuleJS(Integer ruleId);

    /**
     * Delete rule.
     * 
     * @param ruleId
     *            the rule id
     */
    public void deleteRule(Integer ruleId);

    /**
     * Gets the rule type.
     * 
     * @param ruleId
     *            the rule id
     * @return the rule type
     */
    public Integer getRuleType(Integer ruleId);

    /**
     * Adds the group name.
     * 
     * @param Group
     *            the group
     * @return the string
     */
    public boolean addGroupName(Group Group);

    /**
     * List group table.
     * 
     * @return the list
     */
    public List<Group> listGroupTable();

    /**
     * Gets the group id.
     * 
     * @param groupName
     *            the group name
     * @return the group id
     */
    public Integer getGroupId(String groupName);

    /**
     * Gets the group id.
     * 
     * @param ruleName
     *            the rule name
     * @return the group id
     */
    public String getGroupID(Integer ruleName);

    /**
     * Update group name.
     *
     * @param group the group
     * @param isGrpNameChanged the is grp name changed
     * @return the string
     */
    public boolean updateGroupName(Group group, boolean isGrpNameChanged);

    /**
     * Delete group.
     * 
     * @param groupID
     *            the group id
     */
    public void deleteGroup(Integer groupID);

    /**
     * List group download rules.
     * 
     * @param groupID
     *            the group id
     * @return the list
     */
    public List<Rule> listGroupDownloadRules(Integer groupID);

    /**
     * Download group rules.
     * 
     * @param idList
     *            the id list
     * @param isOnline
     *            the is online
     * @return the string builder
     */
    public StringBuilder downloadGroupRules(List<Integer> idList, boolean isOnline);

    /**
     * List rule by group.
     * 
     * @param groupID
     *            the group id
     * @return the list
     */
    public List<Rule> listRuleByGroup(Integer groupID);

    /**
     * Clone group.
     * 
     * @param ruleBOList
     *            the rule bo list
     * @param Group
     *            the group
     * @return the string
     */
    public String cloneGroup(List<Rule> ruleBOList, Group Group);

    /**
     * Merge group.
     * 
     * @param ruleBOList
     *            the rule bo list
     * @param groupId
     *            the group id
     */
    public void mergeGroup(List<Rule> ruleBOList, Integer groupId);

    /**
     * List from group type table.
     * 
     * @return the list
     */
    public List<GroupType> listFromGroupTypeTable();

    /**
     * Gets the group type.
     * 
     * @param groupCloneName
     *            the group clone name
     * @return the group type
     */
    public Integer getGroupType(String groupCloneName);

    /**
     * Table attribute loader.
     * 
     * @return the list
     */
    public List<Map<String, Object>> tableAttributeLoader();
    
    /**
     * Gets the rule existing rulesets.
     *
     * @param ruleId the rule id
     * @return the rule existing rulesets
     */
    public List<Rule> getRuleExistingRulesets(Integer ruleId);
    
    /**
     * Check for rule and rule set count in group.
     *
     * @param groupID the group id
     * @return the list
     */
    List<Rule> checkForRuleAndRuleSetCountInGroup(Integer groupID);
    
    /**
     * Gets the download rules desc.
     *
     * @param downloadRuleList the download rule list
     * @param groupName the group name
     * @param isOnline the is online
     * @return the download rules desc
     */
    StringBuilder getDownloadRulesDesc(List<String> downloadRuleList, String groupName, boolean isOnline);

    /**
     * Gets the rules by group.
     *
     * @param groupId the group id
     * @return the rules by group
     */
    List<String> getRulesByGroup(Integer groupId);

}
