/**
 * 
 */
package com.cognizant.le.rwb.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.domain.Group;
import com.cognizant.le.rwb.domain.GroupType;
import com.cognizant.le.rwb.domain.Rule;
import com.cognizant.le.rwb.util.Utilities;

/**
 * The Class class RuleDaoImpl.
 * 
 * @author 304003
 */
@Repository("ruleDao")
public class RuleDaoImpl implements RuleDao {

    /** The jdbc template. */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /** The environment. */
    @Autowired
    private Environment environment;

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RuleDaoImpl.class);

    /**
     * Creates the business rule.
     * 
     * @param rule
     *            the rule bo
     * @param groupID
     *            the group id
     * @return the string
     */
    public final Map<String, String> createRule(final Rule rule, Integer groupID) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> createBusinessRule");
        Map<String, String> resultMap = new HashMap<String, String>();
        boolean isRuleExists = false;
        String result = "";
        if (!isRuleExists(rule,groupID,rule.getRuleID())) {
            final Object[] values =
                    { rule.getRuleName(), rule.getRuleTypeID(), new Timestamp(Utilities.getCurrentDate().getTime()), GenericConstants.VERSION,
                            rule.getRuleJson(), rule.getRuleJS(), rule.getOfflineRuleJS(), groupID, rule.getClientId(), rule.getComments(),GenericConstants.STATUS_ACTIVE };
            jdbcTemplate.update(environment.getProperty(GenericConstants.INSERT_RULE), values);

            jdbcTemplate.query(environment.getProperty(GenericConstants.RULE_EXISTS),
                    new Object[] { rule.getRuleName(), groupID }, new RowMapper<Rule>() {

                        @Override
                        public Rule mapRow(ResultSet rs, int rowNum) throws SQLException {
                            rule.setRuleID(rs.getInt("ID"));
                            return rule;
                        }

                    });
            result = "Rule saved successfully";
        } else {
            isRuleExists = true;
            result = "Rule already exists";
        }
        resultMap.put("createRuleResult", result);
        resultMap.put("isRuleExists", String.valueOf(isRuleExists));
        LOGGER.trace("Method Exit : RuleDaoImpl-> createBusinessRule");
        return resultMap;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#editRule(com.cognizant.le.rwb.domain.Rule, java.lang.Integer)
     */
    public final void editRule(final Rule rule, Integer groupID) {
    	 LOGGER.trace("Method Entry : RuleDaoImpl-> createBusinessRule");         
         jdbcTemplate.update(environment.getProperty(GenericConstants.UPDATE_RULE), new Object[] { rule.getRuleJson(),
                 rule.getRuleJS(), rule.getOfflineRuleJS(), new Timestamp(Utilities.getCurrentDate().getTime()), rule.getRuleName(), groupID, rule.getComments(),
                 rule.getRuleID() });
    }

    /**
     * List rules.
     * 
     * @param ruleTypeIds
     *            the rule type ids
     * @return the list
     */
    public final List<Rule> listRules(final List<Integer> ruleTypeIds) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> listRules");
        final List<Rule> ruleList =
                jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_RULE_SEL), new RowMapper<Rule>() {

                    @Override
                    public Rule mapRow(ResultSet rs, int rowNo) throws SQLException {
                        final Rule rule = new Rule();
                        rule.setRuleID(rs.getInt("ID"));
                        rule.setRuleTypeID(rs.getInt("TYPE_ID"));
                        rule.setRuleName(rs.getString("RULENAME"));
                        rule.setCreatedDate(rs.getDate("CREATED_DATE"));
                        rule.setCreatedTime(rs.getTime("CREATED_DATE"));
                        rule.setUpdatedDate(rs.getDate("UPDATED_DATE"));
                        rule.setUpdatedTime(rs.getTime("UPDATED_DATE"));
                        rule.setVersion(rs.getFloat("VERSION"));
                        rule.setGroupName(rs.getString("GROUP_NAME"));
                        rule.setComments(rs.getString("COMMENTS"));
                        return rule;
                    }

                });
        LOGGER.trace("Method Exit : RuleDaoImpl-> listRules");
        return ruleList;

    }

    /**
     * List rules in rule set page action.
     * 
     * @param ruleTypeId
     *            the rule type id
     * @param groupId
     *            the group id
     * @return the list
     */

    public final List<Rule> listRulesRuleset(final Integer ruleTypeId, final Integer groupId) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> listRules");

        final List<Rule> ruleList =
                jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_RULES_RULESET), new Object[] {
                        ruleTypeId, groupId }, new RowMapper<Rule>() {

                    @Override
                    public Rule mapRow(ResultSet rs, int rowNo) throws SQLException {
                        final Rule rule = new Rule();
                        rule.setRuleID(rs.getInt("ID"));
                        rule.setRuleTypeID(rs.getInt("TYPE_ID"));
                        rule.setRuleName(rs.getString("RULENAME"));
                        rule.setCreatedDate(rs.getDate("CREATED_DATE"));
                        rule.setCreatedTime(rs.getTime("CREATED_DATE"));
                        rule.setUpdatedDate(rs.getDate("UPDATED_DATE"));
                        rule.setUpdatedTime(rs.getTime("UPDATED_DATE"));
                        rule.setVersion(rs.getFloat("VERSION"));
                        rule.setGroupName(rs.getString("CATEGORY"));
                        return rule;
                    }

                });
        LOGGER.trace("Method Exit : RuleDaoImpl-> listRules");
        return ruleList;

    }

    /**
     * Check if the rule name exists.
     *
     * @param rule the rule
     * @param groupID the group id
     * @return boolean
     */
    public final boolean isRuleExists(final Rule rule, final Integer groupID,final Integer ruleId) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> isRuleExists");
        boolean isExists = false;
        List<Integer> ruleIdList = null;
        if (ruleId != null) {
           ruleIdList =
                    jdbcTemplate.query(environment.getProperty(GenericConstants.RULE_EXISTS_CHK_FOR_EDIT),
                            new Object[] { rule.getRuleName(), groupID, ruleId }, new RowMapper<Integer>() {

                                @Override
                                public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                                    return rs.getInt("ID");
                                }

                            });
        } else {
           ruleIdList =
                    jdbcTemplate.query(environment.getProperty(GenericConstants.RULE_EXISTS),
                            new Object[] { rule.getRuleName(), groupID }, new RowMapper<Integer>() {

                                @Override
                                public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                                    return rs.getInt("ID");
                                }

                            });
        }
        
       
        if (ruleIdList.size() > 0) {
            isExists = true;
            //rule.setRuleID(ruleIdList.get(0));
        }
        LOGGER.trace("Method Exit : RuleDaoImpl-> isRuleExists");
        return isExists;

    }

    /**
     * Retrieves the rule.
     * 
     * @param ruleId
     *            the rule id
     * @return the rule bo
     */
    public final Rule listRule(final Integer ruleId) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> listRule");
        Rule rule =
                jdbcTemplate.queryForObject(environment.getProperty(GenericConstants.SELECT_RULE_EDIT),
                        new Object[] { ruleId }, new RowMapper<Rule>() {
                            @Override
                            public Rule mapRow(ResultSet rs, int rowNum) throws SQLException {
                                final Rule rule = new Rule();
                                rule.setRuleID(rs.getInt("ID"));
                                rule.setRuleTypeID(rs.getInt("TYPE_ID"));
                                rule.setRuleName(rs.getString("RULENAME"));
                                rule.setCreatedDate(rs.getDate("CREATED_DATE"));
                                rule.setVersion(rs.getFloat("VERSION"));
                                rule.setRuleJson(rs.getString("RULEJSON"));
                                rule.setRuleJS(rs.getString("RULEDESC"));
                                rule.setGroupName(rs.getString("GROUP_NAME"));
                                rule.setComments(rs.getString("COMMENTS"));
                                return rule;
                            }
                        });
        LOGGER.trace("Method Exit : RuleDaoImpl-> listRule");
        return rule;

    }

    /**
     * Retrieves the ruleJS.
     * 
     * @param ruleId
     *            the rule id
     * @return the rule JS
     */
    public final Rule downloadRuleJS(final Integer ruleId) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> downloadRuleJS");
        Rule rule =
                jdbcTemplate.queryForObject(environment.getProperty(GenericConstants.SELECT_RULEJS),
                        new Object[] { ruleId }, new RowMapper<Rule>() {
                            @Override
                            public Rule mapRow(ResultSet rs, int rowNum) throws SQLException {
                                final Rule rule = new Rule();
                                rule.setRuleName(rs.getString("RULENAME"));
                                rule.setRuleJS(rs.getString("RULEDESC"));
                                rule.setOfflineRuleJS(rs.getString("OFFLINE_RULEDESC"));
                                return rule;
                            }
                        });

        LOGGER.trace("Method Exit : RuleDaoImpl-> downloadRuleJS");
        return rule;

    }

    /**
     * Delete the rule.
     * 
     * @param ruleId
     *            the rule id
     */
    public final void deleteRule(final Integer ruleId) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> deleteRule");
        jdbcTemplate.update(environment.getProperty(GenericConstants.DELETE_RULE), new Object[] { ruleId });
        LOGGER.trace("Method Exit : RuleDaoImpl-> deleteRule");
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.RuleDao#getRuleSetIdForGroupdelete(java.lang.String)
     */
    public final List<Integer> getRuleSetIdForGroupdelete(final String groupId){
    	LOGGER.trace("Method Entry : RuleDaoImpl-> getRuleSetIdForGroupdelete");
    	 List<Integer> ruleSetIdList =
                 jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_RULEID_BASEDON_GROUP),
                         new Object[] { groupId, GenericConstants.RULETYPE_ID}, new RowMapper<Integer>() {

                             @Override
                             public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                                 return rs.getInt("ID");
                             }

                         });
        LOGGER.trace("Method Exit : RuleDaoImpl-> getRuleSetIdForGroupdelete");
    	return ruleSetIdList;
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.RuleDao#deleteRuleBeforeGroupDelete(java.lang.String)
     */
    public final void deleteRuleBeforeGroupDelete(final String groupId) {
        LOGGER.trace("Method Entry : RuleSetDao-> editRuleSetRuleMapping");
       jdbcTemplate.update(environment.getProperty(GenericConstants.DELETE_RULES_IN_GROUP), new Object[] { groupId });
        LOGGER.trace("Method Exit : RuleSetDao-> editRuleSetRuleMapping");
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.RuleDao#deleteRuleSetRuleMappingBeforeGroupDelete(java.util.List)
     */
    public final void deleteRuleSetRuleMappingBeforeGroupDelete(final List<Integer> ruleSetIdList) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> deleteRuleBeforeGroupDelete");
        int updateCount[] =
                jdbcTemplate.batchUpdate(environment.getProperty(GenericConstants.DELETE_RULESET_IN_GROUP),
                        new BatchPreparedStatementSetter() {

                            @Override
                            public void setValues(PreparedStatement ps, int index) throws SQLException {
                            	Integer ruleSetId = ruleSetIdList.get(index);
                                ps.setInt(1, ruleSetId);
                            }

                            @Override
                            public int getBatchSize() {
                                return ruleSetIdList.size();
                            }
                        });
        if (updateCount.length != ruleSetIdList.size()) {
        	
            LOGGER.error("Rule Delete in a group failed");
        } else {
        	System.out.println("updateCount"+updateCount);
        }
        LOGGER.trace("Method Exit : RuleDaoImpl-> deleteRuleBeforeGroupDelete");
    }

	/* (non-Javadoc)
	 * @see com.cognizant.le.rwb.repository.RuleDao#checkForExistingRuleAndRuleSetCountInGroup(java.lang.Integer)
	 */
	public List<Rule> checkForRuleAndRuleSetCountInGroup(Integer groupID) {
		final List<Rule> ruleList =
                jdbcTemplate.query(environment.getProperty(GenericConstants.GET_RULE_AND_RULESET_COUNT_IN_GROUP), new Object[] { groupID }, new RowMapper<Rule>() {

                    @Override
                    public Rule mapRow(ResultSet rs, int rowNo) throws SQLException {
                        final Rule rule = new Rule();
                        rule.setRuleID(rs.getInt("ID"));
                        rule.setRuleTypeID(rs.getInt("TYPE_ID"));
                        return rule;
                    }

                });
		return 	ruleList;
	}
	
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#getRuleType(java.lang.Integer)
     */
    public Integer getRuleType(Integer ruleId) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> getRuleType");
        Integer ruleType =
                jdbcTemplate.queryForObject(environment.getProperty(GenericConstants.GET_RULE_TYPE),
                        new Object[] { ruleId }, new RowMapper<Integer>() {
                            @Override
                            public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                                Integer ruleType = rs.getInt("TYPE_ID");
                                return ruleType;
                            }
                        });

        LOGGER.trace("Method Exit : RuleDaoImpl-> getRuleType");
        return ruleType;

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#addGroupName(com.cognizant.le.rwb.domain.Group)
     */
    public boolean addGroupName(Group group) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> addGroupName");
        boolean isGroupExists;
        boolean result;
        isGroupExists = isGroupExists(group.getGroupName());
        if (!isGroupExists) {
            createGroup(group);
            result = true;

        } else {
            result = false;
        }

        LOGGER.trace("Method Exit : RuleDaoImpl-> addGroupName");
        return result;

    }

    /**
     * Checks if is group exists.
     *
     * @param groupName the group name
     * @return true, if is group exists
     */
    public boolean isGroupExists(String groupName) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> isGroupExists");
        boolean isExists = false;
        List<Map<String, Object>> list =
                jdbcTemplate.queryForList(environment.getProperty(GenericConstants.GROUP_EXISTS),
                        new Object[] { groupName });
        if (list.size() > 0) {
            isExists = true;
        }
        LOGGER.trace("Method Exit : RuleDaoImpl-> isGroupExists");
        return isExists;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#listGroupTable()
     */
    public List<Group> listGroupTable() {
        LOGGER.trace("Method Entry : RuleDaoImpl-> listGroupTable");
        final List<Group> groupList =
                jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_GROUP_LIST), new RowMapper<Group>() {

                    @Override
                    public Group mapRow(ResultSet rs, int rowNum) throws SQLException {
                        final Group group = new Group();
                        group.setGroupID(rs.getInt("ID"));
                        group.setGroupName(rs.getString("GROUP_NAME"));
                        group.setComments(rs.getString("COMMENTS"));
                        group.setTypeID(rs.getInt("TYPE_ID"));
                        group.setGroupType(rs.getString("GROUP_TYPE"));
                        group.setCanonicalJsonFileName(rs.getString("CANONICAL_JSON_FILENAME"));
                        return group;
                    }

                });

        LOGGER.trace("Method Exit : RuleDaoImpl-> listRules");
        return groupList;

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#getGroupId(java.lang.String)
     */
    public Integer getGroupId(String groupName) {

        LOGGER.trace("Method Entry : RuleDaoImpl-> getGroupId");
        List<Integer> groupIdList =
                jdbcTemplate.queryForList(environment.getProperty(GenericConstants.GET_GROUP_ID),
                        new Object[] { groupName }, Integer.class);
        Integer groupId = null;
        if (groupIdList.size() > 0) {
            groupId = groupIdList.get(0).intValue();
        }
        LOGGER.trace("Method Exit : RuleDaoImpl-> getGroupId");
        return groupId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#getGroupID(java.lang.String)
     */
    public String getGroupID(Integer ruleID) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> getGroupName");
        String groupID =
                jdbcTemplate.queryForObject(environment.getProperty(GenericConstants.GET_GROUP_NAME),
                        new Object[] { ruleID }, String.class);
        LOGGER.trace("Method Exit : RuleDaoImpl-> getGroupName");
        return groupID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#updateGroupName(com.cognizant.le.rwb.domain.Group, java.lang.String)
     */
    public void updateGroupName(Group group) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> updateGroupName");       
	        jdbcTemplate.update(environment.getProperty(GenericConstants.UPDATE_GROUP),
	                new Object[] { group.getGroupName(),group.getTypeID(),group.getComments(), group.getGroupID()});
        LOGGER.trace("Method Exit : RuleDaoImpl-> updateGroupName");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#deleteGroup(java.lang.Integer)
     */
    public void deleteGroup(Integer groupID) {

        LOGGER.trace("Method Entry : RuleDaoImpl-> deleteGroup");
        jdbcTemplate.update(environment.getProperty(GenericConstants.DELETE_GROUP), new Object[] { groupID });
        LOGGER.trace("Method Exit : RuleDaoImpl-> deleteGroup");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#listGroupDownloadRules(java.lang.Integer)
     */
    public List<Rule> listGroupDownloadRules(Integer groupID) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> listGroupDownloadRules");
        final List<Rule> ruleList =
                jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_RULES_BY_GROUPID),
                        new Object[] { groupID }, new RowMapper<Rule>() {
                            @Override
                            public Rule mapRow(ResultSet rs, int rowNum) throws SQLException {
                                final Rule rule = new Rule();
                                rule.setRuleID(rs.getInt("ID"));
                                rule.setRuleTypeID(rs.getInt("TYPE_ID"));
                                rule.setRuleName(rs.getString("RULENAME"));
                                rule.setCreatedDate(rs.getDate("CREATED_DATE"));
                                rule.setCreatedTime(rs.getTime("CREATED_DATE"));
                                rule.setUpdatedDate(rs.getDate("UPDATED_DATE"));
                                rule.setUpdatedTime(rs.getTime("UPDATED_DATE"));
                                rule.setVersion(rs.getFloat("VERSION"));
                                rule.setRuleJson(rs.getString("RULEJSON"));
                                rule.setRuleJS(rs.getString("RULEDESC"));
                                rule.setGroupName(rs.getString("CATEGORY"));
                                return rule;
                            }

                        });
        LOGGER.trace("Method Exit : RuleDaoImpl-> listGroupDownloadRules");
        return ruleList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#downloadGroupRules(java.util.List, boolean)
     */
    public StringBuilder downloadGroupRules(List<Integer> idList, boolean isOnline) {

        LOGGER.trace("Method Entry : RuleDaoImpl-> downloadGroupRules");
        StringBuilder ruleDesc = new StringBuilder();
        StringBuilder sql = new StringBuilder();
        String ruleDescColumn;
        if (isOnline) {
            ruleDescColumn = "RULEDESC";
        } else {
            ruleDescColumn = "OFFLINE_RULEDESC";
        }
        sql.append("select " + ruleDescColumn + " from RULE_TABLE where ID in(");
        for (int i = 0; i < idList.size(); i++) {
            sql.append("?");
            if (i + 1 < idList.size()) {
                sql.append(",");
            }
        }
        sql.append(" AND STATUS='A')");
        LOGGER.trace("downloadGroupRules ---> Constructed SQL --->" + sql.toString());

        Object[] params = new Object[idList.size()];
        for (int i = 0; i < params.length; i++) {
            params[i] = idList.get(i).intValue();
        }
        List<String> ruleDescList = jdbcTemplate.queryForList(sql.toString(), params, String.class);

        for (String ruleDescValue : ruleDescList) {
            ruleDesc.append(ruleDescValue);
        }
        LOGGER.trace("Method Exit : RuleDaoImpl-> downloadGroupRules");
        return ruleDesc;

    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.RuleDao#getDownloadRulesDesc(java.util.List, java.lang.String, boolean)
     */
    public StringBuilder getDownloadRulesDesc(List<String> downloadRuleList, String groupName, boolean isOnline) {

        LOGGER.trace("Method Entry : RuleDaoImpl-> downloadGroupRules");
        StringBuilder ruleDesc = new StringBuilder();
        StringBuilder sql = new StringBuilder();
        String ruleDescColumn;
        if (isOnline) {
            ruleDescColumn = "RULEDESC";
        } else {
            ruleDescColumn = "OFFLINE_RULEDESC";
        }
        sql.append("select " + ruleDescColumn + " from RULE_TABLE where RULENAME in(");
        for (int i = 0; i < downloadRuleList.size(); i++) {
            sql.append("?");
            if (i + 1 < downloadRuleList.size()) {
                sql.append(",");
            }
        }
        sql.append(") AND CATEGORY=? AND STATUS='A'");
        LOGGER.trace("downloadGroupRules ---> Constructed SQL --->" + sql.toString());

        Object[] params = new Object[downloadRuleList.size()+1];
        /*for (int i = 0; i < params.length; i++) {
            params[i] = downloadRuleList.iterator();
        }*/
        int i  = 0;
        for(String ruleName : downloadRuleList){
            params[i] = ruleName;
            i++;
        }
        params[downloadRuleList.size()] = groupName;
        List<String> ruleDescList = jdbcTemplate.queryForList(sql.toString(), params, String.class);

        for (String ruleDescValue : ruleDescList) {
            ruleDesc.append(ruleDescValue);
        }
        LOGGER.trace("Method Exit : RuleDaoImpl-> downloadGroupRules");
        return ruleDesc;

    }

    
    

    // vinitha
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#listRuleByGroup(java.lang.Integer)
     */
    public List<Rule> listRuleByGroup(Integer groupID) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> listRuleByGroup");
        final List<Rule> ruleList =
                jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_RULES_BY_GROUPID),
                        new Object[] { groupID }, new RowMapper<Rule>() {
                            @Override
                            public Rule mapRow(ResultSet rs, int rowNum) throws SQLException {
                                final Rule rule = new Rule();
                                rule.setRuleID(rs.getInt("ID"));
                                rule.setRuleTypeID(rs.getInt("TYPE_ID"));
                                rule.setRuleName(rs.getString("RULENAME"));
                                rule.setCreatedDate(rs.getDate("CREATED_DATE"));
                                rule.setCreatedTime(rs.getTime("CREATED_DATE"));
                                rule.setUpdatedDate(rs.getDate("UPDATED_DATE"));
                                rule.setUpdatedTime(rs.getTime("UPDATED_DATE"));
                                rule.setVersion(rs.getFloat("VERSION"));
                                rule.setGroupName(rs.getString("CATEGORY"));
                                rule.setClientId(rs.getString("CLIENTID"));
                                return rule;
                            }

                        });

        LOGGER.trace("Method Exit : RuleDaoImpl-> listRuleByGroup");
        return ruleList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#cloneGroup(java.util.List, java.lang.Integer)
     */
    public final String cloneGroup(final List<Rule> ruleList, final Integer groupId) throws Exception {
        String result = null;
        int ruleSetId = 0;
        int ruleId = 0;

        List<Map<String, Object>> selectedRulesWithMappingList =
                jdbcTemplate.queryForList(environment.getProperty(GenericConstants.SELECT_RULE_WITH_MAPPING),
                        new Object[] { ruleList.get(0).getGroupName() });

        ArrayList<String> names = new ArrayList<String>();
        for (Map<String, Object> row : selectedRulesWithMappingList) {

            if (!names.contains(row.get("RULENAME").toString())) {
                if (Integer.valueOf(row.get("TYPE_ID").toString()) != 3) {// NOT RULE SET
                    names.add(row.get("RULENAME").toString());
                }
                KeyHolder holder = new GeneratedKeyHolder();
                final Object[] values =
                        { row.get("RULENAME").toString(), Integer.valueOf(row.get("TYPE_ID").toString()),
                        new Timestamp(Utilities.getCurrentDate().getTime()), GenericConstants.VERSION, row.get("RULEJSON").toString(),
                                row.get("RULEDESC").toString(), (row.get("OFFLINE_RULEDESC") != null ? row.get("OFFLINE_RULEDESC").toString() : null),
                                String.valueOf(groupId), row.get("CLIENTID"), (row.get("COMMENTS") != null ? row.get("COMMENTS").toString() : null),GenericConstants.STATUS_ACTIVE};
                jdbcTemplate.update(new PreparedStatementCreator() {

                    @Override
                    public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                        PreparedStatement ps =
                                conn.prepareStatement(environment.getProperty(GenericConstants.INSERT_RULE),
                                        Statement.RETURN_GENERATED_KEYS);
                        for (int i = 0; i < values.length; i++) {
                            ps.setObject(i + 1, values[i]);
                        }
                        return ps;
                    }
                }, holder);

                if (Integer.valueOf(row.get("ID").toString()).equals(Integer.valueOf(row.get("RULESET_ID").toString()))) { // RULE
                                                                                                                      // SET
                    ruleSetId = holder.getKey().intValue();
                } else {
                    ruleId = holder.getKey().intValue();
                }
            }
            if (!Integer.valueOf(row.get("ID").toString()).equals(Integer.valueOf(row.get("RULESET_ID").toString()))) { // RULE
                final Object[] mappingValues = { ruleId, ruleSetId };
                jdbcTemplate.update(environment.getProperty(GenericConstants.MAPPING_TABLE_INSERT), mappingValues);
            }
        }
        final Object[] values = { String.valueOf(ruleList.get(0).getGroupName()) };
        List<Rule> rulesList =
                jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_REST_RULES), values,
                        new RowMapper<Rule>() {

                            @Override
                            public Rule mapRow(ResultSet rs, int rowNum) throws SQLException {
                                final Rule rule = new Rule();
                                rule.setRuleID(rs.getInt("ID"));
                                rule.setRuleTypeID(rs.getInt("TYPE_ID"));
                                rule.setRuleName(rs.getString("RULENAME"));
                                rule.setClientId(rs.getString("CLIENTID"));;
                                rule.setRuleJson(rs.getString("RULEJSON"));
                                rule.setOfflineRuleJS(rs.getString("OFFLINE_RULEDESC"));
                                rule.setRuleJS(rs.getString("RULEDESC"));
                                rule.setGroupName(rs.getString("CATEGORY"));
                                rule.setComments(rs.getString("COMMENTS"));
                                return rule;
                            }

                        });
        for (Rule rule : rulesList) {
            final Object[] ruleValues =
                    { rule.getRuleName(), rule.getRuleTypeID(), new Timestamp(Utilities.getCurrentDate().getTime()), GenericConstants.VERSION,
                            rule.getRuleJson(), rule.getRuleJS(), rule.getOfflineRuleJS(), String.valueOf(groupId),
                            rule.getClientId(),rule.getComments(),GenericConstants.STATUS_ACTIVE };
            jdbcTemplate.update(environment.getProperty(GenericConstants.INSERT_RULE), ruleValues);
            result = "Clone group Successfully";
        }

        return result;

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#createGroup(com.cognizant.le.rwb.domain.Group)
     */
    public final Integer createGroup(final Group group) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> createBusinessRule");
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                PreparedStatement ps =
                        conn.prepareStatement(environment.getProperty(GenericConstants.INSERT_GROUP),
                                Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, group.getGroupName());
                ps.setInt(2, group.getTypeID());
                ps.setString(3, group.getComments());
                return ps;
            }
        }, holder);

        Integer groupId = holder.getKey().intValue();
        LOGGER.trace("Method Exit : RuleDaoImpl-> createGroup");
        return groupId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#listFromGroupTypeTable()
     */
    public List<GroupType> listFromGroupTypeTable() {
        LOGGER.trace("Method Entry : RuleDaoImpl-> listFromGroupTypeTable");
        final List<GroupType> groupTypeList =
                jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_GROUP_TYPE_LIST),
                        new RowMapper<GroupType>() {

                            @Override
                            public GroupType mapRow(ResultSet rs, int rowNum) throws SQLException {
                                final GroupType groupType = new GroupType();
                                groupType.setGroupTypeID(rs.getInt("ID"));
                                groupType.setGroupType(rs.getString("GROUP_TYPE"));
                                return groupType;
                            }

                        });
        LOGGER.trace("Method Exit : RuleDaoImpl-> listFromGroupTypeTable");
        return groupTypeList;
    }

    // Vinitha_TypeCR
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#getGroupType(java.lang.String)
     */
    public final Integer getGroupType(final String groupName) {
        LOGGER.trace("Method Entry : RuleDaoImpl-> getGroupType");
        Integer groupTypeId =
                jdbcTemplate.queryForObject(environment.getProperty(GenericConstants.GET_GROUP_TYPE),
                        new Object[] { groupName }, Integer.class);
        LOGGER.trace("Method Exit : RuleDaoImpl-> getGroupType");

        return groupTypeId;
    }

    // TODO Validate logic
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleDao#tableAttributeLoader()
     */
    public List<Map<String, Object>> tableAttributeLoader() {
        List<Map<String, Object>> tableLoaderList = new ArrayList<Map<String, Object>>();
        List<String> tableNamesList =
                jdbcTemplate.queryForList(environment.getProperty(GenericConstants.GET_TABLE_NAMES), String.class);
        for (String tableName : tableNamesList) {
            List<Map<String, Object>> columnNameList = new ArrayList<Map<String, Object>>();
            Map<String, Object> tableNameMap = new LinkedHashMap<String, Object>();
            tableNameMap.put("tableName", tableName);
            List<String> columnNamesList =
                    jdbcTemplate.queryForList(environment.getProperty(GenericConstants.GET_COLUMN_NAMES),
                            new Object[] { tableName }, String.class);
            for (String columnName : columnNamesList) {
                Map<String, Object> columnNameMap = new LinkedHashMap<String, Object>();
                columnNameMap.put("columnName", columnName);
                columnNameList.add(columnNameMap);
            }
            tableNameMap.put("ColumnNames", columnNameList);
            tableLoaderList.add(tableNameMap);
        }
        return tableLoaderList;
    }
    
	/* (non-Javadoc)
	 * @see com.cognizant.le.rwb.repository.RuleDao#getRuleExistingRulesets(java.lang.Integer)
	 */
	public List<Rule> getRuleExistingRulesets(Integer ruleId) {
		LOGGER.trace("Method Entry : RuleDaoImpl-> getRuleExistingRulesets");
		final List<Rule> ruleList = jdbcTemplate.query(
				environment.getProperty(GenericConstants.SELECT_RULE_EXISTING_RULESET),
				new Object[] { ruleId }, new RowMapper<Rule>() {
					@Override
					public Rule mapRow(ResultSet rs, int rowNo)
							throws SQLException {
						final Rule rule = new Rule();
						rule.setRuleID(rs.getInt("ID"));
						rule.setRuleName(rs.getString("RULENAME"));
						return rule;
					}

				});		
		LOGGER.trace("Method Exit : RuleDaoImpl-> getRuleExistingRulesets");
		return ruleList;
	}
	
	/* (non-Javadoc)
	 * @see com.cognizant.le.rwb.repository.RuleDao#getrulesByGroup(java.lang.Integer)
	 */
	public List<String> getRulesByGroup(Integer groupId){
        List<String> ruleNames = jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_RULENAME), new Object[] { groupId },new RowMapper<String>() {@Override
            public String mapRow(ResultSet resultSet, int i) throws SQLException {
              return resultSet.getString(1);
            }
          });
        return ruleNames;
        
    }
}
