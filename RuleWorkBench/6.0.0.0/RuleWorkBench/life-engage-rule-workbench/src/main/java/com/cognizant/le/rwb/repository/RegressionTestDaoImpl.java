/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 418964
 * @version       : 0.1, May 30, 2014
 */
package com.cognizant.le.rwb.repository;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.domain.JobDetails;

/**
 * The Class class RegressionTestDaoImpl.
 */
@Repository("regressionTestDao")
public class RegressionTestDaoImpl implements RegressionTestDao {

	/** The jdbc template. */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** The environment. */
	@Autowired
	private Environment environment;

	/** The logger. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RegressionTestDaoImpl.class);

	/**
	 * Method used to get the data from the uploaded file.
	 * 
	 * @param file
	 *            the file
	 * @param fileName
	 *            the file name
	 * @return the boolean
	 */
	public final void getUploadedFile(MultipartFile file, String filePath) {

		LOGGER.debug("Method Entry : regressionTestDao-> getUploadedFile");
		if (!file.isEmpty()) {
			byte[] bytes;
			try {
				// Getting data from uploaded file and setting it to a new file
				bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(filePath)));
				stream.write(bytes);
				stream.close();

			} catch (IOException e) {
				LOGGER.error("IO exceptiom", e);
			}

		}
		LOGGER.debug("Method Exit : regressionTestDao-> getUploadedFile");
	}

	/**
	 * Method used to persist job details in database.
	 * 
	 * @param userId
	 *            the user Id
	 * @param jobId
	 *            the job Id
	 * @param ruleName
	 *            the rule name
	 * @param jobStatus
	 *            the job status
	 * @param filePath
	 *            the file path
	 * @param comment
	 *            the comment
	 * @param errorMsg
	 *            the error message
	 */
	public final void createJobDetails(int userId, Long jobId, String ruleName,
			String jobStatus, String filePath, String comment, String errorMsg) {
		LOGGER.debug("Method Entry : regressionTestDao-> createJobDetails");

		final Object[] values = { userId, jobId, ruleName, jobStatus, filePath,
				comment, errorMsg };
		jdbcTemplate.update(
				environment.getProperty(GenericConstants.INSERT_JOB_DETAILS),
				values);
		LOGGER.debug("Method Exit : regressionTestDao-> createJobDetails");

	}

	/**
	 * Method used to retrieve job details from database.
	 * 
	 * @param userId
	 *            the user Id
	 * @return List the JobDetails list
	 */
	public final List<JobDetails> retrieveJobDetails(int userId) {
		LOGGER.debug("Method Entry : regressionTestDao-> retrieveJobDetails");
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final List<JobDetails> jobList = jdbcTemplate.query(
				environment.getProperty(GenericConstants.GET_JOB_DETAILS),
				new Object[] { userId }, new RowMapper<JobDetails>() {
					@Override
					public JobDetails mapRow(ResultSet rs, int rowNo)
							throws SQLException {
						final JobDetails jobDetails = new JobDetails();
						jobDetails.setStatusCode(rs.getString("EXIT_CODE"));
						jobDetails.setJobId(rs.getLong("JOB_EXECUTION_ID"));
						jobDetails.setFilePath(rs.getString("FILE_LOCATION"));
						jobDetails.setRuleName(rs.getString("RULE_NAME"));
						jobDetails.setComment(rs.getString("COMMENT"));
						if (null != rs.getObject("START_TIME")) {
							jobDetails.setStartTime(sdf.format(rs
									.getTimestamp("START_TIME")));
						} else {
							jobDetails.setStartTime("");
						}
						if (null != rs.getObject("END_TIME")) {
							jobDetails.setEndTime(sdf.format(rs
									.getTimestamp("END_TIME")));
						} else {
							jobDetails.setEndTime("");
						}
						jobDetails.setErrorMsg(rs.getString("ERROR_MSG"));
						return jobDetails;
					}

				});
		LOGGER.debug("Method Exit : regressionTestDao-> retrieveJobDetails");
		return jobList;

	}

	/**
	 * Method used to update job details in database.
	 * 
	 * @param jobId
	 *            the job Id
	 * @param ruleName
	 *            the rule name
	 * @param jobStatus
	 *            the job status
	 * @param filePath
	 *            the file path
	 * @param errorMsg
	 *            the error message
	 */
	public final void updateJobDetails(Long jobId, String ruleName,
			String jobStatus, String filePath, String errorMsg) {
		LOGGER.debug("Method Entry : regressionTestDao-> updateJobDetails");
		jdbcTemplate
				.update(environment
						.getProperty(GenericConstants.UPDATE_JOB_DETAILS),
						new Object[] { ruleName, jobStatus, filePath, errorMsg,
								jobId });
		LOGGER.debug("Method Exit: regressionTestDao-> updateJobDetails");

	}

}
