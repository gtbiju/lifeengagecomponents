/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 7, 2014
 */

package com.cognizant.le.rwb.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.le.rwb.domain.User;
import com.cognizant.le.rwb.repository.UserDao;

/**
 * The Class class UserServiceImpl.
 * 
 * @author 304003
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    /** The user dao. */
    @Autowired
    private UserDao userDao;

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    /**
     * Method to validateUser.
     * 
     * @param user
     *            the user
     * @return chkBoolean
     */
    @Override
    public Boolean validateUser(User user) {
        LOGGER.trace("Method Entry : UserServiceImpl-> validateUser");
        Boolean isValidUser = userDao.validateUser(user.getUserName(), user.getPassword());
        LOGGER.trace("Method Exit : UserServiceImpl-> validateUser");
        return isValidUser;
    }
    /**
     * Method to get UserId.
     * 
     * @param user
     *            the user
     * @return user
     */
    @Override
    public User getUserId(User user) {
        LOGGER.trace("Method Entry : UserServiceImpl-> getUserId");
        user = userDao.getUserId(user.getUserName(), user.getPassword());
        LOGGER.trace("Method Exit : UserServiceImpl-> getUserId");
        return user;
    }


}
