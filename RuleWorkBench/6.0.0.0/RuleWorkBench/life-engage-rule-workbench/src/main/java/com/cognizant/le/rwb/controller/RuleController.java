/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 291422
 * @version       : 0.1, Feb 18, 2013
 */
package com.cognizant.le.rwb.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.cognizant.icr.exception.RuleException;
import com.cognizant.icr.rule.bom.RuleCriteria;
import com.cognizant.icr.rule.v2.RuleEngine;
import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.domain.Group;
import com.cognizant.le.rwb.domain.GroupType;
import com.cognizant.le.rwb.domain.Rule;
import com.cognizant.le.rwb.exception.ServiceException;
import com.cognizant.le.rwb.services.RuleService;
import com.cognizant.le.rwb.services.RuleSetService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;

/**
 * The Class class RuleController.
 */
@Controller
public class RuleController {

    /** The environment. */
    @Autowired
    private Environment environment;

    /** The rule service. */
    @Autowired
    private RuleService ruleService;

    /** The rule set service. */
    @Autowired
    private RuleSetService ruleSetService;

    /** The rule engine. */
    @Autowired
    private RuleEngine ruleEngine;

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RuleController.class);

    /**
     * Creates/Updates the business rule.
     *
     * @param ruleId the rule id
     * @param ruleJSON the rule json
     * @param ruleJS the rule js
     * @param offlineRuleJs the offline rule js
     * @param ruleName the rule name
     * @param groupName the group name
     * @param fromCopy the from copy
     * @param isDupChkRequired the is dup chk required
     * @param request the request
     * @param response the response
     * @return the string
     */
    @RequestMapping(value = "/createRule", method = RequestMethod.POST)
    public final @ResponseBody
    Map<String, String> createBusinessRule(@RequestParam("ruleID") final String ruleId,
            @RequestParam("ruleJson") final String ruleJSON, @RequestParam("ruleJS") final String ruleJS,
            @RequestParam("offlineRuleJs") final String offlineRuleJs, @RequestParam("ruleName") final String ruleName,
            @RequestParam("groupName") final String groupName, @RequestParam("fromCopy") final boolean fromCopy,@RequestParam("ruleComment") final String ruleComment, @RequestParam("isDupChkRequired") final boolean isDupChkRequired ,
            final HttpServletRequest request, final HttpServletResponse response) {
        LOGGER.trace("Method Entry : RuleAction-> createBusinessRule");
        Map<String, String> resultMap = new HashMap<String, String>();
        final Rule rule = new Rule();
        try {
            final Integer ruleTypeID = (Integer) request.getSession().getAttribute("ruleTypeID");
            if (ruleId != null && ruleId != "" && ruleId != "null" && !("").equalsIgnoreCase(ruleId.trim())) {
                rule.setRuleID(Integer.parseInt(ruleId));
            }
            rule.setRuleName(ruleName);
            rule.setRuleTypeID(ruleTypeID);
            rule.setRuleJson(ruleJSON);
            rule.setRuleJS(ruleJS);
            rule.setOfflineRuleJS(offlineRuleJs);
            rule.setGroupName(groupName);
            rule.setClientId(environment.getProperty(GenericConstants.CLIENT_ID));
            rule.setComments(ruleComment);
        
            int groupID = ruleService.getGroupId(groupName);           
            if (ruleId != null && ruleId != "" && ruleId != "null" && !("").equalsIgnoreCase(ruleId.trim())) {
            	
                resultMap = ruleService.editRule(rule, groupID, isDupChkRequired);
            } else {
                resultMap = ruleService.createRule(rule, groupID);
            }
            
            //resultMap.put("createRuleResult", createRuleResult);
            //resultMap.put("isRuleExists", String.valueOf(isRuleExists));
            resultMap.put("ruleID", rule.getRuleID() != null ? rule.getRuleID().toString():"");
            LOGGER.trace("Method Exit : RuleAction-> createBusinessRule");
        } catch (Exception e) {
            LOGGER.error("Error while creating / updating rule with name: " + ruleName +
                    " & Rule Id :" + rule.getRuleID() , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }        
        return resultMap;
    }

    /**
     * List rule and rul set.
     * 
     * @param ruleTypeId
     *            the rule type id
     * @param ruleSetTypeId
     *            the rule set type id
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/listRuleAndRulSet", method = RequestMethod.GET)
    public final @ResponseBody
    List<Rule> listRuleAndRulSet(@RequestParam("ruleTypeId") final Integer ruleTypeId,
            @RequestParam("ruleSetTypeId") final Integer ruleSetTypeId, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        LOGGER.trace("Method Entry : RuleAction-> listRuleAndRulSet");
        List<Rule> ruleList = new ArrayList<Rule>();
        try {
            request.getSession().setAttribute("ruleTypeID", ruleTypeId);
            request.getSession().setAttribute("ruleSetTypeID", ruleSetTypeId);
            List<Integer> ruleTypeList = new ArrayList<Integer>();
            if (ruleTypeId == 1) {
                ruleTypeList.add(1);
                ruleTypeList.add(3);
    
            } else if (ruleTypeId == 2) {
                ruleTypeList.add(2);
                ruleTypeList.add(4);
    
            } else {
                ruleTypeList.add(0);
                ruleTypeList.add(0);
    
            }
            ruleList = ruleService.listRules(ruleTypeList);
        } catch (Exception e) {
            LOGGER.error("Error while getting rules and rule set list" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> listRuleAndRulSet");
        return ruleList;
    }

    /**
     * List rule for Update.
     * 
     * @param ruleId
     *            the rule id
     * @param fromCopy
     *            the from copy
     * @param request
     *            the request
     * @return the string
     */
    @RequestMapping(value = "/editRule", method = RequestMethod.POST)
    public final @ResponseBody
    Rule listRule(@RequestParam("ruleID") final Integer ruleId, @RequestParam("fromCopy") final boolean fromCopy,
            final HttpServletRequest request) {
        Rule rule;
        try {
            if (fromCopy == false) {
                request.getSession().setAttribute("editFlag", true);
            }
            rule = ruleService.listRule(ruleId);
        } catch (Exception e) {
            LOGGER.error("Error while getting rules and rule set list" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> listRule");
        return rule;
    }

    /**
     * Download ruleJS.
     * 
     * @param ruleId
     *            the rule id
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string
     */

    @RequestMapping(value = "/downloadRuleJS", method = RequestMethod.GET)
    public void downloadRuleJS(@RequestParam("ruleID") final Integer ruleId, HttpServletRequest request,
            HttpServletResponse response) {
        LOGGER.trace("Method Entry : RuleAction-> downloadRuleJS");
        try {
            Rule rule = ruleService.downloadRuleJS(ruleId);
            String onlineruleJS = rule.getRuleJS();
            String offlineRuleJS = rule.getOfflineRuleJS();
            
            // copy it to response's OutputStream
            /*
             * if (ruleJS != null) { response.setContentType("application/octet-stream");
             * response.setContentLength((int) ruleJS.length()); final String headerKey = "Content-Disposition"; final
             * String headerValue = String.format("attachment; filename=\"%s\"", rule.getRuleName() + ".txt");
             * response.setHeader(headerKey, headerValue); InputStream fileInputStream = new
             * ByteArrayInputStream(ruleJS.getBytes()); IOUtils.copy(fileInputStream, response.getOutputStream());
             * fileInputStream.close(); response.flushBuffer(); }
             */
            String fileName = rule.getRuleName();
            String onProduction =
                    new StringBuilder("Online").append(File.separator)
                        .append("Production")
                        .append(File.separator)
                        .append(fileName)
                        .append(".min.js")
                        .toString();
            String onDebug =
                    new StringBuilder("Online").append(File.separator)
                        .append("Debug")
                        .append(File.separator)
                        .append(fileName)
                        .append(".js")
                        .toString();
            String offProduction =
                    new StringBuilder("Offline").append(File.separator)
                        .append("Production")
                        .append(File.separator)
                        .append(fileName)
                        .append(".min.js")
                        .toString();
            String offDebug =
                    new StringBuilder("Offline").append(File.separator)
                        .append("Debug")
                        .append(File.separator)
                        .append(fileName)
                        .append(".js")
                        .toString();

            String outFilename =
                    new StringBuilder(System.getProperty("java.io.tmpdir")).append(File.separator)
                        .append(fileName)
                        .append(".zip")
                        .toString();

            ZipOutputStream out = null;
            InputStream fileInputStream = null;

            try {
                out = new ZipOutputStream(new FileOutputStream(outFilename));
                String offLinefileContent = null;
                if (offlineRuleJS != null) {
                    offLinefileContent = convertToAsynchronousRule(offlineRuleJS);
                } else {
                    offLinefileContent = onlineruleJS;  
                }
                // copy contents to Zip
                out.putNextEntry(new ZipEntry(offDebug));
                out.write(offLinefileContent.getBytes());
                out.closeEntry();
                out.putNextEntry(new ZipEntry(offProduction));
                out.write(compress(offLinefileContent));
                out.closeEntry();
                out.putNextEntry(new ZipEntry(onDebug));
                out.write(onlineruleJS.getBytes());
                out.closeEntry();
                out.putNextEntry(new ZipEntry(onProduction));
                out.write(compress(onlineruleJS));
                out.closeEntry();
                out.flush();
                out.close();
                fileInputStream = sendResponse(fileName, response, outFilename);

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                response.sendRedirect("jsp/Error.jsp");
                throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);

            } finally {
                // Delete zip
                try {
                    if (null != fileInputStream) {
                        fileInputStream.close();
                        File delete = new File(outFilename);
                        delete.delete();
                    }
                } catch (IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }

        } catch (Exception e) {
            LOGGER.error("Error while downloading ruleJS", e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> downloadRuleJS");
    }

    /**
     * List rule for Update.
     * 
     * @param ruleId
     *            the rule id
     * @param ruleTypeId
     *            the rule type id
     * @return the string
     */
    @RequestMapping(value = "/deleteRule", method = RequestMethod.POST)
    public @ResponseBody
    Map<String, String>
            deleteRule(@RequestParam("ruleID") final Integer ruleId,
                    @RequestParam("ruleTypeId") final Integer ruleTypeId) {
        LOGGER.trace("Method Entry : RuleAction-> deleteRule");
        Map<String, String> resultMap = new HashMap<String, String>();
        StringBuilder sb = new StringBuilder();
        try {
            if (ruleTypeId == 1) {
            	List<Rule> ruleList = ruleService.getRuleExistingRulesets(ruleId);
            	if(ruleList == null || ruleList.isEmpty()){
            		ruleService.deleteRule(ruleId);
            	} else {
            		int count = 0;
            		for (Rule rule : ruleList) {
            			count++;
            			sb = sb.append(count + ". " + rule.getRuleName() + ", ");
            			sb.append(GenericConstants.NEW_LINE);
					}
            		resultMap.put("message",GenericConstants.RULE_DELETE_ERROR_MSG + GenericConstants.NEW_LINE+ sb.toString());
            	}
                
            } else if (ruleTypeId == 3) {
                ruleSetService.deleteRuleSet(ruleId);
            }
        } catch (Exception e) {
            LOGGER.error("Error while deleting rule" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        
        
        return resultMap;
    }

    /**
     * Clone rule.
     * 
     * @param ruleId
     *            the rule id
     * @return the model and view
     */
    @RequestMapping(value = "/clone", method = RequestMethod.GET)
    public ModelAndView cloneRule(@RequestParam("ruleID") final Integer ruleId) {
        ModelAndView mav =  null;
        try {
            Integer ruleTypeId = ruleService.getRuleType(ruleId);
            if (ruleTypeId == 1) {
                mav = new ModelAndView(new RedirectView("jsp/buildBusinessRule.jsp", true));
                mav.addObject("ruleId", ruleId);
                mav.addObject("fromClone", "true");
                return mav;
            } else if (ruleTypeId == 2) {
                mav = new ModelAndView(new RedirectView("jsp/buildCalculationRule.jsp", true));
                mav.addObject("ruleId", ruleId);
                mav.addObject("fromClone", "true");
                return mav;
            } else if (ruleTypeId == 3) {
                mav = new ModelAndView(new RedirectView("jsp/ruleSet.jsp", true));
                mav.addObject("ruleType", "3");
                mav.addObject("ruleSetId", ruleId);
                mav.addObject("fromClone", "true");
                return mav;
            } else if (ruleTypeId == 4) {
                mav = new ModelAndView(new RedirectView("jsp/ruleSet.jsp", true));
                mav.addObject("ruleType", "4");
                mav.addObject("ruleSetId", ruleId);
                mav.addObject("fromClone", "true");
                return mav;
            }
            // default redirection to listing page
            mav = new ModelAndView(new RedirectView("jsp/listBusinessRule.jsp", true));
            mav.addObject("ruleTypeId", "1");
            mav.addObject("ruleSetTypeId", "1");
        } catch (Exception e) {
            LOGGER.error("Error redirecting to clone page" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        return mav;
    }

    /*
     * executeRule Rule test execution
     */

    /**
     * Execute rule.
     *
     * @param inoutMap the inout map
     * @param ruleName the rule name
     * @param ruleID the rule id
     * @return the map
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws NamingException the naming exception
     */
    @RequestMapping(value = "/executeTest", method = RequestMethod.POST)
    public final @ResponseBody
    Map<String, String> executeRule(@RequestParam("inoutMap") final String inoutMap,
            @RequestParam("ruleName") final String ruleName, @RequestParam("ruleID") final Integer ruleID) throws IOException, NamingException {
        Map<String, String> resultMap = new HashMap<String, String>();
        try {
            String groupID = ruleService.getGroupID(ruleID);
            RuleCriteria ruleCriteria = new RuleCriteria();
            ruleCriteria.setCategory(groupID);
            ruleCriteria.setClientId(environment.getProperty(GenericConstants.CLIENT_ID));
            ruleCriteria.setRule(ruleName);
            String jsonOutput = null;
                jsonOutput = ruleEngine.executeRuleWithJson(ruleCriteria, inoutMap);
           
            resultMap.put("ruleJson", jsonOutput);
            
        } catch (SQLException e) {
            LOGGER.error("SQL Exception while execute Rule", e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        } catch (RuleException e) {
            LOGGER.error("Rule Exception while execute Rule" + e.getMessage(), e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION,e.getMessage() +GenericConstants.NEW_LINE +GenericConstants.RULEEXCEPTION_MESSAGE);
        } catch (Exception e) {
            LOGGER.error("Error while execute Rule" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }

        return resultMap;

    }

    /**
     * List from group type table.
     * 
     * @return the list
     */
    @RequestMapping(value = "/listGroupType", method = RequestMethod.POST)
    public final @ResponseBody
    List<GroupType> listFromGroupTypeTable() {
        LOGGER.trace("Method Entry : RuleAction-> listFromGroupTypeTable");
        List<GroupType> groupTypeList;
        try {
            groupTypeList = ruleService.listFromGroupTypeTable();
        } catch (Exception e) {
            LOGGER.error("Error while listing Group Type" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> listFromGroupTypeTable");
        return groupTypeList;
    }

    /**
     * Add group.
     * 
     * @param groupName
     *            the group name
     * @param groupTypeId
     *            the group type id
     * @return the string
     */

    @RequestMapping(value = "/groupAdd", method = RequestMethod.POST)
    public final @ResponseBody
    String addGroupName(@RequestParam("groupName") final String groupName,
            @RequestParam("groupTypeID") final Integer groupTypeId,@RequestParam("comments") final String comments) {
        LOGGER.trace("Method Entry : RuleAction-> addGroupName");
        boolean createGroupResult;
        try {
            final Group group = new Group();
            group.setGroupName(groupName);
            group.setTypeID(groupTypeId);
            group.setComments(comments);
            createGroupResult = ruleService.addGroupName(group);
        } catch (Exception e) {
            LOGGER.error("Error while adding group" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> addGroupName");
        return "{\"createGroupResult\" : \"" + createGroupResult + "\"}";
    }

    /**
     * List from group table.
     * 
     * @return the list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/listGroupTable", method = RequestMethod.POST)
    public final @ResponseBody
    Map<String, Object> listFromGroupTable() throws IOException {
        LOGGER.trace("Method Entry : RuleAction-> listFromGroupTable");
        Map<String, Object> resultMap = new HashMap<String, Object>();
        List<Group> groupList;
        Boolean debugConf;
        String canonicalLookupType;
        try {
            groupList = ruleService.listGroupTable();
            debugConf = Boolean.valueOf(environment.getProperty(GenericConstants.DEBUG_CONF));
            canonicalLookupType = environment.getProperty(GenericConstants.CANONICAL_JSON_LOOKUP_TYPE);
            resultMap.put("groupList",groupList);
            resultMap.put("debugConf",debugConf);
            resultMap.put("canonicalLookupType",canonicalLookupType);
            if("Remote".equals(canonicalLookupType)){
                resultMap.put("filedownloadURL",environment.getProperty(GenericConstants.CONTENT_ADMIN_DOWNLOAD_URL));  
            }
            
        } catch (Exception e) {
            LOGGER.error("Error while listing group" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> listFromGroupTable");

        return resultMap;
    }
    
    /**
     * Gets the canonical json.
     *
     * @param fileName the file name
     * @return the canonical json
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/getCanonicalJSON", method = RequestMethod.POST)
    public final @ResponseBody
    String getCanonicalJSON(@RequestParam("fileName") final String fileName) throws IOException {
        LOGGER.trace("Method Entry : RuleAction-> getCanonicalJSON");
        String canonicalJson;
        try {
            
            PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            Resource resource = resolver.getResource("classpath:/"+fileName);
            InputStream is = resource.getInputStream();
            canonicalJson =  IOUtils.toString(is);
            
        } catch (Exception e) {
            LOGGER.error("Error while fetching canonical JSON file" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> getCanonicalJSON");

        return canonicalJson;
    }
    
    

    /**
     * Update group name.
     *
     * @param groupID the group id
     * @param groupName the group name
     * @param groupTypeID the group type id
     * @param isGrpNameChanged the is grp name changed
     * @return the string
     */
    @RequestMapping(value = "/updateGroupAdd", method = RequestMethod.POST)
    public final @ResponseBody
    String updateGroupName(@RequestParam("groupID") final Integer groupID, @RequestParam("groupName") final String groupName,
            @RequestParam("groupTypeID") final Integer groupTypeID,@RequestParam("isGrpNameChanged") final boolean isGrpNameChanged,@RequestParam("comments") final String comments) {
        LOGGER.trace("Method Entry : RuleAction-> updateGroupName");
        boolean updateGroupResult;
        try {
            final Group group = new Group();
            group.setGroupName(groupName); 
            group.setGroupID(groupID);
            group.setTypeID(groupTypeID) ;
            group.setComments(comments) ;
            updateGroupResult = ruleService.updateGroupName(group, isGrpNameChanged);
        } catch (Exception e) {
            LOGGER.error("Error while updating group name" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> updateGroupName");
        return "{\"updateGroupResult\" : \"" + updateGroupResult + "\"}";
    }

    /*
     * Delete group from group list
     */
    /**
     * Delete group.
     * 
     * @param groupID
     *            the group id
     * @return the string
     */
    @RequestMapping(value = "/deleteGroup", method = RequestMethod.POST)
    public final @ResponseBody
    String deleteGroup(@RequestParam("groupID") final String groupID) {
        LOGGER.trace("Method Entry : RuleAction-> deleteGroup");
        try {
            ruleService.deleteGroup(Integer.parseInt(groupID));
        } catch (Exception e) {
            LOGGER.error("Error while deleting group" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> deleteGroup");
        return "{\"deleteGroupResult\" : \"Group deleted Successfully \"}";
    }
    
    
    /**
     * Confirm group delete.
     *
     * @param groupID the group id
     * @return the map
     */
    @RequestMapping(value = "/confirmGroupDelete", method = RequestMethod.POST)
    public final @ResponseBody
    Map<String, String> confirmGroupDelete(@RequestParam("groupID") final String groupID) {
        LOGGER.trace("Method Entry : RuleAction-> confirmGroupDelete");
        Map<String,String> ruleRuleSetCountMap = new HashMap<String,String>();
        try {
        	Integer ruleCount = 0;
        	Integer ruleSetCount = 0;
        	List<Rule> ruleSetIdList = ruleService.checkForRuleAndRuleSetCountInGroup(Integer.parseInt(groupID));
            if(ruleSetIdList != null && ruleSetIdList.size() > 0){
        		for (Rule rule : ruleSetIdList) {
        			if(rule.getRuleTypeID() == 1){
        				ruleCount++;
        			} else {
        				ruleSetCount++;
        			}
    			}
        		ruleRuleSetCountMap.put("ruleCount",String.valueOf(ruleCount));
            	ruleRuleSetCountMap.put("ruleSetCount",String.valueOf(ruleSetCount));
            	ruleRuleSetCountMap.put("groupID",groupID);
        	} else { 
            	ruleRuleSetCountMap.put("isRuleAndRuleSetExists","false");
            	ruleRuleSetCountMap.put("groupID",groupID);
            }
        } catch (Exception e) {
            LOGGER.error("Error while deleting group" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> confirmGroupDelete");
        return ruleRuleSetCountMap;
    }
    

    /**
     * List group download rules.
     * 
     * @param groupId
     *            the group id
     * @return the list
     */
    @RequestMapping(value = "/listGroupDownloadRules", method = RequestMethod.POST)
    public final @ResponseBody
    List<Rule> listGroupDownloadRules(@RequestParam("groupID") final String groupId) {
        LOGGER.trace("Method Entry : RuleAction-> listGroupDownloadRules");
        List<Rule> ruleList;
        try {
            ruleList = ruleService.listGroupDownloadRules(Integer.parseInt(groupId));
        } catch (Exception e) {
            LOGGER.error("Error while listing group download rules" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> listGroupDownloadRules");
        return ruleList;
    }

    // zip folder

    /**
     * Download rule js.
     *
     * @param ruleNameList the rule name list
     * @param groupId the group id
     * @param fileName the file name
     * @param request the request
     * @param httpResponse the http response
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/downloadGroupRules", method = RequestMethod.GET)
    public void downloadRuleJS(@RequestParam("ruleNameList") final String ruleNameList,
            @RequestParam("groupId") String groupId, @RequestParam("fileName") final String fileName, HttpServletRequest request,
            HttpServletResponse httpResponse) throws IOException {
       
        LOGGER.trace("Method Entry : RuleAction-> downloadGroupRules");
        String onProduction = new StringBuilder("Online")
                            .append(File.separator)
                            .append("Production")
                            .append(File.separator)
                            .append(fileName)
                            .append(".min.js").toString();
        String onDebug = new StringBuilder("Online")
                            .append(File.separator)
                            .append("Debug")
                            .append(File.separator)
                            .append(fileName)
                            .append(".js").toString();
        String offProduction = new StringBuilder("Offline")
                            .append(File.separator)
                            .append("Production")
                            .append(File.separator)
                            .append(fileName)
                            .append(".min.js").toString();
        String offDebug = new StringBuilder("Offline")
                            .append(File.separator)
                            .append("Debug")
                            .append(File.separator)
                            .append(fileName)
                            .append(".js").toString();
        

        String outFilename = new StringBuilder(System.getProperty("java.io.tmpdir"))
                            .append(File.separator)
                            .append(fileName)
                            .append(".zip").toString(); 

        ZipOutputStream out = null;
        InputStream fileInputStream = null;
        try {
            //List<Integer> idList = getIdList(ruleIdList);
            List<String> ruleNames= getNameList(ruleNameList);
            out = new ZipOutputStream(new FileOutputStream(outFilename));
            String offLinefileContent = null;
            String onLinefileContent = null;
            // copy contents to Zip
            out.putNextEntry(new ZipEntry(offDebug));
            offLinefileContent = getFileContents(ruleNames, groupId, "offDebug");
            out.write(offLinefileContent.getBytes());
            out.closeEntry();
            out.putNextEntry(new ZipEntry(onDebug));
            onLinefileContent = getFileContents(ruleNames, groupId, "onDebug");
            out.write(onLinefileContent.getBytes());
            out.closeEntry();
            out.putNextEntry(new ZipEntry(onProduction));
            out.write(compress(onLinefileContent));
            out.closeEntry();
            out.putNextEntry(new ZipEntry(offProduction));
            out.write(compress(offLinefileContent));
            out.closeEntry();
            out.flush();
            out.close();
            fileInputStream = sendResponse(fileName, httpResponse, outFilename);
           
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            httpResponse.sendRedirect("jsp/Error.jsp");
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
            
        } finally {
         // Delete zip
            try {
                if(null != fileInputStream) {
                    fileInputStream.close();
                    File delete = new File(outFilename);
                    delete.delete();
                }
            } catch (IOException e) {
              LOGGER.error(e.getMessage(), e);
            }           
        }
        LOGGER.trace("Method Exit : RuleAction-> downloadGroupRules");
    }
    
    /**
     * Find refered rules.
     *
     * @param ruleNameList the rule name list
     * @param groupId the group id
     * @return the map
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/findReferedRules", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> findReferedRules(@RequestParam("ruleNameList") final String ruleNameList,
            @RequestParam("groupId") String groupId)
            throws IOException {
        Map<String, Object> responseMap = new HashMap<String, Object>();
        List<String> referedRuleList = getReferedRules(getNameList(ruleNameList),groupId);
        StringBuilder message = new StringBuilder();
        if (referedRuleList != null && !referedRuleList.isEmpty() && referedRuleList.size() > 0) {
            int i = 0;
            message.append("Below rules are refered in your selected rules.");
            message.append(GenericConstants.NEW_LINE);
            for (String notSetlectedRule : referedRuleList) {
                message.append(++i + ". " + notSetlectedRule );
                message.append(GenericConstants.NEW_LINE);

            }
            responseMap.put("isNonMatch", true);
            responseMap.put("message", message); 
            responseMap.put("referedRules", referedRuleList); 

        } else {
            responseMap.put("isNonMatch", false);
            responseMap.put("message", ""); 

        }
        return responseMap;
    }
    
    /**
     * Gets the all ids.
     *
     * @param ruleIdList the ruleIdList
     * @return the id list
     * @throws JsonParseException the json parse exception
     * @throws JsonMappingException the json mapping exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private List<Integer> getIdList(String ruleIdList) throws JsonParseException, JsonMappingException, IOException {
        List<Integer> idList = new ArrayList<Integer>();
        ObjectMapper mapper = new ObjectMapper();
        final List<String> ruleDataList = mapper.readValue(ruleIdList, new TypeReference<List<String>>() {});
        for (String ruleid : ruleDataList) {
            idList.add(Integer.valueOf(ruleid));
        } 
        return idList;
    }

    /**
     * Gets the name list.
     *
     * @param ruleNameList the rule name list
     * @return the name list
     * @throws JsonParseException the json parse exception
     * @throws JsonMappingException the json mapping exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private List<String> getNameList(String ruleNameList) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        final List<String> ruleNames = mapper.readValue(ruleNameList, new TypeReference<List<String>>() {});
        return ruleNames;
    }
    
    /**
     * Gets the refered rules.
     *
     * @param ruleNameList the rule name list
     * @param groupId the group id
     * @return the refered rules
     */
    public List<String> getReferedRules(List<String> ruleNameList, String groupId) {
        List<String> ruleNames = ruleService.getRulesByGroup(Integer.valueOf(groupId));
        List<String> listNonMatches = new ArrayList<String>();
        StringBuilder ruleDesc1 = ruleService.getDownloadRulesDesc(ruleNameList, groupId, true); 
        for (String ruleName : ruleNames) {
           if(getReferedRulesByRegEx1(ruleDesc1.toString(),"\\b"+ruleName+"\\b") && !ruleNameList.contains(ruleName) && !listNonMatches.contains(ruleName)){
              
               listNonMatches.add(ruleName);
           }
            
        }
        
        //Map<String, Object> referedRulesMap = new HashMap<String, Object>();
       /* 
        StringBuilder ruleDesc = ruleService.getDownloadRulesDesc(ruleNameList, groupId, true);
        List<String> listMatches = new ArrayList<String>();
        getReferedRulesByRegEx(ruleDesc.toString(),"[a-zA-Z0-9_]+(?<!JSON.stringify)[\\s]*\\([\\s]*inoutMap[\\s]*\\)[\\s]*;",listMatches);
        getReferedRulesByRegEx(ruleDesc.toString(),"[a-zA-Z0-9_]+(?<!JSON.stringify)[\\s]*\\([\\s]*inoutMap[\\s]*,[a-zA-Z0-9_.,\\s]*\\)[\\s]*;",listMatches);
        
        List<String> listNonMatches = new ArrayList<String>();
        for (String referedRule : listMatches) {
            if (!ruleNameList.contains(referedRule) && !listNonMatches.contains(referedRule)) {
                listNonMatches.add(referedRule);
            }

        }*/
        return listNonMatches;
    }
    
    /**
     * Gets the refered rules by reg ex1.
     *
     * @param ruleDesc the rule desc
     * @param regex the regex
     * @return the refered rules by reg ex1
     */
    private static boolean getReferedRulesByRegEx1(String ruleDesc, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(ruleDesc);
        boolean isRuleExists = false;
        if(matcher.find()){
            isRuleExists = true;
        }
        return isRuleExists;
    }
    
    /**
     * Gets the refered rules by reg ex.
     *
     * @param ruleDesc the rule desc
     * @param regex the regex
     * @param listMatches the list matches
     * @return the refered rules by reg ex
     */
    private static List<String> getReferedRulesByRegEx(String ruleDesc, String regex, List<String> listMatches) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(ruleDesc);

        while (matcher.find()) {
            String ruleName = matcher.group();
            listMatches.add(ruleName.substring(0, ruleName.indexOf("(")).trim());
        }
        return listMatches;
    }
    
    /**
     * Get the contents to be written in the file.
     *
     * @param ruleNameList the rule name list
     * @param groupId the group id
     * @param fileType the fileType
     * @return the file contents
     */
    private String getFileContents(List<String> ruleNameList, String groupId, String fileType) {

        StringBuilder ruleDescList = null;
        String output = null;

        if ("onDebug".equals(fileType)) {
            ruleDescList = ruleService.getDownloadRulesDesc(ruleNameList, groupId, true);
            output = ruleDescList.toString();
        } else if ("offDebug".equals(fileType)) {
            ruleDescList = ruleService.getDownloadRulesDesc(ruleNameList, groupId, false);
            output = convertToAsynchronousRule(ruleDescList.toString());
        }
        return output;

    }
    
    /**
     * Convert to asynchronous rule.
     *
     * @param continuationInput the continuation input
     * @return the string
     */
    private String convertToAsynchronousRule(String continuationInput){
        String output = null;
        Client client = Client.create();
        WebResource webResource =
                client.resource(environment.getProperty(GenericConstants.CONTINUATION_SERVICE_URL));
        ClientResponse response =
                webResource.type(MediaType.TEXT_PLAIN).post(ClientResponse.class, continuationInput);
        output = response.getEntity(String.class);
        response.close();
        
        return output;
    }

    /**
     * Initiate Download.
     *
     * @param fileName the file name
     * @param httpResponse the http response
     * @param outFilename the out filename
     * @return the input stream
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private InputStream sendResponse(final String fileName, HttpServletResponse httpResponse, String outFilename)
            throws FileNotFoundException, IOException {
        InputStream fileInputStream;
        // Download Zip
        fileInputStream = new FileInputStream(outFilename);
        httpResponse.setContentType("application/octet-stream");
        final String headerValue = String.format("attachment; filename=\"%s\"", fileName + ".zip");
        httpResponse.setHeader("Content-Disposition", headerValue);
        IOUtils.copy(fileInputStream, httpResponse.getOutputStream());
        httpResponse.flushBuffer();
        
        return fileInputStream;
    }

    /**
     * Compress.
     *
     * @param fileContent the file content
     * @return the byte[]
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private byte[] compress(String fileContent) throws IOException {
       
        byte[] minifiedBytes=null;
        String minifiedContent= null;
        Reader in = null;
        Writer out = new StringWriter();

        // compress configs
        int lineBreakPos = -1;
        boolean munge = true;
        boolean verbose = false;
        boolean preserveAllSemiColons = false;
        boolean disableOptimizations = false;

        in = new InputStreamReader(IOUtils.toInputStream(fileContent, "UTF-8"));
        JavaScriptCompressor compressor = new JavaScriptCompressor(in, null);
        in.close();
        in = null;
        compressor.compress(out, lineBreakPos, munge, verbose, preserveAllSemiColons, disableOptimizations);
        minifiedContent = out.toString();
        out.close();
        minifiedBytes=minifiedContent.getBytes();
        return minifiedBytes;
    }

    

    /**
     * List rule by group.
     * 
     * @param groupId
     *            the group id
     * @return the list
     */
    @RequestMapping(value = "/listRuleByGroup", method = RequestMethod.POST)
    public final @ResponseBody
    List<Rule> listRuleByGroup(@RequestParam("groupID") final Integer groupId) {
        LOGGER.trace("Method Entry : RuleAction-> listRuleByGroup");
        List<Rule> ruleList;
        try {
            ruleList = ruleService.listRuleByGroup(groupId);
        } catch (Exception e) {
            LOGGER.error("Error while listing rule by group" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> listRuleByGroup");

        return ruleList;
    }

    /**
     * Clone group.
     * 
     * @param ruleBOList
     *            the rule bo list
     * @param groupCloneName
     *            the group clone name
     * @param groupId
     *            the group id
     * @param groupTypeId
     *            the group type id
     * @return the map
     * @throws JsonParseException
     *             the json parse exception
     * @throws JsonMappingException
     *             the json mapping exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/cloneGroup", method = RequestMethod.POST)
    public final @ResponseBody
    Map<String, String> cloneGroup(@RequestParam("ruleBOList") final String ruleBOList,
            @RequestParam("groupCloneName") final String groupCloneName, @RequestParam("groupID") final String groupId,
            @RequestParam("groupTypeID") final String groupTypeId) throws JsonParseException, JsonMappingException,
            IOException {
        LOGGER.trace("Method Entry : RuleAction-> cloneGroup");
        Map<String, String> resultMap = new HashMap<String, String>();
        try {
            final Group group = new Group();
            Integer groupID = ruleService.getGroupId(groupCloneName);       
    
            if (groupID != null) {
                resultMap.put("groupexists", "true");
                resultMap.put("groupID", groupID.toString());
            } else {
                group.setGroupName(groupCloneName);
                group.setTypeID(Integer.parseInt(groupTypeId));
                String createGroupResult = ruleService.cloneGroup(deserialzeRuleList(ruleBOList), group);
                resultMap.put("createGroupResult", createGroupResult);
            }
        } catch (Exception e) {
            LOGGER.error("Error while cloning group" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> cloneGroup");

        return resultMap;
    }

    /**
     * Merge group.
     * 
     * @param ruleBOList
     *            the rule bo list
     * @param groupCloneName
     *            the group clone name
     * @param groupId
     *            the group id
     * @param groupTypeId
     *            the group type id
     * @return the map
     * @throws JsonParseException
     *             the json parse exception
     * @throws JsonMappingException
     *             the json mapping exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/mergeGroup", method = RequestMethod.POST)
    public final @ResponseBody
    Map<String, Object> mergeGroup(@RequestParam("ruleBOList") final String ruleBOList,
            @RequestParam("groupCloneName") final String groupCloneName,
            @RequestParam("groupID") final Integer groupId, @RequestParam("groupTypeID") final Integer groupTypeId)
            throws JsonParseException, JsonMappingException, IOException {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            boolean ruleNameExists = false;
    
            Integer groupType = ruleService.getGroupType(groupCloneName);
            // checking merging groups are of same type or not.
            if ((groupTypeId != null && groupType != null) && groupTypeId.equals(groupType)) {
                List<Rule> ruleList = ruleService.listRuleByGroup(groupId);
    
                // rules with same name adding to string builder.
                StringBuilder sb = new StringBuilder();
                int count = 0;
    
                // checking merging group contains any common rule name.
                OUTERLOOP: for (Rule deserializedRule : deserialzeRuleList(ruleBOList)) {
                    for (Rule rule1 : ruleList) {
                        if (deserializedRule.getRuleName().equalsIgnoreCase(rule1.getRuleName())) {
                            ruleNameExists = true;
                            count++;
                            if (count <= 10) {
                                sb.append(count + ". " + deserializedRule.getRuleName() + ", ");
                                sb.append(GenericConstants.NEW_LINE);
                            } else {
                                sb.append(" etc.");
                                break OUTERLOOP;
                            }
                        }
    
                    }
                }
                if (!ruleNameExists) {
                    ruleService.mergeGroup(deserialzeRuleList(ruleBOList), groupId);
                    resultMap.put("isMergeSuccess", true);
    
                } else {
                    resultMap.put("isMergeSuccess", false);
                    resultMap.put("isRuleNameExists", true);
                    resultMap.put("message", GenericConstants.RULE_DUP_MESSAGE + GenericConstants.NEW_LINE + sb.toString());
                }
            } else {
                resultMap.put("isMergeSuccess", false);
                resultMap.put("isSameGroup", false);
                resultMap.put("message", GenericConstants.GROUP_TYPE_DIFF_MESSAGE);
            }
        } catch (Exception e) {
            LOGGER.error("Error while merging group" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        return resultMap;
    }

    /**
     * Deserialze rule list.
     * 
     * @param ruleList
     *            the rule list
     * @return the list
     * @throws JsonParseException
     *             the json parse exception
     * @throws JsonMappingException
     *             the json mapping exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private List<Rule> deserialzeRuleList(String ruleList) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        final List<Map<String, Object>> ruleMapList =
                mapper.readValue(ruleList, new TypeReference<List<Map<String, Object>>>() {
                });
        List<Rule> deserializedRuleList = new ArrayList<Rule>();

        try {
            Rule rule;
            for (Map<String, Object> ruleMap : ruleMapList) {
                rule = new Rule();
                if (ruleMap.get("ruleTypeID") != null && ruleMap.get("ruleTypeID").toString() != "" && !("").equalsIgnoreCase(ruleMap.get("ruleTypeID").toString())) {
                    rule.setRuleTypeID(Integer.parseInt(ruleMap.get("ruleTypeID").toString()));
                }
                rule.setRuleName((String) ruleMap.get("ruleName"));
                rule.setClientId((String) ruleMap.get("clientId"));
                rule.setRuleJson((String) ruleMap.get("ruleJson"));
                rule.setRuleJS((String) ruleMap.get("ruleJS"));
                rule.setGroupName((String) ruleMap.get("groupName"));
                deserializedRuleList.add(rule);
            }

        } catch (Exception e) {
            LOGGER.error("Method Exit : RuleAction-> cloneGroup", e);
        }
        return deserializedRuleList;
    }

    /**
     * Table attribute loader.
     * 
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the list
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/tableLoader", method = RequestMethod.POST)
    public final @ResponseBody
    List<Map<String, Object>>
            tableAttributeLoader(final HttpServletRequest request, final HttpServletResponse response)
                    throws IOException {
        LOGGER.trace("Method Entry : RuleAction-> tableAttributeLoader");
        List<Map<String, Object>> tableLoaderList;
        try {
            tableLoaderList = ruleService.tableAttributeLoader();
        } catch (Exception e) {
            LOGGER.error("Error while creating tableLoader" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleAction-> tableAttributeLoader");

        return tableLoaderList;
    }
}


