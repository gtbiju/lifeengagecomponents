/**
 *
 * Copyright 2014, Cognizant 
 *
 * @author        : 304003
 * @version       : 0.1, Feb 28, 2013
 */
package com.cognizant.le.rwb.domain;

/**
 * The Class class Group.
 */
public class Group {
    /** The group id. */
    private Integer groupID;

    /** The group name. */
    private String groupName;

    /** The rule group Type. */
    private Integer typeID;

    /** The rule group Type. */
    private String groupType;
    
    /** The canonical json file name. */
    private String canonicalJsonFileName;
    
    /** The comments. */
    private String comments;

    /**
     * Gets the group id.
     * 
     * @return the group id
     */
    public Integer getGroupID() {
        return groupID;
    }

    /**
     * Sets the group id.
     * 
     * @param groupID
     *            the group id to set.
     */
    public void setGroupID(Integer groupID) {
        this.groupID = groupID;
    }

    /**
     * Gets the group name.
     * 
     * @return the group name
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Sets the group name.
     * 
     * @param groupName
     *            the group name to set.
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * Gets the type id.
     * 
     * @return the type id
     */
    public Integer getTypeID() {
        return typeID;
    }

    /**
     * Sets the type id.
     * 
     * @param typeID
     *            the type id to set.
     */
    public void setTypeID(Integer typeID) {
        this.typeID = typeID;
    }

    /**
     * Gets the group type.
     * 
     * @return the group type
     */
    public String getGroupType() {
        return groupType;
    }

    /**
     * Sets the group type.
     * 
     * @param groupType
     *            the group type to set.
     */
    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    /**
     * Gets the canonical type.
     *
     * @return the canonical type
     */
    public String getCanonicalJsonFileName() {
        return canonicalJsonFileName;
    }

    /**
     * Sets the canonical json.
     *
     * @param canonicalType the canonical type to set.
     */
    public void setCanonicalJsonFileName(String canonicalJsonFileName) {
        this.canonicalJsonFileName = canonicalJsonFileName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

}
