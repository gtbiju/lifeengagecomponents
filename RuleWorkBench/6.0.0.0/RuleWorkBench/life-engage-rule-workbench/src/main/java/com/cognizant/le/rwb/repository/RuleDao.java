/**
 * 
 */
package com.cognizant.le.rwb.repository;

import java.util.List;
import java.util.Map;

import com.cognizant.le.rwb.domain.Group;
import com.cognizant.le.rwb.domain.GroupType;
import com.cognizant.le.rwb.domain.Rule;

// TODO: Auto-generated Javadoc
/**
 * The Interface interface RuleDao.
 * 
 * @author 304003
 */
public interface RuleDao {

    /**
     * Creates the rule.
     * 
     * @param rule
     *            the rule
     * @param group
     *            the group
     * @return the string
     */
    public Map<String, String> createRule(Rule rule, Integer groupID);

    /**
     * Edits the rule.
     * 
     * @param rule
     *            the rule
     * @param groupID
     *            the group id
     * @return the string
     */
    public void editRule(Rule rule, Integer groupID);

    /**
     * List rules.
     * 
     * @param ruleTypeIds
     *            the rule type ids
     * @return the list
     */
    public List<Rule> listRules(List<Integer> ruleTypeIds);

    /**
     * List rules ruleset.
     * 
     * @param ruleTypeId
     *            the rule type id
     * @param groupId
     *            the group id
     * @return the list
     */
    public List<Rule> listRulesRuleset(Integer ruleTypeId, Integer groupId);

    /**
     * Checks if is rule exists.
     *
     * @param rule the rule
     * @param groupID the group id
     * @return true, if is rule exists
     */
    public boolean isRuleExists(Rule rule,Integer groupID,Integer ruleId);

    /**
     * List rule.
     * 
     * @param ruleId
     *            the rule id
     * @return the rule
     */
    public Rule listRule(Integer ruleId);

    /**
     * Download rule js.
     * 
     * @param ruleId
     *            the rule id
     * @return the rule
     */
    public Rule downloadRuleJS(Integer ruleId);
    
    /**
     * Delete rule.
     * 
     * @param ruleId
     *            the rule id
     */
    public void deleteRule(Integer ruleId);

    /**
     * Gets the rule type.
     * 
     * @param ruleId
     *            the rule id
     * @return the rule type
     */
    public Integer getRuleType(Integer ruleId);

    /**
     * Adds the group name.
     * 
     * @param group
     *            the group
     * @return the string
     */
    public boolean addGroupName(Group group);

    /**
     * List group table.
     * 
     * @return the list
     */
    public List<Group> listGroupTable();

    /**
     * Gets the group id.
     * 
     * @param groupName
     *            the group name
     * @return the group id
     */
    public Integer getGroupId(String groupName);

    /**
     * Gets the group id.
     *
     * @param ruleID the rule id
     * @return the group id
     */
    public String getGroupID(Integer ruleID);

    /**
     * Update group name.
     *
     * @param group the group
     * @return the string
     */
    public void updateGroupName(Group group);

    /**
     * Delete group.
     * 
     * @param groupID
     *            the group id
     */
    public void deleteGroup(Integer groupID);

    /**
     * List group download rules.
     * 
     * @param groupID
     *            the group id
     * @return the list
     */
    public List<Rule> listGroupDownloadRules(Integer groupID);

    /**
     * Download group rules.
     * 
     * @param idList
     *            the id list
     * @param isOnline
     *            the is online
     * @return the string builder
     */
    public StringBuilder downloadGroupRules(List<Integer> idList, boolean isOnline);

    /**
     * List rule by group.
     * 
     * @param groupID
     *            the group id
     * @return the list
     */
    public List<Rule> listRuleByGroup(Integer groupID);

    // TODO FIX IT
    /**
     * Clone group.
     * 
     * @param ruleList
     *            the rule list
     * @param groupId
     *            the group id
     * @return the string
     * @throws Exception
     *             the exception
     */
    public String cloneGroup(List<Rule> ruleList, Integer groupId) throws Exception;

    /**
     * Creates the group.
     * 
     * @param group
     *            the group
     * @return the integer
     */
    public Integer createGroup(Group group);

    /**
     * List from group type table.
     * 
     * @return the list
     */
    public List<GroupType> listFromGroupTypeTable();

    /**
     * Gets the group type.
     * 
     * @param groupName
     *            the group name
     * @return the group type
     */
    public Integer getGroupType(String groupName);

    // TODO FIX IT
    /**
     * Table attribute loader.
     * 
     * @return the list
     */
    public List<Map<String, Object>> tableAttributeLoader();
    
    /**
     * Gets the rule existing ruleset.
     *
     * @param ruleId the rule id
     * @return the rule existing ruleset
     */
    public List<Rule> getRuleExistingRulesets(Integer ruleId);
    
    /**
     * Checks if is group exists.
     *
     * @param groupName the group name
     * @return true, if is group exists
     */
    boolean isGroupExists(String groupName);
    
    /**
     * Gets the rule set id for groupdelete.
     *
     * @param groupId the group id
     * @return the rule set id for groupdelete
     */
    List<Integer> getRuleSetIdForGroupdelete(String groupId);
    
    /**
     * Delete rule before group delete.
     *
     * @param groupId the group id
     */
    void deleteRuleBeforeGroupDelete(String groupId);
    
    /**
     * Delete rule set rule mapping before group delete.
     *
     * @param ruleSetIdList the rule set id list
     */
    void deleteRuleSetRuleMappingBeforeGroupDelete(List<Integer> ruleSetIdList);
    
    /**
     * Check for rule and rule set count in group.
     *
     * @param groupID the group id
     * @return the list
     */
    List<Rule> checkForRuleAndRuleSetCountInGroup(Integer groupID);
    
    /**
     * Gets the download rules desc.
     *
     * @param downloadRuleList the download rule list
     * @param groupName the group name
     * @param isOnline the is online
     * @return the download rules desc
     */
    StringBuilder getDownloadRulesDesc(List<String> downloadRuleList, String groupName, boolean isOnline);

    /**
     * Gets the rules by group.
     *
     * @param groupId the group id
     * @return the rules by group
     */
    List<String> getRulesByGroup(Integer groupId);
}
