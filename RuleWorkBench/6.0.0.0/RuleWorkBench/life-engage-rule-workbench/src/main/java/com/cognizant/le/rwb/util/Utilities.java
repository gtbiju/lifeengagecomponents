/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 304003
 * @version       : 0.1, Mar 7, 2014
 */

package com.cognizant.le.rwb.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

/**
 * The Class class Utilities.
 * 
 * @author 304003
 */
public final class Utilities {
    /**
     * Method to get the current Date.
     * 
     * @return the current date
     */
    public static java.util.Date getCurrentDate() {
        Date date = Calendar.getInstance().getTime();
        return new java.util.Date(date.getTime());
    }

    /**
     * Sets the values.
     * 
     * @param preparedStatement
     *            the prepared statement
     * @param values
     *            the values
     * @throws SQLException
     *             the sQL exception
     */
    public static void setValues(final PreparedStatement preparedStatement, final Object[] values) throws SQLException {
        for (int i = 0; i < values.length; i++) {
            preparedStatement.setObject(i + 1, values[i]);
        }
    }

}
