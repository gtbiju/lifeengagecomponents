/**
 *
 * Copyright 2014, Cognizant 
 *
 * @author        : 304003
 * @version       : 0.1, Feb 28, 2013
 */

package com.cognizant.le.rwb.domain;

/**
 * The Class class GroupType.
 */
public class GroupType {
    /** The group type id. */
    private Integer groupTypeID;

    /** The rule group type. */
    private String groupType;
    
    /** The canonical json file name. */
    private String canonicalJsonFileName;

    /**
     * Gets the group type id.
     * 
     * @return the group type id
     */
    public Integer getGroupTypeID() {
        return groupTypeID;
    }

    /**
     * Sets the group type id.
     * 
     * @param groupTypeID
     *            the group type id to set.
     */
    public void setGroupTypeID(Integer groupTypeID) {
        this.groupTypeID = groupTypeID;
    }

    /**
     * Gets the group type.
     * 
     * @return the group type
     */
    public String getGroupType() {
        return groupType;
    }

    /**
     * Sets the group type.
     * 
     * @param groupType
     *            the group type to set.
     */
    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getCanonicalJsonFileName() {
        return canonicalJsonFileName;
    }

    public void setCanonicalJsonFileName(String canonicalJsonFileName) {
        this.canonicalJsonFileName = canonicalJsonFileName;
    }
}
