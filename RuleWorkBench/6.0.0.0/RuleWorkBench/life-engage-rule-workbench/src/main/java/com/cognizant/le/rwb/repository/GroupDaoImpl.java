/**
 *
 * � Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 15, 2014
 */

package com.cognizant.le.rwb.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.domain.Group;
import com.cognizant.le.rwb.domain.Rule;
import com.cognizant.le.rwb.domain.RuleSetRuleMapping;
import com.cognizant.le.rwb.services.GroupServiceImpl;
import com.cognizant.le.rwb.util.Utilities;

// TODO: Auto-generated Javadoc
/**
 * The Class class GroupDaoImpl.
 * 
 * @author 304003
 */
@Repository("groupDao")
@Transactional
public class GroupDaoImpl implements GroupDao {

    /** The jdbc template. */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /** The named parameter jdbc template. */
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    /** The environment. */
    @Autowired
    private Environment environment;

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupServiceImpl.class);

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#exportCompressedVersion(java.lang.Integer, java.util.List)
     */
    public List<Rule> exportCompressedVersion(final Integer groupId, List<String> ruleNames) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> exportCompressedVersion");

        StringBuilder sql = new StringBuilder();
        sql.append("select * from RULE_TABLE where RULENAME in(");
        for (int i = 0; i < ruleNames.size(); i++) {
            sql.append("?");
            if (i + 1 < ruleNames.size()) {
                sql.append(",");
            }
        }
        sql.append(") AND CATEGORY=? AND STATUS='A'");
        LOGGER.trace("downloadGroupRules ---> Constructed SQL --->" + sql.toString());

        Object[] params = new Object[ruleNames.size() + 1];
        /*
         * for (int i = 0; i < params.length; i++) { params[i] = downloadRuleList.iterator(); }
         */
        int i = 0;
        for (String ruleName : ruleNames) {
            params[i] = ruleName;
            i++;
        }
        params[ruleNames.size()] = groupId;
        /*
         * List<String> ruleDescList = jdbcTemplate.queryForList(sql.toString(), params, String.class);
         * 
         * for (String ruleDescValue : ruleDescList) { ruleDesc.append(ruleDescValue); }
         */

        final List<Rule> ruleList = jdbcTemplate.query(sql.toString(), params, new RowMapper<Rule>() {
            @Override
            public Rule mapRow(ResultSet rs, int rowNum) throws SQLException {
                final Rule rule = new Rule();
                rule.setRuleID(rs.getInt("ID"));
                rule.setRuleTypeID(rs.getInt("TYPE_ID"));
                rule.setRuleName(rs.getString("RULENAME"));
                rule.setVersion(rs.getFloat("VERSION"));
                rule.setClientId(rs.getString("CLIENTID"));
                ;
                rule.setRuleJson(rs.getString("RULEJSON"));
                rule.setOfflineRuleJS(rs.getString("OFFLINE_RULEDESC"));
                rule.setRuleJS(rs.getString("RULEDESC"));
                rule.setGroupName(rs.getString("CATEGORY"));
                rule.setComments(rs.getString("COMMENTS"));
                return rule;
            }

        });

        LOGGER.trace("Method Exit : GroupDaoImpl-> exportCompressedVersion");
        return ruleList;
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#exportUnCompressedRules(java.lang.Integer, java.util.List)
     */
    public List<Rule> exportUnCompressedRules(final Integer groupId, List<String> ruleNames) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> exportUnCompressedRules");
        StringBuilder sql = new StringBuilder();
        sql.append("select * from RULE_TABLE where RULENAME in(");
        for (int i = 0; i < ruleNames.size(); i++) {
            sql.append("?");
            if (i + 1 < ruleNames.size()) {
                sql.append(",");
            }
        }
        sql.append(") AND CATEGORY=? AND STATUS='A'");
        LOGGER.trace("downloadGroupRules ---> Constructed SQL --->" + sql.toString());

        Object[] params = new Object[ruleNames.size() + 1];
        /*
         * for (int i = 0; i < params.length; i++) { params[i] = downloadRuleList.iterator(); }
         */
        int i = 0;
        for (String ruleName : ruleNames) {
            params[i] = ruleName;
            i++;
        }
        params[ruleNames.size()] = groupId;
        /*
         * List<String> ruleDescList = jdbcTemplate.queryForList(sql.toString(), params, String.class);
         * 
         * for (String ruleDescValue : ruleDescList) { ruleDesc.append(ruleDescValue); }
         */

        final List<Rule> ruleList = jdbcTemplate.query(sql.toString(), params, new RowMapper<Rule>() {
            @Override
            public Rule mapRow(ResultSet rs, int rowNum) throws SQLException {
                final Rule rule = new Rule();
                rule.setRuleID(rs.getInt("ID"));
                rule.setRuleTypeID(rs.getInt("TYPE_ID"));
                rule.setRuleName(rs.getString("RULENAME"));
                rule.setVersion(rs.getFloat("VERSION"));
                rule.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
                rule.setUpdatedDate(rs.getTimestamp("UPDATED_DATE"));
                rule.setClientId(rs.getString("CLIENTID"));
                rule.setRuleJson(rs.getString("RULEJSON"));
                rule.setOfflineRuleJS(rs.getString("OFFLINE_RULEDESC"));
                rule.setRuleJS(rs.getString("RULEDESC"));
                rule.setGroupName(rs.getString("CATEGORY"));
                rule.setComments(rs.getString("COMMENTS"));
                rule.setUserId(rs.getString("USER_ID"));
                return rule;
            }

        });

        LOGGER.trace("Method Exit : GroupDaoImpl-> exportUnCompressedRules");
        return ruleList;
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#getGroupToExport(java.lang.Integer)
     */
    public Group getGroupToExport(Integer groupID) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> getGroupToExport");
        Group group =
                jdbcTemplate.queryForObject(environment.getProperty(GenericConstants.SELECT_GROUP_TO_EXPORT),
                        new Object[] { groupID }, new RowMapper<Group>() {
                            @Override
                            public Group mapRow(ResultSet rs, int rowNum) throws SQLException {
                                final Group group = new Group();
                                group.setGroupName(rs.getString("GROUP_NAME"));
                                group.setTypeID(rs.getInt("TYPE_ID"));
                                group.setComments(rs.getString("COMMENTS"));
                                return group;
                            }
                        });
        LOGGER.trace("Method Exit : GroupDaoImpl-> getGroupToExport");
        return group;

    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#createGroup(com.cognizant.le.rwb.domain.Group)
     */
    @Transactional(propagation=Propagation.REQUIRED)
    public final Integer createGroup(final Group group) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> createGroup");
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                PreparedStatement ps =
                        conn.prepareStatement(environment.getProperty(GenericConstants.INSERT_GROUP),
                                Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, group.getGroupName());
                ps.setInt(2, group.getTypeID());
                ps.setString(3, group.getComments());
                return ps;
            }
        }, holder);

        Integer groupId = holder.getKey().intValue();
        LOGGER.trace("Method Exit : GroupDaoImpl-> createGroup");
        return groupId;
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#createRuleAndRuleset(java.util.List, java.lang.Integer, java.lang.String)
     */
    @Transactional(propagation=Propagation.REQUIRED)
    public void createRuleAndRuleset(final List<Rule> ruleList, final Integer groupId, final String status, final String userName) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> createRuleAndRuleset");
        jdbcTemplate.batchUpdate(environment.getProperty(GenericConstants.INSERT_IMPORTED_RULE),
                new BatchPreparedStatementSetter() {

                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        Rule rule = ruleList.get(i);
                        ps.setString(1, rule.getRuleName());
                        ps.setInt(2, rule.getRuleTypeID());
                        ps.setTimestamp(3, new Timestamp(Utilities.getCurrentDate().getTime()));
                        ps.setFloat(4, rule.getVersion());
                        ps.setString(5, rule.getRuleJson());
                        ps.setString(6, rule.getRuleJS());
                        ps.setString(7, rule.getOfflineRuleJS());
                        ps.setString(8, groupId.toString());
                        ps.setString(9, rule.getClientId());
                        ps.setString(10, rule.getComments());
                        ps.setString(11, status);
                        ps.setString(12, userName);
                    }

                    @Override
                    public int getBatchSize() {
                        return ruleList.size();
                    }
                });
        LOGGER.trace("Method Exit : GroupDaoImpl-> createRuleAndRuleset");
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#createRuleAndRulesetHistory(java.util.List, java.lang.Integer, java.lang.String, java.lang.String)
     */
    public void createRuleAndRulesetHistory(final List<Rule> ruleList, final Integer groupId, final String status, final String userName) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> createRuleAndRuleset");
        jdbcTemplate.batchUpdate(environment.getProperty(GenericConstants.CREATE_RULE_RULESET_HISTORY),
                new BatchPreparedStatementSetter() {

                    @Override
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        Rule rule = ruleList.get(i);
                        ps.setString(1, rule.getRuleName());
                        ps.setInt(2, rule.getRuleTypeID());
                        ps.setTimestamp(3, new Timestamp(rule.getCreatedDate().getTime()));
                        ps.setTimestamp(4, (rule.getUpdatedDate() != null ?new Timestamp(rule.getUpdatedDate().getTime()):null));
                        ps.setFloat(5, rule.getVersion());
                        ps.setString(6, rule.getRuleJson());
                        ps.setString(7, rule.getRuleJS());
                        ps.setString(8, rule.getOfflineRuleJS());
                        ps.setString(9, groupId.toString());
                        ps.setString(10, rule.getClientId());
                        ps.setString(11, rule.getComments());
                        ps.setString(12, status);
                        ps.setString(13, rule.getUserId());
                        ps.setTimestamp(14, new Timestamp(Utilities.getCurrentDate().getTime()));
                    }

                    @Override
                    public int getBatchSize() {
                        return ruleList.size();
                    }
                });
        LOGGER.trace("Method Exit : GroupDaoImpl-> createRuleAndRuleset");
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#createRuleSetRuleMapping(java.util.List)
     */
    @Transactional(propagation=Propagation.REQUIRED)
    public final void createRuleSetRuleMapping(final List<RuleSetRuleMapping> ruleSetRuleMappingList) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> createRuleSetRuleMapping");
        int updateCount[] =
                jdbcTemplate.batchUpdate(environment.getProperty(GenericConstants.INSERT_RULESET_RULE_MAPPING),
                        new BatchPreparedStatementSetter() {

                            @Override
                            public void setValues(PreparedStatement ps, int index) throws SQLException {
                                RuleSetRuleMapping ruleSetRuleMapping = ruleSetRuleMappingList.get(index);
                                ps.setInt(1, ruleSetRuleMapping.getRuleSetID());
                                ps.setInt(2, ruleSetRuleMapping.getRuleID());
                            }

                            @Override
                            public int getBatchSize() {
                                return ruleSetRuleMappingList.size();
                            }
                        });
        if (updateCount.length != ruleSetRuleMappingList.size()) {
            LOGGER.error("RuleSet creation failed");
        }
        LOGGER.trace("Method Exit : GroupDao-> createRuleSetRuleMapping");
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#getRuleRuleSetMapping(java.lang.Integer, java.lang.Integer)
     */
    public List<Map<String, Object>> getRuleRuleSetMapping(final Integer groupID, final Integer rulesetId) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> getRuleRuleSetMapping");
        List<Map<String, Object>> selectedRulesWithMappingList =
                jdbcTemplate.queryForList(environment.getProperty(GenericConstants.SELECT_RULES_WITH_MAPPING_TOEXPORT),
                        new Object[] { groupID, rulesetId });
        LOGGER.trace("Method Exit : GroupDaoImpl-> getRuleRuleSetMapping");
        return selectedRulesWithMappingList;
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#getImportedRulesAndRulesets(java.lang.String)
     */
    @Transactional(propagation=Propagation.REQUIRED)
    public List<Rule> getImportedRulesAndRulesets(String groupID) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> getImportedRulesAndRulesets");
        final List<Rule> ruleList =
                jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_IMPORTED_RULE_RULESETS),
                        new Object[] { groupID }, new RowMapper<Rule>() {
                            @Override
                            public Rule mapRow(ResultSet rs, int rowNum) throws SQLException {
                                final Rule rule = new Rule();
                                rule.setRuleID(rs.getInt("ID"));
                                rule.setRuleTypeID(rs.getInt("TYPE_ID"));
                                rule.setRuleName(rs.getString("RULENAME"));
                                return rule;
                            }

                        });

        LOGGER.trace("Method Exit : GroupDaoImpl-> getImportedRulesAndRulesets");
        return ruleList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.GroupDao#isGroupExists(java.lang.String)
     */
    public boolean isGroupExists(String groupName) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> isGroupExists");
        boolean isExists = false;
        List<Map<String, Object>> list =
                jdbcTemplate.queryForList(environment.getProperty(GenericConstants.GROUP_EXISTS),
                        new Object[] { groupName });
        if (list.size() > 0) {
            isExists = true;
        }
        LOGGER.trace("Method Exit : GroupDaoImpl-> isGroupExists");
        return isExists;
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#getGroupId(java.lang.String)
     */
    public Integer getGroupId(String groupName) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> getGroupId");
        List<Integer> groupIdList =
                jdbcTemplate.queryForList(environment.getProperty(GenericConstants.GET_GROUP_ID),
                        new Object[] { groupName }, Integer.class);
        Integer groupId = null;
        if (groupIdList.size() > 0) {
            groupId = groupIdList.get(0).intValue();
        }
        LOGGER.trace("Method Exit : GroupDaoImpl-> getGroupId");
        return groupId;
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#getRulesByGroup(java.lang.Integer)
     */
    public List<String> getRulesByGroup(Integer groupId) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> getRulesByGroup");
        List<String> ruleNames =
                jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_RULENAME), new Object[] { groupId },
                        new RowMapper<String>() {
                            @Override
                            public String mapRow(ResultSet resultSet, int i) throws SQLException {
                                return resultSet.getString(1);
                            }
                        });
        LOGGER.trace("Method Exit : GroupDaoImpl-> getRulesByGroup");
        return ruleNames;

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.GroupDao#fetchCommonRules(java.util.List, java.lang.Integer)
     */
    public List<Rule> fetchCommonRules(List<String> commonRuleList, Integer groupId) {

        return exportUnCompressedRules(groupId, commonRuleList);
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#updateExistingRulesAndRulesets(java.util.List, java.lang.Integer)
     */
    public void updateExistingRulesAndRulesets(List<Rule> existingRuleList, Integer groupId) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> updateExistingRulesAndRulesets");
        for (Rule existingRule : existingRuleList) {
            final Object[] values =
                    { Integer.valueOf(existingRule.getRuleTypeID().toString()),
                            new Timestamp(Utilities.getCurrentDate().getTime()), existingRule.getRuleJson(),
                            existingRule.getRuleJS(),
                            (existingRule.getOfflineRuleJS() != null ? existingRule.getOfflineRuleJS() : null),
                            existingRule.getClientId(),
                            (existingRule.getComments() != null ? existingRule.getComments() : null),
                            existingRule.getRuleName(), String.valueOf(groupId) };
            jdbcTemplate.update(environment.getProperty(GenericConstants.UPDATE_EXISTING_RULE_AND_RULESET), values);
        }
        LOGGER.trace("Method Exit : GroupDaoImpl-> updateExistingRulesAndRulesets");

    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#deleteRuleSetRuleMapping(java.util.List)
     */
    public final void deleteRuleSetRuleMapping(final List<Integer> ruleSetIds) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> deleteRuleSetRuleMapping");
        Set<Integer> set = new HashSet<Integer>(ruleSetIds);
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("rulesetIds", set);
        namedParameterJdbcTemplate.update("DELETE FROM RULESET_RULEMAPPING WHERE RULESET_ID IN (:rulesetIds)",
                namedParameters);
        LOGGER.trace("Method Exit : GroupDaoImpl-> deleteRuleSetRuleMapping");

    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#createCompressedRules(com.cognizant.le.rwb.domain.Rule, java.lang.Integer, java.lang.String)
     */
    @Transactional(propagation=Propagation.REQUIRED)
    public void createCompressedRules(Rule rule, Integer groupId, String status, String userName) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> createCompressedRules");
        final Object[] values =
                { rule.getRuleName(), rule.getRuleTypeID(), new Timestamp(Utilities.getCurrentDate().getTime()),
                        rule.getVersion(), rule.getRuleJson(), rule.getRuleJS(), rule.getOfflineRuleJS(),
                        groupId.toString(), rule.getClientId(), rule.getComments(), status, userName };
        jdbcTemplate.update(environment.getProperty(GenericConstants.INSERT_IMPORTED_RULE), values);
        LOGGER.trace("Method Exit : GroupDaoImpl-> createCompressedRules");
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#createCompressedRulesHistory(com.cognizant.le.rwb.domain.Rule, java.lang.Integer, java.lang.String, java.lang.String)
     */
    public void createCompressedRulesHistory(Rule rule, Integer groupId, String status, String userName) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> createCompressedRules");
        final Object[] values =
                { rule.getRuleName(), rule.getRuleTypeID(), new Timestamp(rule.getCreatedDate().getTime()), (rule.getUpdatedDate() != null ? new Timestamp(rule.getUpdatedDate().getTime()): null),
                        rule.getVersion(), rule.getRuleJson(), rule.getRuleJS(), rule.getOfflineRuleJS(),
                        groupId.toString(), rule.getClientId(), rule.getComments(), status, userName, new Timestamp(Utilities.getCurrentDate().getTime())};
        jdbcTemplate.update(environment.getProperty(GenericConstants.CREATE_RULE_RULESET_HISTORY), values);
        LOGGER.trace("Method Exit : GroupDaoImpl-> createCompressedRules");
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.repository.GroupDao#fetchCompressedRule(java.lang.Integer)
     */
    public final Rule fetchCompressedRule(final Integer groupId) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> fetchCompressedRule");
        Rule rule =
                jdbcTemplate.queryForObject(environment.getProperty(GenericConstants.SELECT_RULES_BY_GROUPID),
                        new Object[] { groupId.toString() }, new RowMapper<Rule>() {
                            @Override
                            public Rule mapRow(ResultSet rs, int rowNum) throws SQLException {
                                final Rule rule = new Rule();
                                rule.setRuleID(rs.getInt("ID"));
                                rule.setRuleTypeID(rs.getInt("TYPE_ID"));
                                rule.setRuleName(rs.getString("RULENAME"));
                                rule.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
                                rule.setUpdatedDate(rs.getTimestamp("UPDATED_DATE"));
                                rule.setClientId(rs.getString("CLIENTID"));
                                rule.setVersion(rs.getFloat("VERSION"));
                                rule.setRuleJson(rs.getString("RULEJSON"));
                                rule.setRuleJS(rs.getString("RULEDESC"));
                                rule.setOfflineRuleJS(rs.getString("OFFLINE_RULEDESC"));
                                rule.setComments(rs.getString("COMMENTS"));
                                rule.setUserId(rs.getString("USER_ID"));
                                return rule;
                            }
                        });
        LOGGER.trace("Method Exit : GroupDaoImpl-> fetchCompressedRule");
        return rule;

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.GroupDao#updateCompressedRules(com.cognizant.le.rwb.domain.Rule,
     * java.lang.Integer)
     */
    public void updateCompressedRules(Rule rule, Integer groupId) {
        LOGGER.trace("Method Entry : GroupDaoImpl-> updateCompressedRules");
        final Object[] values =
                { Integer.valueOf(rule.getRuleTypeID().toString()),
                        new Timestamp(Utilities.getCurrentDate().getTime()), rule.getRuleJson(), rule.getRuleJS(),
                        (rule.getOfflineRuleJS() != null ? rule.getOfflineRuleJS() : null), rule.getClientId(),
                        (rule.getComments() != null ? rule.getComments() : null), String.valueOf(groupId) };
        jdbcTemplate.update(environment.getProperty(GenericConstants.UPDATE_COMPRESSED_RULES), values);
        LOGGER.trace("Method Exit : GroupDaoImpl-> updateCompressedRules");
    }
}
