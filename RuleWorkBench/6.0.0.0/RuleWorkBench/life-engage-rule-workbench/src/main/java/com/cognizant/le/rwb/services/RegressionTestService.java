/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 418964
 * @version       : 0.1, May 30, 2014
 */

package com.cognizant.le.rwb.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cognizant.le.rwb.domain.JobDetails;

/**
 * The Interface interface RegressionTestService.
 * 
 */
public interface RegressionTestService {

	public Boolean executeRegressionTest(String fileName, String categoryId,
			String ruleName, String jobName);

	public String uploadExcelFile(MultipartFile file);

	public List<JobDetails> getJobDetails(int userId);

	public void updateJobDetails(Long jobId, String ruleName, String jobStatus,
			String filePath, String errorMsg);

	public void createJobDetails(int userId, Long jobId, String ruleName,
			String jobStatus, String filePath, String comment, String errorMsg);

}
