/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 7, 2014
 */
package com.cognizant.le.rwb.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.domain.User;

/**
 * The Class class UserDaoImpl.
 */
@Repository("userDao")
public class UserDaoImpl implements UserDao {

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    /** The jdbc template. */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /** The environment. */
    @Autowired
    private Environment environment;

    /**
     * Method used to validate user.
     * 
     * @param userName
     *            the user name
     * @param password
     *            the password
     * @return the boolean
     */
    public final Boolean validateUser(final String userName, final String password) {
        LOGGER.debug("Method Entry : UserDAO-> validateUser");
        Boolean validUser = false;
        String validateUserSQL = environment.getProperty(GenericConstants.VALIDATE_USER_SQL);
        List<Map<String, Object>> rows =
                jdbcTemplate.queryForList(validateUserSQL, new Object[] { userName, password });
        if (rows.size() > 0) {
            validUser = true;
        }
        LOGGER.debug("Method Exit : UserDAO-> validateUser");
        return validUser;
    }
    /**
     * Method used to get user id.
     * 
     * @param userName
     *            the user name
     * @param password
     *            the password
     * @return the user
     */
    public final User getUserId(final String userName, final String password) {
        LOGGER.debug("Method Entry : UserDAO-> getUserId");
        String validateUserSQL = environment.getProperty(GenericConstants.VALIDATE_USER_SQL);
        User user =
                jdbcTemplate.queryForObject(validateUserSQL, new Object[] { userName, password }, new RowMapper<User>() {
                    @Override
                    public User mapRow(ResultSet rs, int rowNo) throws SQLException {
                        final User user = new User();
                        user.setUserID(rs.getInt("USERID"));
                        user.setUserName(rs.getString("USERNAME"));
                        return user;
                    }

                });
        LOGGER.debug("Method Exit : UserDAO-> getUserId");
        return user;
    }
}
