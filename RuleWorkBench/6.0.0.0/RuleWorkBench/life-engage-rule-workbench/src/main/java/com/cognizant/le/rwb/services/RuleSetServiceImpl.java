/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 7, 2014
 */

package com.cognizant.le.rwb.services;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.le.rwb.domain.Rule;
import com.cognizant.le.rwb.domain.RuleSet;
import com.cognizant.le.rwb.domain.RuleSetRuleMapping;
import com.cognizant.le.rwb.repository.RuleDao;
import com.cognizant.le.rwb.repository.RuleSetDao;

/**
 * The Class class RuleSetServiceImpl.
 * 
 * @author 304003
 */
@Service("ruleSetService")
public class RuleSetServiceImpl implements RuleSetService {

    /** The rule set dao. */
    @Autowired
    private RuleSetDao ruleSetDao;

    /** The rule dao. */
    @Autowired
    private RuleDao ruleDao;

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RuleSetServiceImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleSetService#createRuleSet(com.cognizant .le.rwb.domain.Rule,
     * java.util.List)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public Map<String, String> createRuleSet(Rule rule, List<RuleSetRuleMapping> ruleSetRuleMappingList) {
        String result = "Ruleset already exists.";
        boolean isRuleSetExists = true;
        Map<String, String> resultMap = new HashMap<String, String>();
        Integer ruleSetId = ruleSetDao.createRuleSet(rule);
        if (null != ruleSetId) {
            ruleSetDao.createRuleSetRuleMapping(ruleSetId, ruleSetRuleMappingList);
            result = "Ruleset successfully created";
            isRuleSetExists = false;
        }
        
        resultMap.put("result", result);
        resultMap.put("isRuleSetExists", String.valueOf(isRuleSetExists));
        return resultMap;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleSetService#listRuleSetForUpdate(java .lang.Integer)
     */
    @Override
    public List<RuleSet> listRuleSetForUpdate(Integer ruleSetID) {
        return ruleSetDao.listRuleSetForUpdate(ruleSetID);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleSetService#listAssociatedRules(java .lang.Integer)
     */
    @Override
    public List<Rule> listAssociatedRules(Integer ruleSetID) {
        return ruleSetDao.listAssociatedRules(ruleSetID);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleSetService#editRuleSet(com.cognizant .le.rwb.domain.Rule, java.util.List)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public Map<String,String> editRuleSet(Rule rule, List<RuleSetRuleMapping> ruleSetRuleMappingList, boolean isRuleSetNameChanged) {
        boolean isRuleSetExists = false;
        Map<String, String> resultMap = new HashMap<String, String>();
        String result = "Ruleset Updated Successfully";
        if (isRuleSetNameChanged && ruleSetDao.isRuleSetExists(rule)) {
            result = "Ruleset already exists.";
            isRuleSetExists = true;
        } else {
            ruleSetDao.editRuleSet(rule);
            ruleSetDao.deleteRuleSetRuleMapping(rule.getRuleID());
            ruleSetDao.createRuleSetRuleMapping(rule.getRuleID(), ruleSetRuleMappingList);
        }
        resultMap.put("result", result);
        resultMap.put("isRuleSetExists", String.valueOf(isRuleSetExists));
        return resultMap;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleSetService#addRuleSetJson(java.lang .Integer, java.lang.String)
     */
    @Override
    public String addRuleSetJson(Integer ruleId, String combinedJSONString, Map<String, List<String>> canonicalJson) {
        HashMap<String, HashSet<String>> setMap = new HashMap<String, HashSet<String>>();
        HashMap<String,HashSet<JSONObject>>  standardInputMap = new HashMap<String,HashSet<JSONObject>>();
        setMap.put("SingleInput", new HashSet<String>());
        setMap.put("MultiInput", new HashSet<String>());
        setMap.put("SingleOutput", new HashSet<String>());
        setMap.put("MultiOutput", new HashSet<String>());
        standardInputMap.put("StandardInput",  new HashSet<JSONObject>());
        String jsonString = getRuleJson(ruleId).get("rulejson");
        
        JSONObject jsonObj = new JSONObject();
        JSONObject combJsonObj = new JSONObject();
        JSONParser jsonParser = new JSONParser();
        try {
            jsonObj = (JSONObject) jsonParser.parse(jsonString); 
            if (null != combinedJSONString) {
                combJsonObj = (JSONObject) jsonParser.parse(combinedJSONString);
                getVariables(combJsonObj, setMap, standardInputMap, canonicalJson);
            }
            getVariables(jsonObj, setMap,standardInputMap, canonicalJson);
            if (jsonObj.containsKey("Variables")) {
                jsonObj.remove("Variables");
            }
            if (jsonObj.containsKey("Rules")) {
                jsonObj.remove("Rules");
            }
            
            JSONObject variableJsonObj = new JSONObject();
            variableJsonObj.putAll(setMap);
            variableJsonObj.putAll(standardInputMap);
            jsonObj.put("Variables", variableJsonObj);
            
            LOGGER.trace("++++++++++++++++++" + jsonObj.toJSONString());
        } catch (ParseException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return jsonObj.toJSONString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleSetService#getRuleJson(java.lang.Integer)
     */
    @Override
    public Map<String, String> getRuleJson(Integer ruleId) {
        LOGGER.trace("Method Entry : RuleService-> getRuleJson");
        Rule rule = ruleDao.listRule(ruleId);
        String ruleJson = null != rule.getRuleJson() ? rule.getRuleJson() : "";
        String ruleName = null != rule.getRuleName() ? rule.getRuleName() : "";
        String ruleJS = null != rule.getRuleJS() ? rule.getRuleJS() : "";
        Map<String, String> ruleMap = new HashMap<String, String>();
        ruleMap.put("rulename", ruleName);
        ruleMap.put("rulejson", ruleJson);
        ruleMap.put("rulejs", ruleJS);
        return (ruleMap);
    }

    /**
     * Gets the variables.
     * 
     * @param jsonObj
     *            the json obj
     * @param setMap
     *            the set map
     * @return the variables
     */
    private Map<String, HashSet<String>> getVariables(JSONObject jsonObj, HashMap<String, HashSet<String>> setMap,  HashMap<String,HashSet<JSONObject>>  standardInputMap, Map<String, List<String>> canonicalJson) throws ParseException {
        if (null != jsonObj.get("Variables")) {
            JSONObject variables = (JSONObject) jsonObj.get("Variables");
            if (null != variables.get("MultiOutput")) {
                updateSet("MultiOutput", variables, setMap);
            }
            if (null != variables.get("SingleInput")) {
                updateSet("SingleInput", variables, setMap);
            }
            if (null != variables.get("MultiInput")) {
                updateSet("MultiInput", variables, setMap);

            }
            if (null != variables.get("SingleOutput")) {
                updateSet("SingleOutput", variables, setMap);
            }
            if (null != variables.get("StandardInput")) {
                updateSetForStandaraInputs("StandardInput", variables, standardInputMap,canonicalJson);
            }
        }

        return null;

    }

    /**
     * Update set.
     * 
     * @param key
     *            the key
     * @param variables
     *            the variables
     * @param setMap
     *            the set map
     * @return the map
     */
    private Map<String, HashSet<String>> updateSet(String key, JSONObject variables,
            HashMap<String, HashSet<String>> setMap) {
        JSONArray singleIp = (JSONArray) variables.get(key);
        HashSet<String> tempSet = setMap.get(key);
        Iterator<String> iter = singleIp.iterator();
        while (iter.hasNext()) {
            String entry = (String) iter.next();
            if (entry != "") {
                tempSet.add("\"" + entry + "\"");
            }
        }
        tempSet.remove("");
        return setMap;

    }
    
    /**
     * Update set for standara inputs.
     *
     * @param key the key
     * @param variables the variables
     * @param standardInputMap the standard input map
     * @param canonicalJson the canonical json
     * @throws ParseException the parse exception
     */
    private void updateSetForStandaraInputs(String key, JSONObject variables, HashMap<String,HashSet<JSONObject>>  standardInputMap,
            Map<String, List<String>> canonicalJson) throws ParseException {
        JSONArray singleIp = (JSONArray) variables.get(key);
        List<String> singleInputVariables = canonicalJson.get("SingleInput");
        List<String> multiInputVariables = canonicalJson.get("MultiInput");
        HashSet<JSONObject> canonnicalVarSet = standardInputMap.get(key);
        Iterator canonicalVariableItr =  canonnicalVarSet.iterator();
        Map<String,String> canonicalVariableMap  = null;
        
        if(canonicalVariableItr.hasNext()) {
             canonicalVariableMap  =  (Map<String, String>) canonicalVariableItr.next();
        } else {
            canonicalVariableMap = new HashMap<String, String>();
        }
        if(null != singleIp && singleIp.size() > 0 && !singleIp.isEmpty()){
            if (singleIp.get(0) instanceof String) {
                Iterator<String> iter = singleIp.iterator();
                while (iter.hasNext()) {
                    String entry = (String) iter.next();
                    if (!("").equals(entry)) {
                        if(singleInputVariables.contains(entry.trim())){
                            canonicalVariableMap.put( entry.trim(), "SingleInput");  
                        } else if(multiInputVariables.contains(entry.trim())){
                            canonicalVariableMap.put( entry.trim() , "MultiInput" ); 
                        }
                    }
                }
            } else if (singleIp.get(0) instanceof Object) {
                JSONObject singleIpJsonObj = (JSONObject) singleIp.get(0);
                for(Iterator iterator = singleIpJsonObj.keySet().iterator(); iterator.hasNext();) {
                    String standardInpkey = (String) iterator.next();
                    if(standardInpkey != null && !("").equals(standardInpkey)){
                        String standardInpvalue = (String) singleIpJsonObj.get(standardInpkey);
                        canonicalVariableMap.put( standardInpkey ,standardInpvalue ); 
                    }
                }
            }
                JSONObject  canonicalVariableMapJson= new JSONObject();
                canonicalVariableMapJson.putAll(canonicalVariableMap);
                canonnicalVarSet.clear();
                canonnicalVarSet.add(canonicalVariableMapJson);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleSetService#deleteRuleSet(java.lang. Integer)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public void deleteRuleSet(Integer ruleSetId) {
        ruleSetDao.deleteRuleSet(ruleSetId);
        ruleSetDao.deleteRuleSetRuleMapping(ruleSetId);

    }

}
