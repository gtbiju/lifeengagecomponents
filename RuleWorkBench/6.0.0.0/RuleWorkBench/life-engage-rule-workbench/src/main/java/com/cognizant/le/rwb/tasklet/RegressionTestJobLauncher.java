/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 418964
 * @version       : 0.1, May 30, 2014
 */
package com.cognizant.le.rwb.tasklet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.exception.ServiceException;
import com.cognizant.le.rwb.services.RegressionTestService;

/**
 * The Class class RegressionTestJobLauncher.
 * 
 */
public class RegressionTestJobLauncher implements Tasklet,
		StepExecutionListener {

	@Autowired
	private RegressionTestService regressionTestService;

	/** The logger. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RegressionTestJobLauncher.class);

	private String categoryId;

	private String ruleName;

	private String file;

	private String userId;

	private String comment;

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * executes the batch job
	 * 
	 * @param arg0
	 *            the Step Contribution
	 * @param chunkContext
	 *            the chunk Context
	 */
	public RepeatStatus execute(StepContribution arg0, ChunkContext chunkContext) {

		LOGGER.debug("Method Entry : RegressionTestJobLauncher-> execute");
		String filePath = file;
		// calls the method which processes the excel input data and call the
		// rule engine with this data
		try {
			Boolean errorFlag = regressionTestService.executeRegressionTest(
					file, categoryId, ruleName, filePath);
			// set error flag in job execution so that it can be retrieved in
			// controller call
			chunkContext.getStepContext().getStepExecution().getJobExecution()
					.getExecutionContext()
					.putString("errorFlag", errorFlag.toString());
		} catch (ServiceException e) {
			// retrieving error message and set it in job execution so that it can
			// be retrieved in controller call
			String errorMsg = ((ServiceException) e).getDescription();
			chunkContext.getStepContext().getStepExecution().getJobExecution()
					.getExecutionContext().putString("errorMsg", errorMsg);
			LOGGER.error("Service Exception while calling rule engine", e);
			throw new ServiceException(GenericConstants.SERVICE_EXCEPTION,
					GenericConstants.EXCEPTION_MESSAGE);
		} catch (Exception e) {
			// retrieving error message and set it in job execution so that it can
			// be retrieved in controller call
			String errorMsg = e.getMessage();
			chunkContext.getStepContext().getStepExecution().getJobExecution()
					.getExecutionContext().putString("errorMsg", errorMsg);
			LOGGER.error("Error while calling rule engine", e);
			throw new ServiceException(GenericConstants.SERVICE_EXCEPTION,
					GenericConstants.EXCEPTION_MESSAGE);
		}
		LOGGER.debug("Method Exit : RegressionTestJobLauncher-> execute");
		return RepeatStatus.FINISHED;
	}

	/**
	 * performs before each step
	 * 
	 * @param stepExecution
	 *            the Step Contribution
	 */
	@Override
	public void beforeStep(StepExecution stepExecution) {
		LOGGER.debug("Method Entry : RegressionTestJobLauncher-> beforeStep");
		JobExecution jobExecution = stepExecution.getJobExecution();
		Long jobId = jobExecution.getJobId();
		// persisting job details
		regressionTestService.createJobDetails(Integer.parseInt(userId), jobId,
				ruleName, "RUNNING", "", comment, "");
		LOGGER.debug("Method Exit : RegressionTestJobLauncher-> beforeStep");
	}

	/**
	 * performs after each step
	 * 
	 * @param stepExecution
	 *            the Step Contribution
	 */
	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		return null;
	}

}
