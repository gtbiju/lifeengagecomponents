/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 10, 2014
 */

package com.cognizant.le.rwb.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.cognizant.le.rwb.exception.ErrorDetail;
import com.cognizant.le.rwb.exception.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class class ServiceErrorHandler.
 */
@ControllerAdvice
@Order(1)
public class ServiceErrorHandler {

    /** The message source. */
    private MessageSource messageSource;

    /**
     * Instantiates a new service validation error handler.
     * 
     * @param messageSource
     *            the message source
     */
    @Autowired
    public ServiceErrorHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    

    /**
     * Process validation error.
     *
     * @param ex the ex
     * @return the error detail
     */
    @ExceptionHandler(ServiceException.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    @ResponseBody
    public ErrorDetail processValidationError(ServiceException ex) {
    	 ErrorDetail dto = new ErrorDetail("ServiceError", ex.getCode(), ex.getDescription());
    	
        return dto;
    }



}
