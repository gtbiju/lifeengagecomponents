/**
 * 
 */
package com.cognizant.le.rwb.repository;

import java.util.List;

import com.cognizant.le.rwb.domain.Rule;
import com.cognizant.le.rwb.domain.RuleSet;
import com.cognizant.le.rwb.domain.RuleSetRuleMapping;

/**
 * The Interface interface RuleSetDao.
 * 
 * @author 304003
 */
public interface RuleSetDao {

    /**
     * Creates the rule set.
     * 
     * @param ruleSet
     *            the rule set
     * @return the integer
     */
    public Integer createRuleSet(Rule ruleSet);

    /**
     * Creates the rule set rule mapping.
     * 
     * @param ruleSetId
     *            the rule set id
     * @param ruleSetRuleMappingList
     *            the rule set rule mapping list
     */
    public void createRuleSetRuleMapping(Integer ruleSetId, List<RuleSetRuleMapping> ruleSetRuleMappingList);

    /**
     * List rule set for update.
     * 
     * @param ruleSetID
     *            the rule set id
     * @return the list
     */
    public List<RuleSet> listRuleSetForUpdate(Integer ruleSetID);

    /**
     * List associated rules.
     * 
     * @param ruleSetID
     *            the rule set id
     * @return the list
     */
    public List<Rule> listAssociatedRules(Integer ruleSetID);

    /**
     * Edits the rule set.
     * 
     * @param ruleSet
     *            the rule set
     */
    public void editRuleSet(Rule ruleSet);

    /**
     * Delete rule set rule mapping.
     * 
     * @param ruleSetId
     *            the rule set id
     */
    public void deleteRuleSetRuleMapping(Integer ruleSetId);

    /**
     * Delete rule set.
     * 
     * @param ruleSetId
     *            the rule set id
     */
    public void deleteRuleSet(Integer ruleSetId);

    /**
     * Checks if is rule set exists.
     * 
     * @param rule
     *            the rule
     * @return true, if is rule set exists
     */
    public boolean isRuleSetExists(Rule rule);

}
