/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Feb 14, 2014
 */
package com.cognizant.le.rwb.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.cognizant.le.rwb.domain.User;
import com.cognizant.le.rwb.services.UserService;

/**
 * The Class class LoginController.
 * 
 * @author 300797
 */
@Controller
public class LoginController {

    /** The user service. */
    @Autowired
    private UserService userService;

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    /**
     * Login.
     * 
     * @return the model and view
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login() {

        ModelAndView model = new ModelAndView("login", "user", new User());

        return model;

    }

    /**
     * Login.
     * 
     * @param user
     *            the user
     * @param bindingResult
     *            the binding result
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the model and view
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView login(@ModelAttribute("user") User user, BindingResult bindingResult,
            HttpServletRequest request, HttpServletResponse response) {
        try {
            // Using Spring ValidationUtils class to check for empty fields.
            // This will add the error if any in the bindingResult object.
            ValidationUtils.rejectIfEmptyOrWhitespace(bindingResult, "userName", "userName",
                    "Username can not be empty.");
            ValidationUtils.rejectIfEmptyOrWhitespace(bindingResult, "password", "password", "Password cannot be empty");

            if (bindingResult.hasErrors()) {
                // returning the errors on same page if any errors..
                return new ModelAndView("login", "user", user);
            } else {
                HttpSession session = request.getSession(true);
                // If the user details is validated then redirecting the user to
                // success page,
                // else returning the error message on login page.
                if (userService.validateUser(user)) {
                	//retrieving user id and setting in session
                	user = userService.getUserId(user);
                    session.setAttribute("username", user.getUserName());
                    session.setAttribute("userId", user.getUserID());
                    // Creating a redirection view to success page. This will
                    ModelAndView mav = new ModelAndView(new RedirectView("jsp/listBusinessRule.jsp", true));
                    mav.addObject("ruleTypeId", "1");
                    mav.addObject("ruleSetTypeId", "1");
                    return mav;
                } else {
                    bindingResult.addError(new ObjectError("Invalid", "Invalid UserName or Password"));
                    return new ModelAndView("login", "user", user);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new ModelAndView("login", "user", user);
        }
    }
}
