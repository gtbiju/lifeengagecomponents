/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 298738
 * @version       : 0.1, Apr 11, 2013
 */
package com.cognizant.le.rwb.core.config;

import java.beans.PropertyVetoException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.cognizant.icr.rule.v2.DbRuleEngineImpl;
import com.cognizant.icr.rule.v2.RuleEngine;
import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * The Class class WebConfig.
 */
@Configuration
@EnableWebMvc
@ImportResource({ "/WEB-INF/servlet-context.xml" })
@ComponentScan(basePackages = { "com.cognizant.le.rwb" })
@PropertySource({ "classpath:/le-rule-workbench.properties", "classpath:/sqlScriptResources.properties" })
public class WebConfig extends WebMvcConfigurerAdapter {

    /** The environment. */
    @Autowired
    private Environment environment;

    /**
     * Data source.
     * 
     * @return the data source
     * @throws PropertyVetoException
     *             the exception
     */
    @Bean(destroyMethod = "close")
    public DataSource dataSource() throws PropertyVetoException {

        final ComboPooledDataSource dataSoucre = new ComboPooledDataSource();
        dataSoucre.setDriverClass(environment.getRequiredProperty("le.rw.app.jdbc.driverClassName"));
        dataSoucre.setJdbcUrl(environment.getRequiredProperty("le.rw.app.jdbc.url"));
        dataSoucre.setUser(environment.getRequiredProperty("le.rw.app.jdbc.username"));
        dataSoucre.setPassword(environment.getRequiredProperty("le.rw.app.jdbc.password"));
        dataSoucre.setAcquireIncrement(5);
        dataSoucre.setIdleConnectionTestPeriod(60);
        dataSoucre.setMaxPoolSize(100);
        dataSoucre.setMaxStatements(50);
        dataSoucre.setMinPoolSize(10);
        return dataSoucre;
    }

    /**
     * Jdbc template.
     * 
     * @return the jdbc template
     * @throws PropertyVetoException
     *             the property veto exception
     */
    @Bean
    public JdbcTemplate jdbcTemplate() throws PropertyVetoException {
        final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource());
        return jdbcTemplate;
    }
    
    /**
     * Named parameter jdbc template.
     *
     * @return the named parameter jdbc template
     * @throws PropertyVetoException the property veto exception
     */
    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate() throws PropertyVetoException {
        final NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource());
        return namedParameterJdbcTemplate;
    }

    /**
     * Rule engine.
     * 
     * @return the rule engine
     * @throws PropertyVetoException
     *             the property veto exception
     */
    @Bean
    public RuleEngine ruleEngine() throws PropertyVetoException {
        final RuleEngine ruleEngine = new DbRuleEngineImpl(dataSource());
        return ruleEngine;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
     * #addViewControllers(org.springframework.web.servlet.config.annotation. ViewControllerRegistry)
     */
    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/index.html").setViewName("index");
        registry.addViewController("/login.html").setViewName("login");
    }

    /**
     * View resolver.
     * 
     * @return the internal resource view resolver
     */
    @Bean
    public InternalResourceViewResolver viewResolver() {
        final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/jsp/");
        resolver.setSuffix(".jsp");
        resolver.setOrder(1);
        return resolver;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
     * #addResourceHandlers(org.springframework.web.servlet.config.annotation. ResourceHandlerRegistry)
     */
    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
        registry.addResourceHandler("/css/**").addResourceLocations("/resources/css/");
        registry.addResourceHandler("/images/**").addResourceLocations("/resources/images/");
        registry.addResourceHandler("/lib/**").addResourceLocations("/resources/lib/");
        registry.addResourceHandler("/controller/**").addResourceLocations("/resources/controller/");
        registry.addResourceHandler("/directive/**").addResourceLocations("/resources/directive/");
        registry.addResourceHandler("/template/**").addResourceLocations("/resources/template/");
        registry.addResourceHandler("/jsp/css/**").addResourceLocations("/resources/css/");
        registry.addResourceHandler("/jsp/images/**").addResourceLocations("/resources/images/");
        registry.addResourceHandler("/jsp/lib/**").addResourceLocations("/resources/lib/");
        registry.addResourceHandler("/jsp/controller/**").addResourceLocations("/resources/controller/");
        registry.addResourceHandler("/jsp/directive/**").addResourceLocations("/resources/directive/");
        registry.addResourceHandler("/jsp/template/**").addResourceLocations("/resources/template/");
    }
	/**
	 * Multipart Resolver.
	 * 
	 * @return the multipartResolver
	 */
	@Bean
	public org.springframework.web.multipart.commons.CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		return multipartResolver;
	}

}
