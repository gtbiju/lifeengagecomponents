/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 7, 2014
 */

package com.cognizant.le.rwb.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.domain.Group;
import com.cognizant.le.rwb.domain.Rule;
import com.cognizant.le.rwb.domain.RuleSetRuleMapping;
import com.cognizant.le.rwb.repository.GroupDao;

// TODO: Auto-generated Javadoc
/**
 * The Class class GroupServiceImpl.
 * 
 * @author 304003
 */
@Service("groupService")
public class GroupServiceImpl implements GroupService {

    /** The group dao. */
    @Autowired
    private GroupDao groupDao;

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupServiceImpl.class);

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.services.GroupService#exportCompressedGroup(java.lang.Integer, java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public Map<String, Object> exportCompressedGroup(final Integer groupID, final List<String> ruleNames, final String ruleName) {
        LOGGER.trace("Method Entry : GroupServiceImpl-> exportCompressedGroup");
    	Map<String, Object> compressedGPMap = new HashMap<String, Object>();
        List<Rule> ruleList = groupDao.exportCompressedVersion(groupID,ruleNames);
        
        StringBuilder onlineRuleDesc = new StringBuilder();
        StringBuilder offlineRuleDesc = new StringBuilder();
        for (Rule rule : ruleList) {
            onlineRuleDesc.append(rule.getRuleJS());
            if (rule.getOfflineRuleJS() != null) {
                offlineRuleDesc.append(rule.getOfflineRuleJS());
            }
        }
        
        Rule ruleObj = new Rule();
        ruleObj.setRuleTypeID(1);
        ruleObj.setVersion(GenericConstants.VERSION);
        ruleObj.setRuleName(ruleName);
        ruleObj.setRuleJS(onlineRuleDesc.toString());
        ruleObj.setOfflineRuleJS(offlineRuleDesc.toString());
        ruleObj.setStatus(GenericConstants.STATUS_ACTIVE);
        ruleObj.setClientId(ruleList.get(0).getClientId());
        
        Group group = groupDao.getGroupToExport(groupID);        
        compressedGPMap.put("ruleObj", ruleObj);
        compressedGPMap.put("group", group);
        LOGGER.trace("Method Exit : GroupServiceImpl-> exportCompressedGroup");
        return compressedGPMap;
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.services.GroupService#exportUnCompressedGroup(java.lang.Integer, java.util.List)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public Map<String, Object> exportUnCompressedGroup(final Integer groupID, final List<String> ruleNames) {
        LOGGER.trace("Method Entry : GroupServiceImpl-> exportUnCompressedGroup");
        Map<String, Object> uncompressedGPMap = new HashMap<String, Object>();
        Map<String, List<String>> rulesetRuleMap = new LinkedHashMap<String, List<String>>();
        List<Rule> ruleList = groupDao.exportUnCompressedRules(groupID, ruleNames);
        Group group = groupDao.getGroupToExport(groupID);
        uncompressedGPMap.put("ruleList", ruleList);
        uncompressedGPMap.put("group", group);
        for (Rule rule : ruleList) {
            if (rule.getRuleTypeID() == 3) {
                exportRulesetRuleMapping(groupID, rule.getRuleID(), rulesetRuleMap);
            }

        }

        uncompressedGPMap.put("rulesetRuleMap", rulesetRuleMap);
        LOGGER.trace("Method Exit : GroupServiceImpl-> exportUnCompressedGroup");
        return uncompressedGPMap;
    }

    /**
     * Export ruleset rule mapping.
     *
     * @param groupID the group id
     * @param rulesetId the ruleset id
     * @param rulesetRuleMap the ruleset rule map
     * @return the map
     */
    private Map<String, List<String>> exportRulesetRuleMapping(final Integer groupID, final Integer rulesetId,
            final Map<String, List<String>> rulesetRuleMap) {
        LOGGER.trace("Method Entry : GroupServiceImpl-> exportRulesetRuleMapping");
        List<Map<String, Object>> ruleRuleSetMapList = groupDao.getRuleRuleSetMapping(groupID, rulesetId);
        String ruleSetName = null;
        String ruleName = null;
        List<String> ruleNameList = null;

        for (Map<String, Object> ruleRulesetMap : ruleRuleSetMapList) {

            if (Integer.valueOf(ruleRulesetMap.get("TYPE_ID").toString()) == 3) {
                ruleSetName = ruleRulesetMap.get("RULENAME").toString();
                ruleNameList = new ArrayList<String>();
            } else {
                ruleName = ruleRulesetMap.get("RULENAME").toString();
                ruleNameList.add(ruleName);
            }

        }
        rulesetRuleMap.put(ruleSetName, ruleNameList);
        LOGGER.trace("Method Exit : GroupServiceImpl-> exportRulesetRuleMapping");
        return rulesetRuleMap;
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.services.GroupService#importGroup(java.util.Map, java.util.List, java.util.Map)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class },propagation=Propagation.REQUIRED)
    public Map<String,Object> importGroup(Map<String, Object> groupMap, List<Object> ruleList,
            Map<String, List<String>> rulesetRulemap, Object rule, boolean isCompressed, String userName) {
        LOGGER.trace("Method Entry : GroupServiceImpl-> importGroup");
        Map<String,Object> resultMap = new HashMap<String,Object>();
        Integer groupId = null;
        if(isCompressed) {
            groupId = groupDao.createGroup(createGroupObject(groupMap));
            groupDao.createCompressedRules(createRuleRuleSetObject(rule),groupId,GenericConstants.STATUS_ACTIVE,userName);
        } else {
            boolean isRulesetRulemapEmpty = false;
            List<String> ruleNameList = new ArrayList<String>();
            List<Rule> ruleRulesetList = createRuleRuleSetObjectList(ruleList, ruleNameList);
            if (rulesetRulemap != null && rulesetRulemap.size() > 0) {
                List<String> ruleNamesInRuleset = checkForValidRulesetRulemap(rulesetRulemap, ruleNameList);
                if (ruleNamesInRuleset.size() > 0 && !ruleNamesInRuleset.isEmpty()) {
                    resultMap.put("isSuccess", false);
                    resultMap.put("newRuleList", ruleNamesInRuleset);
                    return resultMap;
                }
            } else {
                isRulesetRulemapEmpty = true;
            }
            
            groupId = groupDao.createGroup(createGroupObject(groupMap));
            groupDao.createRuleAndRuleset(ruleRulesetList, groupId, GenericConstants.STATUS_ACTIVE, userName);
            List<Rule> ruleAndRulesetList = groupDao.getImportedRulesAndRulesets(groupId.toString());
            if(!isRulesetRulemapEmpty){
                List<RuleSetRuleMapping> ruleSetRuleMappingList =
                        createRulesetRuleMappingObject(rulesetRulemap, ruleAndRulesetList);
                groupDao.createRuleSetRuleMapping(ruleSetRuleMappingList);
            }

        }
        resultMap.put("isSuccess", true);
        LOGGER.trace("Method Exit : GroupServiceImpl-> importGroup");
        return resultMap;
    }
    
    private List<String> checkForValidRulesetRulemap(Map<String, List<String>> rulesetRulemap, List<String> ruleNameList){
        Set<String> ruleNameSet = new HashSet<String>();
        for (Map.Entry<String, List<String>> entry : rulesetRulemap.entrySet()) {
            List<String> ruleNames = entry.getValue();
            ruleNameSet.addAll(ruleNames);
        }
        List<String> ruleNamesInRuleset = new ArrayList<String>(ruleNameSet);
        ruleNamesInRuleset.removeAll(ruleNameList);
        
        return ruleNamesInRuleset;
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.services.GroupService#importExistingGroup(java.lang.Integer, java.util.List, java.util.Map, java.util.List)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public Map<String,Object> importExistingGroup(Integer groupId, List<Object> ruleListObj,
            Map<String, List<String>> rulesetRulemap, List<String> commonRuleList, String userName) {
        LOGGER.trace("Method Entry : GroupServiceImpl-> importExistingGroup");
        List<Rule> existingRulesList = new ArrayList<Rule>();
        List<Rule> ruleList = createRuleRuleSetObjectList(ruleListObj, null);
        List<String> ruleSetNames = new ArrayList<String>();
        List<Rule> newRuleList = new ArrayList<Rule>();
        Map<String,Object> resultMap = new HashMap<String,Object>();
        List<String>  newRuleNames =  new ArrayList<String>();
        
        
        for (int i = 0; i < ruleList.size(); i++) {
            Rule rule = ruleList.get(i);
            if (commonRuleList.contains(rule.getRuleName())) {
                if (rule.getRuleTypeID() == 3) {
                    ruleSetNames.add(rule.getRuleName());
                }
                existingRulesList.add(rule);
                // ruleList.remove(rule);
            } else {
                newRuleList.add(rule);
                newRuleNames.add(rule.getRuleName());
            }

        }
        
        //check new rules in existing ruleset
        List<String> rulesetRelatdRules = checkForNewRulesInRuleSet(rulesetRulemap,ruleSetNames,newRuleNames,groupId);
        if(rulesetRelatdRules.size() > 0 && !rulesetRelatdRules.isEmpty()){
            resultMap.put("isSuccess", false);
            resultMap.put("newRuleList", rulesetRelatdRules);
            return resultMap;
        }
        
        // insert new rule/ruleset
        if (newRuleList != null && newRuleList.size() > 0) {
            groupDao.createRuleAndRuleset(newRuleList, groupId, GenericConstants.STATUS_ACTIVE,userName);
        }
        if(commonRuleList != null && commonRuleList.size() > 0){
            // create history record of existing rule/ruleset
            List<Rule> commmonRules = groupDao.fetchCommonRules(commonRuleList, groupId);
          // if(commmonRules != null && commmonRules.size() > 0){
            groupDao.createRuleAndRulesetHistory(commmonRules, groupId, GenericConstants.STATUS_HISTORY,userName);
    
            // update existing rule/ruleset
            groupDao.updateExistingRulesAndRulesets(existingRulesList, groupId);
            if (rulesetRulemap != null && rulesetRulemap.size() > 0) {
                List<Integer> rulesetIdList = new ArrayList<Integer>();
                for (Rule commonRule : commmonRules) {
                    if (ruleSetNames.contains(commonRule.getRuleName())) {
                        rulesetIdList.add(commonRule.getRuleID());
                    }
                }
             // delete ruleset rulemapping of existing ruleset
                if(!rulesetIdList.isEmpty() && rulesetIdList.size() > 0){
                    groupDao.deleteRuleSetRuleMapping(rulesetIdList);
                }
            }
        }  
            // For existing rules
        if (rulesetRulemap != null && rulesetRulemap.size() > 0) {
            List<Rule> ruleAndRulesetList = groupDao.getImportedRulesAndRulesets(groupId.toString());
            List<RuleSetRuleMapping> ruleSetRuleMappingList =
                    createRulesetRuleMappingObject(rulesetRulemap, ruleAndRulesetList);
            groupDao.createRuleSetRuleMapping(ruleSetRuleMappingList);
        }
        resultMap.put("isSuccess", true);
        LOGGER.trace("Method Exit : GroupServiceImpl-> importExistingGroup");
        return resultMap;
    }
    
    private List<String> checkForNewRulesInRuleSet(Map<String, List<String>> rulesetRulemap,List<String> ruleSetNames, List<String> newRuleNames, Integer groupId){
        List<String> ruleNames = groupDao.getRulesByGroup(groupId);
        Set<String> ruleNameSet = new HashSet<String>();
        for (String rulesetName: ruleSetNames) {
            ruleNameSet.addAll(rulesetRulemap.get(rulesetName));
        }
        List<String> ruleNamesInRS = new ArrayList<String>(ruleNameSet);
        ruleNamesInRS.removeAll(ruleNames);
        ruleNamesInRS.removeAll(newRuleNames);
        
        return ruleNamesInRS;
    }
    
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public void importExistingCompressedGroup(Integer groupId,Object ruleObj, String userName){
        LOGGER.trace("Method Entry : GroupServiceImpl-> importExistingCompressedGroup");
        Rule compressedRule = groupDao.fetchCompressedRule(groupId);
        groupDao.createCompressedRulesHistory(compressedRule,groupId,GenericConstants.STATUS_HISTORY,userName);
        groupDao.updateCompressedRules(createRuleRuleSetObject(ruleObj),groupId);
        LOGGER.trace("Method Exit : GroupServiceImpl-> importExistingCompressedGroup");
    }
    /**
     * Creates the rule rule set object.
     *
     * @param ruleListObj the rule list obj
     * @return the list
     */
    private List<Rule> createRuleRuleSetObjectList(List<Object> ruleListObj, List<String> ruleNameList) {
        LOGGER.trace("Method Entry : GroupServiceImpl-> createRuleRuleSetObjectList");
        List<Rule> ruleList = new ArrayList<Rule>();
        for (Object ruleobj : ruleListObj) {
            Rule rule = createRuleRuleSetObject(ruleobj);
            if(null != ruleNameList){
                ruleNameList.add(rule.getRuleName());
            }
            ruleList.add(rule);
        }
        LOGGER.trace("Method Exit : GroupServiceImpl-> createRuleRuleSetObjectList");
        return ruleList;
    }

    private Rule createRuleRuleSetObject(Object ruleobj) {
        LOGGER.trace("Method Entry : GroupServiceImpl-> createRuleRuleSetObject");
        Rule rule = new Rule();
        Map<String, Object> ruleMap = (LinkedHashMap<String, Object>) ruleobj;
        rule.setRuleName((String) ruleMap.get("ruleName"));
        rule.setRuleTypeID((Integer) ruleMap.get("ruleTypeID"));
        rule.setClientId((String) ruleMap.get("clientId"));
        rule.setVersion(new Float((Integer) ruleMap.get("version")));

        if (ruleMap.get("ruleJson") != null) {
            rule.setRuleJson((String) ruleMap.get("ruleJson"));
        }
        if (ruleMap.get("ruleJS") != null) {
            rule.setRuleJS((String) ruleMap.get("ruleJS"));
        }
        if (ruleMap.get("offlineRuleJS") != null) {
            rule.setOfflineRuleJS((String) ruleMap.get("offlineRuleJS"));
        }

        if (ruleMap.get("comments") != null) {
            rule.setComments((String) ruleMap.get("comments"));
        }

        rule.setStatus((String) ruleMap.get("status"));
        LOGGER.trace("Method Exit : GroupServiceImpl-> createRuleRuleSetObject");
        return rule;
    }
    /**
     * Creates the ruleset rule mapping object.
     * 
     * @param rulesetRulemap
     *            the ruleset rulemap
     * @param ruleAndRulesetList
     *            the rule and ruleset list
     * @return the list
     */
    private List<RuleSetRuleMapping> createRulesetRuleMappingObject(Map<String, List<String>> rulesetRulemap,
            List<Rule> ruleAndRulesetList) {
        LOGGER.trace("Method Entry : GroupServiceImpl-> createRulesetRuleMappingObject");
        List<RuleSetRuleMapping> ruleSetRuleMappingList = new ArrayList<RuleSetRuleMapping>();
        for (Map.Entry<String, List<String>> entry : rulesetRulemap.entrySet()) {
            List<String> ruleNames = entry.getValue();
            Integer ruleSetId = null;
            for (Rule rule : ruleAndRulesetList) {
                if (rule.getRuleName().equals(entry.getKey())) {
                    ruleSetId = rule.getRuleID();
                    break;
                }
            }

            for (String ruleName : ruleNames) {
                Integer ruleId = null;
                RuleSetRuleMapping ruleSetRuleMapping = new RuleSetRuleMapping();
                for (Rule rule : ruleAndRulesetList) {
                    if (ruleName.equals(rule.getRuleName())) {
                        ruleId = rule.getRuleID();
                        break;
                    }
                }
                ruleSetRuleMapping.setRuleSetID(ruleSetId);
                ruleSetRuleMapping.setRuleID(ruleId);
                ruleSetRuleMappingList.add(ruleSetRuleMapping);
            }

        }
        LOGGER.trace("Method Exit : GroupServiceImpl-> createRulesetRuleMappingObject");
        return ruleSetRuleMappingList;
    }

    /**
     * Creates the group object.
     *
     * @param groupMap the group map
     * @return the group
     */
    private Group createGroupObject(Map<String, Object> groupMap) {
        LOGGER.trace("Method Entry : GroupServiceImpl-> createGroupObject");
        Group group = new Group();
        group.setGroupName((String) groupMap.get("groupName"));
        group.setTypeID((Integer) groupMap.get("typeID"));
        group.setComments((String) groupMap.get("comments"));
        LOGGER.trace("Method Exit : GroupServiceImpl-> createGroupObject");
        return group;

    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.services.GroupService#getImportedRulesAndRulesets(java.lang.String)
     */
    public List<Rule> getImportedRulesAndRulesets(String groupID) {
        LOGGER.trace("Method Entry : GroupServiceImpl-> getImportedRulesAndRulesets");
        List<Rule> ruleList = groupDao.getImportedRulesAndRulesets(groupID);
        LOGGER.trace("Method Exit : GroupServiceImpl-> getImportedRulesAndRulesets");
        return ruleList;
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.services.GroupService#getGroupId(java.lang.String)
     */
    @Override
    public Integer getGroupId(String groupName) {
        return groupDao.getGroupId(groupName);
    }

    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.services.GroupService#getRulesByGroup(java.lang.Integer)
     */
    @Override
    public List<String> getRulesByGroup(Integer groupId) {
        return groupDao.getRulesByGroup(groupId);
    }

    
}
