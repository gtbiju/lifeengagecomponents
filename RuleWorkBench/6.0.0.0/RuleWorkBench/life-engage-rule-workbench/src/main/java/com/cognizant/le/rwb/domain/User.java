/**
 *
 * Copyright 2014, Cognizant 
 *
 * @author        : 304003
 * @version       : 0.1, Feb 28, 2013
 */
package com.cognizant.le.rwb.domain;

import java.util.Date;

/**
 * The Class class User.
 */
public class User {

    /** The user id. */
    private Integer userID;

    /** The user name. */
    private String userName;

    /** The first name. */
    private String firstName;

    /** The last name. */
    private String lastName;

    /** The password. */
    private String password;

    /** The email. */
    private String email;

    /** The created by. */
    private Date createdBy;

    /** The created date. */
    private Date createdDate;

    /** The updated by. */
    private Date updatedBy;

    /** The updated date. */
    private Date updatedDate;

    /**
     * Gets the user id.
     * 
     * @return userID the userID
     */
    public final Integer getUserID() {
        return userID;
    }

    /**
     * Sets the user id.
     * 
     * @param userID
     *            the userID to set
     */
    public final void setUserID(final Integer userID) {
        this.userID = userID;
    }

    /**
     * Gets the user name.
     * 
     * @return userName the userName
     */
    public final String getUserName() {
        return userName;
    }

    /**
     * Sets the user name.
     * 
     * @param userName
     *            the userName to set
     */
    public final void setUserName(final String userName) {
        this.userName = userName;
    }

    /**
     * Gets the first name.
     * 
     * @return firstName the firstName
     */
    public final String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name.
     * 
     * @param firstName
     *            the firstName to set
     */
    public final void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the last name.
     * 
     * @return lastName the lastName
     */
    public final String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name.
     * 
     * @param lastName
     *            the lastName to set
     */
    public final void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the password.
     * 
     * @return password the password
     */
    public final String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     * 
     * @param password
     *            the password to set
     */
    public final void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Gets the email.
     * 
     * @return email the email
     */
    public final String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     * 
     * @param email
     *            the email to set
     */
    public final void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Gets the created by.
     * 
     * @return createdBy the createdBy
     */
    public final Date getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the created by.
     * 
     * @param createdBy
     *            the createdBy to set
     */
    public final void setCreatedBy(final Date createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Gets the created date.
     * 
     * @return createdDate the createdDate
     */
    public final Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Sets the created date.
     * 
     * @param createdDate
     *            the createdDate to set
     */
    public final void setCreatedDate(final Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * Gets the updated by.
     * 
     * @return updatedBy the updatedBy
     */
    public final Date getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Sets the updated by.
     * 
     * @param updatedBy
     *            the updatedBy to set
     */
    public final void setUpdatedBy(final Date updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * Gets the updated date.
     * 
     * @return updatedDate the updatedDate
     */
    public final Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * Sets the updated date.
     * 
     * @param updatedDate
     *            the updatedDate to set
     */
    public final void setUpdatedDate(final Date updatedDate) {
        this.updatedDate = updatedDate;
    }
}
