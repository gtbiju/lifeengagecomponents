/**
 *
 * Copyright 2014, Cognizant 
 *
 * @author        : 304003
 * @version       : 0.1, Feb 28, 2013
 */
package com.cognizant.le.rwb.domain;

/**
 * The Class class RuleType.
 */
public class RuleType {

    /** The rule type id. */
    private Integer ruleTypeID;

    /** The rule type name. */
    private String ruleTypeName;

    /**
     * Gets the rule type id.
     * 
     * @return the rule type id
     */
    public final Integer getRuleTypeID() {
        return ruleTypeID;
    }

    /**
     * Sets the rule type id.
     * 
     * @param ruleTypeID
     *            the rule type id to set.
     */
    public final void setRuleTypeID(final Integer ruleTypeID) {
        this.ruleTypeID = ruleTypeID;
    }

    /**
     * Gets the rule type name.
     * 
     * @return the rule type name
     */
    public final String getRuleTypeName() {
        return ruleTypeName;
    }

    /**
     * Sets the rule type name.
     * 
     * @param ruleTypeName
     *            the rule type name to set.
     */
    public final void setRuleTypeName(final String ruleTypeName) {
        this.ruleTypeName = ruleTypeName;
    }

}
