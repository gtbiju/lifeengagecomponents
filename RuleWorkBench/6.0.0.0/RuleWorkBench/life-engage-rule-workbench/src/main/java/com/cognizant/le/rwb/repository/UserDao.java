/**
 * 
 */
package com.cognizant.le.rwb.repository;

import com.cognizant.le.rwb.domain.User;

/**
 * The Interface interface UserDao.
 * 
 * @author 304003
 */
public interface UserDao {

    /**
     * Validate user.
     * 
     * @param userName
     *            the user name
     * @param password
     *            the password
     * @return the boolean
     */
    public Boolean validateUser(final String userName, final String password);
    public User getUserId(final String userName, final String password);
}
