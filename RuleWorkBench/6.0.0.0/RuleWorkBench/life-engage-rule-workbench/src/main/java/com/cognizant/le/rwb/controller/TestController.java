/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 7, 2014
 */
package com.cognizant.le.rwb.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.services.RuleSetService;

/**
 * The Class TestAction.
 */
@Controller
public class TestController {

    /** The rule set service. */
    @Autowired
    private RuleSetService ruleSetService;
    
    /** The environment. */
    @Autowired
    private Environment environment;

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    /**
     * Gets the json for a rule id.
     * 
     * @param ruleId
     *            the rule id
     * @return the json
     */

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public final @ResponseBody
    Map<String, String> getJson(@RequestParam("ruleID") final String ruleId) {
        LOGGER.trace("Method Entry : TestController-> getJson");
        Map<String, String> resultMap = new HashMap<String, String>();
        if (ruleId != null) {
            Map<String, String> ruleMap = new HashMap<String, String>();
            ruleMap = ruleSetService.getRuleJson(Integer.parseInt(ruleId));
            resultMap.put("ruleJson", ruleMap.get("rulejson"));
            resultMap.put("ruleName", ruleMap.get("rulename"));
            resultMap.put("ruleJs", ruleMap.get("rulejs"));
            resultMap.put("category", ruleMap.get("category"));
            resultMap.put("debugConf", environment.getProperty(GenericConstants.DEBUG_CONF));
            
        }
        LOGGER.trace("Method Exit : TestController-> getJson");
        return resultMap;
    }
    
    
}
