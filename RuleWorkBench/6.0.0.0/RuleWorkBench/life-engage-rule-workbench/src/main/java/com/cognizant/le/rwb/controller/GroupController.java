/**
 *
 * � Copyright 2012, Cognizant 
 *
 * @author        : 291422
 * @version       : 0.1, Nov 5, 2014
 */
package com.cognizant.le.rwb.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.exception.ZipExceptionConstants;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.domain.Group;
import com.cognizant.le.rwb.domain.Rule;
import com.cognizant.le.rwb.exception.ServiceException;
import com.cognizant.le.rwb.services.GroupService;

/**
 * The Class class GroupController.
 */
@Controller
public class GroupController {
    /** The environment. */
    @Autowired
    private Environment environment;

    /** The rule service. */
    @Autowired
    private GroupService groupService;

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupController.class);

    /**
     * Export group.
     *
     * @param groupID the group id
     * @param groupName the group name
     * @param password the password
     * @param ruleName the rule name
     * @param ruleNameList the rule name list
     * @param request the request
     * @param response the response
     */
    @RequestMapping(value = "/exportCompressesdGroup", method = RequestMethod.GET)
    public final @ResponseBody
    void exportGroup(@RequestParam("groupID") final Integer groupID,
    		@RequestParam("groupName") final String groupName, @RequestParam("password") final String password,@RequestParam("ruleName") final String ruleName,
    		@RequestParam("ruleNameList") final String ruleNameList, final HttpServletRequest request,
            final HttpServletResponse response) {
        LOGGER.trace("Method Entry : GroupAction-> exportGroup");
        Date date = new Date() ;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") ;
        String zipFileName = groupName+"_Prod_"+dateFormat.format(date);
        try {
        	List<String> ruleNames = getNameList(ruleNameList);
        	Map<String, Object> compressedGroupMap =
                    groupService.exportCompressedGroup(groupID, ruleNames, ruleName);
            
        	 String outFilename =
                     new StringBuilder(System.getProperty("java.io.tmpdir")).append(File.separator)
                         .append(zipFileName)
                         .append(".zip")
                         .toString();
             String groupCSVFile =
                     new StringBuilder(System.getProperty("java.io.tmpdir")).append(File.separator)
                         .append(environment.getProperty(GenericConstants.GROUP_FILE_NAME))
                         //.append(".csv")
                         .toString();
             String ruleCSVFile =
                     new StringBuilder(System.getProperty("java.io.tmpdir")).append(File.separator)
                         .append(environment.getProperty(GenericConstants.RULE_FILE_NAME))
                        // .append(".csv")
                         .toString();
             String metadataFilePath =
                     new StringBuilder(System.getProperty("java.io.tmpdir")).append(File.separator)
                         .append(environment.getProperty(GenericConstants.METADATA_FILE_NAME))
                         //.append(".json")
                         .toString();
             Map<String, String> csvFilePaths = new HashMap<String, String>();
             csvFilePaths.put("groupFilePath", groupCSVFile);
             csvFilePaths.put("ruleFilePath", ruleCSVFile); 
             csvFilePaths.put("metadataFilePath", metadataFilePath);
             InputStream fileInputStream = null;
             try {
               //create CSV files
                 writeToMetadataFile(true,metadataFilePath);
                 writeToCSVFile(writeGroupData((Group)compressedGroupMap.get("group")),groupCSVFile);
                 writeToCSVFile(writeRuleDataCompressed((Rule)compressedGroupMap.get("ruleObj")),ruleCSVFile);
                 createPasswordProtectedZipFile(outFilename, csvFilePaths, password);

                 fileInputStream = sendResponse(zipFileName, response, outFilename);
             } catch (Exception e) {
                 LOGGER.error(e.getMessage(), e);
                 response.sendRedirect("jsp/Error.jsp");
                 throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);

             } finally {
                 // Delete zip
                 try {
                     if (null != fileInputStream) {
                         fileInputStream.close();
                         File delete = new File(outFilename);
                         delete.delete();
                         delete = new File(groupCSVFile);
                         delete.delete();
                         delete = new File(ruleCSVFile);
                         delete.delete(); 
                         delete = new File(metadataFilePath);
                         delete.delete();
                     }
                 } catch (IOException e) {
                     LOGGER.error(e.getMessage(), e);
                 }
             }

         } catch (Exception e) {
             LOGGER.error("Error while exporting group with name: " + groupName, e);
             throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
         }
         LOGGER.trace("Method Exit : GroupAction-> exportGroup");
      }

    /**
     * Export uncompressed group.
     *
     * @param groupID the group id
     * @param groupName the group name
     * @param password the password
     * @param ruleNameList the rule name list
     * @param request the request
     * @param response the response
     */
    @RequestMapping(value = "/exportUncompressedGroup", method = RequestMethod.GET)
    public final @ResponseBody
    void exportUncompressedGroup(@RequestParam("groupID") final Integer groupID,
            @RequestParam("groupName") final String groupName, @RequestParam("password") final String password,
            @RequestParam("ruleNameList") final String ruleNameList, final HttpServletRequest request,
            final HttpServletResponse response) {
        LOGGER.trace("Method Entry : GroupAction-> exportGroup");
        Date date = new Date() ;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd") ;
        String zipFileName = groupName+"_"+dateFormat.format(date);
        try {
            List<String> ruleNames = getNameList(ruleNameList);
            Map<String, Object> uncompressedGroupMap =
                    groupService.exportUnCompressedGroup(groupID, ruleNames);
            String outFilename =
                    new StringBuilder(System.getProperty("java.io.tmpdir")).append(File.separator)
                        .append(zipFileName)
                        .append(".zip")
                        .toString();
            String groupCSVFile =
                    new StringBuilder(System.getProperty("java.io.tmpdir")).append(File.separator)
                        .append(environment.getProperty(GenericConstants.GROUP_FILE_NAME))
                        //.append(".csv")
                        .toString();
            String ruleCSVFile =
                    new StringBuilder(System.getProperty("java.io.tmpdir")).append(File.separator)
                        .append(environment.getProperty(GenericConstants.RULE_FILE_NAME))
                        //.append(".csv")
                        .toString();
            String rulesetRuleMapCSVFile =
                    new StringBuilder(System.getProperty("java.io.tmpdir")).append(File.separator)
                        .append(environment.getProperty(GenericConstants.RULESET_RULE_MAP_FILE_NAME))
                        //.append(".csv")
                        .toString();
            String metadataFilePath =
                    new StringBuilder(System.getProperty("java.io.tmpdir")).append(File.separator)
                        .append(environment.getProperty(GenericConstants.METADATA_FILE_NAME))
                        //.append(".json")
                        .toString();
            Map<String, String> csvFilePaths = new HashMap<String, String>();
            csvFilePaths.put("groupFilePath", groupCSVFile);
            csvFilePaths.put("ruleFilePath", ruleCSVFile);
            csvFilePaths.put("ruleSetRuleMapFilePath", rulesetRuleMapCSVFile);
            csvFilePaths.put("metadataFilePath", metadataFilePath);
            InputStream fileInputStream = null;
            try {
                
                //create CSV files
                writeToMetadataFile(false,metadataFilePath);
                
               //write group data
                writeToCSVFile(writeGroupData((Group)uncompressedGroupMap.get("group")), groupCSVFile);
                //write rule/ruleset data
                writeToCSVFile(writeRuleData((List<Rule>) uncompressedGroupMap.get("ruleList")), ruleCSVFile);
                //write rulesetrulemapping data
                writeToCSVFile(writeRulesetRuleMapData((Map<String, List<String>>) uncompressedGroupMap.get("rulesetRuleMap")), rulesetRuleMapCSVFile);
                
                //make Zip password protcted
                createPasswordProtectedZipFile(outFilename, csvFilePaths, password);

                fileInputStream = sendResponse(zipFileName, response, outFilename);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                response.sendRedirect("jsp/Error.jsp");
                throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);

            } finally {
                // Delete zip
                try {
                    if (null != fileInputStream) {
                        fileInputStream.close();
                       // File delete = new File(outFilename);
                       // delete.delete();
                        File delete = new File(groupCSVFile);
                        delete.delete();
                        delete = new File(ruleCSVFile);
                        delete.delete();
                        delete = new File(rulesetRuleMapCSVFile);
                        delete.delete();
                        delete = new File(metadataFilePath);
                        delete.delete();

                    }
                } catch (IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }

        } catch (Exception e) {
            LOGGER.error("Error while exporting group with name: " + groupName, e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : GroupAction-> exportGroup");
    }

    /**
     * Gets the name list.
     *
     * @param ruleNameList the rule name list
     * @return the name list
     * @throws JsonParseException the json parse exception
     * @throws JsonMappingException the json mapping exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private List<String> getNameList(String ruleNameList) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        final List<String> ruleNames = mapper.readValue(ruleNameList, new TypeReference<List<String>>() {
        });
        return ruleNames;
    }

    /**
     * Creates the password protected zip file.
     *
     * @param zipFilepath the zip filepath
     * @param filePaths the file paths
     * @param password the password
     * @throws ZipException the zip exception
     */
    private void createPasswordProtectedZipFile(String zipFilepath, Map<String, String> filePaths, String password)
            throws ZipException {
        File fileToAdd = null;
        // zipPath is absolute path of zipped file
        ZipFile zipFile = new ZipFile(zipFilepath);
        // filePath is absolute path of file that want to be zip
        List<File> filelist = new ArrayList<File>();
        for (Map.Entry<String, String> entry : filePaths.entrySet()) {
            fileToAdd = new File(entry.getValue());
            filelist.add(fileToAdd);
        }
        // create zip parameters such a password, encryption method, etc
        ZipParameters parameters = new ZipParameters();
        parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
        parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
        parameters.setEncryptFiles(true);
        parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
        parameters.setPassword(password);
        zipFile.addFiles((ArrayList) filelist, parameters);
    }

    /**
     * Write to metadata file.
     *
     * @param isCompressed the is compressed
     * @param metadataFilePath the metadata file path
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @SuppressWarnings("unchecked")
    private void writeToMetadataFile(boolean isCompressed,String metadataFilePath) throws IOException{
        FileWriter file = new FileWriter(metadataFilePath);
        JSONObject obj = new JSONObject();
        obj.put("isCompressed", isCompressed);
        obj.put("groupFileName", environment.getProperty(GenericConstants.GROUP_FILE_NAME));
        obj.put("ruleFileName", environment.getProperty(GenericConstants.RULE_FILE_NAME));
        if (!isCompressed) {
            obj.put("rulesetRuleMapFileName", environment.getProperty(GenericConstants.RULESET_RULE_MAP_FILE_NAME));
        }
        file.write(obj.toJSONString());
        file.flush();
        file.close();
    }
    
    /**
     * Write to csv file.
     *
     * @param data the data
     * @param filePath the file path
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void writeToCSVFile(List<String[]> data ,  String filePath)
            throws IOException {
        CSVWriter writer = null;
        writer = new CSVWriter(new FileWriter(filePath));
        writer.writeAll(data);
        writer.close();
    } 
    
    /**
     * Write rule data compressed.
     *
     * @param ruleObj the rule obj
     * @return the list
     */
    private List<String[]> writeRuleDataCompressed(Rule ruleObj) {
        List<String[]> records = new ArrayList<String[]>();
        // add header record
        records.add(new String[] { "RULE NAME", "TYPE ID", "VERSION", "RULE_DESC", "OFFLINE_RULEDESC","CLIENT_ID", "STATUS" });       
        
        records.add(new String[] { encodeBase64(ruleObj.getRuleName()),encodeBase64(ruleObj.getRuleTypeID().toString()),
                    encodeBase64(ruleObj.getVersion() == null ? "" : ruleObj.getVersion().toString()),
                    encodeBase64(ruleObj.getRuleJS()),
                    encodeBase64(ruleObj.getOfflineRuleJS() == null ? "" : ruleObj.getOfflineRuleJS()),
                    encodeBase64(ruleObj.getClientId()),                   
                    encodeBase64((ruleObj.getStatus() == null ? "" : ruleObj.getStatus())) 
                    });
        return records;
    
    }

    /**
     * Write rule data.
     *
     * @param ruleList the rule list
     * @return the list
     */
    private List<String[]> writeRuleData(List<Rule> ruleList) {
        List<String[]> records = new ArrayList<String[]>();
        // add header record
        records.add(new String[] { "RULE NAME", "TYPE ID", "VERSION", "RULEJSON", "RULE_DESC", "OFFLINE_RULEDESC",
                "CLIENT_ID", "COMMENTS","STATUS" });
        Iterator<Rule> it = ruleList.iterator();
        while (it.hasNext()) {
            Rule rule = it.next();
            records.add(new String[] { encodeBase64(rule.getRuleName()), encodeBase64(rule.getRuleTypeID().toString()),
                    encodeBase64(rule.getVersion() == null ? "" : rule.getVersion().toString()),
                    encodeBase64(rule.getRuleJson()), encodeBase64(rule.getRuleJS()),
                    encodeBase64(rule.getOfflineRuleJS() == null ? "" : rule.getOfflineRuleJS()),
                    encodeBase64(rule.getClientId()),
                    encodeBase64((rule.getComments() == null ? "" : rule.getComments())) ,
                    encodeBase64((rule.getStatus() == null ? "" : rule.getStatus())) 
                    });
        }
        return records;
    }

    /**
     * Write group data.
     *
     * @param group the group
     * @return the list
     */
    private List<String[]> writeGroupData(Group group) {
        List<String[]> records = new ArrayList<String[]>();
        // add header record
        records.add(new String[] { "GROUP NAME", "TYPE ID", "COMMENTS" });
        records.add(new String[] { encodeBase64(group.getGroupName()), encodeBase64(group.getTypeID().toString()),
                encodeBase64((group.getComments() == null ? "" : group.getComments())) });

        return records;
    }

    /**
     * Write ruleset rule map data.
     *
     * @param rulesetRuleMap the ruleset rule map
     * @return the list
     */
    private List<String[]> writeRulesetRuleMapData(Map<String, List<String>> rulesetRuleMap) {
        List<String[]> records = new ArrayList<String[]>();
        // add header record
        records.add(new String[] { "RULESET NAME", "RULE NAME" });
        for (Map.Entry<String, List<String>> entry : rulesetRuleMap.entrySet()) {
            for (String ruleName : entry.getValue()) {
                records.add(new String[] { encodeBase64(entry.getKey()), encodeBase64(ruleName) });  
            }
            
        }

        return records;
    }

    /**
     * Encode base64.
     *
     * @param value the value
     * @return the string
     */
    private String encodeBase64(String value) {
        byte[] bytesEncoded = Base64.encodeBase64(value.getBytes());
        return new String(bytesEncoded);
    }

    /**
     * Decode base64.
     *
     * @param bytesEncoded the bytes encoded
     * @return the string
     */
    private String decodeBase64(byte[] bytesEncoded) {
        byte[] valueDecoded = Base64.decodeBase64(bytesEncoded);
        return new String(valueDecoded);
    }
    
    
    /**
     * Read compressed rule csv file.
     *
     * @param reader the reader
     * @return the rule
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private Rule readCompressedRuleCSVFile(Reader reader) throws IOException {
        CSVReader csvReader = new CSVReader(reader);
        String[] row = null;
        csvReader.readNext();
        Rule rule = new Rule();
        while ((row = csvReader.readNext()) != null) {
            
            rule.setRuleName(decodeBase64(row[0].getBytes()));
            rule.setRuleTypeID(Integer.valueOf(decodeBase64(row[1].getBytes())));
            rule.setVersion(Float.valueOf(decodeBase64(row[2].getBytes())));
           // rule.setRuleJson(decodeBase64(row[3].getBytes()));
            rule.setRuleJS(decodeBase64(row[3].getBytes()));
            rule.setOfflineRuleJS(decodeBase64(row[4].getBytes()));
            rule.setClientId(decodeBase64(row[5].getBytes()));
            rule.setStatus(decodeBase64(row[6].getBytes()));

        }
        csvReader.close();
        return rule;

    }
    
    /**
     * Read rule csv file.
     *
     * @param reader the reader
     * @return the list
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private List<Rule> readRuleCSVFile(Reader reader) throws IOException {
        List<Rule> ruleList = new ArrayList<Rule>();
        CSVReader csvReader = new CSVReader(reader);
        String[] row = null;
        csvReader.readNext();
        while ((row = csvReader.readNext()) != null) {
            Rule rule = new Rule();
            rule.setRuleName(decodeBase64(row[0].getBytes()));
            rule.setRuleTypeID(Integer.valueOf(decodeBase64(row[1].getBytes())));
            rule.setVersion(Float.valueOf(decodeBase64(row[2].getBytes())));
            rule.setRuleJson(decodeBase64(row[3].getBytes()));
            rule.setRuleJS(decodeBase64(row[4].getBytes()));
            rule.setOfflineRuleJS(decodeBase64(row[5].getBytes()));
            rule.setClientId(decodeBase64(row[6].getBytes()));
            rule.setComments(decodeBase64(row[7].getBytes()));
            rule.setStatus(decodeBase64(row[8].getBytes()));
            ruleList.add(rule);

        }
        csvReader.close();
        /*
         * CSVWriter writer = new CSVWriter(new FileWriter("D://testWrite.csv")); List<String[]> data =
         * toArray(ruleList); writer.writeAll(data);
         */
        return ruleList;

    }

    /**
     * Read group csv file.
     *
     * @param reader the reader
     * @return the group
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private Group readGroupCSVFile(Reader reader) throws IOException {
        CSVReader csvReader = new CSVReader(reader);
        Group group = new Group();
        String[] row = null;
        csvReader.readNext();
        while ((row = csvReader.readNext()) != null) {
            group.setGroupName(decodeBase64(row[0].getBytes()));
            group.setTypeID(Integer.valueOf(decodeBase64(row[1].getBytes())));
            group.setComments((decodeBase64(row[2].getBytes())));

        }
        csvReader.close();
        return group;
    }

    /**
     * Read rule set rule map csv file.
     *
     * @param reader the reader
     * @return the map
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private Map<String, List<String>> readRuleSetRuleMapCSVFile(Reader reader) throws IOException {
        CSVReader csvReader = new CSVReader(reader);
        String[] row = null;
        csvReader.readNext();
        String ruleSetName = null;
        Map<String, List<String>> ruleRuleSetMap = new LinkedHashMap<String, List<String>>();
        List<String> ruleNameList = null;
        while ((row = csvReader.readNext()) != null) {
            if (ruleNameList == null) {
                ruleSetName = decodeBase64(row[0].getBytes());
                ruleNameList = new ArrayList<String>();
            }
            if (ruleSetName.equals(decodeBase64(row[0].getBytes()))) {
                ruleNameList.add(decodeBase64(row[1].getBytes()));
            } else {
                ruleRuleSetMap.put(ruleSetName, ruleNameList);
                ruleNameList = new ArrayList<String>();
                ruleSetName = decodeBase64(row[0].getBytes());
                ruleNameList.add(decodeBase64(row[1].getBytes()));
            }

        }
        if(ruleSetName != null && ruleNameList != null){
            ruleRuleSetMap.put(ruleSetName, ruleNameList);
        }
        csvReader.close();
        return ruleRuleSetMap;
    }

   /*private CSVReader getReaderObject(ZipFile zipFile,String fileName) throws ZipException, IOException {
        
        FileHeader metafileheader = zipFile.getFileHeader(fileName);
        InputStream is = zipFile.getInputStream(metafileheader);
        InputStreamReader ipstreamreader = new InputStreamReader(is);
        CSVReader csvReader = new CSVReader(ipstreamreader);
        is.close();
        return csvReader;
    }
    */
/**
    * Gets the reader object.
    *
    * @param zipFile the zip file
    * @param fileName the file name
    * @return the reader object
    * @throws ZipException the zip exception
    * @throws IOException Signals that an I/O exception has occurred.
    */
   private InputStream getReaderObject(ZipFile zipFile,String fileName) throws ZipException, IOException {
        
        FileHeader metafileheader = zipFile.getFileHeader(fileName);
        InputStream is = zipFile.getInputStream(metafileheader);
        return is;
    }
    
    
    /**
     * Upload csv.
     *
     * @param request the request
     * @param response the response
     * @return the map
     */
    @RequestMapping(value = "/uploadCSV", method = RequestMethod.POST)
    public final @ResponseBody
    Map<String,Object> uploadCSV(final MultipartHttpServletRequest request,
            final HttpServletResponse response) {
        Iterator<String> itr =  request.getFileNames();
        String password = request.getParameter("password");
        MultipartFile file = request.getFile(itr.next());
        String zipFileName = file.getOriginalFilename();
        
        Map<String,Object> resultMap = new HashMap<String,Object>();
        String outFilename =
                new StringBuilder(System.getProperty("java.io.tmpdir")).append(File.separator)
                    .append(zipFileName)
                    .toString();
        boolean isCompressed = false;
        if (!file.isEmpty()) {
            try {

                // copy the zip file into a temporary location
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(outFilename)));
                stream.write(bytes);
                stream.close();

                // read the zip file and execute the scripts
                ZipFile zipFile = new ZipFile(outFilename);
                if (zipFile.isValidZipFile() && zipFile.isEncrypted()) {
                    
                    zipFile.setPassword(password);
                    // List<FileHeader> fileHeaders = zipFile.getFileHeaders();
                    Group group = null;
                    List<Rule> ruleList = null;
                    Map<String, List<String>> ruleRulesetMap = null;
                    Rule compressedRuleObj = null;
                    InputStreamReader ipstreamreader = null;
                    InputStream inputStream = null;
                    try {

                        inputStream =
                                getReaderObject(zipFile, environment.getProperty(GenericConstants.METADATA_FILE_NAME));
                        // FileHeader metafileheader =
                        // zipFile.getFileHeader(environment.getProperty(GenericConstants.METADATA_FILE_NAME));
                        // InputStream is = zipFile.getInputStream(metafileheader);
                        ipstreamreader = new InputStreamReader(inputStream);

                        JSONParser jsonParser = new JSONParser();
                        // JSONObject jsonObject = (JSONObject) jsonParser.parse(metafileReader);
                        JSONObject jsonObject = (JSONObject) jsonParser.parse(ipstreamreader);
                        isCompressed = Boolean.valueOf(jsonObject.get("isCompressed").toString());
                        String groupFileName = (String) jsonObject.get("groupFileName");
                        String ruleFileName = (String) jsonObject.get("ruleFileName");
                        inputStream.close();

                        inputStream = getReaderObject(zipFile, groupFileName);
                        ipstreamreader = new InputStreamReader(inputStream);
                        group = readGroupCSVFile(ipstreamreader);

                        if (!Boolean.valueOf(isCompressed)) {
                            inputStream = getReaderObject(zipFile, ruleFileName);
                            ipstreamreader = new InputStreamReader(inputStream);
                            ruleList = readRuleCSVFile(ipstreamreader);
                            inputStream.close();

                            String rulesetRuleMapFileName = (String) jsonObject.get("rulesetRuleMapFileName");
                            inputStream = getReaderObject(zipFile, rulesetRuleMapFileName);
                            ipstreamreader = new InputStreamReader(inputStream);
                            ruleRulesetMap = readRuleSetRuleMapCSVFile(ipstreamreader);
                            inputStream.close();
                            
                        } else {
                            inputStream = getReaderObject(zipFile, ruleFileName);
                            ipstreamreader = new InputStreamReader(inputStream);
                            compressedRuleObj = readCompressedRuleCSVFile(ipstreamreader);
                            inputStream.close();
                        }

                    } catch (ZipException e) {
                        if (e.getCode() == ZipExceptionConstants.WRONG_PASSWORD) {

                            System.out.println("Wrong password");
                            resultMap.put("isPWDCorrect", false);
                            resultMap.put("errorMessage", "Please enter the correct password.");
                            return resultMap;
                        } else {
                            // Corrupt file
                            LOGGER.error(e.getMessage(), e);
                            response.sendRedirect("jsp/Error.jsp");
                            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION,
                                    GenericConstants.EXCEPTION_MESSAGE);
                        }
                    } catch (IOException e) {
                        System.out.println("Most probably wrong password.");
                        resultMap.put("isPWDCorrect", false);
                        resultMap.put("errorMessage", "Please enter the correct password.");
                        e.printStackTrace();
                        System.out.println(e.getMessage());
                        return resultMap;
                    }
                    
                    resultMap.put("isPWDCorrect", true);
                    boolean isGroupExists = false;
                    Integer groupId = groupService.getGroupId(group.getGroupName());
                    group.setGroupID(groupId);

                    List<String> commonRuleList = new ArrayList<String>();
                    if (groupId != null) {
                        isGroupExists = true;
                        List<String> ruleNames = groupService.getRulesByGroup(groupId);
                        if (!Boolean.valueOf(isCompressed)) {
                            for (Rule rule : ruleList) {
                                if (ruleNames.contains(rule.getRuleName())) {
                                    commonRuleList.add(rule.getRuleName());
                                }
                            }
                            resultMap.put("commonRuleList", commonRuleList);
                            resultMap.put("ruleList", ruleList);
                            resultMap.put("ruleSetRuleMap", ruleRulesetMap);
                        } else {
                            if (ruleNames != null && ruleNames.size() > 0) {
                                if (ruleNames.contains(compressedRuleObj.getRuleName())) {
                                    commonRuleList.add(ruleNames.get(0));
                                    resultMap.put("isRuleNameSame", true);
                                } else {
                                    resultMap.put("isRuleNameSame", false);
                                }
                            }
                            resultMap.put("rule", compressedRuleObj);
                        }

                    } else {
                        if (!Boolean.valueOf(isCompressed)) {
                            resultMap.put("ruleList", ruleList);
                            resultMap.put("ruleSetRuleMap", ruleRulesetMap);
                        } else {
                            resultMap.put("rule", compressedRuleObj);
                        }
                    }
                    resultMap.put("isGroupExists", isGroupExists);
                    resultMap.put("isCompressed", Boolean.valueOf(isCompressed));
                    resultMap.put("group", group);

                } else {
                    resultMap.put("errorMessage", "ZIP is not valid/password protected.");
                }

            } catch (Exception e) {
                LOGGER.error("Error while uploading CSV file.");
                throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
            }
        } else {
            resultMap.put("errorMessage", "File is empty");
        }
        return  resultMap;
    }
    
    
    /**
     * Import group.
     *
     * @param importGroupData the import group data
     * @param isGroupExists the is group exists
     * @param isCompressed the is compressed
     * @param request the request
     * @param response the response
     */
    @RequestMapping(value = "/importGroup", method = RequestMethod.POST)
    public final @ResponseBody
    Map<String, Object> importGroup(@RequestParam("importGroupData") final String importGroupData,
            @RequestParam("isGroupExists") final boolean isGroupExists,
            @RequestParam("isCompressed") final boolean isCompressed, final HttpServletRequest request,
            final HttpServletResponse response) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("isSuccess", true);
        try {
            String userName = (String) request.getSession().getAttribute("username");
            Map<String, List<String>> ruleSetRuleMap = null;
            List<Object> ruleList = null;
            Object rule = null;
            final Map<String, Object> importGpdataMap =
                    mapper.readValue(importGroupData, new TypeReference<Map<String, Object>>() {
                    });
            final Map<String, Object> group = (Map<String, Object>) importGpdataMap.get("group");
            if (!isCompressed) {
                ruleSetRuleMap = (Map<String, List<String>>) importGpdataMap.get("ruleSetRuleMap");
                ruleList = (List<Object>) importGpdataMap.get("ruleList");
            } else {
                rule = (Map<String, Object>) importGpdataMap.get("rule");

            }
            final List<String> commonRuleList = (List<String>) importGpdataMap.get("commonRules");
            if (!isGroupExists) {
                resultMap = groupService.importGroup(group, ruleList, ruleSetRuleMap, rule, isCompressed, userName);
            } else {
                Integer groupId = (Integer) group.get("groupID");
                if (!isCompressed) {
                    resultMap =
                            groupService.importExistingGroup(groupId, ruleList, ruleSetRuleMap, commonRuleList,
                                    userName);
                } else {
                    groupService.importExistingCompressedGroup(groupId, rule, userName);
                }
            }

        } catch (Exception e) {
            LOGGER.error("Error while importing group", e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        return resultMap;
    }
    
    /**
     * Send response.
     *
     * @param fileName the file name
     * @param httpResponse the http response
     * @param outFilename the out filename
     * @return the input stream
     * @throws FileNotFoundException the file not found exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private InputStream sendResponse(final String fileName, HttpServletResponse httpResponse, String outFilename)
            throws FileNotFoundException, IOException {
        InputStream fileInputStream;
        // Download Zip
        fileInputStream = new FileInputStream(outFilename);

        httpResponse.setContentType("application/octet-stream");
        final String headerValue = String.format("attachment; filename=\"%s\"", fileName + ".zip");
        httpResponse.setHeader("Content-Disposition", headerValue);
        IOUtils.copy(fileInputStream, httpResponse.getOutputStream());
        httpResponse.flushBuffer();

        return fileInputStream;
    }

}
