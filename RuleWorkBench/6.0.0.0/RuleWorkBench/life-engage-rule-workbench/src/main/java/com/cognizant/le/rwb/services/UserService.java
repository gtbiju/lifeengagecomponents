/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 7, 2014
 */
package com.cognizant.le.rwb.services;

import com.cognizant.le.rwb.domain.User;

/**
 * The Interface interface UserService.
 * 
 * @author 304003
 */
public interface UserService {

    /**
     * Validate user.
     * 
     * @param user
     *            the user
     * @return the boolean
     */
    public Boolean validateUser(User user);
    public User getUserId(User user);
}
