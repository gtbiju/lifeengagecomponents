/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 418964
 * @version       : 0.1, May 30, 2014
 */
package com.cognizant.le.rwb.domain;

/**
 * The Class class JobDetails.
 */
public class JobDetails {

	/** The status code. */
	private String statusCode;

	/** The job id . */
	private Long jobId;

	/** The file path. */
	private String filePath;

	/** The rule name. */
	private String ruleName;

	/** The start time. */
	private String startTime;

	/** The end time. */
	private String endTime;

	/** The comment. */
	private String comment;

	/** The error message. */
	private String errorMsg;

	/**
	 * Gets the error message.
	 * 
	 * @return errorMsg the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * Sets the error message.
	 * 
	 * @param errorMsg
	 *            the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	/**
	 * Gets the comment.
	 * 
	 * @return comment the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment.
	 * 
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Gets the start time.
	 * 
	 * @return startTime the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * Sets the start time.
	 * 
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * Gets the end time.
	 * 
	 * @return endTime the endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * Sets the end time.
	 * 
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	 * Gets the rule name.
	 * 
	 * @return ruleName the ruleName
	 */
	public String getRuleName() {
		return ruleName;
	}

	/**
	 * Sets the rule name.
	 * 
	 * @param ruleName
	 *            the ruleName to set
	 */
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	/**
	 * Gets the status code.
	 * 
	 * @return statusCode the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * Sets the status code.
	 * 
	 * @param statusCode
	 *            the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * Gets the job id.
	 * 
	 * @return jobId the jobId
	 */
	public Long getJobId() {
		return jobId;
	}

	/**
	 * Sets the job id.
	 * 
	 * @param jobId
	 *            the jobId to set
	 */
	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	/**
	 * Gets the file path.
	 * 
	 * @return filePath the filePath
	 */
	public String getFilePath() {
		return filePath;
	}

	/**
	 * Sets the file path.
	 * 
	 * @param filePath
	 *            the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}
