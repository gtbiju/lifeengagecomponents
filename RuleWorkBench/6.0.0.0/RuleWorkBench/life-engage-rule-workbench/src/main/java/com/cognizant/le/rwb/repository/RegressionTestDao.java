/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 418964
 * @version       : 0.1, May 30, 2014
 */
package com.cognizant.le.rwb.repository;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cognizant.le.rwb.domain.JobDetails;

/**
 * The Interface interface RegressionTestDao.
 * 
 */
public interface RegressionTestDao {

	public void getUploadedFile(MultipartFile file, String filePath);

	public void createJobDetails(int userId, Long jobId, String ruleName,
			String jobStatus, String filePath, String comment, String errorMsg);

	public List<JobDetails> retrieveJobDetails(int userId);

	public void updateJobDetails(Long jobId, String ruleName, String jobStatus,
			String filePath, String errorMsg);

}
