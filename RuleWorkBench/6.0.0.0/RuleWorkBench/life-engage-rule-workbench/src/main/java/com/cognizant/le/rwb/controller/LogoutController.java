/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 300797
 * @version       : 0.1, Feb 17, 2014
 */
package com.cognizant.le.rwb.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cognizant.le.rwb.domain.User;

/**
 * The Class class LogoutController.
 * 
 * @author 300797
 */
@Controller
public class LogoutController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(LogoutController.class);

    /**
     * Logout.
     * 
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the model and view
     */
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.debug("Method Entry : LoginAction-> logout");
        HttpSession session = request.getSession(true);
        session.invalidate();
        ModelAndView model = new ModelAndView("login", "user", new User());
        return model;
    }
}
