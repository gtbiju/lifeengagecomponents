/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 7, 2014
 */

package com.cognizant.le.rwb.services;

import java.util.List;
import java.util.Map;

import com.cognizant.le.rwb.domain.Rule;

// TODO: Auto-generated Javadoc
/**
 * The Interface interface GroupService.
 * 
 * @author 304003
 */
public interface GroupService {

    /**
     * Export compressed group.
     *
     * @param groupID the group id
     * @param ruleNames the rule names
     * @param ruleName the rule name
     * @return the string
     */
    Map<String, Object> exportCompressedGroup(Integer groupID, List<String> ruleNames, String ruleName);

    /**
     * Export un compressed group.
     * 
     * @param groupID
     *            the group id
     * @param ruleNames
     *            the rule names
     * @return the map
     */
    Map<String, Object> exportUnCompressedGroup(Integer groupID, List<String> ruleNames);

    /**
     * Import group.
     *
     * @param group the group
     * @param ruleList the rule list
     * @param rulesetRulemap the ruleset rulemap
     * @param rule the rule
     * @param isCompressed the is compressed
     * @param userName the user name
     */
    Map<String,Object> importGroup(Map<String, Object> group, List<Object> ruleList, Map<String, List<String>> rulesetRulemap,
            Object rule, boolean isCompressed, String userName);

    /**
     * Import existing group.
     *
     * @param groupId the group id
     * @param ruleList the rule list
     * @param rulesetRulemap the ruleset rulemap
     * @param commonRuleList the common rule list
     * @param userName the user name
     */
    Map<String,Object> importExistingGroup(Integer groupId, List<Object> ruleList, Map<String, List<String>> rulesetRulemap,
            List<String> commonRuleList, String userName);

    /**
     * Gets the group id.
     * 
     * @param groupName
     *            the group name
     * @return the group id
     */
    Integer getGroupId(String groupName);

    /**
     * Gets the imported rules and rulesets.
     * 
     * @param groupID
     *            the group id
     * @return the imported rules and rulesets
     */
    List<Rule> getImportedRulesAndRulesets(String groupID);

    /**
     * Gets the rules by group.
     * 
     * @param groupName
     *            the group name
     * @return the rules by group
     */
    List<String> getRulesByGroup(Integer groupName);

    /**
     * Import existing compressed group.
     *
     * @param groupId the group id
     * @param Rule the rule
     * @param userName the user name
     */
    void importExistingCompressedGroup(Integer groupId, Object Rule, String userName);
    
   // List<String> checkForNewRulesInRuleSet(Map<String, List<String>> rulesetRulemap, List<String> ruleSetNames, Integer groupId);

}
