/**
 *
 * Copyright 2014, Cognizant 
 *
 * @author        : 304003
 * @version       : 0.1, Feb 28, 2013
 */
package com.cognizant.le.rwb.domain;

import java.sql.Time;
import java.util.Date;

/**
 * The Class class Rule.
 */
public class Rule {

    /** The rule id. */
    private Integer ruleID;

    /** The rule type id. */
    private Integer ruleTypeID;

    /** The rule name. */
    private String ruleName;

    /** The client id. */
    private String clientId;

    /** The created date. */
    private Date createdDate;

    /** The created time. */
    private Time createdTime;

    /** The updated date. */
    private Date updatedDate;

    /** The updated time. */
    private Time updatedTime;

    /** The version. */
    private Float version;

    /** The rule json. */
    private String ruleJson;

    /** The rule js. */
    private String ruleJS;

    /** The group name. */
    private String groupName;

    /** The offline rule js. */

    private String offlineRuleJS;

    /** The comments. */
    private String comments;

    /** The status. */
    private String status;

    private String userId;

    private Date statusDate;

    /**
     * Gets the rule id.
     * 
     * @return the rule id
     */
    public final Integer getRuleID() {
        return ruleID;
    }

    /**
     * Sets the rule id.
     * 
     * @param ruleID
     *            the rule id to set.
     */
    public final void setRuleID(final Integer ruleID) {
        this.ruleID = ruleID;
    }

    /**
     * Gets the rule type id.
     * 
     * @return the rule type id
     */
    public final Integer getRuleTypeID() {
        return ruleTypeID;
    }

    /**
     * Sets the rule type id.
     * 
     * @param ruleTypeID
     *            the rule type id to set.
     */
    public final void setRuleTypeID(final Integer ruleTypeID) {
        this.ruleTypeID = ruleTypeID;
    }

    /**
     * Gets the rule name.
     * 
     * @return the rule name
     */
    public final String getRuleName() {
        return ruleName;
    }

    /**
     * Sets the rule name.
     * 
     * @param ruleName
     *            the rule name to set.
     */
    public final void setRuleName(final String ruleName) {
        this.ruleName = ruleName;
    }

    /**
     * Gets the client id.
     * 
     * @return the client id
     */
    public final String getClientId() {
        return clientId;
    }

    /**
     * Sets the client id.
     * 
     * @param clientId
     *            the client id to set.
     */
    public final void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    /**
     * Gets the created date.
     * 
     * @return the created date
     */
    public final Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Sets the created date.
     * 
     * @param createdDate
     *            the created date to set.
     */
    public final void setCreatedDate(final Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * Gets the version.
     * 
     * @return the version
     */
    public final Float getVersion() {
        return version;
    }

    /**
     * Sets the version.
     * 
     * @param version
     *            the version to set.
     */
    public final void setVersion(final Float version) {
        this.version = version;
    }

    /**
     * Gets the rule json.
     * 
     * @return the rule json
     */
    public final String getRuleJson() {
        return ruleJson;
    }

    /**
     * Sets the rule json.
     * 
     * @param ruleJson
     *            the rule json to set.
     */
    public final void setRuleJson(final String ruleJson) {
        this.ruleJson = ruleJson;
    }

    /**
     * Gets the rule js.
     * 
     * @return the rule js
     */
    public final String getRuleJS() {
        return ruleJS;
    }

    /**
     * Sets the rule js.
     * 
     * @param ruleJS
     *            the rule js to set.
     */
    public final void setRuleJS(final String ruleJS) {
        this.ruleJS = ruleJS;
    }

    /**
     * Gets the updated date.
     * 
     * @return the updated date
     */
    public final Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * Sets the updated date.
     * 
     * @param updatedDate
     *            the updated date to set.
     */
    public final void setUpdatedDate(final Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    /**
     * Gets the group name.
     * 
     * @return the group name
     */
    public final String getGroupName() {
        return groupName;
    }

    /**
     * Sets the group name.
     * 
     * @param groupName
     *            the group name to set.
     */
    public final void setGroupName(final String groupName) {
        this.groupName = groupName;
    }

    /**
     * Gets the created time.
     * 
     * @return the created time
     */
    public final Time getCreatedTime() {
        return createdTime;
    }

    /**
     * Sets the created time.
     * 
     * @param createdTime
     *            the created time to set.
     */
    public final void setCreatedTime(final Time createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * Gets the updated time.
     * 
     * @return the updated time
     */
    public final Time getUpdatedTime() {
        return updatedTime;
    }

    /**
     * Sets the updated time.
     * 
     * @param updatedTime
     *            the updated time to set.
     */
    public final void setUpdatedTime(final Time updatedTime) {
        this.updatedTime = updatedTime;
    }

    /**
     * Gets the offline rule js.
     * 
     * @return the offline rule js
     */
    public final String getOfflineRuleJS() {
        return offlineRuleJS;
    }

    /**
     * Sets the offline rule js.
     * 
     * @param offlineRuleJS
     *            the offline rule js to set.
     */
    public final void setOfflineRuleJS(final String offlineRuleJS) {
        this.offlineRuleJS = offlineRuleJS;
    }

    /**
     * Gets the comments.
     * 
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the comments.
     * 
     * @param comments
     *            the comments to set.
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }
}
