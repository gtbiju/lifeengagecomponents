/**
 *
 * Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Feb 14, 2013
 */

package com.cognizant.le.rwb.constant;

// TODO: Auto-generated Javadoc
/**
 * The Class class GenericConstants.
 */
public final class GenericConstants {

	/**
	 * Instantiates a new generic constants.
	 */
	private GenericConstants() {

	}

	/** The Constant VALIDATE_USER_SQL. */
	public static final String VALIDATE_USER_SQL = "validateUserSql";

	/** The Constant INSERT_RULE. */
	public static final String INSERT_RULE = "insertRule";

	/** The Constant INSERT_RULE. */
	public static final String INSERT_GROUP = "insertGroup";

	/** The Constant SELECT_BUSINESSRULES. */
	public static final String SELECT_RULES_RULESET = "selectRulesRuleSet";

	/** The Constant INSERT_RULESET_RULE_MAPPING. */
	public static final String INSERT_RULESET_RULE_MAPPING = "insertRuleSetRuleMapping";

	/** The Constant SELECT_RULESET_FOR_RULESETID. */
	public static final String SELECT_RULESET_FOR_RULESETID = "selectRulesetForRuleSetId";

	/** The Constant SELECT_ASSOCIATED_RULES_FOR_RULESETID. */
	public static final String SELECT_ASSOCIATED_RULES_FOR_RULESETID = "selectAssociatedRules";

	/** The Constant VERSION. */
	public static final Float VERSION = 1.00f;
	
	/** The Constant RULETYPE_ID. */
	public static final Integer RULETYPE_ID = 3;
	
	/** The Constant RULE_EXISTS. */
	public static final String RULE_EXISTS = "ruleExists";
	
	/** The Constant COMMON_RULE_EXISTS. */
	public static final String COMMON_RULE_EXISTS = "commonRuleExists";

	/** The Constant RULE_EXISTS. */
	public static final String GROUP_EXISTS = "groupExists";

	/** The Constant UPDATE_RULE. */
	public static final String UPDATE_RULE = "updateRule";

	/** The Constant UPDATE_RULESET. */
	public static final String UPDATE_RULESET = "updateRuleSet";

	/** The Constant UPDATE_RULESET. */
	public static final String DELETE_RULESET_RULE_MAPPING = "deleteRuleSetRuleMapping";

	/** The Constant SELECT_RULEJS. */
	public static final String SELECT_RULEJS = "selectRuleJS";

	/** The Constant DELETE_RULE. */
	public static final String DELETE_RULE = "deleteRule";

	/** The Constant GET_RULE_TYPE. */
	public static final String GET_RULE_TYPE = "getRuleType";

	/** The Constant SELECT_GROUP_LIST. */
	public static final String SELECT_GROUP_LIST = "selectGroupList";

	/** The Constant GET_GROUP_ID. */
	public static final String GET_GROUP_ID = "getGroupId";

	/** The Constant GET_GROUP_NAME. */
	public static final String GET_GROUP_NAME = "getGroupName";

	/** The Constant UPDATE_GROUP. */
	public static final String UPDATE_GROUP = "updateGroup";

	/** The Constant DELETE_GROUP. */
	public static final String DELETE_GROUP = "deleteGroup";

	/** The Constant SELECT_RULES_BY_GROUPID. */
	public static final String SELECT_RULES_BY_GROUPID = "selectRulesByGroupId";

	/** The Constant SELECT_RULE_EDIT. */
	public static final String SELECT_RULE_EDIT = "selectRuleEdit";

	/** The Constant SELECT_RULE_SEL. */
	public static final String SELECT_RULE_SEL = "selectRuleSel";

	/** The Constant SELECT_GROUP_TYPE_LIST. */
	public static final String SELECT_GROUP_TYPE_LIST = "selectGroupTypeList";

	/** The Constant SELECT_RULE_WITH_MAPPING. */
	public static final String SELECT_RULE_WITH_MAPPING = "selectRulesWithMapping";

	/** The Constant MAPPING_TABLE_INSERT. */
	public static final String MAPPING_TABLE_INSERT = "mappingTableInsert";

	/** The Constant SELECT_REST_RULES. */
	public static final String SELECT_REST_RULES = "selectRestRules";

	/** The Constant GET_GROUP_TYPE. */
	public static final String GET_GROUP_TYPE = "selectGroupType";
	
	/** The Constant SELECT_RULE_EXISTING_RULESET. */
	public static final String SELECT_RULE_EXISTING_RULESET = "selectRuleExistingRuleset";
	
	/** The Constant DELETE_RULES_IN_GROUP. */
	public static final String DELETE_RULES_IN_GROUP = "deleteRulesInAGroup";
	
	/** The Constant SELECT_RULEID_BASEDON_GROUP. */
	public static final String SELECT_RULEID_BASEDON_GROUP = "selectRuleIdBasedOnGroup";
	
	/** The Constant DELETE_RULESET_IN_GROUP. */
	public static final String  DELETE_RULESET_IN_GROUP = "deleteRulesetInAGroup";
	
	/** The Constant GET_RULE_AND_RULESET_COUNT_IN_GROUP. */
	public static final String  GET_RULE_AND_RULESET_COUNT_IN_GROUP = "getRuleAndRulesetCountInAGroup";
	
	/** The Constant SELECT_IMPORTED_RULE_RULESETS. */
	public static final String SELECT_IMPORTED_RULE_RULESETS = "selectImpRuleAndRulesets";
	
	/** The Constant SELECT_RULENAME. */
	public static final String  SELECT_RULENAME = "selectRuleName";
	
	/** The Constant SELECT_COMMON_GROUP_RULES. */
	public static final String SELECT_COMMON_GROUP_RULES = "selectCommonGroupRules";

	/** The Constant RULE_DUP_MESSAGE. */
	public static final String RULE_DUP_MESSAGE = "Cannot merge the group.Below rule names are already present in the group.";

	/** The Constant GROUP_TYPE_DIFF_MESSAGE. */
	public static final String GROUP_TYPE_DIFF_MESSAGE = "Cannot merge the group.Group type is different";
	
	/** The Constant RULE_DELETE_ERROR_MSG. */
	public static final String RULE_DELETE_ERROR_MSG = "Rule exists in the below rulesets. Please delete the rule from the rulsets.";
	
	/** The Constant NEW_LINE. */
	public static final String NEW_LINE = System.getProperty("line.separator");

	/** The Constant DELETE_RULESET. */
	public static final String GET_TABLE_NAMES = "getTableNames";

	/** The Constant DELETE_RULESET. */
	public static final String GET_COLUMN_NAMES = "getColumnNames";

	/** The Constant CONTINUATION_SERVICE_URL. */
	public static final String CONTINUATION_SERVICE_URL = "le.rw.continuationServiceURL";

	/** The Constant CLIENT_ID. */
	public static final String CLIENT_ID = "le.rw.clientId";

    /** The Constant EXCEPTION_MESSAGE. */
    public static final String EXCEPTION_MESSAGE =
            "We can't proceed your request now as we are experiencing some technical issues in server. Please try again later or contact administrator.";
    /** The Constant RULEEXCEPTION_MESSAGE. */
    public static final String RULEEXCEPTION_MESSAGE  = "Please contact administrator for more deatils.";
    
    /** The Constant SERVICE_EXCEPTION. */
    public static final String SERVICE_EXCEPTION = "ServiceException";
    
    /** The Constant DEBUG_CONF. */
    public static final String DEBUG_CONF = "le.rw.debug";
    
    /** The Constant CANONICAL_JSON_LOOKUP_TYPE. */
    public static final String CANONICAL_JSON_LOOKUP_TYPE = "le.rw.canonicalJsonLookupType";
    
    /** The Constant CONTENT_ADMIN_DOWNLOAD_URL. */
    public static final String CONTENT_ADMIN_DOWNLOAD_URL ="le.rw.contentAdminDownloadURL";
    
    /** The Constant INPUT_FILE_PATH. */
    public static final String INPUT_FILE_PATH = "le.rw.regressionTestFilePath";

    /** The Constant INSERT_JOB_DETAILS. */
	public static final String INSERT_JOB_DETAILS = "insertJobDetails";
	
	/** The Constant SELECT_JOB_DETAILS. */
	public static final String GET_JOB_DETAILS = "getJobDetails";
	
	/** The Constant UPDATE_JOB_DETAILS. */
	public static final String UPDATE_JOB_DETAILS = "updateJobDetails";	
	
	/** The Constant COMPLETED_WITH_ERROR_STATUS */
	public static final String COMPLETED_WITH_ERROR_STATUS = "COMPLETED WITH ERRORS";
	
	public static final String SELECT_RULES_TOEXPORT = "selectRulesToExport";
	public static final String INSERT_GROUP_QUERY = "INSERT_GROUP";
	public static final String INSERT_RULE_QUERY = "INSERT_RULE";
	public static final String INSERT_RULE_RULESET_MAP_QUERY = "INSERT_RULE_RULESET_MAP";
	public static final String SELECT_GROUP_TO_EXPORT ="selectGroupToExport";
	public static final String SELECT_RULES_WITH_MAPPING_TOEXPORT ="selectRulesWithMappingToExport";
	public static final String UPDATE_EXISTING_RULE_AND_RULESET = "updateExistingRuleAndRuleset";
	public static final String DELETE_EXISTING_RULESETRULEMAPPING = "deleteExistingRulesetRuleMapping";
	public static final String UPDATE_COMPRESSED_RULES = "updateCompressedRules";
	public static final String INSERT_IMPORTED_RULE= "insertImportedRule";
	public static final String CREATE_RULE_RULESET_HISTORY = "createRuleRulesetHistory";
	public static final String RULE_EXISTS_CHK_FOR_EDIT = "ruleExistChkForEdit";
	
	public static final String GROUP_FILE_NAME = "le.rw.groupFileName";
	public static final String RULE_FILE_NAME = "le.rw.ruleFileName";
	public static final String RULESET_RULE_MAP_FILE_NAME = "le.rw.rulesetRuleMapFileName";
	public static final String METADATA_FILE_NAME = "le.rw.metadataFileName";
	
	public static final String  STATUS_ACTIVE = "A";
	public static final String  STATUS_HISTORY = "H";
	
	/** The Constant JSON_EXCEPTION_MESSAGE. */
    public static final String JSON_EXCEPTION_MESSAGE =
            "We can't proceed your request now as we are experiencing some issues in converting input excel sheet into json values. Please verify the input config mapping.";
	
}
