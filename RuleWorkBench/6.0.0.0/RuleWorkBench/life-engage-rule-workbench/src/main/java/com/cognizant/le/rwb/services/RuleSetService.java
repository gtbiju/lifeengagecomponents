/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 7, 2014
 */
package com.cognizant.le.rwb.services;

import java.util.List;
import java.util.Map;

import com.cognizant.le.rwb.domain.Rule;
import com.cognizant.le.rwb.domain.RuleSet;
import com.cognizant.le.rwb.domain.RuleSetRuleMapping;

/**
 * The Interface interface RuleSetService.
 * 
 * @author 304003
 */
public interface RuleSetService {

    /**
     * Creates the rule set.
     * 
     * @param RuleSet
     *            the rule set
     * @param ruleSetRuleMappingBOList
     *            the rule set rule mapping bo list
     * @return the string
     */
    public Map<String, String> createRuleSet(Rule RuleSet, List<RuleSetRuleMapping> ruleSetRuleMappingBOList);

    /**
     * List rule set for update.
     * 
     * @param ruleSetID
     *            the rule set id
     * @return the list
     */
    public List<RuleSet> listRuleSetForUpdate(Integer ruleSetID);

    /**
     * List associated rules.
     * 
     * @param ruleSetID
     *            the rule set id
     * @return the list
     */
    public List<Rule> listAssociatedRules(Integer ruleSetID);

    /**
     * Edits the rule set.
     * 
     * @param RuleSet
     *            the rule set
     * @param ruleSetRuleMappingBOList
     *            the rule set rule mapping bo list
     */
    public Map<String,String> editRuleSet(Rule RuleSet, List<RuleSetRuleMapping> ruleSetRuleMappingBOList, boolean isRuleSetNameChanged);

    /**
     * Delete rule set.
     * 
     * @param ruleSetId
     *            the rule set id
     */
    public void deleteRuleSet(Integer ruleSetId);

    /**
     * Adds the rule set json.
     * 
     * @param ruleId
     *            the rule id
     * @param combinedJSONString
     *            the combined json string
     * @return the string
     */
    public String addRuleSetJson(Integer ruleId, String combinedJSONString, Map<String, List<String>> canonicalJson);
 
    /**
     * Gets the rule json.
     * 
     * @param ruleId
     *            the rule id
     * @return the rule json
     */
    public Map<String, String> getRuleJson(Integer ruleId);

}
