/**
 *
 * © Copyright 2013, Cognizant
 *
 * @author        : 262471
 * @version       : 0.1, Feb 4, 2013
 */

package com.cognizant.le.rwb.exception;

/**
 * The Class class SystemException.
 */
public class SystemException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4140972114171571944L;

	/** The error code. */
	private String errorCode;

	/**
	 * Instantiates a new system exception.
	 */
	public SystemException() {
		super();
	}

	/**
	 * Instantiates a new system exception.
	 * 
	 * @param message
	 *            the message
	 */
	public SystemException(final String message) {
		super(message);
	}

	/**
	 * Instantiates a new system exception.
	 * 
	 * @param errorCode
	 *            the error code
	 * @param message
	 *            the message
	 */
	public SystemException(final String errorCode, final String message) {
		super(message);
		this.errorCode = errorCode;
	}

	/**
	 * Instantiates a new system exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	public SystemException(final Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new system exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public SystemException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new system exception.
	 * 
	 * @param errorCode
	 *            the error code
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public SystemException(final String errorCode, final String message,
			final Throwable cause) {
		super(message, cause);
		this.errorCode = errorCode;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public final void setErrorCode(final String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorCode
	 */
	public final String getErrorCode() {
		return errorCode;
	}

}
