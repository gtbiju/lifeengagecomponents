/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 418964
 * @version       : 0.1, May 30, 2014
 */

package com.cognizant.le.rwb.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.minidev.json.parser.JSONParser;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cognizant.icr.exception.RuleException;
import com.cognizant.icr.rule.bom.RuleCriteria;
import com.cognizant.icr.rule.v2.RuleEngine;
import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.domain.JobDetails;
import com.cognizant.le.rwb.exception.ServiceException;
import com.cognizant.le.rwb.repository.RegressionTestDao;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

/**
 * The Class class RegressionTestServiceImpl.
 * 
 */

@Service("regressionTestService")
public class RegressionTestServiceImpl implements RegressionTestService {

    /** The environment. */
    @Autowired
    private Environment environment;

    /** The rule engine. */
    @Autowired
    private RuleEngine ruleEngine;

    /** The RegressionTest dao. */
    @Autowired
    private RegressionTestDao regressionTestDao;

    /** The rule service. */
    @Autowired
    private RuleService ruleService;

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RegressionTestServiceImpl.class);

    /**
     * uploads the input excel sheet file.
     * 
     * @param file
     *            the file
     * 
     * @return the String
     */
    public String uploadExcelFile(MultipartFile file) {
        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> uploadExcelFile");
        String fileName = file.getOriginalFilename();
        // creating the uploaded file name with time stamp
        String inputFilePath =
                environment.getProperty(GenericConstants.INPUT_FILE_PATH) + new Date().getTime() + "_" + fileName;
        regressionTestDao.getUploadedFile(file, inputFilePath);
        LOGGER.debug("Method Exit : RegressionTestServiceImpl-> uploadExcelFile");
        return inputFilePath;
    }

    /**
     * calls the rule engine and executes rule with excel data values.
     * 
     * @param filePath
     *            the file path
     * @param ruleId
     *            the rule id
     * @param ruleName
     *            the rule name
     * @param outputFilePath
     *            the output file path
     * @return the Boolean
     */
    public Boolean executeRegressionTest(String filePath, String ruleId, String ruleName, String outputFilePath) {
        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> executeRegressionTest");
        Boolean errorFlag = false;
        try {
            File inputFile = new File(filePath);
            FileInputStream inputStream = new FileInputStream(inputFile);
            OPCPackage opc = OPCPackage.open(inputStream);
            XSSFWorkbook workbook = (XSSFWorkbook) WorkbookFactory.create(opc);
            // Get the workbook instance for XLS file
            List<List<Object>> excelValue = new ArrayList<List<Object>>();

            Map<String, List<List<Object>>> illustrationListMap = new LinkedHashMap<String, List<List<Object>>>();
            HashMap<String, LinkedHashMap<String, String>> arrayValueMap =
                    new LinkedHashMap<String, LinkedHashMap<String, String>>();
            // calling getOutputConfigMap method which returns input data config
            // mapping details
            LinkedHashMap<String, String> columnNames = getOutputConfigMap(workbook, 2);

            // calling getOutputConfigMap method which returns illustration data config
            // mapping details
            if (workbook.getNumberOfSheets() == 4) {
                // illustrationTBLColmnNames = getOutputConfigMap(workbook,3);
                arrayValueMap = getArrayConfigMap(workbook, 3);
            }

            String categoryId = ruleService.getGroupID(Integer.valueOf(ruleId));
            RuleCriteria ruleCriteria = new RuleCriteria();
            ruleCriteria.setCategory(categoryId);
            ruleCriteria.setClientId(environment.getProperty(GenericConstants.CLIENT_ID));
            ruleCriteria.setRule(ruleName);
            // Creating the input JSON map from excel data
            String jsonOutput = "";
            JSONArray inoutMapArray = createJSONArray(workbook);
            List<HashMap<String, List<List<Object>>>> arrayValueMapList =
                    new ArrayList<HashMap<String, List<List<Object>>>>();
            for (int n = 0; n < inoutMapArray.length(); n++) {

                List<Object> cellValues = new ArrayList<Object>();
                List<Object> illustrationCellValues = new ArrayList<Object>();
                JSONObject inoutMap = inoutMapArray.getJSONObject(n);
                // calling rule engine with data corresponding to each row in
                // excel sheet
                LOGGER.debug("Calling rule engine for row: " + n);
                try {
                    jsonOutput = ruleEngine.executeRuleWithJson(ruleCriteria, inoutMap.toString());
                    LOGGER.debug("Results obtained from rule engine for row: " + n);
                    cellValues = retrieveOutputData(columnNames, jsonOutput);
                    if (arrayValueMap != null && arrayValueMap.size() > 0) {
                        HashMap<String, List<List<Object>>> arrayOutptMap =
                                getArrayCongigValues(arrayValueMap, jsonOutput);
                        arrayValueMapList.add(arrayOutptMap);
                    }

                } catch (RuleException e) {
                    LOGGER.error("Rule Exception while execute Rule" + e.getMessage(), e);
                    errorFlag = true;
                    cellValues.add(e.getMessage() + GenericConstants.NEW_LINE + GenericConstants.EXCEPTION_MESSAGE);
                    illustrationCellValues.add(e.getMessage() + GenericConstants.NEW_LINE
                            + GenericConstants.EXCEPTION_MESSAGE);
                }
                excelValue.add(cellValues);

            }
            // Creating output excel from the output json obtained from rule
            // engine

            int rowNum = 0;
            XSSFSheet sheet = workbook.createSheet("Output");
            createOutputExcelSheet(workbook, sheet, columnNames, excelValue, errorFlag, rowNum, 0);
            if (workbook.getNumberOfSheets() == 5) {
                XSSFSheet illustrationSheet = workbook.createSheet("ArrayOutput");
                createOutputExcelSheetForArray(workbook, illustrationSheet, arrayValueMapList, arrayValueMap, errorFlag,
                        rowNum);
            }
            
            createOutputExcel(workbook, filePath);

        } catch (SQLException e) {
            LOGGER.error("SQL Exception while execute Rule", e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        } catch (ServiceException e) {
            LOGGER.error("Error while execute Rule", e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, e.getDescription());
        } catch (Exception e) {
            LOGGER.error("Error while execute Rule", e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, e.getMessage() + GenericConstants.NEW_LINE
                    + GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RegressionTestServiceImpl-> excuteRegressionTest");
        return errorFlag;
    }

    /**
     * Creates the output excel sheet for array.
     * 
     * @param workbook
     *            the workbook
     * @param sheet
     *            the sheet
     * @param arrayValueMapList
     *            the array value map list
     * @param arrayKeys
     *            the array keys
     * @param errorFlag
     *            the error flag
     * @param rowNumber
     *            the row number
     */
    private void createOutputExcelSheetForArray(XSSFWorkbook workbook, XSSFSheet sheet,
            List<HashMap<String, List<List<Object>>>> arrayValueMapList,
            HashMap<String, LinkedHashMap<String, String>> arrayKeys, Boolean errorFlag, int rowNumber) {
        if (arrayValueMapList.size() > 0 && !arrayValueMapList.isEmpty()) {
            for (HashMap<String, List<List<Object>>> arrayValueMap : arrayValueMapList) {
                int startCellNum = 0;
                int endCellNum = 0;
                int rowNum = 0;
                for (Entry<String, List<List<Object>>> arrayValueEntry : arrayValueMap.entrySet()) {
                    rowNum = rowNumber;
                    String key = arrayValueEntry.getKey();
                    List<List<Object>> value = arrayValueEntry.getValue();

                    LinkedHashMap<String, String> arrayColumnNames = arrayKeys.get(key);

                    Row testcaseHeader = null;
                    if (isRowEmpty(sheet.getRow(rowNum)) == false) {
                        testcaseHeader = sheet.getRow(rowNum);
                    } else {
                        testcaseHeader = sheet.createRow(rowNum);
                    }

                    CellStyle cellStyle = workbook.createCellStyle();
                    cellStyle.setAlignment(CellStyle.ALIGN_CENTER);

                    /*
                     * cellStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
                     * cellStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
                     * cellStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
                     * cellStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
                     */
                    XSSFFont font = workbook.createFont();
                    font.setBold(true);
                    cellStyle.setFont(font);

                    Cell testCasevalCell = testcaseHeader.createCell(startCellNum);
                    testCasevalCell.setCellValue(key);
                    testCasevalCell.setCellStyle(cellStyle);
                    endCellNum = startCellNum + (arrayColumnNames.size() - 1);
                    sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, startCellNum, endCellNum));
                    rowNum = rowNum + 1;
                    rowNum =
                            createOutputExcelSheet(workbook, sheet, arrayColumnNames, value, errorFlag, rowNum,
                                    startCellNum);
                    rowNum++;
                    startCellNum = endCellNum + 1;

                }

                rowNumber = rowNum;
            }

        }

    }

    /**
     * Creates the output excel sheet from values obtained from output json.
     * 
     * @param workbook
     *            the workbook
     * @param sheet
     *            the sheet
     * @param columnNames
     *            the column names
     * @param excelValue
     *            the excel value
     * @param errorFlag
     *            the error flag
     * @param rowNumber
     *            the row number
     * @return the int
     */
    private int createOutputExcelSheet(XSSFWorkbook workbook, XSSFSheet sheet, Map<String, String> columnNames,
            List<List<Object>> excelValue, Boolean errorFlag, int rowNumber, int startCellNum) {

        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> createOutputExcel");
        int cellnum = startCellNum;
        // setting cell style and font for column header cells
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        // Setting the column header values in excel
        Row firstRow = null;
        if (isRowEmpty(sheet.getRow(rowNumber)) == false) {
            firstRow = sheet.getRow(rowNumber);
        } else {
            firstRow = sheet.createRow(rowNumber);
        }

        for (String it : columnNames.keySet()) {
            Cell c = firstRow.createCell(startCellNum++);
            c.setCellValue((String) it);
            c.setCellStyle(style);
        }
        // setting cell style and values to cells other than header values
        CellStyle valueStyle = workbook.createCellStyle();
        valueStyle.setAlignment(CellStyle.ALIGN_CENTER);
        // setting cell style and values to cells having errors
        CellStyle errorStyle = workbook.createCellStyle();
        errorStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        errorStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        // setting values to cells other than header
        rowNumber = rowNumber + 1;
        for (List<Object> list1 : excelValue) {
            startCellNum = cellnum;
            // checking if error is returned from rule engine and setting this
            // error message
            // as cell value and setting the error specific style for that row
            Row secRow = null;
            if (list1.size() == 1 && errorFlag) {
                sheet.addMergedRegion(new CellRangeAddress(rowNumber, rowNumber, 0, columnNames.size() - 1));

                if (isRowEmpty(sheet.getRow(rowNumber)) == false) {
                    secRow = sheet.getRow(rowNumber);
                } else {
                    secRow = sheet.createRow(rowNumber);
                }

                Cell cell = secRow.createCell(startCellNum++);
                cell.setCellValue((String) list1.get(0));
                cell.setCellStyle(errorStyle);
                rowNumber = rowNumber + 1;
            } else {
                // converting the values obtained from output json into string
                // and setting it as
                // cell values

                if (isRowEmpty(sheet.getRow(rowNumber)) == false) {
                    secRow = sheet.getRow(rowNumber);
                } else {
                    secRow = sheet.createRow(rowNumber);
                }
                rowNumber = rowNumber + 1;
                for (Object obj : list1) {
                    Cell cell = secRow.createCell(startCellNum++);
                    if (obj instanceof String) {
                        cell.setCellValue((String) obj);
                    } else if (obj instanceof Integer) {
                        cell.setCellValue(Integer.toString((Integer) obj));
                    } else if (obj instanceof Boolean) {
                        cell.setCellValue(String.valueOf((Boolean) obj));
                    } else if (obj instanceof Double) {
                        cell.setCellValue(Double.toString((Double) obj));
                    } else if (obj instanceof BigDecimal) {
                        cell.setCellValue(((BigDecimal) obj).toPlainString());
                    } else if (obj instanceof Long) {
                        cell.setCellValue(Long.toString((Long) obj));
                    } else if (obj instanceof Float) {
                        cell.setCellValue(Float.toString((Float) obj));
                    } else if (obj instanceof Date) {
                        cell.setCellValue((Date) obj);
                    } else {
                        if (null == obj) {
                            cell.setCellValue("");
                        } else {
                            cell.setCellValue(String.valueOf(obj));
                        }
                    }
                    cell.setCellStyle(valueStyle);
                }
            }
        }
        // for auto sizing the column widths
        for (int columnPosition = 0; columnPosition < columnNames.size(); columnPosition++) {
            sheet.autoSizeColumn(columnPosition);
        }
        // setting the excel sheet names
        /*
         * workbook.setSheetName(0, "Input"); workbook.setSheetName(1, "Input Config"); workbook.setSheetName(2,
         * "Output Config"); FileOutputStream out; try { out = new FileOutputStream(filePath); workbook.write(out);
         * out.close(); } catch (FileNotFoundException e) { LOGGER.error(
         * "File not found exception while creating output excel", e); throw new
         * ServiceException(GenericConstants.SERVICE_EXCEPTION, e.getMessage() + GenericConstants.NEW_LINE +
         * GenericConstants.EXCEPTION_MESSAGE); } catch (IOException e) {
         * LOGGER.error("IO Exception while creating output excel", e); throw new
         * ServiceException(GenericConstants.SERVICE_EXCEPTION, e.getMessage() + GenericConstants.NEW_LINE +
         * GenericConstants.EXCEPTION_MESSAGE); } catch (Exception e) {
         * LOGGER.error(" Exception while creating output excel", e); throw new
         * ServiceException(GenericConstants.SERVICE_EXCEPTION, e.getMessage() + GenericConstants.NEW_LINE +
         * GenericConstants.EXCEPTION_MESSAGE); }
         */

        LOGGER.debug("Method Exit : RegressionTestServiceImpl-> createOutputExcel");
        return rowNumber;
    }

    /**
     * Gets the array congig values.
     * 
     * @param arrayValueMap
     *            the array value map
     * @param jsonOutput
     *            the json output
     * @return the array congig values
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws JSONException
     *             the jSON exception
     */
    private HashMap<String, List<List<Object>>> getArrayCongigValues(
            HashMap<String, LinkedHashMap<String, String>> arrayValueMap, String jsonOutput) throws IOException,
            JSONException {
        HashMap<String, List<List<Object>>> arrayOutptMap = new LinkedHashMap<String, List<List<Object>>>();
        for (Map.Entry<String, LinkedHashMap<String, String>> entry : arrayValueMap.entrySet()) {
            List<Object> arrayCellValues = new ArrayList<Object>();
            List<List<Object>> arrayCellValuesList = new ArrayList<List<Object>>();
            String key = entry.getKey();
            LinkedHashMap<String, String> value = entry.getValue();
            String path = "$." + key;

            net.minidev.json.JSONArray arrayObject = JsonPath.read(jsonOutput, path);
            // arrayObject.s
            for (int i = 0; i < arrayObject.size(); i++) {
                net.minidev.json.JSONObject tableData = (net.minidev.json.JSONObject) arrayObject.get(i);
                arrayCellValues = retrieveOutputData(value, tableData.toString());
                arrayCellValuesList.add(arrayCellValues);
            }

            // arrayCellValues = retrieveOutputData(value, arrayObject.toString());
            arrayOutptMap.put(key, arrayCellValuesList);
        }
        return arrayOutptMap;

    }

    /**
     * Gets the array config map.
     * 
     * @param workbook
     *            the workbook
     * @param position
     *            the position
     * @return the array config map
     */
    private HashMap<String, LinkedHashMap<String, String>> getArrayConfigMap(XSSFWorkbook workbook, int position) {
        HashMap<String, LinkedHashMap<String, String>> arrayMap =
                new LinkedHashMap<String, LinkedHashMap<String, String>>();
        XSSFSheet worksheetOne = workbook.getSheetAt(position);
        int rowCount = workbook.getSheetAt(position).getPhysicalNumberOfRows();

        for (int i = 0; i < rowCount; i++) {
            XSSFRow firstRowOne = worksheetOne.getRow(i);

            String cellValue = firstRowOne.getCell(1).toString();
            String arrayKey = cellValue.substring(0, cellValue.indexOf("."));
            String arrayValue = cellValue.substring(cellValue.indexOf(".") + 1);

            LinkedHashMap<String, String> arrayValueMap = getArrayValueMap(arrayMap, arrayKey);
            arrayValueMap.put(firstRowOne.getCell(0).toString(), arrayValue);
            arrayMap.put(arrayKey, arrayValueMap);

        }
        return arrayMap;
    }

    /**
     * Gets the array value map.
     * 
     * @param arrayMap
     *            the array map
     * @param arrayKey
     *            the array key
     * @return the array value map
     */
    private LinkedHashMap<String, String> getArrayValueMap(HashMap<String, LinkedHashMap<String, String>> arrayMap,
            String arrayKey) {
        LinkedHashMap<String, String> arrayValueMap = null;
        if (!arrayMap.containsKey(arrayKey)) {
            arrayValueMap = new LinkedHashMap<String, String>();
        } else {
            arrayValueMap = arrayMap.get(arrayKey);
        }
        return arrayValueMap;

    }

    /**
     * Returns the Json input by extracting values from user input and input config mapping details.
     * 
     * @param workbook
     *            the workbook
     * 
     * @return the JSONArray
     */

    private JSONArray createJSONArray(XSSFWorkbook workbook) {

        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> createJSONArray");
        JSONArray objArray = new JSONArray();
        XSSFSheet worksheet = workbook.getSheetAt(0);
        Row firstRow = worksheet.getRow(0);

        // Accessing the column header values
        List<String> canonicalVariables = new ArrayList<String>();
        for (Cell cell : firstRow) {
            canonicalVariables.add(cell.getStringCellValue());
        }
        // Reading the row details and setting it into a list
        Iterator<Row> rowsIterator = worksheet.rowIterator();
        rowsIterator.next();
        List<List<Object>> valueList = new ArrayList<List<Object>>();
        while (rowsIterator.hasNext()) {
            List<Object> values = new ArrayList<Object>();
            XSSFRow xssfRow = (XSSFRow) rowsIterator.next();
            Iterator<Cell> cellsIterator = xssfRow.cellIterator();
            while (cellsIterator.hasNext()) {
                values.add(returnCellValue(cellsIterator.next()));
            }
            // checking if a row is blank or not and reads the contents only if
            // at least one column with values exists
            Boolean emptyRow = checkEmptyRow(values);
            if (!emptyRow) {
                valueList.add(values);
            }
        }

        XSSFSheet worksheetOne = workbook.getSheetAt(1);
        int rowCount = workbook.getSheetAt(1).getPhysicalNumberOfRows();
        // Putting the column header names into a map
        HashMap<String, String> ColumnNames = new HashMap<String, String>();
        for (int i = 0; i < rowCount; i++) {
            Row firstRowOne = worksheetOne.getRow(i);
            ColumnNames.put(firstRowOne.getCell(0).toString(), firstRowOne.getCell(1).toString());
        }
        try {
            // Building input json array using column header names as key and
            // corresponding
            // excel sheet cell data as values
            JSONArray inoutMapArray = new JSONArray();
            if (canonicalVariables.size() == valueList.get(0).size()) {
                for (List<Object> list : valueList) {
                    JSONObject inoutMap = new JSONObject();
                    JSONArray innerVal = new JSONArray();
                    for (int i = 0; i < canonicalVariables.size(); i++) {
                        String columnKey = canonicalVariables.get(i);
                        String key = ColumnNames.get(columnKey);
                        Boolean innerArray = false;
                        String value = (String) list.get(i);
                        // Getting multi-valued input variables by splitting
                        // using
                        // ','
                        if (value.contains(",")) {
                            innerArray = true;
                            for (String val : value.split("\\,")) {
                                innerVal.put(val);
                            }
                        }
                        if (innerArray) {
                            inoutMap.put(key, innerVal);

                        } else {
                            inoutMap.put(key, value);
                        }
                    }
                    inoutMapArray.put(inoutMap);
                }

            } else {
                LOGGER.error("Wrong Execel Sheet");
            }
            // converting canonical input json field names with dot notation
            // into json object
            if (inoutMapArray.toString().contains(".")) {
                JSONParser p = new JSONParser(JSONParser.DEFAULT_PERMISSIVE_MODE);
                for (int n = 0; n < inoutMapArray.length(); n++) {
                    JSONObject inoutMapObj = inoutMapArray.getJSONObject(n);
                    net.minidev.json.JSONObject JSONobj = new net.minidev.json.JSONObject();
                    Iterator<?> keys = inoutMapObj.keys();
                    if (inoutMapArray.toString().contains(".")) {
                        while (keys.hasNext()) {
                            JSONObject inoutObj = new JSONObject();
                            String path = (String) keys.next();
                            String val = inoutMapObj.getString(path);
                            if (path.contains(".")) {
                                inoutObj = stringJSONToObj(path, val);
                            } else {
                                inoutObj.put(path, val);
                            }
                            // merging the multi-level json objects together
                            net.minidev.json.JSONObject obj =
                                    (net.minidev.json.JSONObject) p.parse(inoutObj.toString());
                            JSONobj.merge(obj);
                        }
                        inoutMapObj = new JSONObject(JSONobj.toString());
                    }
                    objArray.put(inoutMapObj);
                }
            } else {
                objArray = new JSONArray(inoutMapArray.toString());
            }
        } catch (JSONException e) {
            LOGGER.error("JSONException while creating inputOutputConfig map", e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, e.getMessage() + GenericConstants.NEW_LINE
                    + GenericConstants.JSON_EXCEPTION_MESSAGE);
        } catch (net.minidev.json.parser.ParseException e) {
            LOGGER.error("JSONException while parsing inputOutputConfig map", e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, e.getMessage() + GenericConstants.NEW_LINE
                    + GenericConstants.EXCEPTION_MESSAGE);
        } catch (Exception e) {
            LOGGER.error("Exception while creating inputOutputConfig map", e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, e.getMessage() + GenericConstants.NEW_LINE
                    + GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.debug("Method Exit : RegressionTestServiceImpl-> createJSONArray");
        return objArray;
    }

    /**
     * converts the JSON dot notation to a JSON object.
     * 
     * @param path
     *            the path
     * @param value
     *            the value
     * @return the JSONObject
     * @throws JSONException
     *             the jSON exception
     */
    JSONObject stringJSONToObj(String path, String value) throws JSONException {
        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> stringJSONToObj");
        // converting json string with dot notation into a json object
        JSONObject JSONobj = new JSONObject();
        String[] part = path.split("\\.");
        for (int i = part.length - 1; i >= 0; i--) {
            if (i == part.length - 1) {
                JSONobj.put(part[i], value);
            } else {
                JSONObject obj = new JSONObject(JSONobj.toString());
                JSONobj = new JSONObject();
                JSONobj.put(part[i], obj);
            }
        }
        LOGGER.debug("Method Exit : RegressionTestServiceImpl-> stringJSONToObj");
        return JSONobj;
    }

    /**
     * Returns the cell value.
     * 
     * @param cell
     *            the cell
     * @return the string
     */
    private String returnCellValue(final Cell cell) {

        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> returnCellValue");
        String blankVal = "";
        // checking the cell type and retrieving the cell data
        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            BigDecimal decVal = BigDecimal.valueOf(cell.getNumericCellValue());
            BigDecimal zeroVal = new BigDecimal(0.0);
            BigDecimal result =
                    decVal.subtract(decVal.setScale(0, BigDecimal.ROUND_FLOOR)).movePointRight(decVal.scale());
            if (result.equals(zeroVal))
                return (decVal.toBigIntegerExact().toString());
            else return (decVal.setScale(2, BigDecimal.ROUND_CEILING).toString());

        } else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
            return (cell.getStringCellValue());
        } else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
            return (new Boolean(cell.getBooleanCellValue()).toString());
        } else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
            return (blankVal);
        }
        LOGGER.debug("Method Exit : RegressionTestServiceImpl-> returnCellValue");
        return blankVal;

    }

    /**
     * Returns the output config mapping details by reading the 3rd sheet in the excel file .
     * 
     * @param workbook
     *            the workbook
     * @param position
     *            the position
     * @return the LinkedHashMap
     */
    private LinkedHashMap<String, String> getOutputConfigMap(XSSFWorkbook workbook, int position) {
        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> getOutputConfigMap");
        // reading the output config mapping details and setting it into a map
        XSSFSheet worksheetOne = workbook.getSheetAt(position);
        int rowCount = workbook.getSheetAt(position).getPhysicalNumberOfRows();
        LinkedHashMap<String, String> ColumnNames = new LinkedHashMap<String, String>();
        for (int i = 0; i < rowCount; i++) {
            XSSFRow firstRowOne = worksheetOne.getRow(i);
            ColumnNames.put(firstRowOne.getCell(0).toString(), firstRowOne.getCell(1).toString());
        }
        LOGGER.debug("Method Exit: RegressionTestServiceImpl-> getOutputConfigMap");
        return ColumnNames;
    }

    /**
     * Creates the output excel.
     * 
     * @param workbook
     *            the workbook
     * @param filePath
     *            the file path
     */
    private void createOutputExcel(XSSFWorkbook workbook, String filePath) {
        // creating output excel sheet
        // setting the excel sheet names and saving the excel to a specific location.
        workbook.setSheetName(0, "Input");
        workbook.setSheetName(1, "Input Config");
        workbook.setSheetName(2, "Output Config");
        if (workbook.getNumberOfSheets() == 6) {
            workbook.setSheetName(3, "Illustration Output Config");
        }
        FileOutputStream out;
        try {
            out = new FileOutputStream(filePath);
            workbook.write(out);
            out.close();
        } catch (FileNotFoundException e) {
            LOGGER.error("File not found exception while creating output excel", e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, e.getMessage() + GenericConstants.NEW_LINE
                    + GenericConstants.EXCEPTION_MESSAGE);
        } catch (IOException e) {
            LOGGER.error("IO Exception while creating output excel", e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, e.getMessage() + GenericConstants.NEW_LINE
                    + GenericConstants.EXCEPTION_MESSAGE);
        } catch (Exception e) {
            LOGGER.error(" Exception while creating output excel", e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, e.getMessage() + GenericConstants.NEW_LINE
                    + GenericConstants.EXCEPTION_MESSAGE);
        }

    }

    /**
     * Retrieves values from the output json array.
     * 
     * @param columnNames
     *            the Column Names
     * @param root
     *            the root
     * @return List
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    static List<Object> retrieveOutputData(Map<String, String> columnNames, String root) throws IOException {
        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> retrieveOutputData");
        List<Object> cellVal = new ArrayList<Object>();
        // retrieving the json values by providing json path
        for (String key : columnNames.values()) {
            String path = "$." + key;
            try {
                Object val = JsonPath.read(root, path);
                cellVal.add(val);
            } catch (PathNotFoundException e) {
                // setting blank value if no such json path is found
                cellVal.add("");
            }
        }
        LOGGER.debug("Method Exit : RegressionTestServiceImpl-> retrieveOutputData");
        return cellVal;
    }

    /**
     * retrieves the job details.
     * 
     * @param userId
     *            the user id
     * @return the List<JobDetails>
     */
    public List<JobDetails> getJobDetails(int userId) {
        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> getJobDetails");
        List<JobDetails> jobList = regressionTestDao.retrieveJobDetails(userId);
        LOGGER.debug("Method Exit : RegressionTestServiceImpl-> getJobDetails");
        return jobList;
    }

    /**
     * updates the job details.
     * 
     * @param jobId
     *            the job Id
     * @param ruleName
     *            the rule name
     * @param jobStatus
     *            the job status
     * @param filePath
     *            the file path
     * @param errorMsg
     *            the error message
     */
    public void updateJobDetails(Long jobId, String ruleName, String jobStatus, String filePath, String errorMsg) {
        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> updateJobDetails");
        regressionTestDao.updateJobDetails(jobId, ruleName, jobStatus, filePath, errorMsg);
        LOGGER.debug("Method Exit : RegressionTestServiceImpl-> updateJobDetails");
    }

    /**
     * creates the job details.
     * 
     * @param userId
     *            the user id
     * @param jobId
     *            the job id
     * @param ruleName
     *            the rule name
     * @param jobStatus
     *            the job status
     * @param filePath
     *            the file path
     * @param comment
     *            the comment
     * @param errorMsg
     *            the error message
     */
    public void createJobDetails(int userId, Long jobId, String ruleName, String jobStatus, String filePath,
            String comment, String errorMsg) {
        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> createJobDetails");
        regressionTestDao.createJobDetails(userId, jobId, ruleName, jobStatus, filePath, comment, errorMsg);
        LOGGER.debug("Method Exit : RegressionTestServiceImpl-> createJobDetails");
    }

    /**
     * checks whether an entire row is blank or not.
     * 
     * @param rowValues
     *            the row values
     * @return boolean
     */
    public boolean checkEmptyRow(List<Object> rowValues) {
        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> checkEmptyRow");
        for (int i = 0; i < rowValues.size(); i++) {
            String callValue = rowValues.get(i).toString();
            if (!callValue.equals("")) {
                return false;
            }
        }
        LOGGER.debug("Method Entry : RegressionTestServiceImpl-> checkEmptyRow");
        return true;
    }

    /**
     * Checks if is row empty.
     * 
     * @param row
     *            the row
     * @return true, if is row empty
     */
    private boolean isRowEmpty(Row row) {

        if (row == null) {
            return true;
        } else {
            return false;
        }
    }

}
