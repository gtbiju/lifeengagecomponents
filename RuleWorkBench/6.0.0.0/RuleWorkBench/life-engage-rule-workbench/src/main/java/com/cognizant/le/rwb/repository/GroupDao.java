/**
 *
 * � Copyright 2012, Cognizant
 *
 * @author        : 291422
 * @version       : 0.1, Dec 15, 2014
 */

package com.cognizant.le.rwb.repository;

import java.util.List;
import java.util.Map;

import com.cognizant.le.rwb.domain.Group;
import com.cognizant.le.rwb.domain.Rule;
import com.cognizant.le.rwb.domain.RuleSetRuleMapping;

// TODO: Auto-generated Javadoc
/**
 * The Interface interface GroupDao.
 * 
 * @author 304003
 */
public interface GroupDao {

    /**
     * Export compressed version.
     *
     * @param groupId the group id
     * @param ruleNames the rule names
     * @return the list
     */
    List<Rule> exportCompressedVersion(Integer groupId, List<String> ruleNames);

    /**
     * Export un compressed rules.
     * 
     * @param groupId
     *            the group id
     * @param ruleNames
     *            the rule names
     * @return the list
     */
    List<Rule> exportUnCompressedRules(Integer groupId, List<String> ruleNames);

    /**
     * Creates the rule and ruleset.
     * 
     * @param ruleList
     *            the rule list
     * @param groupId
     *            the group id
     * @param status
     *            the status
     */
    void createRuleAndRuleset(List<Rule> ruleList, Integer groupId, String status, String userName);

    /**
     * Gets the rule rule set mapping.
     * 
     * @param groupID
     *            the group id
     * @param rulesetId
     *            the ruleset id
     * @return the rule rule set mapping
     */
    List<Map<String, Object>> getRuleRuleSetMapping(Integer groupID, Integer rulesetId);

    /**
     * Creates the group.
     * 
     * @param group
     *            the group
     * @return the integer
     */
    Integer createGroup(Group group);

    /**
     * Creates the rule set rule mapping.
     * 
     * @param ruleSetRuleMappingList
     *            the rule set rule mapping list
     */
    void createRuleSetRuleMapping(List<RuleSetRuleMapping> ruleSetRuleMappingList);

    /**
     * Gets the imported rules and rulesets.
     * 
     * @param groupID
     *            the group id
     * @return the imported rules and rulesets
     */
    List<Rule> getImportedRulesAndRulesets(String groupID);

    /**
     * Gets the group to export.
     * 
     * @param groupID
     *            the group id
     * @return the group to export
     */
    Group getGroupToExport(Integer groupID);

    /**
     * Checks if is group exists.
     * 
     * @param groupName
     *            the group name
     * @return true, if is group exists
     */
    boolean isGroupExists(String groupName);

    /**
     * Gets the group id.
     * 
     * @param groupName
     *            the group name
     * @return the group id
     */
    Integer getGroupId(String groupName);

    /**
     * Gets the rules by group.
     * 
     * @param groupId
     *            the group id
     * @return the rules by group
     */
    List<String> getRulesByGroup(Integer groupId);

    /**
     * Fetch common rules.
     * 
     * @param commonRuleList
     *            the common rule list
     * @param groupId
     *            the group id
     * @return the list
     */
    List<Rule> fetchCommonRules(List<String> commonRuleList, Integer groupId);

    /**
     * Update existing rules and rulesets.
     * 
     * @param existingRuleList
     *            the existing rule list
     * @param groupId
     *            the group id
     */
    void updateExistingRulesAndRulesets(List<Rule> existingRuleList, Integer groupId);

    /**
     * Delete rule set rule mapping.
     * 
     * @param ruleSetIds
     *            the rule set ids
     */
    void deleteRuleSetRuleMapping(List<Integer> ruleSetIds);

    /**
     * Creates the compressed rules.
     *
     * @param rule the rule
     * @param groupId the group id
     * @param status the status
     */
    void createCompressedRules(Rule rule, Integer groupId, String status, String userName);

    /**
     * Fetch compressed rule.
     *
     * @param groupId the group id
     * @return the rule
     */
    Rule fetchCompressedRule(Integer groupId);

    /**
     * Update compressed rules.
     *
     * @param rule the rule
     * @param groupId the group id
     */
    void updateCompressedRules(Rule rule, Integer groupId);
    void createRuleAndRulesetHistory(List<Rule> ruleList, Integer groupId, String status, String userName); 
    void createCompressedRulesHistory(Rule rule, Integer groupId, String status, String userName);

}
