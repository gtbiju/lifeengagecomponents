/**
 *
 * Copyright 2014, Cognizant 
 *
 * @author        : 304003
 * @version       : 0.1, Feb 28, 2013
 */
package com.cognizant.le.rwb.domain;

/**
 * The Class class RuleSetRuleMapping.
 */
public class RuleSetRuleMapping {

    /** The rule set rule mapping id. */
    private Integer ruleSetRuleMappingID;

    /** The rule id. */
    private Integer ruleID;

    /** The rule set id. */
    private Integer ruleSetID;

    /**
     * Gets the rule set rule mapping id.
     * 
     * @return the rule set rule mapping id
     */
    public final Integer getRuleSetRuleMappingID() {
        return ruleSetRuleMappingID;
    }

    /**
     * Sets the rule set rule mapping id.
     * 
     * @param ruleSetRuleMappingID
     *            the rule set rule mapping id to set.
     */
    public final void setRuleSetRuleMappingID(final Integer ruleSetRuleMappingID) {
        this.ruleSetRuleMappingID = ruleSetRuleMappingID;
    }

    /**
     * Gets the rule id.
     * 
     * @return the rule id
     */
    public final Integer getRuleID() {
        return ruleID;
    }

    /**
     * Sets the rule id.
     * 
     * @param ruleID
     *            the rule id to set.
     */
    public final void setRuleID(final Integer ruleID) {
        this.ruleID = ruleID;
    }

    /**
     * Gets the rule set id.
     * 
     * @return the rule set id
     */
    public final Integer getRuleSetID() {
        return ruleSetID;
    }

    /**
     * Sets the rule set id.
     * 
     * @param ruleSetID
     *            the rule set id to set.
     */
    public final void setRuleSetID(final Integer ruleSetID) {
        this.ruleSetID = ruleSetID;
    }

}
