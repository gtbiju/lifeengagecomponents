/**
 *
 * Copyright 2014, Cognizant 
 *
 * @author        : 304003
 * @version       : 0.1, Feb 28, 2013
 */
package com.cognizant.le.rwb.domain;

import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class class RuleSet.
 */
public class RuleSet {
    /** The rule set id. */
    private Integer ruleSetID;

    /** The rule set name. */
    private String ruleSetName;

    /** The created date. */
    private Date createdDate;

    /** The created date. */
    private Date updatedDate;

    /** The rule set type id. */
    private Integer ruleSetTypeID;

    /** The rule set JS. */
    private String ruleSetJS;

    /** The rule set group name. */
    private String ruleSetGroupName;
    
    /** The comment. */
    private String comments;
    
    /** The comment. */
    private String status;

    /**
     * Gets the rule set id.
     * 
     * @return the rule set id
     */
    public final Integer getRuleSetID() {
        return ruleSetID;
    }

    /**
     * Sets the rule set id.
     * 
     * @param ruleSetID
     *            the rule set id to set.
     */
    public final void setRuleSetID(final Integer ruleSetID) {
        this.ruleSetID = ruleSetID;
    }

    /**
     * Gets the rule set name.
     * 
     * @return the rule set name
     */
    public final String getRuleSetName() {
        return ruleSetName;
    }

    /**
     * Sets the rule set name.
     * 
     * @param ruleSetName
     *            the rule set name to set.
     */
    public final void setRuleSetName(final String ruleSetName) {
        this.ruleSetName = ruleSetName;
    }

    /**
     * Gets the created date.
     * 
     * @return the created date
     */
    public final Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Sets the created date.
     * 
     * @param createdDate
     *            the created date to set.
     */
    public final void setCreatedDate(final Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * Gets the rule set type id.
     * 
     * @return the rule set type id
     */
    public Integer getRuleSetTypeID() {
        return ruleSetTypeID;
    }

    /**
     * Sets the rule set type id.
     * 
     * @param ruleSetTypeID
     *            the rule set type id to set.
     */
    public void setRuleSetTypeID(Integer ruleSetTypeID) {
        this.ruleSetTypeID = ruleSetTypeID;
    }

    /**
     * Gets the rule set js.
     * 
     * @return the ruleSetJS
     */
    public String getRuleSetJS() {
        return ruleSetJS;
    }

    /**
     * Sets the rule set js.
     * 
     * @param ruleSetJS
     *            the ruleSetJS to set
     */
    public void setRuleSetJS(String ruleSetJS) {
        this.ruleSetJS = ruleSetJS;
    }

    /**
     * Gets the updated date.
     * 
     * @return the updated date
     */
    public Date getUpdatedDate() {
        return updatedDate;
    }

    /**
     * Sets the updated date.
     * 
     * @param updatedDate
     *            the updated date to set.
     */
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    /**
     * Gets the rule set group name.
     * 
     * @return the rule set group name
     */
    public String getRuleSetGroupName() {
        return ruleSetGroupName;
    }

    /**
     * Sets the rule set group name.
     * 
     * @param ruleSetGroupName
     *            the rule set group name to set.
     */
    public void setRuleSetGroupName(String ruleSetGroupName) {
        this.ruleSetGroupName = ruleSetGroupName;
    }

    /**
     * Gets the comment.
     *
     * @return the comment
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the comment.
     *
     * @param comment the comment to set.
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
