/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 418964
 * @version       : 0.1, May 30, 2014
 */
package com.cognizant.le.rwb.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.domain.JobDetails;
import com.cognizant.le.rwb.services.RegressionTestService;

/**
 * The Class class RegressionTestController.
 */
@Controller
public class RegressionTestController {

	@Autowired
	Job job;

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	private RegressionTestService regressionTestService;

	/** The logger. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RegressionTestController.class);

	/**
	 * calls the job launcher which in turn calls the rule engine and executes rule for the test
	 * datas in excel sheet
	 * 
	 * @param file
	 *            the file
	 * @param categoryId
	 *            the categoryId
	 * @param ruleName
	 *            the ruleName
	 * @param userId
	 *            the user Id
	 * @param comment
	 *            the comment
	 * 
	 * @return the ModelAndView
	 */
	@RequestMapping(value = "/jobLauncher", method = RequestMethod.POST)
	public void callJobLaunch(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "ruleId") String categoryId,
			@RequestParam(value = "ruleName") String ruleName,
			@RequestParam(value = "userId") int userId,
			@RequestParam(value = "comment") String comment) throws Exception {

		LOGGER.debug("Method Entry : RegressionTestController-> callJobLaunch");
		String errorMessage = "";
		String errorFlag = "";
		String filePath = regressionTestService.uploadExcelFile(file);
		// launching the job with job parameters as category id , rule name and
		// file path , comment , userId
		JobExecution jobExecution = jobLauncher.run(
				job,
				new JobParametersBuilder().addString("ruleName", ruleName)
						.addString("categoryId", categoryId)
						.addString("file", filePath)
						.addString("comment", comment)
						.addString("userId", Integer.toString(userId))
						.toJobParameters());

		// retrieving the job id, job status, error message and error flag
		// values which were set during job
		// execution
		String jobStatus = jobExecution.getExitStatus().getExitCode();
		Long jobId = jobExecution.getJobId();
		if (null != jobExecution.getExecutionContext()
				&& !jobExecution.getExecutionContext().isEmpty()) {
			if (jobExecution.getExecutionContext().containsKey("errorMsg")) {
				errorMessage = jobExecution.getExecutionContext().getString(
						"errorMsg");
				if (errorMessage.length() > 2500) {
					errorMessage = errorMessage.substring(0, 2499);
				}
			}
			if (jobExecution.getExecutionContext().containsKey("errorFlag")) {
				errorFlag = jobExecution.getExecutionContext().getString(
						"errorFlag");
			}
		}
		if (errorFlag.equalsIgnoreCase("true")) {
			jobStatus = GenericConstants.COMPLETED_WITH_ERROR_STATUS;
		}
		// calling dao to persist job details
		regressionTestService.updateJobDetails(jobId, ruleName, jobStatus,
				filePath, errorMessage);
		LOGGER.debug("Method Exit : RegressionTestController-> callJobLaunch");

	}

	/**
	 * downloads the output excel sheet
	 * 
	 * @param session
	 *            the http session
	 * @param response
	 *            the response
	 */
	@RequestMapping(value = "/downloadExcelFile")
	public void getExcelFile(@RequestParam("filePath") String filePath,
			HttpServletResponse response) throws Exception {
		LOGGER.debug("Method Entry : RegressionTestController-> getExcelFile");
		try {
			File fileToDownload = new File(filePath);
			String fileName = "excelValues";
			InputStream inputStream = new FileInputStream(fileToDownload);
			response.setContentType("application/force-download");
			response.setHeader("Content-Disposition", "attachment; filename="
					+ fileName + ".xlsx");
			IOUtils.copy(inputStream, response.getOutputStream());
			response.flushBuffer();

		} catch (Exception e) {
			LOGGER.debug("Request could not be completed at this moment. Please try again.");
		}
		LOGGER.debug("Method Exit : RegressionTestController-> getExcelFile");

	}

	/**
	 * retrieving the job details from database
	 * 
	 * @param userId
	 *            the user Id
	 */

	@RequestMapping(value = "/getJobDetails", method = RequestMethod.POST)
	public final @ResponseBody
	List<JobDetails> getJobDetails(@RequestParam("userId") int userId)
			throws Exception {
		LOGGER.debug("Method Entry : RegressionTestController-> getJobDetails");
		List<JobDetails> jobList = regressionTestService.getJobDetails(userId);
		LOGGER.debug("Method Exit : RegressionTestController-> getJobDetails");
		return jobList;

	}

}
