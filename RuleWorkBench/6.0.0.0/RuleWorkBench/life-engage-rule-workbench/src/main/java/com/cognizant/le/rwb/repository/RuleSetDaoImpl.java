/**
 * 
 */
package com.cognizant.le.rwb.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.domain.Rule;
import com.cognizant.le.rwb.domain.RuleSet;
import com.cognizant.le.rwb.domain.RuleSetRuleMapping;
import com.cognizant.le.rwb.util.Utilities;

/**
 * The Class class RuleSetDaoImpl.
 * 
 * @author 304003
 */
@Repository("ruleSetDao")
public class RuleSetDaoImpl implements RuleSetDao {

    /** The jdbc template. */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /** The environment. */
    @Autowired
    private Environment environment;

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RuleSetDaoImpl.class);

    /**
     * Creates the rule set.
     * 
     * @param ruleSet
     *            the rule set bo
     * @return the integer
     */
    public final Integer createRuleSet(final Rule ruleSet) {
        LOGGER.trace("Method Entry : RuleSetDao-> createRuleSet");
        boolean isRuleSetExists;
        Integer ruleSetId = null;
        isRuleSetExists = isRuleSetExists(ruleSet);
        if (!isRuleSetExists) {
            final Object[] values =
                    { ruleSet.getRuleName(), ruleSet.getRuleTypeID(), new Timestamp(Utilities.getCurrentDate().getTime()), 1,
                            ruleSet.getRuleJson(), ruleSet.getRuleJS(), ruleSet.getOfflineRuleJS(),
                            ruleSet.getGroupName(), ruleSet.getClientId(), ruleSet.getComments(),GenericConstants.STATUS_ACTIVE};
            KeyHolder holder = new GeneratedKeyHolder();
            jdbcTemplate.update(new PreparedStatementCreator() {

                @Override
                public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
                    PreparedStatement ps =
                            conn.prepareStatement(environment.getProperty(GenericConstants.INSERT_RULE),
                                    Statement.RETURN_GENERATED_KEYS);
                    for (int i = 0; i < values.length; i++) {
                        ps.setObject(i + 1, values[i]);
                    }
                    return ps;
                }
            }, holder);
            ruleSetId = holder.getKey().intValue();
            ruleSet.setRuleID(ruleSetId);
        }

        LOGGER.trace("Method Exit : RuleSetDao-> createRuleSet");
        return ruleSetId;

    }

    /**
     * Creates the rule set rule mapping.
     * 
     * @param ruleSetId
     *            the rule set id
     * @param ruleSetRuleMappingList
     *            the rule set rule mapping list
     */
    public final void createRuleSetRuleMapping(final Integer ruleSetId,
            final List<RuleSetRuleMapping> ruleSetRuleMappingList) {
        LOGGER.trace("Method Entry : RuleSetDao-> createRuleSetRuleMapping");
        int updateCount[] =
                jdbcTemplate.batchUpdate(environment.getProperty(GenericConstants.INSERT_RULESET_RULE_MAPPING),
                        new BatchPreparedStatementSetter() {

                            @Override
                            public void setValues(PreparedStatement ps, int index) throws SQLException {
                                RuleSetRuleMapping ruleSetRuleMapping = ruleSetRuleMappingList.get(index);
                                ps.setInt(1, ruleSetId);
                                ps.setInt(2, ruleSetRuleMapping.getRuleID());
                            }

                            @Override
                            public int getBatchSize() {
                                return ruleSetRuleMappingList.size();
                            }
                        });
        if (updateCount.length != ruleSetRuleMappingList.size()) {
            LOGGER.error("RuleSet creation failed");
        }
        LOGGER.trace("Method Exit : RuleSetDao-> createRuleSetRuleMapping");
    }

    /**
     * List rule set.
     * 
     * @param ruleSetID
     *            the rule set id
     * @return the list
     */
    public final List<RuleSet> listRuleSetForUpdate(final Integer ruleSetID) {
        LOGGER.trace("Method Entry : RuleSetDao-> listRuleSet");
        final List<RuleSet> ruleSetList =
                jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_RULESET_FOR_RULESETID),
                        new Object[] { ruleSetID }, new RowMapper<RuleSet>() {

                            @Override
                            public RuleSet mapRow(ResultSet rs, int rowNum) throws SQLException {
                                final RuleSet ruleSet = new RuleSet();
                                ruleSet.setRuleSetName(rs.getString("RULENAME"));
                                ruleSet.setRuleSetGroupName(rs.getString("CATEGORY"));
                                ruleSet.setRuleSetID(rs.getInt("ID"));
                                ruleSet.setRuleSetTypeID(rs.getInt("TYPE_ID"));
                                ruleSet.setCreatedDate(rs.getDate("CREATED_DATE"));
                                ruleSet.setUpdatedDate(rs.getDate("UPDATED_DATE"));
                                ruleSet.setComments(rs.getString("COMMENTS"));
                                return ruleSet;
                            }

                        });
        LOGGER.trace("Method Exit : RuleSetDao-> listRuleSet");
        return ruleSetList;
    }

    /**
     * List associated rules.
     * 
     * @param ruleSetID
     *            the rule set id
     * @return the list
     */
    public final List<Rule> listAssociatedRules(final Integer ruleSetID) {
        LOGGER.trace("Method Entry : RuleSetDao-> listAssociatedRules");
        final List<Rule> ruleList =
                jdbcTemplate.query(environment.getProperty(GenericConstants.SELECT_ASSOCIATED_RULES_FOR_RULESETID),
                        new Object[] { ruleSetID }, new RowMapper<Rule>() {

                            @Override
                            public Rule mapRow(ResultSet rs, int rowNum) throws SQLException {
                                final Rule rule = new Rule();
                                rule.setRuleName(rs.getString("RULENAME"));
                                rule.setRuleID(rs.getInt("ID"));
                                return rule;
                            }

                        });
        LOGGER.trace("Method Exit : RuleSetDao-> listAssociatedRules");
        return ruleList;

    }

    /**
     * Updates the rule set.
     * 
     * @param ruleSet
     *            the rule set
     */
    public final void editRuleSet(final Rule ruleSet) {
        LOGGER.trace("Method Entry : RuleSetDao-> editRuleSet");
        jdbcTemplate.update(
                environment.getProperty(GenericConstants.UPDATE_RULESET),
                new Object[] { ruleSet.getRuleName(), ruleSet.getRuleJS(), ruleSet.getOfflineRuleJS(),
                    new Timestamp(Utilities.getCurrentDate().getTime()), ruleSet.getRuleJson(), ruleSet.getGroupName(),ruleSet.getComments(), ruleSet.getRuleID()});
        LOGGER.trace("Method Exit : RuleSetDao-> editRuleSet");

    }

    /**
     * Deletes the rule set rule mapping.
     * 
     * @param ruleSetId
     *            the rule set id
     */
    public final void deleteRuleSetRuleMapping(final Integer ruleSetId) {
        LOGGER.trace("Method Entry : RuleSetDao-> editRuleSetRuleMapping");
        jdbcTemplate.update(environment.getProperty(GenericConstants.DELETE_RULESET_RULE_MAPPING),
                new Object[] { ruleSetId });
        LOGGER.trace("Method Exit : RuleSetDao-> editRuleSetRuleMapping");

    }

    /**
     * Delete the rule set.
     * 
     * @param ruleSetId
     *            the rule set id
     */
    public final void deleteRuleSet(final Integer ruleSetId) {
        LOGGER.trace("Method Entry : RuleSetDao-> deleteRuleSet");
        jdbcTemplate.update(environment.getProperty(GenericConstants.DELETE_RULE), new Object[] { ruleSetId });
        LOGGER.trace("Method Exit : RuleSetDao-> deleteRuleSet");

    }


    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.repository.RuleSetDao#isRuleSetExists(com.cognizant.le.rwb.domain.Rule)
     */
    public final boolean isRuleSetExists(final Rule rule) {

        boolean isExists = false;
        List<Integer> ruleIdList = null;
        if (rule.getRuleID() != null && rule.getRuleID() != 0) {
            ruleIdList =
                     jdbcTemplate.query(environment.getProperty(GenericConstants.RULE_EXISTS_CHK_FOR_EDIT),
                             new Object[] { rule.getRuleName(),  rule.getGroupName(),  rule.getRuleID() }, new RowMapper<Integer>() {

                                 @Override
                                 public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                                     return rs.getInt("ID");
                                 }

                             });
         } else {
             ruleIdList =
                     jdbcTemplate.query(environment.getProperty(GenericConstants.RULE_EXISTS),
                             new Object[] { rule.getRuleName(), rule.getGroupName() }, new RowMapper<Integer>() {

                                 @Override
                                 public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                                     return rs.getInt("ID");
                                 }

                             });;
         }
      
        if (ruleIdList.size() > 0) {
            isExists = true;
            //rule.setRuleID(ruleIdList.get(0));
        }
        LOGGER.trace("Method Exit : RuleDAO-> isRuleExists");
        return isExists;

    }

}
