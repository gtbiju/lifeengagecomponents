/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 7, 2014
 */

package com.cognizant.le.rwb.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.le.rwb.domain.Group;
import com.cognizant.le.rwb.domain.GroupType;
import com.cognizant.le.rwb.domain.Rule;
import com.cognizant.le.rwb.repository.RuleDao;

// TODO: Auto-generated Javadoc
/**
 * The Class class RuleServiceImpl.
 * 
 * @author 304003
 */
@Service("ruleService")
public class RuleServiceImpl implements RuleService {

    /** The rule dao. */
    @Autowired
    private RuleDao ruleDao;

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RuleServiceImpl.class);

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#createRule(com.cognizant.le .rwb.domain.Rule, java.lang.Integer)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public Map<String,String> createRule(Rule rule, Integer groupID) {
        LOGGER.trace("Method Entry : RuleServiceImpl-> createRule");
        Map<String,String> resultMap = ruleDao.createRule(rule, groupID);
        LOGGER.trace("Method Entry : RuleServiceImpl-> createRule");
        return resultMap;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#editRule(com.cognizant.le.rwb .domain.Rule, java.lang.Integer)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public Map<String,String> editRule(Rule rule, Integer groupID, boolean isDupChkRequired) {
        LOGGER.trace("Method Entry : RuleServiceImpl-> editRule");
        Map<String, String> resultMap = new HashMap<String, String>();
        String result = "Rule Updated Successfully";
        boolean isRuleExists = false;
        if (isDupChkRequired && ruleDao.isRuleExists(rule, groupID,rule.getRuleID())) {
           result = "Rule already exists";
           isRuleExists = true;
        } else {
            ruleDao.editRule(rule, groupID);

        }
        resultMap.put("createRuleResult", result);
        resultMap.put("isRuleExists", String.valueOf(isRuleExists));
        LOGGER.trace("Method Entry : RuleServiceImpl-> editRule");
        return resultMap;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#listRules(java.util.List)
     */
    @Override
    public List<Rule> listRules(List<Integer> ruleTypeIds) {
        LOGGER.trace("Method Entry : RuleServiceImpl-> listRules");
        List<Rule> ruleList = ruleDao.listRules(ruleTypeIds);
        LOGGER.trace("Method Entry : RuleServiceImpl-> listRules");
        return ruleList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#listRulesForRuleSet(java.lang .Integer, java.lang.Integer)
     */
    @Override
    public List<Rule> listRulesForRuleSet(Integer ruleTypeId, Integer groupId) {
        LOGGER.trace("Method Entry : RuleServiceImpl-> listRulesForRuleSet");
        List<Rule> ruleList = ruleDao.listRulesRuleset(ruleTypeId, groupId);
        LOGGER.trace("Method Entry : RuleServiceImpl-> listRulesForRuleSet");
        return ruleList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#listRule(java.lang.Integer)
     */
    @Override
    public Rule listRule(Integer ruleId) {
        LOGGER.trace("Method Entry : RuleServiceImpl-> listRuleTypes");
        Rule rule = ruleDao.listRule(ruleId);
        LOGGER.trace("Method Entry : RuleServiceImpl-> listRuleTypes");
        return rule;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#downloadRuleJS(java.lang.Integer )
     */
    @Override
    public Rule downloadRuleJS(Integer ruleId) {
        LOGGER.trace("Method Entry : RuleServiceImpl-> downloadRuleJS");
        Rule rule = ruleDao.downloadRuleJS(ruleId);
        LOGGER.trace("Method Entry : RuleServiceImpl-> downloadRuleJS");
        return rule;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#deleteRule(java.lang.Integer)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public void deleteRule(Integer ruleId) {
        LOGGER.trace("Method Entry : RuleServiceImpl-> deleteRule");
        ruleDao.deleteRule(ruleId);
        LOGGER.trace("Method Entry : RuleServiceImpl-> deleteRule");

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#getRuleType(java.lang.Integer)
     */
    @Override
    public Integer getRuleType(Integer ruleId) {
        LOGGER.trace("Method Entry : RuleServiceImpl-> deleteRule");
        Integer ruleType = ruleDao.getRuleType(ruleId);
        LOGGER.trace("Method Entry : RuleServiceImpl-> deleteRule");
        return ruleType;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#addGroupName(com.cognizant. le.rwb.domain.Group)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public boolean addGroupName(Group group) {
    	boolean result = ruleDao.addGroupName(group);
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#listGroupTable()
     */
    @Override
    public List<Group> listGroupTable() {
        List<Group> groupList = ruleDao.listGroupTable();
        return groupList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#getGroupId(java.lang.String)
     */
    @Override
    public Integer getGroupId(String groupName) {
        Integer groupId = ruleDao.getGroupId(groupName);
        return groupId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#getGroupID(java.lang.String)
     */
    @Override
    public String getGroupID(Integer ruleID) {
        String groupID = ruleDao.getGroupID(ruleID);
        return groupID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#updateGroupName(com.cognizant .le.rwb.domain.Group,
     * java.lang.String)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public boolean updateGroupName(Group group,boolean isGrpNameChanged) {
    	boolean result = true;
    	if(isGrpNameChanged){
    		if(ruleDao.isGroupExists(group.getGroupName())){
    			result = false;    			
    		} else {
    			ruleDao.updateGroupName(group);
    		}
    		
    	} else {
    		ruleDao.updateGroupName(group);
    	}
    	
    	
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#deleteGroup(java.lang.Integer)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public void deleteGroup(Integer groupID) {
    	List<Integer> ruleSetIdList = ruleDao.getRuleSetIdForGroupdelete(String.valueOf(groupID));
    	if(ruleSetIdList != null && ruleSetIdList.size() > 0){
    		ruleDao.deleteRuleSetRuleMappingBeforeGroupDelete(ruleSetIdList);
    	}
    	ruleDao.deleteRuleBeforeGroupDelete(String.valueOf(groupID));
        ruleDao.deleteGroup(groupID);
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.services.RuleService#checkForRuleAndRuleSetCountInGroup(java.lang.Integer)
     */
    public List<Rule>  checkForRuleAndRuleSetCountInGroup(Integer groupID){
    	List<Rule> ruleSetIdList = ruleDao.checkForRuleAndRuleSetCountInGroup(groupID);
    	return ruleSetIdList;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#listGroupDownloadRules(java .lang.Integer)
     */
    @Override
    public List<Rule> listGroupDownloadRules(Integer groupID) {
        List<Rule> ruleList = ruleDao.listGroupDownloadRules(groupID);
        return ruleList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#downloadGroupRules(java.util .List, boolean)
     */
    @Override
    public StringBuilder downloadGroupRules(List<Integer> idList, boolean isOnline) {
        StringBuilder ruleDescription = ruleDao.downloadGroupRules(idList, isOnline);
        return ruleDescription;
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.services.RuleService#getDownloadRulesDesc(java.util.List, java.lang.String, boolean)
     */
    public StringBuilder getDownloadRulesDesc(List<String> downloadRuleList, String groupName, boolean isOnline) {
        StringBuilder ruleDescription = ruleDao.getDownloadRulesDesc(downloadRuleList,groupName,isOnline);
        return ruleDescription;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#listRuleByGroup(java.lang.Integer )
     */
    @Override
    public List<Rule> listRuleByGroup(Integer groupID) {
        List<Rule> ruleList = ruleDao.listRuleByGroup(groupID);
        return ruleList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#cloneGroup(java.util.List, com.cognizant.le.rwb.domain.Group)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public String cloneGroup(List<Rule> ruleList, Group group) {
        Integer cloneGroupId = ruleDao.createGroup(group);
        String result = "";
        try {
            result = ruleDao.cloneGroup(ruleList, cloneGroupId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#mergeGroup(java.util.List, java.lang.Integer)
     */
    @Override
    @Transactional(rollbackFor = { Exception.class })
    public void mergeGroup(List<Rule> ruleList, Integer groupId) {
        try {
            ruleDao.cloneGroup(ruleList, groupId);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#listFromGroupTypeTable()
     */
    @Override
    public List<GroupType> listFromGroupTypeTable() {
        List<GroupType> groupTypes = ruleDao.listFromGroupTypeTable();
        return groupTypes;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#getGroupType(java.lang.String)
     */
    @Override
    public Integer getGroupType(String groupCloneName) {
        Integer groupTypeId = ruleDao.getGroupType(groupCloneName);
        return groupTypeId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.cognizant.le.rwb.services.RuleService#tableAttributeLoader()
     */
    @Override
    public List<Map<String, Object>> tableAttributeLoader() {
        List<Map<String, Object>> tableLoaderList = ruleDao.tableAttributeLoader();
        return tableLoaderList;
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.services.RuleService#getRuleExistingRulesets(java.lang.Integer)
     */
    @Override
    public List<Rule> getRuleExistingRulesets(Integer ruleId){
    	List<Rule> ruleList = ruleDao.getRuleExistingRulesets(ruleId);
        return ruleList;
    }
    
    /* (non-Javadoc)
     * @see com.cognizant.le.rwb.services.RuleService#getrulesByGroup(java.lang.Integer)
     */
    public List<String> getRulesByGroup(Integer groupId){
        List<String> ruleNames = ruleDao.getRulesByGroup(groupId);
        return ruleNames;
        
    }
}
