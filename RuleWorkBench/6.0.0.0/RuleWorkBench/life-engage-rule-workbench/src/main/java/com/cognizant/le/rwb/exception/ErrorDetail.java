/**
 *
 * © Copyright 2012, Cognizant
 *
 * @author        : 300797
 * @version       : 0.1, Mar 10, 2014
 */

package com.cognizant.le.rwb.exception;

import java.io.Serializable;

/**
 * The Class class ErrorDetail.
 */
public class ErrorDetail implements Serializable {

    /** Serial version. */
    private static final long serialVersionUID = 1L;

    /** The error code. */
    private String errorCode;

    /** The message. */
    private String message;
    
    /** The details. */
    private String details;
    
    /**
     * Instantiates a new validation error dto.
     * 
     * @param errorCode
     *            the error code
     */
    public ErrorDetail(String errorCode) {
        this.errorCode = errorCode;
    }
    
    /**
     * Instantiates a new error detail.
     */
    public ErrorDetail() {
       
    }
    

    /**
     * Instantiates a new error detail.
     *
     * @param errorCode the error code
     * @param message the message
     * @param details the details
     */
    public ErrorDetail(String errorCode, String message, String details) {
		super();
		this.errorCode = errorCode;
		this.message = message;
		this.details = details;
	}

    /**
     * Gets the error code.
     * 
     * @return the error code
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the error code.
     * 
     * @param errorCode
     *            the error code to set.
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }



	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}



	/**
	 * Sets the message.
	 *
	 * @param message the message to set.
	 */
	public void setMessage(String message) {
		this.message = message;
	}



	/**
	 * Gets the details.
	 *
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}



	/**
	 * Sets the details.
	 *
	 * @param details the details to set.
	 */
	public void setDetails(String details) {
		this.details = details;
	}


}
