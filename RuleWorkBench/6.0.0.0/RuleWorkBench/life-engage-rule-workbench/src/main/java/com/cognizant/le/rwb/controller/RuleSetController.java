/**
 *
 * Copyright 2012, Cognizant 
 *
 * @author        : 291422
 * @version       : 0.1, Mar 4, 2013
 */
package com.cognizant.le.rwb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.le.rwb.constant.GenericConstants;
import com.cognizant.le.rwb.domain.Rule;
import com.cognizant.le.rwb.domain.RuleSet;
import com.cognizant.le.rwb.domain.RuleSetRuleMapping;
import com.cognizant.le.rwb.exception.ServiceException;
import com.cognizant.le.rwb.services.RuleService;
import com.cognizant.le.rwb.services.RuleSetService;

/**
 * The Class class RuleSetAction.
 */
@Controller
public class RuleSetController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RuleSetController.class);

    /** The environment. */
    @Autowired
    Environment environment;

    /** The rule set service. */
    @Autowired
    private RuleSetService ruleSetService;

    /** The rule service. */
    @Autowired
    private RuleService ruleService;

    /**
     * List rule names.
     * 
     * @param groupId
     *            the group id
     * @param request
     *            the request
     * @param response
     *            the response
     * @return the string
     */
    @RequestMapping(value = "/listRuleName", method = RequestMethod.POST)
    public final @ResponseBody
    List<Rule> listRuleNames(@RequestParam("firstGroupID") final Integer groupId, final HttpServletRequest request,
            final HttpServletResponse response) {
        LOGGER.trace("Method Entry : RuleSetAction-> listRuleNames");
        List<Rule> ruleList;
        try {
            final Integer ruleTypeID = (Integer) request.getSession().getAttribute("ruleTypeID");
            ruleList = ruleService.listRulesForRuleSet(ruleTypeID, groupId);
        } catch (Exception e) {
            LOGGER.error("Error while listing rule names" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleSetAction-> listRuleNames");
        return ruleList;
    }

    /**
     * Creates the rule set.
     * 
     * @param ruleSetData
     *            the rule set data
     * @param ruleSetTypeID
     *            the rule set type id
     * @return the string
     * @throws JsonParseException
     *             the json parse exception
     * @throws JsonMappingException
     *             the json mapping exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/createRuleSet", method = RequestMethod.POST)
    public final @ResponseBody
    Map<String, String> createRuleSet(@RequestParam("ruleSetData") final String ruleSetData,
            @RequestParam("ruleType") final Integer ruleSetTypeID, @RequestParam("isRuleSetNameChanged") final boolean isRuleSetNameChanged) throws JsonParseException, JsonMappingException,
            IOException {
        LOGGER.trace("Method Entry : RuleSetAction-> createRuleSet");
        Map<String, String> resultMap = new HashMap<String, String>();
        try {
            final List<RuleSetRuleMapping> ruleSetRuleMappingList = new ArrayList<RuleSetRuleMapping>();
            final Rule rule = new Rule();
            ObjectMapper mapper = new ObjectMapper();
            final Map<String, Object> ruleSetDataMap =
                    mapper.readValue(ruleSetData, new TypeReference<Map<String, Object>>() {
                    });
            
            final Map<String, List<String>> canonicalVariables =
                    (Map<String, List<String>>) ruleSetDataMap.get("canonicalVariables");
            
            final String ruleSetname = (String) ruleSetDataMap.get("ruleSetName");
            final String ruleSetGroupName = (String) ruleSetDataMap.get("ruleSetGroupName");
            final String ruleSetJS = (String) ruleSetDataMap.get("ruleSetJS");
            final String ruleSetOfflineJS = (String) ruleSetDataMap.get("ruleSetOfflineJS");
            final String ruleSetComment = (String) ruleSetDataMap.get("comments");
            
            String ruleSetJSON = null;
            String ruleSetId = "";
            if ((ruleSetDataMap.get("ruleSetId") instanceof Long)) {
                ruleSetId = new Integer(((Long) ruleSetDataMap.get("ruleSetId")).intValue()).toString();
            } else {
                ruleSetId = String.valueOf(ruleSetDataMap.get("ruleSetId"));
            }
            final List<Map<String, Object>> associatedRuleList =
                    (List<Map<String, Object>>) ruleSetDataMap.get("associatedRules");
            RuleSetRuleMapping ruleSetRuleMapping;
            for (Map<String, Object> associatedRuleMap : associatedRuleList) {
                ruleSetRuleMapping = new RuleSetRuleMapping();
                final String ruleId = (String) associatedRuleMap.get("ruleId");
                ruleSetRuleMapping.setRuleID(Integer.valueOf(ruleId));
                ruleSetRuleMappingList.add(ruleSetRuleMapping);
                ruleSetJSON = ruleSetService.addRuleSetJson(Integer.valueOf(ruleId), ruleSetJSON, canonicalVariables);
            }
            rule.setRuleID(Integer.valueOf(ruleSetId));
            rule.setRuleName(ruleSetname);
            rule.setRuleTypeID(ruleSetTypeID);
            rule.setRuleJS(ruleSetJS);
            rule.setOfflineRuleJS(ruleSetOfflineJS);
            rule.setRuleJson(ruleSetJSON);
            rule.setGroupName(ruleSetGroupName);
            rule.setClientId(environment.getProperty(GenericConstants.CLIENT_ID));
            rule.setComments(ruleSetComment);
            
            if (ruleSetId != null && Integer.valueOf(ruleSetId) != 0) {
                resultMap = ruleSetService.editRuleSet(rule, ruleSetRuleMappingList, isRuleSetNameChanged);
            } else {
                resultMap = ruleSetService.createRuleSet(rule, ruleSetRuleMappingList);
            }
            
            resultMap.put("ruleID", rule.getRuleID() != null ? rule.getRuleID().toString():"");
        } catch (Exception e) {
            LOGGER.error("Error while creating ruleset" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleSetAction-> createRuleSet");
        return resultMap;
    }

    /**
     * List rule set.
     * 
     * @param ruleSetId
     *            the rule set id
     * @return the string
     */
    @RequestMapping(value = "/listRuleSet", method = RequestMethod.POST)
    public final @ResponseBody
    Map<String, Object> listRuleSet(@RequestParam("ruleSetID") final Integer ruleSetId) {
        LOGGER.trace("Method Entry : RuleSetAction-> listRuleSet");
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            List<RuleSet> ruleSetList = ruleSetService.listRuleSetForUpdate(ruleSetId);
            List<Rule> ruleList = ruleSetService.listAssociatedRules(ruleSetId);
            resultMap.put("ruleSetBOList", ruleSetList);
            resultMap.put("ruleBOList", ruleList);
        } catch (Exception e) {
            LOGGER.error("Error while listing ruleset" , e);
            throw new ServiceException(GenericConstants.SERVICE_EXCEPTION, GenericConstants.EXCEPTION_MESSAGE);
        }
        LOGGER.trace("Method Exit : RuleSetAction-> listRuleSet");
        return resultMap;
    }
}
